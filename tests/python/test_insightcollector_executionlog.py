# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Insight Collector unit tests."""

import logging
import os
import sys
import unittest


from unittest.mock import MagicMock, patch

from requests import Response


sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.insightcollector import executionlog
from opentf.plugins.insightcollector.exceptions import ExecutionLogError

########################################################################
# Templates and helpers

mockresponse = Response()
mockresponse.status_code = 200

EXECUTIONLOG_PARAMS = {
    'step-depth': 60,
    'job-depth': 70,
    'max-command-length': 80,
}

########################################################################


class TestInsightCollectorExecutionlog(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # _make_outputcontext

    def test_get_executionlog_params_from_params(self):
        result = executionlog._make_outputcontext(None, EXECUTIONLOG_PARAMS)
        self.assertEqual(result.job_depth, 70)
        self.assertEqual(result.max_command_length, 80)
        self.assertEqual(result.step_depth, 60)

    def test_get_executionlog_params_default(self):
        result = executionlog._make_outputcontext(None, {})
        self.assertEqual(result.job_depth, 1)
        self.assertEqual(result.max_command_length, 15)
        self.assertEqual(result.step_depth, 1)

    def test_get_executionlog_params_ko(self):
        mock_warning = MagicMock()
        with patch(
            'opentf.plugins.insightcollector.executionlog.logging.warning', mock_warning
        ):
            result = executionlog._make_outputcontext(None, {'step-depth': 'foo'})  # type: ignore
        self.assertEqual(result.job_depth, 1)
        self.assertEqual(result.max_command_length, 15)
        self.assertEqual(result.step_depth, 1)
        mock_warning.assert_called_once_with(
            'Invalid context parameter step-depth: foo, using the default value 1.',
        )

    # dump_events

    def test_dump_events(self):
        events = [{'kind': 1}, {'kind': 2}, {'kind': 3}]
        mock_event = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.executionlog.emit_event', mock_event
            ),
            patch(
                'opentf.plugins.insightcollector.executionlog._make_outputcontext',
                MagicMock(
                    return_value={
                        'step_depth': 1,
                        'job_depth': 1,
                        'max_command-length': 15,
                    }
                ),
            ),
        ):
            executionlog._dump_events(events, None)  # type: ignore
        mock_event.assert_called()

    # prepare_executionlog_event

    def test_prepare_executionlog_event_ok(self):
        mock_event = MagicMock()
        mock_proxy = MagicMock()
        mock_proxy.get_events = MagicMock(
            return_value=[{'event': 'one', 'another event': 'event two'}]
        )
        args = [
            mock_proxy,
            {'name': 'name', 'spec': None},
            'wf_id',
            'req_id',
        ]
        with patch(
            'opentf.plugins.insightcollector.executionlog.make_workflowresult_event',
            mock_event,
        ):
            executionlog.prepare_executionlog_event(*args)
        mock_event.assert_called_once()
        self.assertEqual('name', mock_event.call_args[0][1])

    def test_prepare_executionlog_event_ko(self):
        mock_event = MagicMock()
        mock_proxy = MagicMock()
        mock_proxy.get_events = MagicMock(return_value=[])
        args = [
            mock_proxy,
            {'name': 'name', 'spec': None},
            'wf_id',
            'req_id',
        ]
        with patch(
            'opentf.plugins.insightcollector.executionlog.make_workflowresult_event',
            mock_event,
        ):
            self.assertRaisesRegex(
                ExecutionLogError,
                'No events retrieved for the workflow wf_id, not publishing the execution log.',
                executionlog.prepare_executionlog_event,
                *args,
            )
        mock_event.assert_not_called()
