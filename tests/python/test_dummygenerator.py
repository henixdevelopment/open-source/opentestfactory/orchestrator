# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Dummy generator unit tests."""

import os
import unittest
import sys

from unittest.mock import MagicMock, patch

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.dummygenerator import main, implementation


class TestDummyGenerator(unittest.TestCase):

    # implementation

    def test_generate_ok(self):
        inputs = {'a': 'b', 'c': 'd'}
        self.assertTrue(inputs, implementation.generate(inputs))

    # main

    def test_main_make_plugin_ok(self):
        mock_plugin = MagicMock()
        mock_generate = MagicMock()
        with (
            patch('opentf.plugins.dummygenerator.main.make_plugin', mock_plugin),
            patch('opentf.plugins.dummygenerator.main.generate', mock_generate),
        ):
            main.make_plugin(name='foo', description='bar', generator=mock_generate)
        mock_plugin.assert_called_once_with(
            name='foo', description='bar', generator=mock_generate
        )

    def test_main_run_plugin_ok(self):
        mock_run = MagicMock()
        mock_plugin = MagicMock()
        with (
            patch('opentf.plugins.dummygenerator.main.run_plugin', mock_run),
            patch('opentf.plugins.dummygenerator.main.plugin', mock_plugin),
        ):
            main.run_plugin(mock_plugin)
        mock_run.assert_called_once_with(mock_plugin)
