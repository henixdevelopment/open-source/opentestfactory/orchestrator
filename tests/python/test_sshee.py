# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""SSH Channel unit tests."""

import logging
import os
import unittest
import sys

from datetime import datetime, timedelta, timezone
from queue import Queue

from unittest.mock import MagicMock, patch, mock_open, call

from requests import Response

import yaml

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.sshee import main

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

ENV_PARTIAL = {'SSH_CHANNEL_HOST': 'hOSt'}

ENV_SSH_FULL = {
    'SSH_CHANNEL_HOST': 'hOSt',
    'SSH_CHANNEL_USER': 'usEr',
    'SSH_CHANNEL_TAGS': 'ta,gs',
    'SSH_CHANNEL_PASSWORD': 'passWorD',
}

ENV_SSH_FULL_BADTAG = {
    'SSH_CHANNEL_HOST': 'hOSt',
    'SSH_CHANNEL_USER': 'usEr',
    'SSH_CHANNEL_TAGS': 'ta,gs,oh_no',
    'SSH_CHANNEL_PASSWORD': 'passWorD',
}

ENV_SSH_FULL_POOLS = {
    'SSH_CHANNEL_HOST': 'hOSt',
    'SSH_CHANNEL_USER': 'usEr',
    'SSH_CHANNEL_TAGS': 'ta,gs',
    'SSH_CHANNEL_PASSWORD': 'passWorD',
    'SSH_CHANNEL_POOLS': 'foo',
}

SSHEE_YAML = '''pools:
  ssh-dummy:
  - host: dummy.example.com
    username: foo
    password: secret
    tags: [ssh, linux]
'''

BAD_SSHEE_YAML = '''pool:
  ssh-dummy:
  - host: dummy.example.com
    username: foo
    password: secret
    tags: [ssh, linux]
'''

EXECUTION_COMMAND_0 = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ExecutionCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
        'step_sequence_id': 0,
        'channel_id': 'chAnnel_Id',
    },
    "step": {},
    "job": {
        "name": "JOBNAME",
        "runs-on": "linux",
    },
}

EXECUTION_COMMAND_RELEASE = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ExecutionCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
        'step_sequence_id': main.CHANNEL_RELEASE,
        'channel_id': 'chAnnel_Id',
    },
    "step": {},
    "job": {
        "name": "JOBNAME",
        "runs-on": "linux",
    },
}

POOLS = yaml.safe_load(
    '''pools:
  aws:
  - host: ec2-3-250-212-135.eu-west-1.compute.amazonaws.com
    port: 22
    username: ec2-user
    ssh_host_keys: /home/rv5617/.ssh/known_hosts
    key_filename: /home/rv5617/.ssh/test-tf-2.pem
    missing_host_key_policy: reject
    tags: [ssh, linux]
  ssh-dummy:
  - host: dummy.example.com
    username: foo
    password: secret
    tags: [ssh, linux]
  ssh1:
  - host: ec2-3-250-212-135.eu-west-1.compute.amazonaws.com
    port: 22
    username: ec2-user
    password: foo
    ssh_host_keys: /home/rv5617/.ssh/known_hosts
    key_filename: /home/rv5617/.ssh/test-tf-2.pem
    missing_host_key_policy: reject
    tags: [ssh]
  - hosts: [foo.example.com, bar.example.com]
    port: 22
    username: ec2-user
    password: foo
    ssh_host_keys: /home/rv5617/.ssh/known_hosts
    key_filename: /home/rv5617/.ssh/test-tf-2.pem
    missing_host_key_policy: auto-add
    tags: [example]
  agents:
  - host: robotframework.agents
    port: 2222
    username: linuxserver.io
    key_filename: /app/sshchannel/ssh-agents-key.pem
    missing_host_key_policy: auto-add
    tags: [ssh, linux, roboframework]
  - host: cypress-agent.agents
    port: 22
    username: squashtf
    script_path: /home/squashtf
    key_filename: /app/sshchannel/ssh-agents-key.pem
    missing_host_key_policy: auto-add
    tags: [ssh, linux, cypress]
  - host: junit.agents
    port: 22
    username: squashtf
    script_path: /home/squashtf
    key_filename: /app/sshchannel/ssh-agents-key.pem
    missing_host_key_policy: auto-add
    tags: [ssh, linux, junit]
'''
)

HOSTS_HOSTID = {'hOsT_Id': {'tags': ['linux'], 'host': 'hOst', 'username': 'bOb'}}
HOSTS_MAKEOFFER = {
    'hOstId': {
        'tags': ['windows'],
        'host': 'hOst',
        'username': 'fOe',
        'script_path': 'pAth',
    }
}

PLUGIN_CONF_MAKEOFFER = {'CONFIG': {'hooks': {'event': 'eVent'}}, 'CONTEXT': {}}

CHANNELS_PENDING = {'chAnnel_Id': ['hOsT_Id']}
CHANNELS_RESOLVED = {'chAnnel_Id': 'hOsT_Id'}

########################################################################


def _make_mocks_for_ssh():
    """Prepare mocs for SSH connections.

    # Returned values

    - mock_publish
    - mock_scp
    - mock_sshclient
    """
    mock_publish = MagicMock()
    mock_scp = MagicMock()
    mock_stdout = MagicMock()
    mock_stdout.recv_exit_status = MagicMock(return_value=0)
    mock_client = MagicMock()
    mock_client.exec_command = lambda _: (None, mock_stdout, None)
    mock_context = MagicMock()
    mock_context.__enter__ = MagicMock(return_value=mock_client)
    mock_sshclient = MagicMock(return_value=mock_context)
    return mock_publish, mock_scp, mock_sshclient


class TestSSHEE(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_listjobsteps(self):
        JOB_ID = 'foo'
        what = main.list_job_steps(JOB_ID)
        self.assertEqual(len(main.JOBS), 1)
        self.assertIn(JOB_ID, main.JOBS)

    def test_notify_available_channels(self):
        mock_publish = MagicMock()
        with patch('opentf.plugins.sshee.main.publish', mock_publish):
            main.notify_available_channels()
        mock_publish.assert_called_once()

    def test_make_offers_none(self):
        mock_publish = MagicMock()
        mock_info = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.publish', mock_publish),
            patch('opentf.plugins.sshee.main.info', mock_info),
        ):
            main.make_offers({'job_id': 'jOb_iD'}, [])
        mock_publish.assert_not_called()
        mock_info.assert_called_once()

    def test_make_offers_with_hooks(self):
        mock_is_av = MagicMock(return_value=True)
        mock_is_al = MagicMock(return_value=True)
        mock_publish = MagicMock()
        mock_me = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.publish', mock_publish),
            patch('opentf.plugins.sshee.main.is_available', mock_is_av),
            patch('opentf.plugins.sshee.main.is_alive', mock_is_al),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_MAKEOFFER),
            patch('opentf.plugins.sshee.main.plugin.config', PLUGIN_CONF_MAKEOFFER),
            patch('opentf.plugins.sshee.main.make_event', mock_me),
            patch('opentf.plugins.sshee.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.make_offers({'job_id': 'jOb_iD'}, ['hOstId'])
            self.assertEqual(mock_me.call_count, 2)
            metadata = mock_me.call_args_list[0][1]['metadata']
            self.assertEqual({'event': 'eVent'}, metadata['annotations']['hooks'])
            assert (
                call(main.SSHChannelMetrics.CHANNEL_OFFERS_COUNTER, 1)
                in mock_manager.set_metric.call_args_list
            )

    # _get_phase

    def test_get_phase_unknown(self):
        with patch('opentf.plugins.sshee.main.is_alive', MagicMock(return_value=True)):
            self.assertEqual(main._get_phase('uNkNown'), 'IDLE')

    def test_get_phase_unreachable(self):
        with patch('opentf.plugins.sshee.main.is_alive', MagicMock(return_value=False)):
            self.assertEqual(main._get_phase('SsHId'), 'UNREACHABLE')

    def test_get_phase_busy(self):
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.sshee.main.JOBS_HOSTS', {'job_Id': host_id}),
            patch('opentf.plugins.sshee.main.is_alive', MagicMock(return_value=True)),
        ):
            self.assertEqual(main._get_phase(host_id), 'BUSY')

    def test_get_phase_pending_expired(self):
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.sshee.main.JOBS_HOSTS', {}),
            patch(
                'opentf.plugins.sshee.main.HOSTS_LEASES',
                {host_id: datetime.now(timezone.utc)},
            ),
            patch('opentf.plugins.sshee.main.is_alive', MagicMock(return_value=True)),
        ):
            self.assertEqual(main._get_phase(host_id), 'IDLE')

    def test_get_phase_pending_notexpired(self):
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.sshee.main.JOBS_HOSTS', {}),
            patch(
                'opentf.plugins.sshee.main.HOSTS_LEASES',
                {host_id: datetime.now(timezone.utc) + timedelta(seconds=10)},
            ),
            patch('opentf.plugins.sshee.main.is_alive', MagicMock(return_value=True)),
        ):
            self.assertEqual(main._get_phase(host_id), 'PENDING')

    # _get_job

    def test_get_job_empty(self):
        self.assertIsNone(main._get_job('uNkNown'))

    def test_get_job_notempty(self):
        host_id = 'hOsT_Id'
        with patch('opentf.plugins.sshee.main.JOBS_HOSTS', {'job_id': host_id}):
            self.assertIsNotNone(main._get_job(host_id))

    # get_matching_hosts

    def test_get_matching_hosts_none(self):
        mock_info = MagicMock()
        with patch('opentf.plugins.sshee.main.info', mock_info):
            self.assertFalse(main.get_matching_hosts({'foo', 'bar'}, 'namEspacE'))
        mock_info.assert_called_once()

    def test_is_alive_ko_port(self):
        mock_debug = MagicMock()
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.sshee.main.debug', mock_debug),
            patch(
                'opentf.plugins.sshee.main.HOSTS',
                {
                    'hOsT_Id': {
                        'host': 'aabbccddee.example.com',
                        'username': 'alicE',
                        'port': 12345,
                    }
                },
            ),
        ):
            self.assertFalse(main.is_alive(host_id))
        mock_debug.assert_called_once()
        self.assertIn('alicE@aabbccddee.example.com:12345', mock_debug.call_args[0][0])

    def test_is_alive_ko_defaultport(self):
        mock_debug = MagicMock()
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.sshee.main.debug', mock_debug),
            patch(
                'opentf.plugins.sshee.main.HOSTS',
                {
                    'hOsT_Id': {
                        'host': 'aabbccddee.example.com',
                        'username': 'alicE',
                    }
                },
            ),
        ):
            self.assertFalse(main.is_alive(host_id))
        mock_debug.assert_called_once()
        self.assertIn('alicE@aabbccddee.example.com:22', mock_debug.call_args[0][0])

    def test_get_job_host_unknown(self):
        with (
            patch('opentf.plugins.sshee.main.JOBS_HOSTS', {}),
            patch('opentf.plugins.sshee.main.CHANNELS', {'chAnnel_Id': []}),
        ):
            self.assertIsNone(main.get_job_host('jOb_Id', 'chAnnel_Id'))

    def test_get_job_host_used(self):
        job_id = 'jOb_Id'
        host_id = 'hOsT_Id'
        with (
            patch('opentf.plugins.sshee.main.JOBS_HOSTS', {job_id: host_id}),
            patch('opentf.plugins.sshee.main.CHANNELS', {}),
        ):
            self.assertEqual(main.get_job_host(job_id, 'chAnnel_Id'), host_id)

    def test_get_job_host_lease(self):
        job_id = 'jOb_Id'
        channel_id = 'chAnnel_Id'
        host_id = 'hOsT_Id'
        mock = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.JOBS_HOSTS', {}),
            patch('opentf.plugins.sshee.main.CHANNELS', {channel_id: [host_id]}),
            patch(
                'opentf.plugins.sshee.main.HOSTS_LEASES',
                {host_id: datetime.now(timezone.utc) + timedelta(days=1)},
            ),
            patch('opentf.plugins.sshee.main.notify_available_channels', mock),
        ):
            self.assertEqual(main.get_job_host(job_id, channel_id), host_id)
        mock.assert_called_once()

    # _initialiaze_pools

    def test_initialize_pools_partial(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONFIG': {'pools': {}},
            'CONTEXT': {'targets': ['unChangeD']},
        }
        with patch('os.environ', ENV_PARTIAL):
            self.assertRaises(SystemExit, main._initialize_pools, mock_plugin)

    def test_initialize_pools_badtag(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONFIG': {'pools': {}},
            'CONTEXT': {'targets': ['unChangeD']},
        }
        with patch('os.environ', ENV_SSH_FULL_BADTAG):
            self.assertRaises(SystemExit, main._initialize_pools, mock_plugin)

    def test_initialize_pools_missingos(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONFIG': {'pools': {}},
            'CONTEXT': {'targets': ['unChangeD']},
        }
        with patch('os.environ', ENV_SSH_FULL):
            self.assertRaises(SystemExit, main._initialize_pools, mock_plugin)

    # main

    def test_main_nopool(self):
        mock_fatal = MagicMock()
        mock_plugin = MagicMock()
        with patch('opentf.plugins.sshee.main.fatal', mock_fatal):
            self.assertRaises(KeyError, main.main, mock_plugin)

    def test_main_mock(self):
        mock_fatal = MagicMock()
        mock_threading = MagicMock()
        mock_threading.Thread = MagicMock()
        mock_watch = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.fatal', mock_fatal),
            patch('opentf.plugins.sshee.main.threading', mock_threading),
            patch('opentf.plugins.sshee.main._initialize_pools'),
            patch('opentf.plugins.sshee.main.run_plugin'),
            patch('opentf.plugins.sshee.main.watch_and_notify', mock_watch),
        ):
            main.main(MagicMock())
        mock_fatal.assert_not_called()
        self.assertEqual(mock_threading.Thread.call_count, 2)
        mock_watch.assert_called_once()

    # release_resource

    def test_release_resources_transient(self):
        hosts = {'yidi': {'transient': True}, 'yada': {}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main._fill_host_data'),
        ):
            main.release_resources('yidi', 'job_id', 'channel_id')
            self.assertEqual(len(main.HOSTS), 1)
            self.assertNotIn('yidi', main.HOSTS)

    def test_release_resources_notransient(self):
        hosts = {'yidi': {'transient': True}, 'yada': {}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
        ):
            main.release_resources('yada', 'job_id', 'channel_id')
            self.assertEqual(len(main.HOSTS), 2)
            self.assertIn('yada', main.HOSTS)

    def test_release_resources_become(self):
        hosts = {
            'yada': {
                'host': 'foo',
                'username': 'bar',
                'tags': ['tag0'],
                'id': 'olDiD',
                'become': {
                    'host': 'foo',
                    'username': 'bar',
                    'tags': ['tag1', 'tag2'],
                    'id': 'neWId',
                },
            },
            'yidi': {},
        }
        with (
            patch('opentf.plugins.sshee.main.HOSTS', hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main._update_hosts_counters_gauges'),
        ):
            main.release_resources('yada', 'job_id', 'channel_id')
            self.assertEqual(len(main.HOSTS), 2)
            self.assertNotIn('olDiD', main.HOSTS)
            self.assertIn('neWId', main.HOSTS)
            self.assertNotIn('tag0', main.HOSTS['neWId']['tags'])
            self.assertIn('tag2', main.HOSTS['neWId']['tags'])

    # run_command

    def test_run_command_firststep(self):
        mock_debug = MagicMock(side_effect=SystemExit)
        mock_gjh = MagicMock(return_value='hOsT_Id')
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', CHANNELS_PENDING),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_HOSTID),
            patch('opentf.plugins.sshee.main.debug', mock_debug),
            patch('opentf.plugins.sshee.main.get_job_host', mock_gjh),
        ):
            self.assertRaises(SystemExit, main.run_command, EXECUTION_COMMAND_0)
        mock_debug.assert_called_once_with(
            'SSH host %s handles job %s.', 'bOb@hOst:22', 'JOB_ID'
        )

    def test_run_command_laststep(self):
        mock_debug = MagicMock()
        mock_gjh = MagicMock(return_value='hOsT_Id')
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', CHANNELS_RESOLVED),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_HOSTID),
            patch('opentf.plugins.sshee.main.debug', mock_debug),
            patch('opentf.plugins.sshee.main.get_job_host', mock_gjh),
            patch(
                'opentf.plugins.sshee.main.make_script',
                MagicMock(return_value=('a', 'b', 'c')),
            ),
            patch('opentf.plugins.sshee.main.publish'),
            patch('opentf.plugins.sshee.main.release_resources'),
            patch(
                'opentf.plugins.sshee.main.connect', MagicMock(side_effect=Exception())
            ),
        ):
            main.run_command(EXECUTION_COMMAND_RELEASE)
        mock_debug.assert_called_once_with(
            'SSH host %s released from handling job %s.', 'bOb@hOst:22', 'JOB_ID'
        )

    def test_run_command_nossh(self):
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', CHANNELS_RESOLVED),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_HOSTID),
            patch(
                'opentf.plugins.sshee.main.make_script',
                MagicMock(return_value=('a', 'b', 'c')),
            ),
            patch('opentf.plugins.sshee.main.publish', mock_publish),
        ):
            main.run_command(EXECUTION_COMMAND_0)
        mock_publish.assert_called_once()

    def test_run_command_ssh(self):
        mock_publish, mock_scp, mock_sshclient = _make_mocks_for_ssh()
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', CHANNELS_RESOLVED),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_HOSTID),
            patch(
                'opentf.plugins.sshee.main.make_script',
                MagicMock(return_value=('a', 'b', 'c')),
            ),
            patch('opentf.plugins.sshee.main.publish', mock_publish),
            patch('opentf.plugins.sshee.main.connect'),
            patch('opentf.plugins.sshee.main.SCPClient', mock_scp),
            patch('opentf.plugins.sshee.main.SSHClient', mock_sshclient),
            patch(
                'opentf.plugins.sshee.main.process_output',
                MagicMock(return_value={'metadata': {'id': 'foo'}}),
            ),
        ):
            main.run_command(EXECUTION_COMMAND_0)
        mock_sshclient.assert_called_once()
        mock_publish.assert_called_once()
        self.assertEqual(mock_publish.call_args[0][0], {'metadata': {'id': 'foo'}})

    def test_run_command_attach(self):
        mock_publish, mock_scp, mock_sshclient = _make_mocks_for_ssh()
        mock_po = MagicMock()
        mock_debug = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', CHANNELS_RESOLVED),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_HOSTID),
            patch(
                'opentf.plugins.sshee.main.make_script',
                MagicMock(return_value=('a', 'b', 'c')),
            ),
            patch('opentf.plugins.sshee.main.publish', mock_publish),
            patch('opentf.plugins.sshee.main.connect'),
            patch('opentf.plugins.sshee.main.SCPClient', mock_scp),
            patch('opentf.plugins.sshee.main.SSHClient', mock_sshclient),
            patch('opentf.plugins.sshee.main.process_output', mock_po),
            patch('opentf.plugins.sshee.main.debug', mock_debug),
        ):
            main.run_command(EXECUTION_COMMAND_0)
            mock_po.assert_called_once()
            args = mock_po.call_args[0][5]
            args('remotePath', 'deStinAtion')
        debug_calls = mock_debug.call_args_list
        self.assertEqual(
            debug_calls[-1][0],
            (
                'Got attachment %s from host %s, locally stored at %s.',
                'remotePath',
                'bOb@hOst:22',
                'deStinAtion',
            ),
        )
        self.assertEqual(
            debug_calls[-2][0],
            ('Awaiting attachment %s from host %s.', 'remotePath', 'bOb@hOst:22'),
        )
        mock_publish.assert_called_once()
        mock_sshclient.assert_called_once()

    def test_run_command_put(self):
        mock_publish, mock_scp, mock_sshclient = _make_mocks_for_ssh()
        mock_po = MagicMock()
        mock_debug = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', CHANNELS_RESOLVED),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_HOSTID),
            patch(
                'opentf.plugins.sshee.main.make_script',
                MagicMock(return_value=('a', 'b', 'c')),
            ),
            patch('opentf.plugins.sshee.main.publish', mock_publish),
            patch('opentf.plugins.sshee.main.connect'),
            patch('opentf.plugins.sshee.main.SCPClient', mock_scp),
            patch('opentf.plugins.sshee.main.SSHClient', mock_sshclient),
            patch('opentf.plugins.sshee.main.process_output', mock_po),
            patch('opentf.plugins.sshee.main.debug', mock_debug),
        ):
            main.run_command(EXECUTION_COMMAND_0)
            mock_po.assert_called_once()
            args = mock_po.call_args[0][6]
            args('remotePath', 'sourcePath')
        debug_calls = mock_debug.call_args_list
        self.assertEqual(
            debug_calls[-1][0],
            (
                'Sent file %s to host %s, remotely stored as %s.',
                'sourcePath',
                'bOb@hOst:22',
                'remotePath',
            ),
        )
        self.assertEqual(
            debug_calls[-2][0],
            ('Sending file %s to host %s.', 'sourcePath', 'bOb@hOst:22'),
        )
        mock_publish.assert_called_once()
        mock_sshclient.assert_called_once()

    def test_run_command_with_upload(self):
        mock_publish, mock_scp, mock_sshclient = _make_mocks_for_ssh()
        mock_po = MagicMock(return_value={'metadata': {'upload': 0}})
        mock_upload = MagicMock(return_value='bar')
        mock_debug = MagicMock()
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', CHANNELS_RESOLVED),
            patch('opentf.plugins.sshee.main.HOSTS', HOSTS_HOSTID),
            patch(
                'opentf.plugins.sshee.main.make_script',
                MagicMock(return_value=('a', 'b', 'c')),
            ),
            patch('opentf.plugins.sshee.main.publish', mock_publish),
            patch('opentf.plugins.sshee.main.connect'),
            patch('opentf.plugins.sshee.main.SCPClient', mock_scp),
            patch('opentf.plugins.sshee.main.SSHClient', mock_sshclient),
            patch('opentf.plugins.sshee.main.process_output', mock_po),
            patch('opentf.plugins.sshee.main.debug', mock_debug),
            patch('opentf.plugins.sshee.main.process_upload', mock_upload),
        ):
            main.run_command(EXECUTION_COMMAND_0)
            mock_po.assert_called_once()
            args = mock_po.call_args[0][5]
            args('remotePath', 'deStinAtion')
        debug_calls = mock_debug.call_args_list
        self.assertEqual(
            debug_calls[-1][0],
            (
                'Got attachment %s from host %s, locally stored at %s.',
                'remotePath',
                'bOb@hOst:22',
                'deStinAtion',
            ),
        )
        self.assertEqual(
            debug_calls[-2][0],
            ('Awaiting attachment %s from host %s.', 'remotePath', 'bOb@hOst:22'),
        )
        self.assertEqual(2, mock_publish.call_count)
        mock_upload.assert_called_once_with({'metadata': {'upload': 0}})
        mock_sshclient.assert_called_once()

    # read_pools

    def test_read_pools_noconf(self):
        env = {}
        mock_app = MagicMock()
        mock_app.config = {
            'CONFIG': {'pools': {}},
            'CONTEXT': {'targets': ['unChangeD']},
        }
        with (
            patch('os.environ', env),
            patch('opentf.plugins.sshee.main.plugin', mock_app),
            patch('opentf.plugins.sshee.main.validate_schema', MagicMock()),
        ):
            main.read_pools()
        self.assertEqual(mock_app.config['CONTEXT']['targets'], ['unChangeD'])

    def test_read_pools_env_ok(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONFIG': {'pools': {'bar': {}}},
            'CONTEXT': {'targets': ['unChangeD']},
        }
        with (
            patch('os.environ', ENV_SSH_FULL),
            patch('opentf.plugins.sshee.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.sshee.main.validate_schema',
                MagicMock(return_value=(True, None)),
            ),
        ):
            main.read_pools()
        self.assertEqual(mock_plugin.config['CONTEXT']['targets'], ['agents'])
        self.assertIn('agents', mock_plugin.config['CONFIG']['pools'])
        agents = mock_plugin.config['CONFIG']['pools']['agents']
        self.assertEqual(len(agents), 1)
        agent = agents.pop()
        self.assertEqual(agent['host'], 'hOSt')
        self.assertEqual(agent['port'], 22)

    def test_read_pools_ssh_channel_pools_OK(self):
        mock_app = MagicMock()
        mock_app.config = {'CONFIG': {}, 'CONTEXT': {'targets': ['unChangeD']}}
        with (
            patch('os.environ', {'SSH_CHANNEL_POOLS': 'foo'}),
            patch('opentf.plugins.sshee.main.plugin', mock_app),
            patch(
                'opentf.plugins.sshee.main.validate_schema',
                MagicMock(return_value=(True, None)),
            ),
            patch('builtins.open', mock_open(read_data=SSHEE_YAML)),
        ):
            main.read_pools()
        self.assertIn('ssh-dummy', mock_app.config['CONTEXT']['targets'])
        agents = mock_app.config['CONFIG']['pools']['ssh-dummy']
        agent = agents[0]
        self.assertEqual(agent['host'], 'dummy.example.com')

    def test_read_pools_ssh_channel_pools_NOK(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {'CONFIG': {}, 'CONTEXT': {'targets': ['unChangeD']}}
        mock_error = MagicMock()
        with (
            patch('os.environ', {'SSH_CHANNEL_POOLS': 'foo'}),
            patch('opentf.plugins.sshee.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.sshee.main.validate_schema',
                MagicMock(return_value=(True, None)),
            ),
            patch('builtins.open', mock_open(read_data=BAD_SSHEE_YAML)),
            patch('opentf.plugins.sshee.main.error', mock_error),
        ):
            main.read_pools()
        mock_error.assert_called_with(
            'External pools definition "%s" needs a "pools" entry.  Ignoring.', 'foo'
        )
        self.assertEqual(mock_plugin.config['CONTEXT']['targets'], [])

    def test_read_pools_invalid_format_no_environ_host(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONFIG': {'pools': {'bar': 'baz'}},
            'CONTEXT': {'targets': ['unChangeD']},
        }
        with (
            patch('opentf.plugins.sshee.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.sshee.main._read_external_pools_definition',
                MagicMock(return_value=['not a dictionary']),
            ),
            patch(
                'opentf.plugins.sshee.main._maybe_add_environ_host',
                MagicMock(return_value=False),
            ),
            patch(
                'opentf.plugins.sshee.main._get_environ', MagicMock(return_value='foo')
            ),
        ):
            main.read_pools()
        self.assertEqual(mock_plugin.config['CONTEXT']['targets'], [])
        self.assertEqual(mock_plugin.config['CONFIG']['pools'], {})

    def test_read_pools_invalid_format_environ_host(self):
        mock_plugin = MagicMock()
        mock_plugin.config = {
            'CONFIG': {'pools': {'bar': 'baz'}},
            'CONTEXT': {'targets': ['unChangeD']},
        }
        with (
            patch('os.environ', ENV_SSH_FULL_POOLS),
            patch('opentf.plugins.sshee.main.plugin', mock_plugin),
            patch(
                'opentf.plugins.sshee.main._read_external_pools_definition',
                MagicMock(return_value=['not a dictionary']),
            ),
        ):
            main.read_pools()
        self.assertEqual(mock_plugin.config['CONTEXT']['targets'], ['agents'])
        self.assertIn('agents', mock_plugin.config['CONFIG']['pools'])

    # get_hosts_from_conf

    def test_get_hosts_from_conf_missingtarget(self):
        mock_app = MagicMock()
        mock_app.config = {
            'CONFIG': {
                'pools': {
                    'founD': [
                        {
                            'password': 'seCreT',
                            'tags': ['linux'],
                            'host': 'example.com',
                        }
                    ]
                }
            },
            'CONTEXT': {'targets': ['miSSinG', 'founD']},
        }
        mock_error = MagicMock()
        mock_queue = Queue()
        with (
            patch('opentf.plugins.sshee.main.plugin', mock_app),
            patch('opentf.plugins.sshee.main.read_pools'),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE', mock_queue),
            patch('opentf.plugins.sshee.main.error', mock_error),
        ):
            main.get_hosts_from_conf()
            mock_error.assert_called_once_with(
                'Could not find target "miSSinG" in pool.'
            )
            hosts_tuple = mock_queue.get()
            self.assertEqual(len(hosts_tuple), 1)
            self.assertEqual(len(hosts_tuple[0]), 1)

    def test_get_hosts_from_conf_target_host(self):
        mock_app = MagicMock()
        mock_app.config = {
            'CONFIG': {
                'pools': {
                    'targeT': [
                        {
                            'password': 'seCreT',
                            'tags': ['windows', 'foo'],
                            'host': 'dEmO.exAmple.coM',
                        }
                    ]
                }
            },
            'CONTEXT': {'targets': ['targeT']},
        }
        mock_queue = Queue()
        with (
            patch('opentf.plugins.sshee.main.plugin', mock_app),
            patch('opentf.plugins.sshee.main.read_pools'),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE', mock_queue),
        ):
            main.get_hosts_from_conf()
            hosts = mock_queue.get()
            self.assertEqual(len(hosts), 1)
            self.assertEqual(len(hosts[0]), 1)

    def test_get_hosts_from_conf_target_hosts(self):
        mock_app = MagicMock()
        mock_app.config = {
            'CONFIG': {
                'pools': {
                    'targeT': [
                        {
                            'password': 'seCreT',
                            'tags': ['windows', 'foo'],
                            'hosts': ['dEmO1.exAmple.coM', 'dEmO2.exAmple.coM'],
                        }
                    ]
                }
            },
            'CONTEXT': {'targets': ['targeT']},
        }
        mock_queue = Queue()
        with (
            patch('opentf.plugins.sshee.main.plugin', mock_app),
            patch('opentf.plugins.sshee.main.read_pools'),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE', mock_queue),
        ):
            main.get_hosts_from_conf()
            hosts = mock_queue.get()
            self.assertEqual(len(hosts), 1)
            self.assertEqual(len(hosts[0]), 2)

    # refresh_hosts

    def test_refresh_hosts_empty_empty(self):
        mock_hosts = {}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
        ):
            main.refresh_hosts({})
            self.assertFalse(mock_hosts)

    def test_refresh_hosts_notempty_empty(self):
        mock_hosts = {'yada': {'host': 'foo', 'username': 'bar'}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
        ):
            main.refresh_hosts({})
            self.assertFalse(mock_hosts)

    def test_refresh_hosts_notempty_empty_busy_transient(self):
        mock_hosts = {'yada': {'host': 'foo', 'username': 'bar', 'tags': ['tag']}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.is_available', lambda x: x != 'yada'),
            patch('opentf.plugins.sshee.main._update_hosts_counters_gauges'),
        ):
            main.refresh_hosts({})
            self.assertEqual(len(mock_hosts), 1)
            self.assertIn('yada', mock_hosts)
            self.assertIn('transient', mock_hosts['yada'])

    def test_refresh_hosts_notempty_notempty_busy_nottransient(self):
        mock_hosts = {'yada': {'host': 'foo', 'username': 'bar', 'tags': ['tag']}}
        mock_new = {'not_yada': {'host': 'foo', 'username': 'bar', 'tags': ['tag']}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.is_available', lambda x: x != 'yada'),
            patch('opentf.plugins.sshee.main._update_hosts_counters_gauges'),
        ):
            main.refresh_hosts(mock_new)
            self.assertEqual(len(mock_hosts), 1)
            self.assertIn('yada', mock_hosts)
            self.assertNotIn('transient', mock_hosts['yada'])

    def test_refresh_hosts_empty_notempty(self):
        mock_hosts = {}
        mock_new = {'yada': {'host': 'foo', 'username': 'bar', 'tags': ['tag']}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
        ):
            main.refresh_hosts(mock_new)
            self.assertEqual(len(mock_hosts), 1)
            self.assertIn('yada', mock_hosts)

    def test_refresh_hosts_notempty_notempty_notbusy(self):
        mock_hosts = {'yada': {'host': 'foo', 'username': 'bar', 'tags': ['tag']}}
        mock_new = {'yidi': {'host': 'foobar', 'username': 'bar', 'tags': ['tag']}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main._update_hosts_counters_gauges'),
        ):
            main.refresh_hosts(mock_new)
            self.assertEqual(len(mock_hosts), 1)
            self.assertIn('yidi', mock_hosts)

    def test_refresh_hosts_notempty_notempty_busy_become(self):
        mock_hosts = {'yada': {'host': 'foo', 'username': 'bar', 'tags': ['tag']}}
        mock_new = {'yidi': {'host': 'foo', 'username': 'bar', 'tags': ['tag', 'tag1']}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.is_available', lambda x: x != 'yada'),
            patch('opentf.plugins.sshee.main._update_hosts_counters_gauges'),
        ):
            main.refresh_hosts(mock_new)
            self.assertEqual(len(mock_hosts), 1)
            self.assertIn('yada', mock_hosts)
            self.assertIn('become', mock_hosts['yada'])
            self.assertIn('tag1', mock_hosts['yada']['become']['tags'])

    # prepare_channels

    def test_prepare_channels_mo(self):
        mock_queue = Queue()
        mock_queue.put(('foo', 'bar'))
        mock_makeoffer = MagicMock(side_effect=SystemExit)
        with (
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE', mock_queue),
            patch('opentf.plugins.sshee.main.make_offers', mock_makeoffer),
        ):
            self.assertRaises(SystemExit, main.prepare_channels)
        mock_makeoffer.assert_called_once_with('foo', 'bar')

    def test_prepare_channels_rf(self):
        mock_queue = Queue()
        mock_queue.put(('foo',))
        mock_refreshhosts = MagicMock(side_effect=SystemExit)
        with (
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE', mock_queue),
            patch('opentf.plugins.sshee.main.refresh_hosts', mock_refreshhosts),
        ):
            self.assertRaises(SystemExit, main.prepare_channels)
        mock_refreshhosts.assert_called_once_with('foo')

    def test_prepare_channels_rr(self):
        mock_queue = Queue()
        mock_queue.put(('foo', 'bar', 'baz'))
        mock_releaseresources = MagicMock(side_effect=SystemExit)
        with (
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE', mock_queue),
            patch('opentf.plugins.sshee.main.release_resources', mock_releaseresources),
        ):
            self.assertRaises(SystemExit, main.prepare_channels)
        mock_releaseresources.assert_called_once_with('foo', 'bar', 'baz')

    # process_inbox

    def test_process_inbox_request_notsatisfied(self):
        body = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionCommand',
            'metadata': {
                'name': 'foo',
                'workflow_id': 'n/a',
                'job_id': 'JOB_ID',
                'job_origin': [],
                'step_id': 'STEP_ID',
                'step_origin': [],
                'step_sequence_id': -1,
            },
            'runs-on': ['foo'],
            'scripts': [],
        }
        with (
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE') as mock_rq,
            patch('opentf.plugins.sshee.main.STEPS_QUEUE') as mock_sq,
            patch('opentf.plugins.sshee.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.process_executioncommand(body)
        mock_rq.put.assert_not_called()
        mock_sq.put.assert_not_called()
        assert (
            call(main.SSHChannelMetrics.CHANNEL_REQUESTS_COUNTER, 1)
            in mock_manager.set_metric.call_args_list
        )

    def test_process_inbox_unknowntarget(self):
        body = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionCommand',
            'metadata': {
                'name': 'foo',
                'workflow_id': 'n/a',
                'job_id': 'JOB_ID',
                'job_origin': [],
                'step_id': 'STEP_ID',
                'step_origin': [],
                'step_sequence_id': 0,
                'channel_id': 'somewhere_else',
            },
            'runs-on': ['foo'],
            'scripts': [],
        }
        with (
            patch('opentf.plugins.sshee.main.REQUESTS_QUEUE') as mock_rq,
            patch('opentf.plugins.sshee.main.STEPS_QUEUE') as mock_sq,
        ):
            main.process_executioncommand(body)
        mock_rq.put.assert_not_called()
        mock_sq.put.assert_not_called()

    def test_process_inbox_duplicate(self):
        body = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionCommand',
            'metadata': {
                'name': 'foo',
                'workflow_id': 'n/a',
                'job_id': 'JOB_ID',
                'job_origin': [],
                'step_id': 'STEP_ID',
                'step_origin': [],
                'step_sequence_id': 0,
                'channel_id': 'here',
            },
            'runs-on': ['foo'],
            'scripts': [],
        }
        with (
            patch('opentf.plugins.sshee.main.CHANNELS', {'here': {}}),
            patch(
                'opentf.plugins.sshee.main.list_job_steps', MagicMock(return_value=[0])
            ),
            patch('opentf.plugins.sshee.main.STEPS_QUEUE') as mock_sq,
            patch('opentf.plugins.sshee.main.warning') as mock_warning,
        ):
            main.process_executioncommand(body)
        mock_sq.put.assert_not_called()
        mock_warning.assert_called_once_with('Step 0 already received for job JOB_ID.')

    # telemetry

    def test_update_metrics_refresh_hosts(self):
        mock_hosts_phases = {'yada': 'IDLE'}
        mock_hosts = {'yada': {'host': 'foo', 'username': 'foobar', 'tags': ['tag']}}
        mock_new = {'yidi': {'host': 'foo', 'username': 'bar', 'tags': ['tag', 'tag1']}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.is_available', return_value=True),
            patch('opentf.plugins.sshee.main.HOSTS_PHASES', mock_hosts_phases),
            patch('opentf.plugins.sshee.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.refresh_hosts(mock_new)
        self.assertEqual({'yidi': 'IDLE'}, mock_hosts_phases)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.SSHChannelMetrics.HOSTS_CURRENT_GAUGE, 1) in calls
        assert call(main.SSHChannelMetrics.HOSTS_CURRENT_GAUGE, -1) in calls
        assert call(main.SSHChannelMetrics.CHANNELS_IDLE_GAUGE, -1) in calls
        assert call(main.SSHChannelMetrics.CHANNELS_IDLE_GAUGE, 1) in calls
        assert call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, 1, {'tag': 'tag'}) in calls
        assert (
            call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, -1, {'tag': 'tag'}) in calls
        )
        assert (
            call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, 1, {'tag': 'tag1'}) in calls
        )

    def test_update_metrics_refresh_hosts_tags_only(self):
        mock_hosts_phases = {'yada': 'IDLE'}
        mock_hosts = {'yada': {'host': 'foo', 'username': 'bar', 'tags': ['tag']}}
        mock_new = {'yidi': {'host': 'foo', 'username': 'bar', 'tags': ['tag', 'tag1']}}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.is_available', return_value=True),
            patch('opentf.plugins.sshee.main.HOSTS_PHASES', mock_hosts_phases),
            patch('opentf.plugins.sshee.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.refresh_hosts(mock_new)
        self.assertEqual({'yidi': 'IDLE'}, mock_hosts_phases)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.SSHChannelMetrics.HOSTS_CURRENT_GAUGE, 1) not in calls
        assert call(main.SSHChannelMetrics.CHANNELS_IDLE_GAUGE, -1) in calls
        assert call(main.SSHChannelMetrics.CHANNELS_IDLE_GAUGE, 1) in calls
        assert call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, 1, {'tag': 'tag'}) in calls
        assert (
            call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, -1, {'tag': 'tag'}) in calls
        )
        assert (
            call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, 1, {'tag': 'tag1'}) in calls
        )

    def test_update_metrics_release_resources_transient(self):
        mock_hosts = {
            'yidi': {
                'username': 'foo',
                'host': 'bar',
                'transient': True,
                'tags': ['tag'],
            },
            'yada': {},
        }
        mock_hosts_phases = {'yidi': 'BUSY'}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.HOSTS_PHASES', mock_hosts_phases),
            patch('opentf.plugins.sshee.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.release_resources('yidi', 'job_id', 'channel_id')
        self.assertEqual({}, mock_hosts_phases)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.SSHChannelMetrics.HOSTS_CURRENT_GAUGE, -1) in calls
        assert call(main.SSHChannelMetrics.CHANNELS_BUSY_GAUGE, -1) in calls
        assert call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, -1, {'tag': 'tag'})

    def test_update_metrics_release_resources_become(self):
        hosts = {
            'yada': {
                'host': 'foo',
                'username': 'bar',
                'tags': ['tag0'],
                'id': 'olDiD',
                'become': {
                    'host': 'foo',
                    'username': 'bar',
                    'tags': ['tag1', 'tag2'],
                    'id': 'neWId',
                },
            },
            'yidi': {},
        }
        hosts_phases = {'yada': 'BUSY'}
        with (
            patch('opentf.plugins.sshee.main.HOSTS', hosts),
            patch('opentf.plugins.sshee.main.notify_available_channels'),
            patch('opentf.plugins.sshee.main.HOSTS_PHASES', hosts_phases),
            patch('opentf.plugins.sshee.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.release_resources('yada', 'job_id', 'channel_id')
        self.assertEqual({'neWId': 'IDLE'}, hosts_phases)
        calls = mock_manager.set_metric.call_args_list
        assert call(main.SSHChannelMetrics.CHANNELS_BUSY_GAUGE, -1) in calls
        assert call(main.SSHChannelMetrics.CHANNELS_IDLE_GAUGE, 1) in calls
        assert (
            call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, -1, {'tag': 'tag0'}) in calls
        )
        assert (
            call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, 1, {'tag': 'tag1'}) in calls
        )
        assert (
            call(main.SSHChannelMetrics.HOSTS_TAGS_GAUGE, 1, {'tag': 'tag2'}) in calls
        )

    def test_update_metrics_notify_available_channels(self):
        mock_hosts = {
            'host_a': {'host': 'foo', 'tags': []},
            'host_b': {'host': 'foo', 'tags': []},
            'host_c': {'host': 'foo', 'tags': []},
            'host_d': {'host': 'foo', 'tags': []},
        }
        mock_phases = {
            'host_a': 'IDLE',
            'host_b': 'IDLE',
            'host_c': 'IDLE',
            'host_d': 'BUSY',
        }
        with (
            patch('opentf.plugins.sshee.main.publish'),
            patch('opentf.plugins.sshee.main.make_event'),
            patch('opentf.plugins.sshee.main._get_job', return_value='jOb_Id'),
            patch('opentf.plugins.sshee.main.HOSTS', mock_hosts),
            patch('opentf.plugins.sshee.main.HOSTS_PHASES', mock_phases),
            patch('opentf.plugins.sshee.main.JOBS_HOSTS', {'job': 'host_b'}),
            patch(
                'opentf.plugins.sshee.main.HOSTS_LEASES',
                {'host_c': datetime.now(timezone.utc) + timedelta(seconds=10)},
            ),
            patch(
                'opentf.plugins.sshee.main.is_alive',
                side_effect=[False, True, True, True],
            ),
            patch('opentf.plugins.sshee.main.TELEMETRY_MANAGER') as mock_manager,
        ):
            main.notify_available_channels()
        self.assertEqual(
            {
                'host_a': 'UNREACHABLE',
                'host_b': 'BUSY',
                'host_c': 'PENDING',
                'host_d': 'IDLE',
            },
            mock_phases,
        )
        calls = mock_manager.set_metric.call_args_list
        self.assertEqual(
            3, calls.count(call(main.SSHChannelMetrics.CHANNELS_IDLE_GAUGE, -1))
        )
        assert call(main.SSHChannelMetrics.CHANNELS_BUSY_GAUGE, -1)
        assert call(main.SSHChannelMetrics.CHANNELS_UNREACHABLE_GAUGE, 1)
        assert call(main.SSHChannelMetrics.CHANNELS_BUSY_GAUGE, 1)
        assert call(main.SSHChannelMetrics.CHANNELS_PENDING_GAUGE, 1)
        assert call(main.SSHChannelMetrics.CHANNELS_IDLE_GAUGE, 1)


if __name__ == '__main__':
    unittest.main()
