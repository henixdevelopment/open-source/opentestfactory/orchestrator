# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""EventBus unit tests."""

import logging
import os
import unittest
import sys

from collections import defaultdict
from queue import Queue
from unittest.mock import MagicMock, patch

from requests import exceptions, Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
logging.disable(logging.CRITICAL)
from opentf.core import eventbus

logging.disable(logging.NOTSET)

mockresponse = Response()
mockresponse.status_code = 200

########################################################################

SUBSCRIPTION_NAME = SUBSCRIPTION_UUID = 'cafebabe'
SUBSCRIPTIONS_DATA = {
    SUBSCRIPTION_NAME: {
        'metadata': {
            'name': SUBSCRIPTION_NAME,
            'subscription_id': SUBSCRIPTION_UUID,
            'dispatcher_id': 'disPatcheR',
        },
        'spec': {'subscriber': {'endpoint': 'eNDpOINt'}},
        'status': {
            'publicationCount': 0,
            'publicationStatusSummary': defaultdict(int),
            'quarantine': 0,
        },
    }
}


class TestEventBus(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        app = eventbus.app
        app.config['CONTEXT']['enable_insecure_login'] = True
        self.app = app.test_client()
        self.assertFalse(app.debug)

    # list_subscriptions

    def test_list_subscriptions_badselector(self):
        response = self.app.get('/subscriptions?fieldSelector=xxx>2')
        self.assertEqual(response.status_code, 422)

    def test_list_subscriptions_labelselector(self):
        subscriptions = {
            'app1': {'metadata': {'name': 'app1', 'labels': {'app': 'web'}}},
            'app2': {'metadata': {'name': 'app2', 'labels': {'app': 'db'}}},
        }
        with patch('opentf.core.eventbus.SUBSCRIPTIONS', subscriptions):
            response = self.app.get('/subscriptions?labelSelector=app=web')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json['items']), 1)
        self.assertIn('app1', response.json['items'])

    def test_list_subscriptions_fieldselector(self):
        subscriptions = {
            'app1': {'metadata': {'name': 'app1', 'labels': {'app': 'web'}}},
            'app2': {'metadata': {'name': 'app2', 'labels': {'app': 'db'}}},
        }
        with patch('opentf.core.eventbus.SUBSCRIPTIONS', subscriptions):
            response = self.app.get(
                '/subscriptions?fieldSelector=metadata.labels.app==db'
            )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.json['items']), 1)
        self.assertIn('app2', response.json['items'])

    def test_list_subscriptions_noselector(self):
        response = self.app.get('/subscriptions')
        self.assertEqual(response.status_code, 200)

    # register_subscription

    def test_subscription_noselector(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 201)
        self.assertIsNotNone(response.json['details'].get('uuid'))
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre + 1)

    def test_subscription(self):
        dispatchers = {}
        subscriptions = {}
        with (
            patch('opentf.core.eventbus.DISPATCHERS', dispatchers),
            patch('opentf.core.eventbus.SUBSCRIPTIONS', subscriptions),
            patch('opentf.core.eventbus.make_dispatchqueue'),
        ):
            response = self.app.post(
                '/subscriptions',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Subscription",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {"matchFields": {"def.i-1.y": "12"}},
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
            self.assertEqual(response.status_code, 201)
            uuid = response.json['details'].get('uuid')
            self.assertIsNotNone(uuid)
            annotations = subscriptions[uuid]['metadata']['annotations']
            self.assertFalse(annotations[eventbus.LABEL_SELECTOR])
            self.assertEqual(annotations[eventbus.FIELD_SELECTOR], 'def.i-1.y==12')
            self.assertEqual(len(subscriptions), 1)
            self.assertEqual(len(dispatchers), 1)
            mock = MagicMock(return_value=mockresponse)
            with patch('opentf.core.eventbus.make_status_response', mock):
                eventbus.cancel_subscription(uuid)
            self.assertEqual(len(subscriptions), 0)
            self.assertEqual(len(dispatchers), 0)
            self.assertEqual(mock.call_args.args[0], 'OK')
            with patch('opentf.core.eventbus.make_status_response', mock):
                eventbus.cancel_subscription(uuid)
            self.assertEqual(mock.call_args.args[0], 'NotFound')
            self.assertEqual(len(subscriptions), 0)

    def test_subscription_kind(self):
        dispatchers = {}
        subscriptions = {}
        with (
            patch('opentf.core.eventbus.DISPATCHERS', dispatchers),
            patch('opentf.core.eventbus.SUBSCRIPTIONS', subscriptions),
            patch('opentf.core.eventbus.make_dispatchqueue') as mock_mdq,
        ):
            response = self.app.post(
                '/subscriptions',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Subscription",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {"matchKind": "KIND"},
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
            self.assertEqual(response.status_code, 201)
            mock_mdq.assert_called_once()
            uuid = response.json['details'].get('uuid')
            self.assertIsNotNone(uuid)
            annotations = subscriptions[uuid]['metadata']['annotations']
            self.assertFalse(annotations[eventbus.LABEL_SELECTOR])
            self.assertEqual(annotations[eventbus.FIELD_SELECTOR], 'kind==KIND')
            self.assertEqual(len(subscriptions), 1)
            self.assertTrue(eventbus.is_interested(uuid, {'kind': 'KIND'}))
            self.assertFalse(eventbus.is_interested(uuid, {'kind': 'kIND'}))

    def test_subscription_labels(self):
        dispatchers = {}
        subscriptions = {}
        with (
            patch('opentf.core.eventbus.DISPATCHERS', dispatchers),
            patch('opentf.core.eventbus.SUBSCRIPTIONS', subscriptions),
            patch('opentf.core.eventbus.make_dispatchqueue') as mock_mdq,
        ):
            response = self.app.post(
                '/subscriptions',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Subscription",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {"matchLabels": {"a.b.c": "12"}},
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
            self.assertEqual(response.status_code, 201)
            uuid = response.json['details'].get('uuid')
            self.assertIsNotNone(uuid)
            annotations = subscriptions[uuid]['metadata']['annotations']
            self.assertTrue(annotations[eventbus.LABEL_SELECTOR])
            self.assertFalse(annotations[eventbus.FIELD_SELECTOR])
            self.assertEqual(len(subscriptions), 1)

    def test_subscription_fieldexpression(self):
        dispatchers = {}
        subscriptions = {}
        with (
            patch('opentf.core.eventbus.DISPATCHERS', dispatchers),
            patch('opentf.core.eventbus.SUBSCRIPTIONS', subscriptions),
            patch('opentf.core.eventbus.make_dispatchqueue') as mock_mdq,
        ):
            response = self.app.post(
                '/subscriptions',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Subscription",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {
                            "matchFieldExpressions": [
                                {
                                    "key": "foo",
                                    "operator": "ContainsAll",
                                    "values": ["value1", "value2"],
                                },
                                {
                                    "key": "bar",
                                    "operator": "DoesNotContainAll",
                                    "values": ["value1", "value2"],
                                },
                                {
                                    "key": "baz",
                                    "operator": "In",
                                    "values": ["value1", "value2"],
                                },
                            ]
                        },
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
            self.assertEqual(response.status_code, 201)
            uuid = response.json['details'].get('uuid')
            self.assertIsNotNone(uuid)
            annotations = subscriptions[uuid]['metadata']['annotations']
            self.assertFalse(annotations[eventbus.LABEL_SELECTOR])
            self.assertTrue(annotations[eventbus.FIELD_SELECTOR])
            self.assertIn(
                '(value1, value2) in foo', annotations[eventbus.FIELD_SELECTOR]
            )
            self.assertIn(
                '(value1, value2) notin bar', annotations[eventbus.FIELD_SELECTOR]
            )
            self.assertIn(
                'baz in (value1, value2)', annotations[eventbus.FIELD_SELECTOR]
            )
            self.assertEqual(len(subscriptions), 1)

    def test_subscription_badselector(self):
        dispatchers = {}
        subscriptions = {}
        with (
            patch('opentf.core.eventbus.DISPATCHERS', dispatchers),
            patch('opentf.core.eventbus.SUBSCRIPTIONS', subscriptions),
            patch('opentf.core.eventbus.make_dispatchqueue') as mock_mdq,
        ):
            response = self.app.post(
                '/subscriptions',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Subscription",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {"matchLabels": {"def.i-1.": "12"}},
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
            mock_mdq.assert_not_called()
            self.assertEqual(response.status_code, 422)
            self.assertFalse(subscriptions)

    def test_subscription_emptyselector(self):
        pre = len(eventbus.SUBSCRIPTIONS)
        response = self.app.post(
            '/subscriptions',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Subscription",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertEqual(response.status_code, 422)
        self.assertEqual(len(eventbus.SUBSCRIPTIONS), pre)

    # add_publication

    def test_publication_formatted(self):
        response = self.app.post(
            '/publications',
            json={
                "apiVersion": "opentestfactory.org/v1alpha1",
                "kind": "Publication",
                "metadata": {"name": "foo"},
                "spec": {
                    "selector": {"matchFields": {"def.i-1.y": "12"}},
                    "subscriber": {"endpoint": "xxx"},
                },
            },
        )
        self.assertIn('no matching subscription', response.json['message'])

    def test_publication_unformatted(self):
        response = self.app.post(
            '/publications',
            json={},
        )
        self.assertIn('no matching subscription', response.json['message'])

    def test_publication_onesubscriber(self):
        dispatchers = {'disPatcheR': {'queue': MagicMock()}}
        with (
            patch(
                'opentf.core.eventbus.SUBSCRIPTIONS',
                {
                    '123': {
                        'spec': {'subscriber': {'endpoint': 'eNDpOINt'}},
                        'status': {'quarantine': 0},
                        'metadata': {'dispatcher_id': 'disPatcheR'},
                    }
                },
            ),
            patch('opentf.core.eventbus.is_interested', MagicMock(return_value=True)),
            patch('opentf.core.eventbus.DISPATCHERS', dispatchers),
        ):
            response = self.app.post(
                '/publications',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Publication",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {"matchFields": {"def.i-1.y": "12"}},
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
        self.assertIn('Publication', response.json['message'])
        self.assertIn('received.', response.json['message'])
        dispatchers['disPatcheR']['queue'].put.assert_called_once()

    def test_publication_onesubscriber_quarantine(self):
        dispatchers = {'disPatcheR': {'queue': MagicMock()}}
        with (
            patch(
                'opentf.core.eventbus.SUBSCRIPTIONS',
                {
                    '123': {
                        'spec': {'subscriber': {'endpoint': 'eNDpOINt'}},
                        'status': {'quarantine': 1},
                        'metadata': {'dispatcher_id': 'disPatcheR'},
                    }
                },
            ),
            patch('opentf.core.eventbus.is_interested', MagicMock(return_value=True)),
            patch('opentf.core.eventbus.DISPATCHERS', dispatchers),
        ):
            response = self.app.post(
                '/publications',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Publication",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {"matchFields": {"def.i-1.y": "12"}},
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
        self.assertIn('Publication', response.json['message'])
        self.assertIn('received.', response.json['message'])
        dispatchers['disPatcheR']['queue'].put.assert_called_once()

    # dispatch

    def test_dispatch_unknownsubscription(self):
        mock_debug = MagicMock()
        mock_info = MagicMock()
        with (
            patch('opentf.core.eventbus.SUBSCRIPTIONS', {}),
            patch('opentf.core.eventbus.debug', mock_debug),
            patch('opentf.core.eventbus.info', mock_info),
        ):
            eventbus.dispatch(({}, SUBSCRIPTION_NAME, 'tIMeSTAMp', 'pubId', 123), None)
        mock_debug.assert_not_called()
        mock_info.assert_called_once_with(
            'Subscription %s no longer exists.  Ignoring.', SUBSCRIPTION_NAME
        )

    def test_dispatch_ok(self):
        mock_session = MagicMock()
        mock_session.post = MagicMock()
        mock_dispatchers = {'disPatcheR': {'session': mock_session}}
        pub = {}
        with (
            patch('opentf.core.eventbus.SUBSCRIPTIONS', SUBSCRIPTIONS_DATA),
            patch('opentf.core.eventbus.DISPATCHERS', mock_dispatchers),
        ):
            eventbus.dispatch((pub, SUBSCRIPTION_NAME, 'tIMeSTAMp', 'pubId', 123), None)
        mock_session.post.assert_called_once_with(
            'eNDpOINt',
            data=pub,
            headers={
                'X-Subscription-ID': SUBSCRIPTION_NAME,
                'X-Publication-ID': 'pubId',
                'Content-Type': 'application/json',
            },
            verify=True,
            timeout=10,
        )

    def test_dispatch_failed_500_retry(self):
        mock_response = MagicMock()
        mock_response.status_code = 500
        mock_post = MagicMock(return_value=mock_response)
        pub = {}
        pub_id = 'pubId'
        with (
            patch('opentf.core.eventbus.SUBSCRIPTIONS', SUBSCRIPTIONS_DATA),
            patch('requests.post', mock_post),
        ):
            self.assertRaises(
                Exception,
                eventbus.dispatch,
                (pub, SUBSCRIPTION_NAME, 'tIMeSTAMp', pub_id, 123),
                None,
            )

    def test_dispatch_failed_400_noretry(self):
        mock_response = MagicMock()
        mock_response.status_code = 400
        mock_session = MagicMock()
        mock_session.post = MagicMock(return_value=mock_response)
        mock_dispatchers = {'disPatcheR': {'session': mock_session}}
        pub = {}
        pub_id = 'pubId'
        with (
            patch('opentf.core.eventbus.SUBSCRIPTIONS', SUBSCRIPTIONS_DATA),
            patch('opentf.core.eventbus.DISPATCHERS', mock_dispatchers),
        ):
            eventbus.dispatch((pub, SUBSCRIPTION_NAME, 'tIMeSTAMp', pub_id, 123), None)
        mock_session.post.assert_called_once()

    # telemetry

    def test_update_status_set_metrics(self):
        sub = {
            'metadata': {'name': 'service', 'subscription_id': 'sub_id'},
            'status': {
                'publicationCount': 0,
                'lastPublicationTimestamp': '',
                'publicationStatusSummary': {'200': 0},
            },
        }
        mock_response = MagicMock()
        mock_response.status_code = 200
        with (
            patch(
                'opentf.core.eventbus.GAUGES',
                {'sub_id': {'count': 0, 'sum': 0, 'max': 0, 'min': 0}},
            ),
            patch('opentf.core.eventbus.TELEMETRY_MANAGER') as mock_telemetry,
        ):
            eventbus.update_status(sub, mock_response, '123', 345)
        metrics = mock_telemetry.set_metric.call_args_list
        self.assertEqual(1, metrics[0][0][1])
        self.assertEqual({'subscription_name': 'service'}, metrics[1][0][2])
        self.assertEqual({'http.response.status_code': 200}, metrics[2][0][2])
        self.assertEqual(
            {'subscription_name': 'service', 'http.response.status_code': 200},
            metrics[3][0][2],
        )

    def test_publication_set_metric(self):
        with patch('opentf.core.eventbus.TELEMETRY_MANAGER') as mock_telemetry:
            self.app.post(
                '/publications',
                json={
                    "apiVersion": "opentestfactory.org/v1alpha1",
                    "kind": "Publication",
                    "metadata": {"name": "foo"},
                    "spec": {
                        "selector": {"matchFields": {"def.i-1.y": "12"}},
                        "subscriber": {"endpoint": "xxx"},
                    },
                },
            )
        mock_telemetry.set_metric.assert_called_once()
        self.assertEqual(1, mock_telemetry.set_metric.call_args[0][1])

    # main

    def test_main_preparation_thread_fail(self):
        svc = 'serVicE'
        mock_run = MagicMock()
        with patch('opentf.core.eventbus.run_app', mock_run):
            eventbus.main(svc)
        mock_run.assert_called_once_with(svc)


if __name__ == '__main__':
    unittest.main()
