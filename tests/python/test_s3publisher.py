# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""S3 publisher unit tests."""

import os
import logging
import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response


########################################################################

EVENT_WR = {
    'kind': 'WorkflowResult',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

EVENT_OTHER = {
    'kind': 'FooBar',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

########################################################################

mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
logging.disable(logging.CRITICAL)
from opentf.plugins.s3publisher import main


class TestS3Publisher(unittest.TestCase):
    def test_main_mock(self):
        svc = MagicMock()
        credentials = {'s3credentials': 'fOOBAR'}
        mock_run = MagicMock()
        with (
            patch('opentf.plugins.s3publisher.main.subscribe', MagicMock()),
            patch('opentf.plugins.s3publisher.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.s3publisher.main.run_app', mock_run),
            patch(
                'opentf.plugins.s3publisher.main.check_credential_file',
                MagicMock(return_value=False),
            ),
            patch('opentf.plugins.s3publisher.main.context', credentials),
        ):
            main.main(svc)
        mock_run.assert_called_once_with(svc)
        svc.logger.warning.assert_called_once()

    def test_process_result_badjson(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(side_effect=Exception('ooOps'))
        with (
            patch('opentf.plugins.s3publisher.main.request', mock_request),
            patch('opentf.plugins.s3publisher.main.make_status_response') as mock_msr,
        ):
            main.process_result()
        mock_msr.assert_called_once_with('BadRequest', 'Could not parse body: ooOps.')

    def test_process_result_unexpected(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value={})
        with (
            patch('opentf.plugins.s3publisher.main.request', mock_request),
            patch('opentf.plugins.s3publisher.main.make_status_response') as mock_msr,
        ):
            main.process_result()
        mock_msr.assert_called_once_with('BadRequest', 'Missing apiVersion or kind.')

    def test_process_result_attachments(self):
        result = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionResult',
            'metadata': {'workflow_id': 'wOrKflow_iD'},
            'attachments': ['foo', 'bar'],
        }
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=result)
        with (
            patch('opentf.plugins.s3publisher.main.request', mock_request),
            patch('opentf.plugins.s3publisher.main.make_status_response') as mock_msr,
            patch(
                'opentf.plugins.s3publisher.main.validate_schema',
                MagicMock(return_value=(True, None)),
            ),
            patch('opentf.plugins.s3publisher.main.ATTACHMENTS') as mock_queue,
        ):
            main.process_result()
        mock_msr.assert_called_once_with('OK', '')
        mock_queue.put.assert_called_once()

    def test_process_result_noattachments(self):
        result = {
            'apiVersion': 'opentestfactory.org/v1alpha1',
            'kind': 'ExecutionResult',
            'metadata': {'workflow_id': 'wOrKflow_iD'},
        }
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=result)
        with (
            patch('opentf.plugins.s3publisher.main.request', mock_request),
            patch('opentf.plugins.s3publisher.main.make_status_response') as mock_msr,
            patch(
                'opentf.plugins.s3publisher.main.validate_schema',
                MagicMock(return_value=(True, None)),
            ),
            patch('opentf.plugins.s3publisher.main.ATTACHMENTS') as mock_queue,
        ):
            main.process_result()
        mock_msr.assert_called_once_with('OK', '')
        mock_queue.put.assert_not_called()

    def test_put_attachments(self):
        mock_app = MagicMock()
        mock_client = MagicMock()
        with (
            patch(
                'opentf.plugins.s3publisher.main.check_credential_file',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.s3publisher.main.create_s3_client',
                MagicMock(return_value=mock_client),
            ),
            patch('opentf.plugins.s3publisher.main.app', mock_app),
        ):
            main.push_attachments_to_s3(['foO', 'bAr', 'baZ'], 'wOrkflow_id')
        self.assertEqual(mock_client.upload_file.call_count, 3)


if __name__ == '__main__':
    unittest.main()
