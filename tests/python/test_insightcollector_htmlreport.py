# Copyright (c) 2023-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Insight Collector HTML reports generator unit tests."""

import logging
import os
import sys
import unittest

from unittest.mock import MagicMock, patch

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.insightcollector.htmlreport import (
    prepare_html_report_event,
    _create_report_html,
    _create_report_items,
    _generate_jobs_cloud,
    _generate_report_doughnuts,
    _generate_rows,
    _generate_testcases_table,
    _get_css,
    _get_datasources,
    _maybe_adjust_progressbar_segments,
    _maybe_complete_with_errorslist,
    DEFAULT_COLUMNS_TESTCASES,
    ScopeError,
)
from opentf.plugins.insightcollector.main import ObserverProxy

########################################################################
# Templates and helpers

WF_EVENT = [
    {
        "kind": "Workflow",
        "metadata": {
            "creationTimestamp": "2023-12-08T09:56:47.958817",
            "completionTimestamp": "2023-12-08T09:59:47.958817",
        },
    }
]

EVENTS = [
    {
        "kind": "Workflow",
        "metadata": {
            "creationTimestamp": "2023-12-08T09:56:47.958817",
        },
    },
    {
        "kind": "ExecutionCommand",
        "metadata": {
            "creationTimestamp": "2023-12-08T09:56:47.986590",
            "job_id": "0b1c919d-9ef7-45c6-8247-fd238753898d",
            "name": "robot-job",
            "step_sequence_id": -1,
        },
        "runs-on": ["robotframework", "linux"],
        "variables": {"SOME_VAR": "VAR_SOME"},
    },
    {
        "kind": "ExecutionCommand",
        "metadata": {
            "creationTimestamp": "2023-12-08T09:56:56.028640",
            "job_id": "9bb378da-8dd8-414b-8ddb-37d307de34be",
            "name": "cucumber-job",
            "step_sequence_id": -1,
        },
        "runs-on": ["cucumber5", "linux"],
        "variables": {"VAR_ENV": "ENV_VAR"},
    },
    {
        "kind": "ExecutionCommand",
        "metadata": {
            "creationTimestamp": "2023-12-08T09:57:40.177939",
            "job_id": "ca3807b2-a3be-4cef-9303-89a94d779af4",
            "name": "cypress-job",
            "step_sequence_id": -1,
        },
        "runs-on": ["cypress", "linux"],
    },
    {
        "kind": "WorkflowCompleted",
        "metadata": {
            "creationTimestamp": "2023-12-08T09:58:24.249853",
        },
    },
]

HTML_REPORT_MODEL = {
    'name': 'DefaultReport',
    'kind': 'SummaryReport',
    'spec': {
        'template': [
            {'name': 'Summary', 'kind': 'SummaryBlock'},
            {'name': 'Jobs', 'kind': 'Cloud', 'datasource': 'jobs'},
            {'name': 'Tags', 'kind': 'Table', 'datasource': 'tags'},
            {'name': 'Testcases', 'kind': 'Table', 'datasource': 'testcases'},
        ]
    },
}


SPEC_WITH_TEMPLATE_SCOPE = {
    'style': 'Style',
    'scripts': 'Some nasty script',
    'template': [
        {'name': 'tAble', 'kind': 'Table', 'scope': 'true', 'datasource': 'testcases'}
    ],
}


TESTCASES_WITH_FAILUREDETAILS_AND_ERRORSLIST = {
    "9833881f-2bf9-452d-b2a8-60fb2c292f3f": {
        "metadata": {
            "name": "Test Backslashes Reporter#Test  single",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
        },
        "status": "SUCCESS",
        "test": {
            "job": "robot-job",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "success",
            "suiteName": "Test Backslashes Reporter",
            "testCaseName": "Test  single",
        },
        "execution": {
            "errorsList": [
                {"message": "Robot Framework General Error", "timestamp": "today"}
            ],
        },
    },
    "9933881f-2bf9-452d-b2a8-60fb2c292f3f": {
        "metadata": {
            "name": "Test Backslashes Reporter#Test  double",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
        },
        "status": "FAILURE",
        "test": {
            "job": "robot-job",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "failure",
            "suiteName": "Test Backslashes Reporter",
            "testCaseName": "Test  double",
        },
        "execution": {
            "failureDetails": {"text": "Small failure", "type": "AssertionError"},
        },
    },
}

TESTCASE_METADATA_DURATION_ZERO_TWO = {
    "9833881f-2bf9-452d-b2a8-60fb2c292f3f": {
        "metadata": {
            "name": "Test Backslashes Reporter#Test  single",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "SUCCESS",
        "test": {
            "job": "robot-job",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "success",
            "suiteName": "Test Backslashes Reporter",
            "testCaseName": "Test  single",
        },
        "execution": {"duration": 0},
    },
    "2f438bde-f882-4eb0-ab73-d8fa0dfdde95": {
        "metadata": {
            "name": "Stock Management To Check Duplicate Execution#Current stock duplicate",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "FAILURE",
        "test": {
            "job": "cucumber-job",
            "uses": "cucumber5/execute@v1",
            "technology": "cucumber5",
            "runs-on": ["cucumber5", "linux"],
            "managed": False,
            "outcome": "failure",
            "suiteName": "Stock Management To Check Duplicate Execution",
            "testCaseName": "Current stock duplicate",
        },
        "execution": {"duration": 2},
    },
}

TESTCASE_METADATA_EMPTY_AGGREGATE = {
    "9833881f-2bf9-452d-b2a8-60fb2c292f3f": {
        "metadata": {
            "name": "Test Backslashes Reporter#Test  single",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "SUCCESS",
        "test": {
            "job": "robot-job",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "success",
            "suiteName": "Test Backslashes Reporter",
            "testCaseName": "Test  single",
        },
        "execution": {"duration": 0},
    },
    "2f438bde-f882-4eb0-ab73-d8fa0dfdde95": {
        "metadata": {
            "name": "Stock Management To Check Duplicate Execution#Current stock duplicate",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "FAILURE",
        "test": {
            "job": "cucumber-job",
            "uses": "cucumber5/execute@v1",
            "technology": "cucumber5",
            "runs-on": ["cucumber5", "linux"],
            "managed": False,
            "outcome": "failure",
            "suiteName": "Stock Management To Check Duplicate Execution",
            "testCaseName": "Current stock duplicate",
        },
        "execution": {"duration": 0},
    },
}

TESTCASE_METADATA_NOT_MANAGED = {
    "9833881f-2bf9-452d-b2a8-60fb2c292f3f": {
        "metadata": {
            "name": "Test Backslashes Reporter#Test  single",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "SUCCESS",
        "test": {
            "job": "robot-job",
            "uses": "robotframework/execute@v1",
            "technology": "robotframework",
            "runs-on": ["robotframework", "linux"],
            "managed": False,
            "outcome": "success",
            "suiteName": "Test Backslashes Reporter",
            "testCaseName": "Test  single",
        },
        "execution": {"duration": 1},
    },
    "2f438bde-f882-4eb0-ab73-d8fa0dfdde95": {
        "metadata": {
            "name": "Stock Management To Check Duplicate Execution#Current stock duplicate",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "FAILURE",
        "test": {
            "job": "cucumber-job",
            "uses": "cucumber5/execute@v1",
            "technology": "cucumber5",
            "runs-on": ["cucumber5", "linux"],
            "managed": False,
            "outcome": "failure",
            "suiteName": "Stock Management To Check Duplicate Execution",
            "testCaseName": "Current stock duplicate",
        },
        "execution": {"duration": 2},
    },
    "21a896ac-9a27-46d6-a26f-fba9babdb5a2": {
        "metadata": {
            "name": "multFailure#CalculatorMult multFailure",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "ERROR",
        "test": {
            "job": "cypress-job",
            "uses": "cypress/execute@v1",
            "technology": "cypress",
            "runs-on": ["cypress", "linux"],
            "managed": False,
            "outcome": "error",
            "suiteName": "multFailure",
            "testCaseName": "CalculatorMult multFailure",
        },
        "execution": {"duration": 3},
    },
    "22a896ac-9a27-46d6-a26f-fba9babdb5a2": {
        "metadata": {
            "name": "wroom#CalculatorMult multFailure",
            "execution_id": "10c88322-b835-40e6-89aa-ae3921fbd251",
            "creationTimestamp": "today",
        },
        "status": "SKIPPED",
        "test": {
            "job": "cypress-job",
            "uses": "cypress/execute@v1",
            "technology": "cypress",
            "runs-on": ["cypress", "linux"],
            "managed": False,
            "outcome": "skipped",
            "suiteName": "wroom",
            "testCaseName": "CalculatorMult multFailure",
        },
        "execution": {"duration": 4},
    },
}

JOBS_NOT_MANAGED = {
    "robot-job": {
        "metadata": {"name": "job_name"},
        "status": {
            "testCaseStatusSummary": {
                "error": 0,
                "failure": 0,
                "skipped": 0,
                "success": 1,
            },
            "testCaseCount": 1,
        },
        "spec": {
            "runs-on": ["robotframework", "linux"],
            "variables": {"SOME_VAR": "VAR_SOME"},
        },
        "uuid": "robot-job",
    },
    "cypress-job": {
        "metadata": {"name": "job_name"},
        "status": {
            "testCaseStatusSummary": {
                "error": 1,
                "failure": 0,
                "skipped": 1,
                "success": 0,
            },
            "testCaseCount": 2,
        },
        "spec": {
            "runs-on": ["cypress", "linux"],
            "variables": {"SOME_VAR": "VAR_SOME"},
        },
        "uuid": "cypress-job",
    },
    "cucumber-job": {
        "metadata": {"name": "job_name"},
        "status": {
            "testCaseStatusSummary": {
                "error": 0,
                "failure": 1,
                "skipped": 0,
                "success": 0,
            },
            "testCaseCount": 1,
        },
        "spec": {
            "runs-on": ["cucumber5", "linux"],
            "variables": {"VAR_ENV": "ENV_VAR"},
        },
        "uuid": "cucumber-job",
    },
}

TAGS_NOT_MANAGED = {
    "robotframework": {
        "metadata": {"name": "robotframework"},
        "status": {
            "testCaseStatusSummary": {
                "error": 0,
                "failure": 0,
                "skipped": 0,
                "success": 1,
            },
            "testCaseCount": 1,
        },
        "uuid": "robotframework",
    },
    "linux": {
        "metadata": {"name": "linux"},
        "status": {
            "testCaseStatusSummary": {
                "error": 1,
                "failure": 1,
                "skipped": 1,
                "success": 1,
            },
            "testCaseCount": 4,
        },
        "uuid": "linux",
    },
    "cypress": {
        "status": {
            "testCaseStatusSummary": {
                "error": 1,
                "failure": 0,
                "skipped": 1,
                "success": 0,
            },
            "testCaseCount": 2,
        },
        "uuid": "cypress",
    },
    "cucumber5": {
        "metadata": {"name": "cucumber5"},
        "status": {
            "testCaseStatusSummary": {
                "error": 0,
                "failure": 0,
                "skipped": 0,
                "success": 1,
            },
            "testCaseCount": 1,
        },
        "uuid": "cucumber5",
    },
}

TESTCASE_METADATA_MANAGED = {
    "a5201d3a-47b4-4d73-984e-467ad419466a": {
        "metadata": {"name": "addSuccess#CalculatorAdd addSuccess"},
        "status": "FAILURE",
        "test": {
            "job": "squashTMJob-0",
            "uses": "cypress/execute@v1",
            "technology": "cypress",
            "runs-on": ["cypress", "linux"],
            "managed": True,
            "reference": "cypress-calc-single-spec#cypress/integration/calculator.add.ok.spec.js",
            "uuid": "39496e59-724b-4e5f-9ad3-c083573638f9",
            "outcome": "failure",
            "suiteName": "addSuccess",
            "testCaseName": "CalculatorAdd addSuccess",
            "data": {
                "DSNAME": "Dataset 1",
                "DS_bar2": "foo1",
                "DS_foo1": "bar1",
                "TC_CUF_CHECKBOX": "true",
                "TC_CUF_DATE": "2023-06-28",
            },
        },
        "execution": {},
    }
}

JOBS_INCEPTION = {
    "6a7746d9-9138-49a5-a737-0ed82543728c": {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "Job",
        "metadata": {
            "creationTimestamp": "2024-06-03T11:00:24.842490",
            "id": "6a7746d9-9138-49a5-a737-0ed82543728c",
            "name": "prepare",
            "namespace": "default",
            "workflow_id": "03f41e5b-5465-471a-bf4a-1ae1a9b3e163",
        },
        "spec": {"runs-on": ["inception"], "variables": {}},
        "status": {
            "duration": 379.719,
            "endTime": "2024-06-03T11:00:25.322056",
            "phase": "SUCCEEDED",
            "requestTime": "2024-06-03T11:00:24.853849",
            "startTime": "2024-06-03T11:00:24.942337",
            "testCaseCount": 0,
            "testCaseStatusSummary": {
                "cancelled": 0,
                "error": 0,
                "failure": 0,
                "skipped": 0,
                "success": 0,
            },
        },
        "uuid": "6a7746d9-9138-49a5-a737-0ed82543728c",
    },
    "6af6a996-eaf1-470e-ba42-aa5ec0bf53e3": {
        "apiVersion": "opentestfactory.org/v1alpha1",
        "kind": "Job",
        "metadata": {
            "creationTimestamp": "2024-06-03T11:00:26.022436",
            "id": "6af6a996-eaf1-470e-ba42-aa5ec0bf53e3",
            "name": "squashTMJob-0",
            "namespace": "default",
            "workflow_id": "03f41e5b-5465-471a-bf4a-1ae1a9b3e163",
        },
        "spec": {"runs-on": ["katalon", "inception"], "variables": {}},
        "status": {
            "duration": 3706.5389999999998,
            "endTime": "2024-06-03T11:00:29.834042",
            "phase": "SUCCEEDED",
            "requestTime": "2024-06-03T11:00:26.036175",
            "startTime": "2024-06-03T11:00:26.127503",
            "testCaseCount": 2,
            "testCaseStatusSummary": {
                "cancelled": 0,
                "error": 0,
                "failure": 1,
                "skipped": 0,
                "success": 1,
            },
        },
        "uuid": "6af6a996-eaf1-470e-ba42-aa5ec0bf53e3",
    },
}


########################################################################


class TestInsightCollectorHtmlReport(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    # prepare_report_event

    def test_prepare_html_report_event_ok(self):
        mock_event = MagicMock()
        mock_file_open = MagicMock()
        mock_table = MagicMock()
        mock_doughnut = MagicMock()
        mock_block = MagicMock()
        mock_element = MagicMock()

        datasources = {
            'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED},
            'jobs': {'(true) && (true)': JOBS_NOT_MANAGED},
            'tags': {'(true) && (true)': TAGS_NOT_MANAGED},
        }
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', 'yada')
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = WF_EVENT
        target = '/tmp/wFid-uuid_WR_DefaultReport.html'
        with (
            patch(
                'opentf.core.datasources._has_testresult',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_workflowresult_event',
                mock_event,
            ),
            patch('builtins.open', mock_file_open),
            patch('opentf.plugins.insightcollector.htmlreport.make_table', mock_table),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_doughnut',
                mock_doughnut,
            ),
            patch('opentf.plugins.insightcollector.htmlreport.make_block', mock_block),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_element', mock_element
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DOUGHNUT_COLORS',
                ['a', 'b', 'c', 'd'],
            ),
        ):
            prepare_html_report_event(
                mock_proxy,
                HTML_REPORT_MODEL,
                'wFid',
                None,
                {'workflow': {'creationTimestamp': '2023-01-01'}},
                ['style'],
                'true',
            )
        self.assertEqual(1, mock_file_open.call_count)
        mock_event.assert_called_once()
        self.assertEqual(4, mock_table.call_count)
        table_calls = mock_table.call_args_list
        first_call_data = table_calls[0][0][0]
        for row in first_call_data:
            ratio = row[1]
            self.assertEqual('25.00%', ratio)
        second_call_data = table_calls[1][0][0]
        self.assertIn('robotframework', second_call_data[0][0])
        self.assertIn('cucumber5', second_call_data[1][0])
        third_call_data = table_calls[2][0][0]
        self.assertEqual(('robotframework', 1, 0, 1), third_call_data[0])
        self.assertEqual(('linux', 1, 1, 4), third_call_data[1])
        self.assertEqual(4, len(table_calls[3][0][0]))
        self.assertEqual(5, mock_doughnut.call_count)
        doughnut_calls = mock_doughnut.call_args_list
        self.assertEqual([1, 1, 1, 1], list(doughnut_calls[0][0][2].values()))
        self.assertEqual([1, 0, 0, 0], list(doughnut_calls[1][0][2].values()))
        self.assertEqual([0, 0, 1, 1], list(doughnut_calls[2][0][2].values()))
        self.assertEqual([0, 1, 0, 0], list(doughnut_calls[3][0][2].values()))
        self.assertEqual([1, 1, 2], list(doughnut_calls[4][0][2].values()))
        self.assertEqual(4, mock_block.call_count)
        block_calls = mock_block.call_args_list
        self.assertEqual(
            ['Summary', 'Jobs', 'Tags', 'Testcases'],
            [call[0][0] for call in block_calls],
        )
        self.assertEqual(3, mock_element.call_count)
        element_calls = mock_element.call_args_list
        self.assertEqual(['ok', 'nokok', 'nok'], [call[0][1] for call in element_calls])
        self.assertIn('SOME_VAR: VAR_SOME', element_calls[0][0][0])

    def test_prepare_html_report_event_ko_css_exception(self):
        mock_debug = MagicMock()
        mock_event = MagicMock()
        mock_file_open = MagicMock(side_effect=Exception('Boom!Boom!'))
        datasources = {
            'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED},
            'jobs': {'(true) && (true)': JOBS_NOT_MANAGED},
            'tags': {'(true) && (true)': TAGS_NOT_MANAGED},
        }
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = WF_EVENT
        with (
            patch(
                'opentf.core.datasources._has_testresult',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_workflowresult_event',
                mock_event,
            ),
            patch('opentf.plugins.insightcollector.htmlreport._make_summary_block'),
            patch('opentf.plugins.insightcollector.htmlreport._make_jobs_block'),
            patch('opentf.plugins.insightcollector.htmlreport._make_tags_block'),
            patch('opentf.plugins.insightcollector.htmlreport._make_testcases_block'),
            patch('opentf.plugins.insightcollector.htmlreport._generate_html_report'),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DOUGHNUT_COLORS',
                ['a', 'b', 'c', 'd'],
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.logging.debug', mock_debug
            ),
            patch('builtins.open', mock_file_open),
        ):
            prepare_html_report_event(
                mock_proxy,
                HTML_REPORT_MODEL,
                'wFID',
                None,
                {'workflow': {'creationTimestamp': '2023-01-01'}},
                ['style'],
                'true',
            )
        mock_event.assert_not_called()
        self.assertEqual(1, mock_file_open.call_count)
        self.assertEqual(2, mock_debug.call_count)
        self.assertEqual(
            ('Failed to import report resource file: %s.', 'Boom!Boom!'),
            mock_debug.call_args_list[1][0],
        )

    # _create_report_html

    def test_create_report_html_ko_scopes_errors(self):
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(
            side_effect=ScopeError('No test cases matching scope')
        )
        mock_get_tc = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.htmlreport.evaluate_bool',
                MagicMock(return_value=True),
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.get_sorted_testcases',
                mock_get_tc,
            ),
        ):
            response = _create_report_html(
                mock_proxy,
                SPEC_WITH_TEMPLATE_SCOPE,
                'wFiD',
                {'workflow': {'creationTimestamp': '2023-01-01'}},
                ['style'],
                'true',
            )
        mock_get_tc.assert_not_called()
        self.assertIn(
            '<li>Cannot generate Table "tAble". No test cases matching scope</li>',
            response,
        )

    def test_create_report_html_no_relevant_templates(self):
        spec = {'template': [{'name': 'A Table', 'kind': 'Table', 'if': 'never'}]}
        mock_debug = MagicMock()
        with (
            patch(
                'opentf.plugins.insightcollector.htmlreport.evaluate_bool',
                MagicMock(return_value=False),
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.logging.debug', mock_debug
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_table',
                MagicMock(return_value='<table>Table</table>'),
            ),
        ):
            result = _create_report_html(
                {'some': 'metadata'},
                spec,
                'irrelevant',
                {'workflow': {'workflow_id': 'wfffw'}},
                ['style'],
                'true',
            )
        mock_debug.assert_called_once()
        self.assertIn(
            'No relevant template items found for the report "irrelevant" (workflow wfffw).',
            result,
        )

    def test_create_report_items_skip_if_not_found(self):
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(return_value={})
        errors, _, body = _create_report_items(
            [{'kind': 'Table', 'skipIfNotFound': 'true', 'datasource': 'testcases'}],
            mock_proxy,
            'true',
        )
        self.assertEqual(set(), errors)
        self.assertEqual('', body)

    def test_create_report_items_empty_datasources(self):
        mock_proxy = MagicMock()
        mock_proxy.get_datasource = MagicMock(return_value={})
        mock_debug = MagicMock()
        with patch(
            'opentf.plugins.insightcollector.htmlreport.logging.debug', mock_debug
        ):
            _create_report_items(
                [{'kind': 'SummaryBlock', 'name': 'Block'}], mock_proxy, 'true'
            )
        mock_debug.assert_called_once_with(
            'SummaryBlock Block not published: no data retrieved.'
        )

    def test_create_report_items_ok_progressbar(self):
        templates = [
            {
                'name': 'progress',
                'kind': 'ProgressBar',
                'aggregateFunction': 'sum',
                'datasource': 'testcases',
                'groupBy': 'test.technology',
                'data': 'execution.duration',
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('cypress: 7 (2 technology items counted)', result)
        self.assertIn('cucumber5: 2 (1 technology items counted)', result)
        self.assertIn('robotframework: 1 (1 technology items counted)', result)
        self.assertEqual(2, result.count('20.0%'))
        self.assertEqual(2, result.count('10.0%'))
        self.assertEqual(2, result.count('70.0%'))

    def test_create_report_items_ok_progressbar_count(self):
        templates = [
            {
                'name': 'progress',
                'kind': 'ProgressBar',
                'aggregateFunction': 'count',
                'datasource': 'testcases',
                'groupBy': 'test.technology',
                'data': 'foo.bar',
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('COUNT()', result)
        self.assertIn('robotframework: 1', result)
        self.assertIn('cucumber5: 1', result)
        self.assertIn('cypress: 2', result)
        self.assertEqual(4, result.count('25.0%'))
        self.assertEqual(2, result.count('50.0%'))

    def test_create_report_items_ok_progressbar_min(self):
        templates = [
            {
                'name': 'progress',
                'kind': 'ProgressBar',
                'aggregateFunction': 'min',
                'datasource': 'testcases',
                'groupBy': 'test.technology',
                'data': 'execution.duration',
            }
        ]
        datasources = {
            'testcases': {'(true) && (true)': TESTCASE_METADATA_DURATION_ZERO_TWO}
        }
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('cucumber5: 2 (1 technology items counted)', result)
        self.assertEqual(2, result.count('100.0%'))

    def test_create_report_items_progressbar_ko_aggregates_zero(self):
        templates = [
            {
                'name': 'progress',
                'kind': 'ProgressBar',
                'aggregateFunction': 'min',
                'datasource': 'testcases',
                'groupBy': 'test.technology',
                'data': 'execution.duration',
            }
        ]
        datasources = {
            'testcases': {'(true) && (true)': TESTCASE_METADATA_EMPTY_AGGREGATE}
        }
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('Aggregated values sum is 0, cannot generate ratios.', result)

    def test_create_report_items_progressbar_ko_no_datapath(self):
        templates = [
            {
                'name': 'progress',
                'kind': 'ProgressBar',
                'aggregateFunction': 'avg',
                'datasource': 'testcases',
                'groupBy': 'test.technology',
            }
        ]
        datasources = {
            'testcases': {'(true) && (true)': TESTCASE_METADATA_DURATION_ZERO_TWO}
        }
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('No defined data path for aggregate function avg', result)

    def test_create_report_items_progressbar_ko_unsupported_datatype(self):
        templates = [
            {
                'name': 'progress',
                'kind': 'ProgressBar',
                'aggregateFunction': 'avg',
                'datasource': 'testcases',
                'groupBy': 'test.technology',
                'data': 'execution.duration',
            }
        ]
        datasources = {
            'testcases': {'(true) && (true)': TESTCASE_METADATA_DURATION_ZERO_TWO}
        }
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        with patch(
            'opentf.plugins.insightcollector.htmlreport._get_path',
            MagicMock(return_value='a'),
        ):
            result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('Unsupported data type for aggregated data', result)

    def test_create_report_items_progressbar_ko_keyerror(self):
        templates = [
            {
                'name': 'progress',
                'kind': 'ProgressBar',
                'aggregateFunction': 'sum',
                'datasource': 'testcases',
                'groupBy': 'something.technology',
                'data': 'another.duration',
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn(
            'Invalid path: something.technology',
            result,
        )

    def test_create_report_items_ok_barchart(self):
        templates = [
            {
                'name': 'bar',
                'kind': 'BarChart',
                'datasource': 'testcases',
                'orderBy': {'key': 'execution.duration', 'order': 'ascending'},
                'maxItems': 3,
                'primaryAxis': {
                    'title': 'CustomPrimaryTitle',
                    'reversed': 'true',
                    'data': 'test.testCaseName',
                },
                'secondaryAxis': {'data': 'execution.duration'},
                'legend': 'CustomChartLegend',
                'orientation': 'vertical',
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('CustomChartLegend', result)
        self.assertIn('CustomPrimaryTitle', result)
        self.assertIn("indexAxis: 'y'", result)
        self.assertIn('data: [3, 2, 1]', result)
        self.assertIn(
            "labels: ['CalculatorMult multFailure', 'Current stock duplicate', 'Test  single']",
            result,
        )

    def test_create_report_items_barchart_ko_zero_labels_values(self):
        templates = [
            {
                'name': 'bar',
                'kind': 'BarChart',
                'datasource': 'testcases',
                'maxItems': 0,
                'primaryAxis': {'data': 'test.testCaseName'},
                'secondaryAxis': {'data': 'execution.duration'},
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('No bar chart data retrieved (maxItems=0)', result)

    def test_create_report_items_barchart_ko_keyerror(self):
        templates = [
            {
                'name': 'bar',
                'kind': 'BarChart',
                'datasource': 'testcases',
                'primaryAxis': {'data': 'somepath.testCaseName'},
                'secondaryAxis': {'data': 'execution.duration'},
                'orderBy': {'key': 'somepath.testCaseName'},
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn("Invalid path: somepath.testCaseName", result)

    def test_create_report_items_ok_scatterchart(self):
        templates = [
            {
                'name': 'scatter',
                'kind': 'ScatterChart',
                'datasource': 'testcases',
                'groupBy': 'test.outcome',
                'primaryAxis': {
                    'data': 'execution.duration',
                    'title': 'CustomPrimaryTitle',
                    'ticks': 'hidden',
                    'min': 1,
                    'max': 32,
                },
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn("var yTicks = ['success', 'failure', 'error', 'skipped']", result)
        self.assertEqual(4, result.count("data: [{'x':"))
        self.assertIn("CustomPrimaryTitle", result)
        self.assertIn('min: 1, max: 32', result)

    def test_create_report_items_scatterchart_ko_keyerror(self):
        templates = [
            {
                'name': 'scatter',
                'kind': 'ScatterChart',
                'datasource': 'testcases',
                'groupBy': 'something.like.outcome',
                'primaryAxis': {
                    'data': 'execution.duration',
                },
            }
        ]
        datasources = {'testcases': {'(true) && (true)': TESTCASE_METADATA_NOT_MANAGED}}
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = datasources
        mock_proxy.workflow_event = EVENTS
        result = _create_report_items(templates, mock_proxy, 'true')[2]
        self.assertIn('Invalid path: something.like.outcome', result)

    # _get_css

    def test_get_css_ok(self):
        colors = {}
        css_list = [
            '--success: a;\n--failure: b;',
            '--error: c;\n--skipped: d;',
            '--error: e;',
            '--failure: f;\n--foobar: g',
        ]
        with patch('opentf.plugins.insightcollector.htmlreport.CSS_COLORS', colors):
            res = _get_css(css_list, {'style': '--success: h;'})
        self.assertEqual(
            {'success': 'h', 'failure': 'f', 'error': 'e', 'skipped': 'd'}, colors
        )
        self.assertEqual('\n\n'.join(css_list) + '\n\n--success: h;', res)

    # _get_datasources

    def test_get_datasources_no_test_matching_scope(self):
        warnings = set()
        mock_proxy = ObserverProxy('yada', 'yada', 'yada', {})
        mock_proxy.datasources = {
            'tags': {'(true) && (true)': {'linux': {'status': {'testCaseCount': 0}}}}
        }
        _get_datasources(
            {
                'kind': 'ScatterChart',
                'datasource': 'tags',
                'name': 'foo',
                'scope': 'true',
            },
            'true',
            mock_proxy,
            set(),
            warnings,
        )
        self.assertEqual(
            'No test cases matching scope `(true) && (true)` for ScatterChart "foo", generated item may contain no data.',
            warnings.pop(),
        )

    # _generate_testcases_table

    def test_generate_testcases_table_ok_not_managed(self):
        mock_table = MagicMock()
        mock_action = MagicMock()
        mock_dialog = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.htmlreport.make_table', mock_table),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_action', mock_action
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_dialog', mock_dialog
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DETAILS_DIALOG_COUNTER', 1
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DATASET_DIALOG_COUNTER', 1
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DOUGHNUT_COLORS',
                ['a', 'b', 'c', 'd'],
            ),
        ):
            _generate_testcases_table(
                TESTCASE_METADATA_NOT_MANAGED, DEFAULT_COLUMNS_TESTCASES, 'testcases'
            )
            mock_table.assert_called_once()
            rows, cols = mock_table.call_args[0][:2]
            details = cols[1][3](rows[0][1])
            dataset = cols[2][3](rows[0][2])
            status = cols[3][3](rows[0][3])
        mock_action.assert_called_once_with(
            '<span class="testCaseName">Test  single</span>', 'execution_details_1'
        )
        mock_dialog.assert_called_once()
        self.assertEqual('execution_details_1', mock_dialog.call_args[0][0])
        self.assertEqual('', dataset)
        self.assertIn('span class="ok outcome"', status)

    def test_generate_testcases_table_ok_managed(self):
        mock_table = MagicMock()
        mock_action = MagicMock()
        mock_dialog = MagicMock()
        with (
            patch('opentf.plugins.insightcollector.htmlreport.make_table', mock_table),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_action', mock_action
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.make_dialog', mock_dialog
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DETAILS_DIALOG_COUNTER', 1
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DATASET_DIALOG_COUNTER', 1
            ),
            patch(
                'opentf.plugins.insightcollector.htmlreport.DOUGHNUT_COLORS',
                ['a', 'b', 'c', 'd'],
            ),
        ):
            _generate_testcases_table(
                TESTCASE_METADATA_MANAGED, DEFAULT_COLUMNS_TESTCASES, 'testcases'
            )
            mock_table.assert_called_once()
            rows, cols = mock_table.call_args[0][:2]
            details = cols[1][3](rows[0][1])
            dataset = cols[2][3](rows[0][2])
            status = cols[3][3](rows[0][3])
            self.assertEqual(2, mock_action.call_count)
            self.assertEqual('execution_details_1', mock_action.call_args_list[0][0][1])
            self.assertEqual(
                ('<span class="data">Dataset 1</span>', 'dataset_details_1'),
                mock_action.call_args_list[1][0],
            )
            self.assertEqual(2, mock_dialog.call_count)
            self.assertIn('TC_CUF_DATE', mock_dialog.call_args_list[0][0][2])
            self.assertEqual('dataset_details_1', mock_dialog.call_args_list[1][0][0])
            self.assertIn('DS_foo1', mock_dialog.call_args_list[1][0][2])
            self.assertIsNotNone(dataset)
            self.assertIn('span class="nok outcome"', status)

    def test_generate_testcases_table_ok_with_failure_details_and_errorslist(self):
        _maybe_complete_with_errorslist(TESTCASES_WITH_FAILUREDETAILS_AND_ERRORSLIST)
        table = _generate_testcases_table(
            TESTCASES_WITH_FAILUREDETAILS_AND_ERRORSLIST,
            DEFAULT_COLUMNS_TESTCASES,
            'testcases',
        )
        self.assertIn('Small failure', table[1])
        self.assertIn('type:', table[1])
        self.assertIn('AssertionError', table[1])
        self.assertIn('today: Robot Framework General Error', table[1])

    # _generate_rows

    def test_generate_rows_ok(self):
        with patch(
            'opentf.plugins.insightcollector.htmlreport._get_path',
            MagicMock(return_value='Returned content.'),
        ):
            response = _generate_rows(
                {'column1': 'some.path'}, {'dataset1': 'some.data'}, 'dataSource'
            )
        self.assertEqual([('Returned content.',)], response)

    def test_generate_rows_ko_empty_path(self):
        response = _generate_rows({'column1': ''}, {'dataset1': 'some.data'}, 'sOurce')
        self.assertEqual([('Invalid path: empty path.',)], response)

    def test_generate_rows_return_invalid_path(self):
        with patch(
            'opentf.plugins.insightcollector.htmlreport._get_path',
            MagicMock(side_effect=KeyError('Wroom.')),
        ):
            response = _generate_rows(
                {'column1': 'some.path'}, {'dataset1': 'some.data'}, 'dataSource'
            )
        self.assertEqual([("'Wroom.'",)], response)

    def test_generate_rows_return_itpi_path(self):
        with patch(
            'opentf.plugins.insightcollector.htmlreport._get_path',
            MagicMock(return_value=['a', 'b']),
        ):
            response = _generate_rows(
                {'column1': 'some.path'}, {'dataset1': 'some.data'}, 'dataSource'
            )
        self.assertEqual([('a > b',)], response)

    def test_generate_rows_handle_list_content(self):
        with patch(
            'opentf.plugins.insightcollector.htmlreport._get_path',
            MagicMock(return_value=['linux', 'windows']),
        ):
            response = _generate_rows(
                {'column1': 'some.some'}, {'dataset1': 'some.data'}, 'dataSource'
            )
        self.assertEqual([('linux, windows',)], response)

    def test_generate_rows_return_path_not_supported(self):
        with patch(
            'opentf.plugins.insightcollector.htmlreport._get_path',
            MagicMock(return_value={'a': 'b'}),
        ):
            response = _generate_rows(
                {'column1': 'test'}, {'dataset1': 'some.data'}, 'dataSource'
            )
        self.assertEqual([('Path test not supported.',)], response)

    def test_generate_rows_get_path_keyerror(self):
        response = _generate_rows(
            {'column1': 'wroom.wroom.wroom'},
            {'dataset1': {'some': 'data'}},
            'dataSource',
        )
        self.assertEqual([("'Invalid path: wroom.wroom.wroom'",)], response)

    # Charts

    def test_maybe_adjust_progressbar_segments_ok(self):
        result = _maybe_adjust_progressbar_segments({'a': 50, 'b': 40, 'c': 30}, 100)
        self.assertEqual(100.0, sum(result.values()))

    # Varia

    def test_generate_jobs_cloud_parent_job_ok(self):
        cloud = _generate_jobs_cloud(JOBS_INCEPTION)
        self.assertEqual(2, len(cloud))
        self.assertIn('class="empty"', cloud[0])
        self.assertIn('class="nokok"', cloud[1])

    def test_generate_report_doughnuts_parent_job_ok(self):
        doughnuts = _generate_report_doughnuts(
            {'testcases': TESTCASE_METADATA_MANAGED, 'jobs': JOBS_INCEPTION}
        )
        self.assertEqual(1, doughnuts.count('<span class="empty-doughnut title"'))
        self.assertEqual(1, doughnuts.count('<span class="empty-doughnut content"'))
        self.assertIn('Summary for job prepare', doughnuts)
        self.assertIn('No test cases executed for this job.', doughnuts)
