# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""JUnit Provider unit tests.

- execute tests
- mvntest tests
"""

import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.junit import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    ('junit', 'execute', None): ({'test': {'required': True}}, False),
    ('junit', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
}


########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestJUnit(unittest.TestCase):
    """
    The class containing the JUnit provider unit test methods.
    """

    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        app.config['CONTEXT'][opentf.toolkit.OUTPUTS_KEY] = {}
        self.app = app.test_client()
        self.assertFalse(app.debug)

    ###########################
    # execute

    def test_execute_nok(self):
        """
        In a JUnit context, checks the error message content when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'junit/execute'}
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn("'test'", msg)

    def test_execute_ok(self):
        """
        In a JUnit context, checks the presence of 8 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/execute',
                'with': {'test': 'JUnitProject/src/test/java/features/sample.feature'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)

    def test_execute_extra_options_win(self):
        """
        In a JUnit/Windows context, checks the presence of the JUNIT_EXTRA_OPTIONS
        environment variable in the test execution command.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/execute',
                'with': {'test': 'JUnitProject/SampleTest01'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertEqual(
            {
                'run': 'mvn test -f "JUnitProject/pom.xml" -Dtest=SampleTest01 -Dmaven.test.failure.ignore=true -DfailIfNoTests=true --log-file JUnitProject/target/junit-run-log.txt %JUNIT_EXTRA_OPTIONS%',
                'continue-on-error': True,
            },
            steps[2],
        )

    def test_execute_extra_options_lin(self):
        """
        In a JUnit/Linux context, checks the presence of the JUNIT_EXTRA_OPTIONS
        environment variable in the test execution command.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/execute',
                'with': {'test': 'JUnitProject/SampleTest02'},
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            command['runs-on'] = ['linux']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertEqual(
            {
                'run': 'mvn test -f "JUnitProject/pom.xml" -Dtest=SampleTest02 -Dmaven.test.failure.ignore=true -DfailIfNoTests=true --log-file JUnitProject/target/junit-run-log.txt $JUNIT_EXTRA_OPTIONS',
                'continue-on-error': True,
            },
            steps[2],
        )

    def test_junit_nok(self):
        """
        In a JUnit context, checks the error message content when native
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'junit/mvntest'}
            _dispatch(implementation.mvntest_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn("'test'", msg)

    def test_junit_properties(self):
        """
        In a JUnit context, checks that providing properties is fine.
        """
        mock = MagicMock(return_value=mockresponse)
        properties = {'prop1': 'AaXx', 'prop2': 'C:\\'}
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/mvntest',
                'with': {
                    'test': 'JUnitProject/src/test/java/features/sample.feature',
                    'properties': properties,
                },
            }
            _dispatch(implementation.mvntest_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn("-Dprop1=AaXx -Dprop2=C:\\", steps[2]['run'])

    def test_junit_extra_options(self):
        """
        In a JUnit context, checks that providing extra options is fine.
        """
        mock = MagicMock(return_value=mockresponse)
        extra_options = '--settings YourSettings.xml'
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/mvntest',
                'with': {
                    'test': 'JUnitProject/src/test/java/features/sample.feature',
                    'extra-options': extra_options,
                },
            }
            _dispatch(implementation.mvntest_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn(extra_options, steps[2]['run'])

    def test_param_linux_ok(self):
        """
        Checks the runner command with params when param action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.junit.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        step_0_uses = steps[0]['uses']
        step_0_with = steps[0]['with']
        self.assertIn('actions/create-file@v1', step_0_uses)
        self.assertIn('strange_value', step_0_with['data']['global']['firstGlobal'])
        self.assertIn('actions/create-file@v1', steps[1]['uses'])
        self.assertIn('value1', steps[1]['with']['data']['test']['key1'])
        self.assertTrue(
            steps[2]['run'].startswith('echo export _SQUASH_TF_TESTCASE_PARAM_FILES=')
        )
        self.assertIn(
            'echo export _SQUASH_TF_TESTCASE_PARAM_FILES_ABSOLUTE=$OPENTF_WORKSPACE/',
            steps[2]['run'],
        )

    def test_param_windows_ok(self):
        """
        Checks the runner command with params when param action is called
        on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.junit.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 3)
        self.assertIn('actions/create-file@v1', steps[0]['uses'])
        self.assertIn(
            'strange_value', steps[0]['with']['data']['global']['firstGlobal']
        )
        self.assertIn('actions/create-file@v1', steps[1]['uses'])
        self.assertIn('value1', steps[1]['with']['data']['test']['key1'])
        self.assertTrue(
            steps[2]['run'].startswith('@echo set "_SQUASH_TF_TESTCASE_PARAM_FILES=')
        )
        self.assertIn(
            '@echo set "_SQUASH_TF_TESTCASE_PARAM_FILES_ABSOLUTE=%OPENTF_WORKSPACE%\\',
            steps[2]['run'],
        )

    def test_param_invalid_format_nok(self):
        """
        Checks the runner command with an invalid format param
        when param action is called.
        """
        with patch(
            'opentf.plugins.junit.implementation.core.validate_params_inputs',
            MagicMock(side_effect=SystemExit(2)),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'junit/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'invalid_format/params@v1',
                },
            }
            self.assertRaisesRegex(
                SystemExit, '2', _dispatch, implementation.param_action, command
            )


if __name__ == '__main__':
    unittest.main()
