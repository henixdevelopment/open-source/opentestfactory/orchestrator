# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Provider aggregator unit tests."""

import os
import logging
import unittest
import sys

from unittest.mock import MagicMock, patch, mock_open

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.allinone import main

# import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    ('cypress', 'execute', None): ({'test': {'required': True}}, False),
    ('cypress', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
}

########################################################################


class TestProviderAggregator(unittest.TestCase):
    """
    The class containing the ProviderAggregator unit test methods.
    """

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_merge_empty_empty(self):
        previous = None
        new = []
        provider = 'foobar'
        what = main._merge(previous, new, provider)
        self.assertFalse(what)

    def test_merge_preserve_ignoring(self):
        previous = [{'events': [{'categoryPrefix': 'previous'}]}]
        new = [{'events': [{'categoryPrefix': 'bar'}]}]
        provider = 'foobar'
        with patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': []}):
            what = main._merge(previous, new, provider)
        self.assertEqual(len(what), 1)
        self.assertEqual(what[0]['events'], [{'categoryPrefix': 'previous'}])

    def test_merge_preserve_notempty(self):
        previous = [{'events': [{'categoryPrefix': 'previous'}]}]
        new = [{'events': [{'categoryPrefix': 'foobar'}]}]
        provider = 'foobar'
        with patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': ['foobar']}):
            what = main._merge(previous, new, provider)
        self.assertEqual(len(what), 2)

    def test_merge_preserve_duplicates(self):
        previous = [{'events': [{'categoryPrefix': 'previous'}]}]
        new = [{'events': [{'categoryPrefix': '_'}]}]
        provider = 'foobar'
        with patch(
            'opentf.plugins.allinone.main.CATEGORIES',
            {'foobar': ['foobar', 'foobarbaz']},
        ):
            what = main._merge(previous, new, provider)
        self.assertEqual(len(what), 2)

    def test_merge_empty_ignoring(self):
        previous = None
        new = [{'events': [{'categoryPrefix': 'bar'}]}]
        provider = 'foobar'
        with patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': []}):
            what = main._merge(previous, new, provider)
        self.assertFalse(what)

    def test_merge_empty_notempty(self):
        previous = None
        new = [{'events': [{'categoryPrefix': 'foobar'}]}]
        provider = 'foobar'
        with patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': ['foobar']}):
            what = main._merge(previous, new, provider)
        self.assertEqual(len(what), 1)
        self.assertEqual(len(what[0]['events']), 1)

    def test_merge_empty_duplicates(self):
        previous = None
        new = [{'events': [{'categoryPrefix': '_'}]}]
        provider = 'foobar'
        with patch(
            'opentf.plugins.allinone.main.CATEGORIES',
            {'foobar': ['foobar', 'foobarbaz']},
        ):
            what = main._merge(previous, new, provider)
        self.assertEqual(len(what), 1)
        self.assertEqual(len(what[0]['events']), 2)

    def test_merge_replace_notempty(self):
        previous = [{'events': [{'categoryPrefix': 'foobar'}]}]
        new = [{'events': [{'categoryPrefix': 'foobar'}]}]
        provider = 'foobar'
        with patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': ['foobar']}):
            what = main._merge(previous, new, provider)
        self.assertEqual(len(what), 1)
        self.assertEqual(len(what[0]['events']), 1)

    def test_merge_replace_duplicates(self):
        previous = [{'events': [{'categoryPrefix': 'foobar'}]}]
        new = [{'events': [{'categoryPrefix': '_'}]}]
        provider = 'foobar'
        with patch(
            'opentf.plugins.allinone.main.CATEGORIES',
            {'foobar': ['foobar', 'foobarbaz']},
        ):
            what = main._merge(previous, new, provider)
        self.assertEqual(len(what), 1)
        self.assertEqual(len(what[0]['events']), 2)

    # _make_invalid_definition_hooks

    def test_makeinvaliddef(self):
        what = main._make_invalid_definition_hook('foobar')
        self.assertEqual(len(what[0]['before']), 2)
        self.assertIn('foobar'.upper(), what[0]['before'][0]['run'])

    # _read_hooks

    def test_read_hooks_exception(self):
        svc = MagicMock()
        svc.config = {'CONFIG': {}}
        with patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': ['foobar']}):
            main._read_hooks(svc, 'foobar.baz', 'foobar')
        self.assertIn('hooks', svc.config['CONFIG'])
        self.assertTrue(svc.config['CONFIG']['hooks'])

    def test_read_hooks_previous(self):
        svc = MagicMock()
        svc.config = {
            'CONFIG': {'hooks': [{'events': [], 'before': [{'run': 'foobar'}]}]}
        }
        rf = mock_open(read_data='{"hooks": []}')
        with (
            patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': ['foobar']}),
            patch('builtins.open', rf),
        ):
            main._read_hooks(svc, 'foobar.baz', 'foobar')
        self.assertIn('hooks', svc.config['CONFIG'])
        self.assertFalse(svc.config['CONFIG']['hooks'])

    def test_read_hooks_empty(self):
        svc = MagicMock()
        svc.config = {'CONFIG': {}}
        rf = mock_open(read_data='{"hooks": []}')
        with (
            patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': ['foobar']}),
            patch('builtins.open', rf),
        ):
            main._read_hooks(svc, 'foobar.baz', 'foobar')
        self.assertIn('hooks', svc.config['CONFIG'])
        self.assertFalse(svc.config['CONFIG']['hooks'])

    def test_read_hooks_invalid(self):
        svc = MagicMock()
        svc.config = {'CONFIG': {}}
        rf = mock_open(read_data='{"not-hooks": [{"before": [{"run": "foobar"}]}]}')
        with (
            patch('opentf.plugins.allinone.main.CATEGORIES', {'foobar': ['foobar']}),
            patch('builtins.open', rf),
        ):
            main._read_hooks(svc, 'foobar.baz', 'foobar')
        self.assertIn('hooks', svc.config['CONFIG'])
        self.assertTrue(svc.config['CONFIG']['hooks'])
        hooks = svc.config['CONFIG']['hooks']
        self.assertEqual(len(hooks), 1)
        self.assertEqual(hooks[0].get('name'), 'invalid-external-hooks-definition')

    # _read_descriptors

    def test_read_descriptors_all_disabled(self):
        aggregated_plugins = {'foo', 'bar', 'baz'}
        disabled = aggregated_plugins
        svc = MagicMock()
        svc.config = {}
        with patch(
            'opentf.plugins.allinone.main.AGGREGATED_PLUGINS', aggregated_plugins
        ):
            main._read_descriptors(svc, disabled)
        self.assertIn('DESCRIPTOR', svc.config)
        self.assertEqual(len(svc.config['DESCRIPTOR']), 0)

    def test_read_descriptors_some_aggregated_no_disabled(self):
        aggregated_plugins = {'foo', 'bar', 'baz'}
        descriptor = {'metadata': {'name': 'aName'}, 'events': []}
        disabled = {'something'}
        mock_module = MagicMock()
        mock_module.KNOWN_CATEGORIES = {}
        mock_module.__file__ = 'foo'
        mock_readdescriptor = MagicMock(return_value=(None, [descriptor]))
        svc = MagicMock()
        svc.config = {}
        with (
            patch('opentf.plugins.allinone.main.read_descriptor', mock_readdescriptor),
            patch(
                'opentf.plugins.allinone.main.AGGREGATED_PLUGINS', aggregated_plugins
            ),
            patch('importlib.import_module', MagicMock(return_value=mock_module)),
        ):
            main._read_descriptors(svc, disabled)
        self.assertIn('DESCRIPTOR', svc.config)
        self.assertEqual(len(svc.config['DESCRIPTOR']), 3)

    def test_read_descriptors_some_aggregated_some_disabled(self):
        aggregated_plugins = {'foo', 'bar', 'baz'}
        descriptor = {'metadata': {'name': 'aName'}, 'events': []}
        disabled = {'something', 'bar'}
        mock_module = MagicMock()
        mock_module.KNOWN_CATEGORIES = {}
        mock_module.__file__ = 'foo'
        mock_readdescriptor = MagicMock(return_value=(None, [descriptor]))
        svc = MagicMock()
        svc.config = {}
        with (
            patch('opentf.plugins.allinone.main.read_descriptor', mock_readdescriptor),
            patch(
                'opentf.plugins.allinone.main.AGGREGATED_PLUGINS', aggregated_plugins
            ),
            patch('importlib.import_module', MagicMock(return_value=mock_module)),
        ):
            main._read_descriptors(svc, disabled)
        self.assertIn('DESCRIPTOR', svc.config)
        self.assertEqual(len(svc.config['DESCRIPTOR']), 2)

    # _initialize_aggregated

    def test_initialize_aggregated_nothing(self):
        svc = MagicMock()
        svc.config = {'CONFIG': {}}
        main._initialize_aggregated(svc, {})
        self.assertIn('CONFIG', svc.config)
        self.assertIn('hooks', svc.config['CONFIG'])
        self.assertFalse(svc.config['CONFIG']['hooks'])


if __name__ == '__main__':
    unittest.main()
