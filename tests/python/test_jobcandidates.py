# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
from opentf.core import jobcandidates

########################################################################

JOBS_DIAMOND = {
    "job_a": {
        "name": "Job A",
        "needs": {"job_b", "job_c"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job A needing jobs B and C'",
                "id": "fa1dfc00-16b3-4c4a-9b14-95e716cba77c",
            }
        ],
        "_id": "job_a",
        "metadata": {"job_origin": []},
    },
    "job_b": {
        "name": "Job B",
        "needs": {"job_d"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job B needing job D'",
                "id": "a7028e1b-e7d0-4311-b155-052fc1890555",
            }
        ],
        "_id": "job_b",
        "metadata": {"job_origin": []},
    },
    "job_c": {
        "name": "Job C",
        "needs": {"job_d"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job C needing job D'",
                "id": "cd05ef2e-183c-4c7d-9b0b-da60cc6ab0b5",
            }
        ],
        "_id": "job_c",
        "metadata": {"job_origin": []},
    },
    "job_d": {
        "name": "Job D",
        "runs-on": ["linux"],
        "needs": set(),
        "steps": [
            {
                "run": "echo 'This is job D, absolutely free.'",
                "id": "0845c927-48ab-401d-94e8-ff8b3f673887",
            }
        ],
        "_id": "job_d",
        "metadata": {"job_origin": []},
    },
}

JOBS_DIAMONDS = {
    "job_a": {
        "name": "Job A",
        "needs": {"job_b", "job_c"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job A needing jobs B and C'",
                "id": "fa1dfc00-16b3-4c4a-9b14-95e716cba77c",
            }
        ],
        "_id": "job_a",
        "metadata": {"job_origin": []},
    },
    "job_b": {
        "name": "Job B",
        "needs": {"job_d"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job B needing job D'",
                "id": "a7028e1b-e7d0-4311-b155-052fc1890555",
            }
        ],
        "_id": "job_b",
        "metadata": {"job_origin": []},
    },
    "job_c": {
        "name": "Job C",
        "needs": {"job_d"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job C needing job D'",
                "id": "cd05ef2e-183c-4c7d-9b0b-da60cc6ab0b5",
            }
        ],
        "_id": "job_c",
        "metadata": {"job_origin": []},
    },
    "job_d": {
        "name": "Job D",
        "runs-on": ["linux"],
        "needs": set(),
        "steps": [
            {
                "run": "echo 'This is job D, absolutely free.'",
                "id": "0845c927-48ab-401d-94e8-ff8b3f673887",
            }
        ],
        "_id": "job_d",
        "metadata": {"job_origin": []},
    },
    "job_ae": {
        "name": "Job AE",
        "runs-on": ["linux"],
        "needs": set(),
        "steps": [
            {
                "run": "echo 'This is job D, absolutely free.'",
                "id": "0845c927-48ab-401d-94e8-ff8b3f673887",
            }
        ],
        "_id": "job_ae",
        "metadata": {"job_origin": []},
    },
}

JOBS_LINEAR = {
    "job_a": {
        "name": "Job A",
        "needs": {"job_b"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job A needing job B.",
                "id": "fa1dfc00-16b3-4c4a-9b14-95e716cba77c",
            }
        ],
        "_id": "job_a",
        "metadata": {"job_origin": []},
    },
    "job_b": {
        "name": "Job B",
        "runs-on": ["linux"],
        "needs": set(),
        "steps": [
            {
                "run": "echo 'This is independent job B.",
                "id": "a7028e1b-e7d0-4311-b155-052fc1890555",
            }
        ],
        "_id": "job_b",
        "metadata": {"job_origin": []},
    },
}

NEW_JOBS = {
    "job_a": {
        "name": "Job A",
        "needs": {"job_b job_b", "job_b job_c"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job A needing jobs B and C'",
                "id": "fa1dfc00-16b3-4c4a-9b14-95e716cba77c",
            }
        ],
        "_id": "job_a",
        "metadata": {"job_origin": []},
    },
    "job_b": {
        "name": "Job B",
        "needs": {"job_b job_d"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job B needing job D'",
                "id": "a7028e1b-e7d0-4311-b155-052fc1890555",
            }
        ],
        "_id": "job_b",
        "metadata": {"job_origin": []},
    },
    "job_c": {
        "name": "Job C",
        "needs": {"job_b job_d"},
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job C needing job D'",
                "id": "cd05ef2e-183c-4c7d-9b0b-da60cc6ab0b5",
            }
        ],
        "_id": "job_c",
        "metadata": {"job_origin": []},
    },
    "job_d": {
        "name": "Job D",
        "needs": set(),
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job D, absolutely free.'",
                "id": "0845c927-48ab-401d-94e8-ff8b3f673887",
            }
        ],
        "_id": "job_d",
        "metadata": {"job_origin": []},
    },
    "job_ae": {
        "name": "Job AE",
        "needs": set(),
        "runs-on": ["linux"],
        "steps": [
            {
                "run": "echo 'This is job D, absolutely free.'",
                "id": "0845c927-48ab-401d-94e8-ff8b3f673887",
            }
        ],
        "_id": "job_ae",
        "metadata": {"job_origin": []},
    },
}

########################################################################


class TestJobCandidates(unittest.TestCase):
    def setUp(self):
        for job in JOBS_DIAMOND.values():
            try:
                del job['status']
            except KeyError:
                pass
        for job in JOBS_DIAMONDS.values():
            try:
                del job['status']
            except KeyError:
                pass
        for job in JOBS_LINEAR.values():
            try:
                del job['status']
            except KeyError:
                pass

    def test_create_jobcandidates_badwf(self):
        self.assertRaises(ValueError, jobcandidates.JobCandidates, {})

    def test_create_jobcandidate_goodwf(self):
        self.assertIsInstance(
            jobcandidates.JobCandidates(JOBS_DIAMOND), jobcandidates.JobCandidates
        )

    # diamond

    def test_getnextcandidate_diamond(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMOND)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_d'})

    def test_getnextcandidate_diamond_twice(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMOND)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_d'})
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_d'})

    def test_getnextcandidate_diamondafterjobd_running(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMOND)
        jc.update_job_status('job_d', jobcandidates.CANDIDATE_RUNNING)
        what = jc.get_next_candidates()
        self.assertEqual(what, set())

    def test_getnextcandidate_diamondafterjobd_done(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMOND)
        jc.update_job_status('job_d', jobcandidates.CANDIDATE_DONE)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_b', 'job_c'})

    # diamonds

    def test_getnextcandidate_diamonds_twice(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMONDS)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_d', 'job_ae'})
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_d', 'job_ae'})

    def test_getnextcandidate_diamondsafterjobd_running(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMONDS)
        jc.update_job_status('job_d', jobcandidates.CANDIDATE_RUNNING)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_ae'})

    def test_getnextcandidate_diamondsafterjobd_done(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMONDS)
        jc.update_job_status('job_d', jobcandidates.CANDIDATE_DONE)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_b', 'job_c', 'job_ae'})

    # linear

    def test_getnextcandidate_linear_exhaust(self):
        jc = jobcandidates.JobCandidates(JOBS_LINEAR)
        jc.update_job_status('job_b', jobcandidates.CANDIDATE_DONE)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_a'})
        jc.update_job_status('job_a', jobcandidates.CANDIDATE_DONE)
        what = jc.get_next_candidates()
        self.assertIsNone(what)

    # generators

    def test_nested_jobs(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMONDS)
        self.assertEqual(jc.get_next_candidates(), {'job_d', 'job_ae'})
        jc.update_job_status('job_d', jobcandidates.CANDIDATE_DONE)
        self.assertEqual(jc.get_next_candidates(), {'job_b', 'job_c', 'job_ae'})
        jc.expand_job('job_b', NEW_JOBS)
        what = jc.get_next_candidates()
        self.assertEqual(what, {'job_c', 'job_ae', 'job_b job_d', 'job_b job_ae'})
        jc.update_job_status('job_c', jobcandidates.CANDIDATE_DONE)
        self.assertNotIn('job_a', jc.get_next_candidates())

    def test_nested_jobs_oh_no(self):
        jc = jobcandidates.JobCandidates(JOBS_DIAMOND)
        self.assertEqual(jc.get_next_candidates(), {'job_d'})
        jc.update_job_status('job_d', jobcandidates.CANDIDATE_DONE)
        jc.update_job_status('job_c', jobcandidates.CANDIDATE_DONE)
        self.assertEqual(jc.get_next_candidates(), {'job_b'})
        jc.expand_job('job_b', JOBS_LINEAR)
        what = jc.get_next_candidates()
        jc.update_job_status('job_b job_b', jobcandidates.CANDIDATE_DONE)
        self.assertNotIn('job_a', what)
