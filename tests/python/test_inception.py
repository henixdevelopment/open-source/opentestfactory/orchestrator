# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Inception unit tests."""

import logging
import os
import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response


########################################################################

EVENT_COMPLETED = {
    'kind': 'WorkflowCompleted',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

EVENT_CANCELED = {
    'kind': 'WorkflowCanceled',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

EVENT_OTHER = {
    'kind': 'ExecutionCommand',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'job_id': 'jOb_iD',
        'step_sequence_id': 123,
        'channel_id': 'cHannel_id',
    },
}

EVENT_OTHER_INVALID = {
    'kind': 'ExecutionCommand',
    'apiVersion': 'opentestfactory.org/v1',
}

EVENT_OTHER_UNKNOWNCHANNELID = {
    'kind': 'ExecutionCommand',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'job_id': 'jOb_iD',
        'step_sequence_id': 123,
        'channel_id': 'another_cHannel_id',
    },
}

EVENT_CHANNELREQUEST = {
    'kind': 'ExecutionCommand',
    'apiVersion': 'opentestfactory.org/v1',
    'metadata': {
        'workflow_id': 'wOrKflow_iD',
        'job_id': 'jOb_iD',
        'step_sequence_id': -1,
        'channel_id': 'cHannel_id',
    },
}

########################################################################

mockresponse = Response()
mockresponse.status_code = 200

mock_validate = MagicMock(return_value=(True, None))

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.inceptionee import main


class TestInception(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def test_main_mock(self):
        mock_fatal = MagicMock()
        mock_threading = MagicMock()
        with (
            patch('opentf.plugins.inceptionee.main.subscribe', MagicMock()),
            patch('opentf.plugins.inceptionee.main.unsubscribe', MagicMock()),
            patch('opentf.plugins.inceptionee.main.run_app', MagicMock()),
        ):
            main.main()
        mock_fatal.assert_not_called()
        mock_threading.assert_not_called()

    def test_make_offer_windows(self):
        job_id = 'jOb_Id'
        workflow_id = 'wOrKflow_iD'
        body = {
            'runs-on': ['windows', 'inception'],
            'metadata': {'workflow_id': workflow_id},
        }
        mock_channels = {}
        mock_cache = {}
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr,
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.CHANNELS', mock_channels),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
        ):
            _ = main._maybe_make_offer(body, body['metadata'], job_id)
        self.assertIn(workflow_id, mock_cache)
        self.assertIn('channel_id', body['metadata'])
        self.assertIn('channel_tags', body['metadata'])
        self.assertIn('channel_os', body['metadata'])
        self.assertEqual(body['metadata']['channel_os'], 'windows')
        self.assertIn('channel_temp', body['metadata'])
        self.assertIn('channel_lease', body['metadata'])
        self.assertIn(body['metadata']['channel_id'], mock_channels)
        mock_msr.assert_called_once()
        mock_publish.assert_called_once()

    def test_make_offer_linux(self):
        job_id = 'jOb_Id'
        workflow_id = 'wOrKflow_iD'
        body = {
            'runs-on': ['linux', 'inception'],
            'metadata': {'workflow_id': workflow_id},
        }
        mock_channels = {}
        mock_cache = {}
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr,
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.CHANNELS', mock_channels),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
        ):
            _ = main._maybe_make_offer(body, body['metadata'], job_id)
        self.assertIn(workflow_id, mock_cache)
        self.assertIn('channel_id', body['metadata'])
        self.assertIn('channel_tags', body['metadata'])
        self.assertIn('channel_os', body['metadata'])
        self.assertEqual(body['metadata']['channel_os'], 'linux')
        self.assertIn('channel_temp', body['metadata'])
        self.assertIn('channel_lease', body['metadata'])
        self.assertIn(body['metadata']['channel_id'], mock_channels)
        mock_msr.assert_called_once()
        mock_publish.assert_called_once()

    def test_make_offer_noos_defaults_to_linux(self):
        job_id = 'jOb_Id'
        workflow_id = 'wOrKflow_iD'
        body = {'runs-on': ['inception'], 'metadata': {'workflow_id': workflow_id}}
        mock_channels = {}
        mock_cache = {}
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr,
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.CHANNELS', mock_channels),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
        ):
            _ = main._maybe_make_offer(body, body['metadata'], job_id)
        self.assertIn(workflow_id, mock_cache)
        self.assertIn('channel_id', body['metadata'])
        self.assertIn('channel_tags', body['metadata'])
        self.assertIn('channel_os', body['metadata'])
        self.assertEqual(body['metadata']['channel_os'], 'linux')
        self.assertIn('channel_temp', body['metadata'])
        self.assertIn('channel_lease', body['metadata'])
        self.assertIn(body['metadata']['channel_id'], mock_channels)
        mock_msr.assert_called_once()
        mock_publish.assert_called_once()

    def test_make_offer_noinception_no_offer(self):
        job_id = 'jOb_Id'
        workflow_id = 'wOrKflow_iD'
        body = {'runs-on': ['windows'], 'metadata': {'workflow_id': workflow_id}}
        mock_channels = {}
        mock_cache = {}
        mock_publish = MagicMock()
        with (
            patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr,
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.CHANNELS', mock_channels),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
        ):
            _ = main._maybe_make_offer(body, body['metadata'], job_id)
        self.assertNotIn(workflow_id, mock_cache)
        self.assertFalse(mock_channels)
        mock_publish.assert_not_called()
        mock_msr.assert_called_once()

    # _process_command

    def test_process_command_simple_onefile(self):
        workflow_id = 'wOrKflow_iD'
        job_id = 'jOb_iD'
        body = {
            'metadata': {
                'workflow_id': workflow_id,
                'job_id': job_id,
                'step_sequence_id': 123,
            },
            'scripts': [
                f'::inception::{workflow_id}::name::target',
                'do something',
                'echo "::attach type=atype::a/b/c/d/name',
                'echo "done"',
            ],
            'runs-on': ['linux', 'inception'],
        }
        mock_publish = MagicMock()
        mock_msr = MagicMock()
        mock_cache = {workflow_id: {}}
        with (
            patch('opentf.plugins.inceptionee.main.app'),
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.make_status_response', mock_msr),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
            patch('opentf.plugins.inceptionee.main.make_uuid', return_value='x'),
            patch('shutil.copy') as mock_copy,
        ):
            main._process_command(body, body['metadata'], job_id)
        mock_publish.assert_called_once()
        mock_msr.assert_called_once()
        msg = mock_publish.call_args[0][0]
        self.assertTrue(msg['attachments'])
        self.assertTrue(msg['metadata']['attachments'])
        mock_copy.assert_called_once_with(src='target', dst=f'/tmp/{job_id}-x_123_name')

    def test_process_command_simple_onefile_nomatch(self):
        workflow_id = 'wOrKflow_iD'
        job_id = 'jOb_iD'
        body = {
            'metadata': {
                'workflow_id': workflow_id,
                'job_id': 'jOb_iD',
                'step_sequence_id': 123,
            },
            'scripts': [
                f'::inception::{workflow_id}::name::target',
                'do something',
                'echo "::attach type=atype::a/b/c/d/foo',
                'echo "done"',
            ],
            'runs-on': ['linux', 'inception'],
        }
        mock_publish = MagicMock()
        mock_msr = MagicMock()
        mock_cache = {workflow_id: {}}
        with (
            patch('opentf.plugins.inceptionee.main.app'),
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.make_status_response', mock_msr),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
            patch('shutil.copy') as mock_copy,
        ):
            main._process_command(body, body['metadata'], job_id)
        mock_publish.assert_called_once()
        mock_msr.assert_called_once()
        msg = mock_publish.call_args[0][0]
        self.assertNotIn('attachments', msg)
        self.assertNotIn('attachments', msg['metadata'])
        mock_copy.assert_not_called()

    def test_process_command_simple_onefile_pattern(self):
        workflow_id = 'wOrKflow_iD'
        job_id = 'jOb_iD'
        body = {
            'metadata': {
                'workflow_id': workflow_id,
                'job_id': job_id,
                'step_sequence_id': 123,
            },
            'scripts': [
                f'::inception::{workflow_id}::name::target',
                'do something',
                'echo "::attach type=atype::a/b/c/d/nam*',
                'echo "done"',
            ],
            'runs-on': ['linux', 'inception'],
        }
        mock_publish = MagicMock()
        mock_msr = MagicMock()
        mock_cache = {workflow_id: {}}
        with (
            patch('opentf.plugins.inceptionee.main.app'),
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.make_status_response', mock_msr),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
            patch('opentf.plugins.inceptionee.main.make_uuid', return_value='x'),
            patch('shutil.copy') as mock_copy,
        ):
            main._process_command(body, body['metadata'], job_id)
        mock_publish.assert_called_once()
        mock_msr.assert_called_once()
        msg = mock_publish.call_args[0][0]
        self.assertTrue(msg['attachments'])
        self.assertTrue(msg['metadata']['attachments'])
        mock_copy.assert_called_once_with(src='target', dst=f'/tmp/{job_id}-x_123_name')

    def test_process_command_simple_twofiles_pattern_onematch(self):
        workflow_id = 'wOrKflow_iD'
        job_id = 'jOb_iD'
        body = {
            'metadata': {
                'workflow_id': workflow_id,
                'job_id': job_id,
                'step_sequence_id': 123,
            },
            'scripts': [
                f'::inception::{workflow_id}::name.txt::targettxt',
                f'::inception::{workflow_id}::name.png::targetpng',
                'do something',
                'echo "::attach type=atype::a/b/c/d/*.txt',
                'echo "done"',
            ],
            'runs-on': ['linux', 'inception'],
        }
        mock_publish = MagicMock()
        mock_msr = MagicMock()
        mock_cache = {workflow_id: {}}
        with (
            patch('opentf.plugins.inceptionee.main.app'),
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.make_status_response', mock_msr),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
            patch('opentf.plugins.inceptionee.main.make_uuid', return_value='x'),
            patch('shutil.copy') as mock_copy,
        ):
            main._process_command(body, body['metadata'], job_id)
        mock_publish.assert_called_once()
        mock_msr.assert_called_once()
        msg = mock_publish.call_args[0][0]
        self.assertTrue(msg['attachments'])
        self.assertTrue(msg['metadata']['attachments'])
        mock_copy.assert_called_once_with(
            src='targettxt', dst=f'/tmp/{job_id}-x_123_name.txt'
        )

    def test_process_command_getfiles_twofiles_onematch(self):
        workflow_id = 'wOrKflow_iD'
        job_id = 'jOb_iD'
        body = {
            'metadata': {
                'workflow_id': workflow_id,
                'job_id': job_id,
                'step_sequence_id': 123,
            },
            'scripts': [
                f'::inception::{workflow_id}::name.txt::targettxt',
                f'::inception::{workflow_id}::name.png::targetpng',
                'do something',
                'if test -z "$(find . -name \'*.txt\' -print -quit)"; then echo "::attach type=atype::$(pwd)/$f"; fi; done',
                'echo "done"',
            ],
            'runs-on': ['linux', 'inception'],
        }
        mock_publish = MagicMock()
        mock_msr = MagicMock()
        mock_cache = {workflow_id: {}}
        with (
            patch('opentf.plugins.inceptionee.main.app'),
            patch('opentf.plugins.inceptionee.main.publish', mock_publish),
            patch('opentf.plugins.inceptionee.main.make_status_response', mock_msr),
            patch('opentf.plugins.inceptionee.main.CACHE', mock_cache),
            patch('opentf.plugins.inceptionee.main.make_uuid', return_value='x'),
            patch('shutil.copy') as mock_copy,
        ):
            main._process_command(body, body['metadata'], job_id)
        mock_publish.assert_called_once()
        mock_msr.assert_called_once()
        msg = mock_publish.call_args[0][0]
        self.assertTrue(msg['attachments'])
        self.assertTrue(msg['metadata']['attachments'])
        mock_copy.assert_called_once_with(
            src='targettxt', dst=f'/tmp/{job_id}-x_123_name.txt'
        )

    # _maybe_release_workflow

    def test_maybe_release_channel_not_handled(self):
        workflow_id = 'wOrKflow_iD'
        with patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr:
            main._maybe_release_workflow(workflow_id)
        mock_msr.assert_called_once_with(
            'OK', f'Workflow {workflow_id} not handled by this channel.'
        )

    def test_maybe_release_channel_handled(self):
        workflow_id = 'wOrKflow_iD'
        channels = {'cHannel_id': workflow_id}
        with (
            patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr,
            patch('opentf.plugins.inceptionee.main.CHANNELS', channels),
        ):
            main._maybe_release_workflow('wOrKflow_iD')
        mock_msr.assert_called_once_with(
            'OK', f'Inception channel released for workflow {workflow_id}.'
        )
        self.assertIn('cHannel_id', channels)

    # process_inbox

    def test_process_inbox_completed(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=EVENT_COMPLETED)
        with (
            patch(
                'opentf.plugins.inceptionee.main._maybe_release_workflow'
            ) as mock_mrw,
            patch('opentf.plugins.inceptionee.main.request', mock_request),
            patch('opentf.plugins.inceptionee.main.validate_schema', mock_validate),
        ):
            main.process_inbox()
        mock_mrw.assert_called_once_with('wOrKflow_iD')

    def test_process_inbox_canceled(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=EVENT_CANCELED)
        with (
            patch(
                'opentf.plugins.inceptionee.main._maybe_release_workflow'
            ) as mock_mrw,
            patch('opentf.plugins.inceptionee.main.request', mock_request),
            patch('opentf.plugins.inceptionee.main.validate_schema', mock_validate),
        ):
            main.process_inbox()
        mock_mrw.assert_called_once_with('wOrKflow_iD')

    def test_process_inbox_other(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=EVENT_OTHER)
        with (
            patch('opentf.plugins.inceptionee.main._process_command') as mock_pc,
            patch(
                'opentf.plugins.inceptionee.main.CHANNELS',
                {'cHannel_id': 'wOrKflow_iD'},
            ),
            patch('opentf.plugins.inceptionee.main.request', mock_request),
            patch('opentf.plugins.inceptionee.main.validate_schema', mock_validate),
        ):
            main.process_inbox()
        mock_pc.assert_called_once()

    def test_process_inbox_invalid(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=EVENT_OTHER_INVALID)
        with (
            patch('opentf.plugins.inceptionee.main._process_command') as mock_pc,
            patch(
                'opentf.plugins.inceptionee.main.CHANNELS',
                {'cHannel_id': 'wOrKflow_iD'},
            ),
            patch('opentf.plugins.inceptionee.main.request', mock_request),
            patch('opentf.plugins.inceptionee.main.validate_schema') as mock_validate,
            patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr,
        ):
            main.process_inbox()
        mock_pc.assert_not_called()
        mock_validate.assert_not_called()
        mock_msr.assert_called_once_with(
            'BadRequest',
            f'Not a valid {main.EXECUTIONCOMMAND} request: Missing metadata section.',
        )

    def test_process_inbox_other_nottargetted(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=EVENT_OTHER_UNKNOWNCHANNELID)
        with (
            patch('opentf.plugins.inceptionee.main._process_command') as mock_pc,
            patch(
                'opentf.plugins.inceptionee.main.CHANNELS',
                {'cHannel_id': 'wOrKflow_iD'},
            ),
            patch('opentf.plugins.inceptionee.main.request', mock_request),
            patch('opentf.plugins.inceptionee.main.validate_schema') as mock_validate,
            patch('opentf.plugins.inceptionee.main.make_status_response') as mock_msr,
        ):
            main.process_inbox()
        mock_pc.assert_not_called()
        mock_validate.assert_not_called()
        mock_msr.assert_called_with('OK', 'Job not handled by this channel plugin.')

    def test_process_inbox_channelrequest(self):
        mock_request = MagicMock()
        mock_request.get_json = MagicMock(return_value=EVENT_CHANNELREQUEST)
        with (
            patch('opentf.plugins.inceptionee.main._maybe_make_offer') as mock_mmo,
            patch('opentf.plugins.inceptionee.main._process_command') as mock_pc,
            patch(
                'opentf.plugins.inceptionee.main.CHANNELS',
                {'cHannel_id': 'wOrKflow_iD'},
            ),
            patch('opentf.plugins.inceptionee.main.request', mock_request),
            patch('opentf.plugins.inceptionee.main.validate_schema', mock_validate),
        ):
            main.process_inbox()
        mock_pc.assert_not_called()
        mock_mmo.assert_called_once()


if __name__ == '__main__':
    unittest.main()
