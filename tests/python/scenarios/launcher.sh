# Launcher created by ChatGTP 4o
#!/bin/bash

# Functions

# Function to find min start time, max end time, and compute elapsed time
get_workflow_times() {
    # Get workflow metadata
    WORKFLOW_DATA=$(opentf-ctl get workflows -o custom-columns=ID:.metadata.workflow_id,START:.metadata.creationTimestamp,END:.metadata.completionTimestamp)

    # Initialize arrays
    START_TIMES=()
    END_TIMES=()

    # Process output line by line, skipping the header
    while read -r ID START END; do
        # Ignore empty lines
        [[ -z "$ID" || -z "$START" || -z "$END" ]] && continue

        # Store timestamps in arrays
        START_TIMES+=("$START")
        END_TIMES+=("$END")
    done < <(echo "$WORKFLOW_DATA" | tail -n +2)

    # Check if we found valid timestamps
    if [[ ${#START_TIMES[@]} -eq 0 || ${#END_TIMES[@]} -eq 0 ]]; then
        echo "No valid workflows found."
        exit 1
    fi

    # Find earliest (min) start time and latest (max) end time
    MIN_START=$(printf "%s\n" "${START_TIMES[@]}" | sort | head -n 1)
    MAX_END=$(printf "%s\n" "${END_TIMES[@]}" | sort | tail -n 1)

    # Convert to proper datetime format
    START_FORMATTED=$(date -d "${MIN_START/Z/}" +"%Y-%m-%d %H:%M:%S,%3N")
    END_FORMATTED=$(date -d "${MAX_END/Z/}" +"%Y-%m-%d %H:%M:%S,%3N")

    # Convert to milliseconds for elapsed time calculation
    START_MS=$(date -d "${MIN_START/Z/}" +%s%3N)
    END_MS=$(date -d "${MAX_END/Z/}" +%s%3N)
    ELAPSED_MS=$((END_MS - START_MS))

    # Convert milliseconds to HH:MM:SS,MS format
    ELAPSED_FORMATTED=$(printf "%02d:%02d:%02d,%03d" \
        $((ELAPSED_MS / 3600000)) \
        $(((ELAPSED_MS / 60000) % 60)) \
        $(((ELAPSED_MS / 1000) % 60)) \
        $((ELAPSED_MS % 1000)))

    # Print results
    echo "Start time: $START_FORMATTED"
    echo "End time: $END_FORMATTED"
    echo "Elapsed time: $ELAPSED_FORMATTED"
}

# Function to check if all workflows are done
wait_for_completion() {
    echo "Waiting for all workflows to complete..."
    
    while true; do
        # Get workflow statuses
        STATUS_OUTPUT=$(opentf-ctl get workflows)

        # Check if any workflow is still running
        if echo "$STATUS_OUTPUT" | grep -q "RUNNING"; then
            echo "Some workflows are still running..."
        else
            echo "All workflows are completed!"
            break
        fi

        sleep 5  # Check every 5 seconds
    done
}

# Main

# Check if the orchestrator is running
if ! opentf-ctl get workflows > /dev/null 2>&1; then
    echo "Error: Orchestrator is not running or unreachable."
    exit 2
fi

# Check if exactly two arguments are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 <path_to_agent_launcher> <path_to_workflow_launcher>"
    exit 1
fi

AGENT_LAUNCHER="$(realpath "$1")"  # Convert to absolute path
WORKFLOW_LAUNCHER="$(realpath "$2")"  # Convert to absolute path

# Check if the scripts exist and are executable
if [ ! -x "$AGENT_LAUNCHER" ]; then
    echo "Error: $AGENT_LAUNCHER is not found or not executable."
    exit 1
fi

if [ ! -x "$WORKFLOW_LAUNCHER" ]; then
    echo "Error: $WORKFLOW_LAUNCHER is not found or not executable."
    exit 1
fi

echo "Launching $AGENT_LAUNCHER and $WORKFLOW_LAUNCHER in parallel..."

# Run both scripts using their full paths in the background
/bin/bash "$AGENT_LAUNCHER" &
AGENT_PID=$!

/bin/bash "$WORKFLOW_LAUNCHER" &
WORKFLOW_PID=$!

# Wait for both scripts to finish
wait $AGENT_PID
wait $WORKFLOW_PID

wait_for_completion
get_workflow_times

echo "Both scripts have completed execution."

# Kill all opentf-agent processes
echo "Stopping all running opentf-agent processes..."
pkill -f "opentf-agent"

# Unregister all remaining agents
echo "Unregistering all agents..."
opentf-ctl delete agent --all

echo "Main launcher complete."