<!--- Scenario launcher doc generated by ChatGPT 4o. -->

# OpenTestFactory Orchestrator Automation Scripts

This repository contains three shell scripts designed to automate the execution of OpenTestFactory workflows and agents:

- **`launcher.sh`**: The main launcher that coordinates workflow execution and agent management.
- **`agent_launcher.sh`**: Launches OpenTestFactory agents.
- **`workflow_launcher.sh`**: Runs workflows and monitors their completion.

## Prerequisites

Before using these scripts, ensure you have:
- OpenTestFactory (`opentf-ctl`, `opentf-agent`) installed and configured.
- Necessary permissions to execute shell scripts (`chmod +x` required).
- A running OpenTestFactory **orchestrator**.

## 1️⃣ Main Launcher (`launcher.sh`)

### Description
The main launcher:
1. Checks if the orchestrator is up by running `opentf-ctl get workflows`.
2. Executes the agent launcher and workflow launcher in parallel.
3. Waits for both to complete.
4. Waits until all workflows are complete.
5. Calculates execution time (start time, end time, elapsed time).
6. Stops all running `opentf-agent` processes.
7. Deregisters all agents using `opentf-ctl delete agent --all`.

### Usage
```sh
./launcher.sh <path_to_agent_launcher> <path_to_workflow_launcher>
```

**Example:**

```sh
./launcher.sh agent_launcher.sh workflow_launcher.sh
```

### Error Handling

If the orchestrator is not running, it exits with status 2.
If script arguments are missing, it exits with status 1.

## 2️⃣ Agent Launcher (`agent_launcher.sh`)

### Description

The agent launcher:
1. Starts OpenTestFactory agents in daemon mode.

You can modify the script at your convenience.

## 3️⃣ Workflow Launcher (`workflow_launcher.sh`)

### Description

The workflow launcher:
1. Runs OpenTestFactory workflows.

You can modify the script at your convenience.

## Final Notes

Ensure OpenTestFactory services are running before executing.

Scripts are designed for automated testing environments.

Modify `agent_launcher.sh` and `workflow_launcher.sh` as per your needs.

🚀 Happy Testing with OpenTestFactory!