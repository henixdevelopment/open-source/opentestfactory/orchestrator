# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Robot Framework Provider unit tests.

- execute tests
- mvntest tests
"""

import os
import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.robotframework import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    ('robotframework', 'execute', None): ({'test': {'required': True}}, False),
    ('robotframework', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
    ('robotframework', 'robot', None): (
        {
            'datasource': {'description': 'The datasource to use.', 'required': True},
            'test': {
                'description': 'Specify a test case present in the datasource.  By default, all test cases in the datasource are executed.',
                'required': False,
            },
            'reports-for-allure': {
                'description': 'A boolean.  Set to `true` to enable the generation of Allure reports. By default, Allure reports are not generated.',
                'required': False,
            },
            'extra-options': {
                'description': 'Specify additional parameters to pass to Robot Framework.',
                'required': False,
            },
        },
        False,
    ),
}

########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestRobotFramework(unittest.TestCase):
    """
    The class containing the Robot Framework provider unit test methods.
    """

    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        app.config['CONTEXT'][opentf.toolkit.OUTPUTS_KEY] = {}
        self.app = app.test_client()
        self.assertFalse(app.debug)

    ###########################
    # execute

    def test_execute_nok(self):
        """
        In a Robot Framework context, checks the error message content when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'robotframework/execute'}
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn('\'test\'', msg)

    def test_execute_ok(self):
        """
        In a Robot Framework context, checks the presence of 7 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {'test': 'RobotProject/src/test/java/features/sample.feature'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertNotIn('allure', steps[1]['run'])

    def test_execute_ok_allure(self):
        """
        In a Robot Framework context, checks the presence of 7 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {'test': 'RobotProject/src/test/java/features/sample.feature'},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('allure', steps[1]['run'])

    def test_execute_ok_testcase_allure(self):
        """
        In a Robot Framework context, checks the presence of 7 steps when execute
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {
                    'test': 'RobotProject/src/test/java/features/sample.feature#a "test"'
                },
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('\'a "test"\'', steps[1]['run'])

    def test_execute_backslashes_windows(self):
        """
        In a Robot Framework context, checks that on windows, backslashes preceding
        a doublequote or ending the test reference are escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {
                    'test': 'RobotProject/src/test/java/features/sample.feature#\\"One" two \\\\" three\\\\\\'
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('\\\\""One"" two \\\\\\\\"" three\\\\\\\\\\\\', steps[1]['run'])

    def test_execute_doublequotes_windows(self):
        """
        In a Robot Framework context, checks that on windows, doublequotes are escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {
                    'test': 'RobotProject/src/test/java/features/sample.feature#test "doublequotes"'
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('test ""doublequotes""', steps[1]['run'])

    def test_execute_percent_windows_allure(self):
        """
        In a Robot Framework context, checks that on windows, percent signs are escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {
                    'test': 'RobotProject/src/test/java/features/sample.feature#test %PATH%'
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('"test %%PATH%%"', steps[1]['run'])

    def test_execute_ok_regex_chars_parsing_allure(self):
        """
        In a Robot Framework context, checks that regex characters in test case name
        are correctly parsed or escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {
                    'test': 'RobotProject/src/test/java/features/sample.feature#a [test]? **'
                },
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('a [[]test[]][?] [*][*]', steps[1]['run'])

    def test_execute_extra_options_win_allure(self):
        """
        In a Robot Framework/Windows context, checks the presence of the
        ROBOTFRAMEWORK_EXTRA_OPTIONS_VARIABLE in the test execution step.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {'test': 'RobotProject/sample_test.robot'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertEqual(
            'robot %ROBOTFRAMEWORK_EXTRA_OPTIONS% --nostatusrc --listener "allure_robotframework;." "RobotProject/sample_test.robot"',
            steps[1]['run'],
        )

    def test_execute_extra_options_lin_allure(self):
        """
        In a Robot Framework/Linux context, checks the presence of the
        ROBOTFRAMEWORK_EXTRA_OPTIONS_VARIABLE in the test execution step.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.ALLURE_REPORTING_ENABLED',
                True,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/execute',
                'with': {'test': 'RobotProject/sample_test.robot'},
            }
            command['contexts'] = {'runner': {'os': 'linux'}}
            command['runs-on'] = ['linux']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertEqual(
            'robot $ROBOTFRAMEWORK_EXTRA_OPTIONS --nostatusrc --listener "allure_robotframework;." "RobotProject/sample_test.robot"',
            steps[1]['run'],
        )

    def test_robotframework_nok(self):
        """
        In a Robot Framework context, checks the error message content when native
        action is called.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'robotframework/robot'}
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn('\'datasource\'', msg)

    def test_robotframework_ok(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE'},
            }
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn('rm -f', steps[0]['run'])
        self.assertIn('run', steps[1])
        cmd = steps[1]['run']
        self.assertIn('"DataSourCE"', cmd)

    def test_robotframework_ok_test(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'test': 'yada"'},
            }
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn('run', steps[1])
        cmd = steps[1]['run']
        self.assertIn('\'yada"\'', cmd)

    def test_robotframework_backslashes_windows(self):
        """
        In a Robot Framework context, checks that on windows, backslashes
        preceding a doublequote or ending test reference are escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {
                    'datasource': 'DataSourCE',
                    'test': 'One \\"little \\"vampire"\\',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn('run', steps[1])
        cmd = steps[1]['run']
        self.assertIn('One \\\\""little \\\\""vampire""\\\\', cmd)

    def test_robotframework_doublequotes_windows(self):
        """
        In a Robot Framework context, checks that on windows, doublequotes are escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'test': 'Test "test"'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn('run', steps[1])
        cmd = steps[1]['run']
        self.assertIn('Test ""test""', cmd)

    def test_robotframework_percent_windows(self):
        """
        In a Robot Framework context, checks that on windows, percent signs are escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'test': 'Test %ENV%'},
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn('run', steps[1])
        cmd = steps[1]['run']
        self.assertIn('"Test %%ENV%%"', cmd)

    def test_robotframework_ok_allure(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata'] = command['metadata'].copy()
            command['metadata']['labels'] = {
                'opentestfactory.org/category': 'robot',
                'opentestfactory.org/categoryPrefix': 'robotframework',
            }
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'reports-for-allure': True},
            }
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('run', steps[1])
        self.assertIn('robot', steps[1]['run'])
        self.assertIn('get-files', steps[6]['uses'])

    def test_robotframework_ok_allure_underscore(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata'] = command['metadata'].copy()
            command['metadata']['labels'] = {
                'opentestfactory.org/category': 'robot',
                'opentestfactory.org/categoryPrefix': 'robotframework',
            }
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'reports_for_allure': True},
            }
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 7)
        self.assertIn('run', steps[1])
        self.assertIn('robot', steps[1]['run'])
        self.assertIn('get-files', steps[6]['uses'])

    def test_robotframework_nok_allure_string(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_error', mock):
            command = PROVIDERCOMMAND.copy()
            command['metadata'] = command['metadata'].copy()
            command['metadata']['labels'] = {
                'opentestfactory.org/category': 'robot',
                'opentestfactory.org/categoryPrefix': 'robotframework',
            }
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'reports_for_allure': 'True'},
            }
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertIn('reports-for-allure must be a boolean', msg)

    def test_robotframework_ok_extra_options(self):
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'extra-options': '--settag foo'},
            }
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn('run', steps[1])
        self.assertIn('--settag foo', steps[1]['run'])

    def test_robotframework_ok_regex_chars_parsing(self):
        """
        In a Robot Framework context, checks that regex characters in test case name
        are correctly parsed or escaped.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/robot',
                'with': {'datasource': 'DataSourCE', 'test': '[holy?] test ? '},
            }
            _dispatch(implementation.robot_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 5)
        self.assertIn('run', steps[1])
        cmd = steps[1]['run']
        self.assertIn('[[]holy[?][]] test [?] ', cmd)

    def test_param_linux_ok(self):
        """
        Checks the runner command with params when param action is called
        on a Linux execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 2)
        self.assertIn('actions/create-file@v1', steps[0]['uses'])
        self.assertIn(
            'strange_value', steps[0]['with']['data']['global']['firstGlobal']
        )
        self.assertIn('echo export', steps[1]['run'])

    def test_param_windows_ok(self):
        """
        Checks the runner command with params when param action is called
        on a Windows execution environment.
        """
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.robotframework.implementation.core.validate_params_inputs',
                MagicMock(),
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'tm.squashtest.org/params@v1',
                },
            }
            command['contexts'] = {'runner': {'os': 'windows'}}
            command['runs-on'] = ['windows']
            _dispatch(implementation.param_action, command)
        mock.assert_called_once()
        steps = mock.call_args[0][0]
        self.assertEqual(len(steps), 2)
        self.assertIn('actions/create-file@v1', steps[0]['uses'])
        self.assertIn(
            'strange_value', steps[0]['with']['data']['global']['firstGlobal']
        )
        self.assertIn('@echo set "_SQUASH_TF_', steps[1]['run'])

    def test_param_invalid_format_nok(self):
        """
        Checks the runner command with an invalid format param
        when param action is called.
        """
        with patch(
            'opentf.plugins.robotframework.implementation.core.validate_params_inputs',
            MagicMock(side_effect=SystemExit(2)),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'robotframework/params',
                'with': {
                    'data': {
                        'global': {'firstGlobal': 'strange_value'},
                        'test': {'key1': 'value1', 'key2': 'value2'},
                    },
                    'format': 'invalid_format/params@v1',
                },
            }
            self.assertRaisesRegex(
                SystemExit, '2', _dispatch, implementation.param_action, command
            )


if __name__ == '__main__':
    unittest.main()
