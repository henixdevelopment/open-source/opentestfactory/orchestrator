# Copyright (c) 2022 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""LocalCleaner unit tests."""

import os
import sys
import unittest

from unittest.mock import MagicMock, patch

from requests import Response


########################################################################

EVENT_WR = {
    'kind': 'WorkflowResult',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

EVENT_OTHER = {
    'kind': 'FooBar',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {'workflow_id': 'wOrKflow_iD'},
}

########################################################################

mockresponse = Response()
mockresponse.status_code = 200

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.rewriter import main


class TestRewriter(unittest.TestCase):
    # AllureCollectorOutput

    def test_aco_no(self):
        mock_queue = MagicMock()
        with patch('opentf.plugins.rewriter.main.DISPATCH_QUEUE', mock_queue):
            main.aco(None, {}, None)
        mock_queue.put.assert_not_called()

    def test_aco_ok(self):
        mock_queue = MagicMock()
        with (
            patch('opentf.plugins.rewriter.main.DISPATCH_QUEUE', mock_queue),
            patch('os.path.exists', MagicMock(return_value=True)),
        ):
            main.aco(None, {'with': {'allureReport': 'YadA'}}, None)
        mock_queue.put.assert_called_once()

    def test_aco_ko_file(self):
        mock_queue = MagicMock()
        mock_exist = MagicMock(return_value=False)
        with (
            patch('opentf.plugins.rewriter.main.DISPATCH_QUEUE', mock_queue),
            patch('os.path.exists', mock_exist),
        ):
            main.aco(None, {'with': {'allureReport': 'BoO'}}, 'wf_Id')
        mock_exist.assert_called_once_with('BoO')
        mock_queue.put.assert_called_once()
        call = mock_queue.put.call_args[0][0]
        self.assertEqual('Notification', call['kind'])
        self.assertIn(
            'Attachment BoO from AllureCollectorOutput not found.',
            call['spec']['logs'][0],
        )

    # process_inbox

    @patch('opentf.plugins.rewriter.main.make_status_response')
    @patch('opentf.plugins.rewriter.main.validate_schema')
    def test_processinbox_valid(self, mock_validate_schema, mock_msr):
        subs = {'v1/test': MagicMock()}
        request = MagicMock()
        request.get_json.return_value = {
            'kind': 'test',
            'apiVersion': 'v1',
            'metadata': {'workflow_id': '1234'},
        }
        mock_validate_schema.return_value = (True, '')

        with (
            patch('opentf.plugins.rewriter.main.request', request),
            patch('opentf.plugins.rewriter.main.SUBSCRIPTIONS', subs),
        ):
            result = main.process_inbox()

        mock_validate_schema.assert_called_once_with(
            'v1/test', request.get_json.return_value
        )
        mock_msr.assert_called_once_with('OK', '')

        self.assertEqual(result, mock_msr.return_value)

    @patch('opentf.plugins.rewriter.main.make_status_response')
    def test_processinbox_invalid_json(self, mock_msr):
        request = MagicMock()
        request.get_json.side_effect = ValueError('Invalid JSON')

        with patch('opentf.plugins.rewriter.main.request', request):
            result = main.process_inbox()
        mock_msr.assert_called_once_with(
            'BadRequest', 'Could not parse body: Invalid JSON.'
        )
        self.assertEqual(result, mock_msr.return_value)

    @patch('opentf.plugins.rewriter.main.make_status_response')
    def test_processinbox_invalid_dict(self, mock_msr):
        request = MagicMock()
        request.get_json.return_value = 'not a dictionary'

        with patch('opentf.plugins.rewriter.main.request', request):
            result = main.process_inbox()
        mock_msr.assert_called_once_with(
            'BadRequest', 'Could not parse body, was expecting a dictionary.'
        )
        self.assertEqual(result, mock_msr.return_value)

    @patch('opentf.plugins.rewriter.main.make_status_response')
    def test_processinbox_missing_kind(self, mock_msr):
        request = MagicMock()
        request.get_json.return_value = {'apiVersion': 'v1'}

        with patch('opentf.plugins.rewriter.main.request', request):
            result = main.process_inbox()

        mock_msr.assert_called_once_with('BadRequest', 'Missing kind or apiVersion.')
        self.assertEqual(result, mock_msr.return_value)

    @patch('opentf.plugins.rewriter.main.make_status_response')
    @patch('opentf.plugins.rewriter.main.validate_schema')
    def test_processinbox_invalid_schema(self, mock_validate_schema, mock_msr):
        subs = {'v1/test': MagicMock()}
        request = MagicMock()
        request.get_json.return_value = {
            'kind': 'test',
            'apiVersion': 'v1',
            'metadata': {'workflow_id': '1234'},
        }
        mock_validate_schema.return_value = (False, 'Invalid schema')

        with (
            patch('opentf.plugins.rewriter.main.request', request),
            patch('opentf.plugins.rewriter.main.SUBSCRIPTIONS', subs),
        ):
            result = main.process_inbox()

        mock_validate_schema.assert_called_once_with(
            'v1/test', request.get_json.return_value
        )
        mock_msr.assert_called_once_with(
            'BadRequest', 'Not a valid test request: Invalid schema'
        )
        self.assertEqual(result, mock_msr.return_value)

    @patch('opentf.plugins.rewriter.main.make_status_response')
    def test__processinbox_unexpected(self, mock_msr):
        request = MagicMock()
        request.get_json.return_value = {
            'kind': 'test',
            'apiVersion': 'v1',
            'metadata': {'workflow_id': '1234'},
        }

        with patch('opentf.plugins.rewriter.main.request', request):
            result = main.process_inbox()

        mock_msr.assert_called_once_with('BadRequest', 'Unexpected event v1/test.')
        self.assertEqual(result, mock_msr.return_value)


if __name__ == '__main__':
    unittest.main()
