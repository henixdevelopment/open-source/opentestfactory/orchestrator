# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Metadata unit tests."""

from copy import deepcopy
import unittest
from unittest.mock import MagicMock, patch

from opentf.core import datasources


########################################################################
# Datasets

TESTS_EVENTS = [
    {
        'kind': 'ProviderCommand',
        'metadata': {'step_id': '123', 'creationTimestamp': 'today'},
    },
    {
        'kind': 'ExecutionResult',
        'metadata': {
            'attachments': {'/one/two/three.xml': {'uuid': 'foo'}},
            'step_origin': ['bAr', '123'],
        },
        'attachments': 'files',
    },
    {
        'kind': 'Workflow',
        'metadata': {
            'namespace': 'default',
            'workflow_id': 'wFID',
            'managedTests': {
                'testCases': {
                    'bAr': {
                        'param_step_id': 'fOo',
                        'technology': 'nOtJunIt',
                        'path': ['path', 'to', 'some', 'Test 4 test'],
                        'type': 'TYP_TYPe',
                        'name': '112233',
                    },
                    'notBar': {'name': '442200'},
                },
                'testPlan': {
                    'path': ['path', 'to', 'exit'],
                    'type': 'test suite',
                },
            },
        },
        'jobs': {
            'jOb': {
                'runs-on': ['junit', 'linux'],
                'steps': [
                    {
                        'id': 'bAr',
                        'name': 'execStepName',
                        'uses': 'cypress/execute@v3',
                        'with': {'test': 'some#reference'},
                    },
                    {
                        'id': 'fOo',
                        'name': 'paramStepName',
                        'with': {
                            'data': {
                                'test': {
                                    'CUF_CUF': '445566',
                                    'CUF_NOT_CUF': '998877',
                                }
                            }
                        },
                    },
                ],
            }
        },
    },
]

TESTRESULTS_WITH_DETAILS = [
    {
        'metadata': {'attachment_origin': ['foo']},
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'foo',
                    'duration': 1,
                    'id': '000',
                    'name': 'brandNewTest#Case',
                    'status': 'FAILURE',
                    'failureDetails': {
                        'message': 'Well.',
                        'type': 'ErrorError',
                        'text': 'It is quite unexpected.',
                    },
                }
            ]
        },
    },
    {
        'metadata': {'attachment_origin': ['foo']},
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'foo',
                    'duration': 21,
                    'id': '001',
                    'name': 'JustTest#Case',
                    'status': 'SUCCESS',
                    'errorsList': [
                        {'message': 'Hello. I am error.', 'timestamp': 'today'}
                    ],
                }
            ]
        },
    },
]

THREE_TESTRESULTS = [
    {
        'metadata': {'attachment_origin': ['foo']},
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'foo',
                    'duration': 1,
                    'id': '000',
                    'name': 'brandNewTest#Case',
                    'status': 'FAILURE',
                    'failureDetails': {
                        'message': 'Well.',
                        'type': 'ErrorError',
                        'text': 'It is quite unexpected.',
                    },
                }
            ]
        },
    },
    {
        'metadata': {'attachment_origin': ['foo']},
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'foo',
                    'duration': 21,
                    'id': '001',
                    'name': 'JustTest#Case',
                    'status': 'SUCCESS',
                    'errorsList': [
                        {'message': 'Hello. I am error.', 'timestamp': 'today'}
                    ],
                }
            ]
        },
    },
    {
        'metadata': {'attachment_origin': ['foo']},
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'foo',
                    'duration': 22,
                    'id': '002',
                    'name': 'JustTestTest#Case',
                    'status': 'SUCCESS',
                }
            ]
        },
    },
]

MANAGED_TESTRESULTS = [
    {
        'metadata': {'step_id': 'bAr'},
        'spec': {'managedTestResult': {'duration': 1, 'reportStatus': 'PASS'}},
    },
    {
        'metadata': {'step_id': 'bAr'},
        'spec': {
            'managedTestResult': {
                'duration': 1,
                'reportStatus': 'FAIL',
                'failureDetails': ['SomeErrorMessage'],
            }
        },
    },
]

TESTRESULTS_INCEPTION = [
    {
        'metadata': {'attachment_origin': ['foo']},
        'kind': 'Notification',
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'foo',
                    'duration': 1,
                    'id': '000',
                    'name': 'brandNewTest#Case',
                    'status': 'FAILURE',
                },
                {
                    'attachment_origin': 'foo',
                    'duration': 2,
                    'id': '001',
                    'name': 'TooOLdTest#Case',
                    'status': 'SUCCESS',
                },
            ]
        },
    },
    {
        'metadata': {'attachment_origin': ['foo']},
        'kind': 'Notification',
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'foo',
                    'duration': 1,
                    'id': '000',
                    'name': 'brandNewTest#Case',
                    'status': 'FAILURE',
                },
                {
                    'attachment_origin': 'foo',
                    'duration': 2,
                    'id': '001',
                    'name': 'TooOLdTest#Case',
                    'status': 'SUCCESS',
                },
            ]
        },
    },
    {
        'metadata': {'attachment_origin': ['bar']},
        'kind': 'Notification',
        'spec': {
            'testResults': [
                {
                    'attachment_origin': 'bar',
                    'duration': 21,
                    'id': '002',
                    'name': 'JustTest#Case',
                    'status': 'SUCCESS',
                }
            ]
        },
    },
    {
        'metadata': {'step_id': 'bAr'},
        'kind': 'Notification',
        'spec': {'managedTestResult': {'duration': 1, 'reportStatus': 'PASS'}},
    },
]

TAG_EVENTS = [
    {
        'kind': 'Workflow',
        'metadata': {'workflow_id': 'iD', 'namespace': 'ns'},
        'jobs': {
            'jOb': {
                'runs-on': ['linux', 'cypress', 'foo'],
                'steps': {'name': 'step1', 'run': 'something'},
            }
        },
    }
]

TAG_EVENTS_RUNS_ON_AS_STRING = [
    {
        'kind': 'Workflow',
        'metadata': {'workflow_id': 'iD', 'namespace': 'ns'},
        'jobs': {
            'jOb': {
                'runs-on': 'linux',
                'steps': {'name': 'step1', 'run': 'something'},
            }
        },
    }
]

JOB_EVENTS = [
    {
        'kind': 'Workflow',
        'metadata': {
            'workflow_id': 'iD',
            'namespace': 'ns',
            'creationTimestamp': '2023-06-28T09:45:34.020220',
        },
        'jobs': {
            'jOb': {
                'runs-on': 'robotframework',
                'steps': {'name': 'step1', 'run': 'something'},
                'variables': {'var1': 1, 'var2': 2},
            }
        },
    },
    {
        'kind': 'ExecutionCommand',
        'runs-on': 'robotframework',
        'metadata': {
            'name': 'jOb',
            'job_id': 'uuid',
            'attachments': {'/one/two/three.xml': {'uuid': 'foo'}},
            'step_origin': ['bAr', '123'],
            'step_sequence_id': -1,
            'creationTimestamp': '2023-06-28T09:46:34.020220',
            'job_origin': [],
        },
        'attachments': 'files',
    },
    {
        'kind': 'ExecutionCommand',
        'runs-on': 'robotframework',
        'metadata': {
            'name': 'jOb',
            'job_id': 'uuid',
            'step_sequence_id': 0,
            'creationTimestamp': '2023-06-28T09:47:34.020220',
        },
    },
    {
        'kind': 'ExecutionResult',
        'runs-on': 'robotframework',
        'metadata': {
            'name': 'jOb',
            'job_id': 'uuid',
            'step_sequence_id': -2,
            'creationTimestamp': '2023-06-28T09:48:34.020220',
        },
    },
]

GENERATOR_JOB_EVENTS = [
    {
        'kind': 'Workflow',
        'metadata': {
            'workflow_id': 'iD',
            'namespace': 'ns',
            'creationTimestamp': '2023-06-28T09:45:34.020220',
        },
        'jobs': {'foobar': {}},
    },
    {
        'kind': 'GeneratorResult',
        'jobs': {
            'jOb': {
                'runs-on': ['robotframework'],
                'steps': {'name': 'step1', 'run': 'something'},
                'variables': {'var1': 1, 'var2': 2},
            }
        },
        'metadata': {
            'job_id': 'generator_job',
            'job_origin': [],
            'name': 'foobar',
            'namespace': 'ns',
            'workflow_id': 'iD',
            'creationTimestamp': '2023-06-28T09:45:34.020220',
        },
    },
    {
        'kind': 'ExecutionCommand',
        'runs-on': ['robotframework'],
        'metadata': {
            'name': 'jOb',
            'job_id': 'uuid',
            'attachments': {'/one/two/three.xml': {'uuid': 'foo'}},
            'step_origin': ['bAr', '123'],
            'step_sequence_id': -1,
            'creationTimestamp': '2023-06-28T09:46:34.020220',
            'job_origin': ['generator_job'],
        },
        'attachments': 'files',
    },
    {
        'kind': 'ExecutionCommand',
        'runs-on': ['robotframework'],
        'metadata': {
            'name': 'jOb',
            'job_id': 'uuid',
            'step_sequence_id': 0,
            'creationTimestamp': '2023-06-28T09:47:34.020220',
        },
    },
    {
        'kind': 'ExecutionResult',
        'runs-on': ['robotframework'],
        'metadata': {
            'name': 'jOb',
            'job_id': 'uuid',
            'step_sequence_id': -2,
            'creationTimestamp': '2023-06-28T09:48:34.020220',
        },
    },
]

TESTCASES_DICT = {
    '000': {
        'metadata': {
            'name': 'brandNewTest#Case',
            'status': 'FAILURE',
            'creationTimestamp': 'today',
            'job_id': 'uuid',
        },
        'test': {'outcome': 'failure', 'job': 'jOb', 'runs-on': ['linux', 'cypress']},
        'execution': {
            'duration': 1,
            'failureDetails': {
                'message': 'Well.',
                'type': 'ErrorError',
                'text': 'It is quite unexpected.',
            },
        },
    },
    '001': {
        'metadata': {'name': 'JustTest#Case', 'timestamp': 'today', 'job_id': 'uuid'},
        'status': 'SUCCESS',
        'test': {
            'outcome': 'success',
            'job': 'jOb',
            'runs-on': ['linux', 'cypress', 'foo'],
        },
        'execution': {
            'errorsList': [{'message': 'Hello. I am error.', 'timestamp': 'today'}],
            'duration': 21,
        },
    },
}

TESTCASE_ONE_TAG = {
    '000': {
        'metadata': {
            'name': 'oneTag#FitsAll',
            'status': 'FAILURE',
            'creationTimestamp': 'today',
            'job_id': 'uuid',
        },
        'test': {'outcome': 'failure', 'job': 'jOb', 'runs-on': ['linux']},
        'execution': {
            'duration': 1,
            'failureDetails': {
                'message': 'Well.',
                'type': 'ErrorError',
                'text': 'It is quite unexpected.',
            },
        },
    }
}

DEFAULT_STATE = {
    'last_notification_used': 0,
    'last_testresult_used': 0,
    'page': 1,
    'per_page': 100,
    'fieldselector': None,
    'reset': True,
}

########################################################################


class TestDatasources(unittest.TestCase):
    # get testresult labels

    def test_get_testresult_labels_managed_tests(self):
        labels = datasources._get_testresult_labels('bAr', TESTS_EVENTS, True)
        result = labels['test']  # type: ignore
        self.assertEqual(13, len(result))
        self.assertTrue(result['managed'])
        self.assertEqual('nOtJunIt', result['technology-name'])
        self.assertEqual('TYP_TYPe', result['type'])
        self.assertEqual(['path', 'to', 'some', 'Test 4 test'], result['path'])
        self.assertEqual(['path', 'to', 'exit'], result['collection']['path'])
        self.assertEqual('test suite', result['collection']['type'])
        self.assertEqual({'CUF_CUF': '445566', 'CUF_NOT_CUF': '998877'}, result['data'])
        self.assertNotEqual('442200', result['name'])

    def test_get_testresult_labels_no_managed_tests(self):
        no_managed_tests = deepcopy(TESTS_EVENTS)
        del no_managed_tests[2]['metadata']['managedTests']
        labels = datasources._get_testresult_labels('foo', no_managed_tests, False)
        result = labels['test']  # type: ignore
        self.assertEqual(6, len(result))
        self.assertFalse(result['managed'])
        self.assertEqual('jOb', result['job'])
        self.assertEqual(['junit', 'linux'], result['runs-on'])
        self.assertEqual('cypress', result['technology'])
        self.assertEqual('cypress/execute@v3', result['uses'])
        self.assertIsNone(result.get('technology-name'))
        self.assertIsNone(result.get('collection'))
        self.assertIsNone(result.get('data'))

    def test_get_testresult_labels_none(self):
        self.assertIsNone(datasources._get_testresult_labels('foo', TESTS_EVENTS, True))

    def test_get_testresult_labels_no_testcases(self):
        no_testcases = deepcopy(TESTS_EVENTS)
        del no_testcases[2]['metadata']['managedTests']['testCases']
        app_mock = MagicMock()
        app_mock.logger.warning = MagicMock()
        with patch('opentf.core.datasources.current_app', app_mock):
            labels = datasources._get_testresult_labels('bAr', no_testcases, True)
        result = labels['test']  # type: ignore
        app_mock.logger.warning.assert_called_once_with(
            'Was expecting a "testCases" part in parent of step bAr, ignoring.'
        )
        self.assertIsNone(result.get('collection'))
        self.assertIsNone(result.get('data'))
        self.assertIsNone(result.get('name'))
        self.assertFalse(result['managed'])
        self.assertEqual('cypress', result['technology'])

    def test_get_testresult_labels_no_params(self):
        no_params = deepcopy(TESTS_EVENTS)
        no_params[2]['metadata']['managedTests']['testCases']['bAr'][
            'param_step_id'
        ] = '000'
        app_mock = MagicMock()
        app_mock.logger.warning = MagicMock()
        with patch('opentf.core.datasources.current_app', app_mock):
            labels = datasources._get_testresult_labels('bAr', no_params, True)
        result = labels['test']  # type: ignore
        app_mock.logger.warning.assert_called_once_with(
            'Could not find "params" step associated to "execute" step bAr, ignoring.'
        )
        self.assertIsNone(result.get('data'))
        self.assertTrue(result['managed'])
        self.assertEqual('112233', result['name'])

    # _get_testcases

    def test_get_testcases_with_failure_details_and_duration(self):
        with (
            patch(
                'opentf.core.datasources._get_testresult_labels',
                MagicMock(return_value={'label': 'one'}),
            ),
            patch(
                'opentf.core.datasources.get_testresults',
                MagicMock(return_value=TESTRESULTS_WITH_DETAILS),
            ),
        ):
            result = datasources.get_testcases(TESTS_EVENTS)
        self.assertEqual(2, len(result))
        self.assertEqual(1, result['000']['execution']['duration'])
        self.assertEqual(21, result['001']['execution']['duration'])
        self.assertIsNotNone(result['001']['execution']['errorsList'])
        self.assertEqual(
            'It is quite unexpected.',
            result['000']['execution']['failureDetails']['text'],
        )
        self.assertEqual(
            result['000']['status'].lower(), result['000']['test']['outcome']
        )
        self.assertEqual(
            result['001']['status'].lower(), result['001']['test']['outcome']
        )

    def test_get_testcases_for_inception_workflow(self):
        mock_inception = MagicMock(return_value=TESTRESULTS_WITH_DETAILS)
        with (
            patch('opentf.core.datasources._get_inception_testresults', mock_inception),
            patch(
                'opentf.core.datasources._get_testresult_labels',
                MagicMock(side_effect=[None, {'label': 'two'}]),
            ),
            patch(
                'opentf.core.datasources._uses_inception', MagicMock(return_value=True)
            ),
        ):
            datasources.get_testcases(TESTRESULTS_INCEPTION)
        mock_inception.assert_called_once_with(TESTRESULTS_INCEPTION)

    def test_get_testcases_generator_workflow_no_runs_on(self):
        mock_inception = MagicMock()
        event = [{'kind': 'Workflow', 'jobs': {'generator_job': {'generator': 'foo'}}}]
        with (
            patch(
                'opentf.core.datasources.get_testresults', MagicMock(return_value={})
            ),
            patch('opentf.core.datasources._get_inception_testresults', mock_inception),
        ):
            self.assertEqual({}, datasources.get_testcases(event))

    def test_get_testcases_valuerror_no_testresults(self):
        with patch(
            'opentf.core.datasources.get_testresults', MagicMock(return_value={})
        ):
            self.assertEqual(
                {},
                datasources.get_testcases(TESTS_EVENTS),
            )

    def test_get_testcases_managed_testresults(self):
        with (
            patch(
                'opentf.core.datasources._get_testresult_labels',
                MagicMock(
                    side_effect=[
                        {
                            'metadata': {'execution_id': 'uuid1'},
                            'test': {'name': 'Test case 1'},
                        },
                        {
                            'metadata': {'execution_id': 'uuid2'},
                            'test': {'name': 'Test case 2'},
                        },
                    ]
                ),
            ),
            patch(
                'opentf.core.datasources.get_testresults',
                MagicMock(return_value=MANAGED_TESTRESULTS),
            ),
            patch('opentf.core.datasources.uuid4', side_effect=['uuid1', 'uuid2']),
        ):
            result = datasources.get_testcases(TESTS_EVENTS)
        self.assertEqual('Test case 1', result['uuid1']['test']['name'])
        self.assertEqual(
            result['uuid2']['test']['testCaseName'], result['uuid2']['test']['name']
        )
        self.assertEqual('SUCCESS', result['uuid1']['status'])
        self.assertEqual(
            'SomeErrorMessage',
            result['uuid2']['execution']['failureDetails']['text'],
        )

    # _get_inception_testresults

    def test_get_inception_testresults_ok(self):
        results = datasources._get_inception_testresults(TESTRESULTS_INCEPTION)
        self.assertEqual(3, len(results))
        self.assertEqual(
            ['000', '001'], [item['id'] for item in results[1]['spec']['testResults']]
        )
        self.assertEqual(
            ['002'], [item['id'] for item in results[2]['spec']['testResults']]
        )
        self.assertEqual('bAr', results[0]['metadata']['step_id'])

    # _in_scope

    def test_in_scope_ok(self):
        mock_eb = MagicMock(return_value=True)
        with patch('opentf.core.datasources.evaluate_bool', mock_eb):
            res = datasources.in_scope('exPr', {'some': 'context'})
        self.assertTrue(res)

    def test_in_scope_ok_valueerror(self):
        mock_eb = MagicMock(side_effect=ValueError('Well..'))
        with patch('opentf.core.datasources.evaluate_bool', mock_eb):
            self.assertRaisesRegex(
                ValueError,
                'Invalid conditional exPr: Well...',
                datasources.in_scope,
                'exPr',
                {'some': 'context'},
            )

    def test_in_scope_keyerror(self):
        mock_eb = MagicMock(side_effect=KeyError('Not this time'))
        with patch('opentf.core.datasources.evaluate_bool', mock_eb):
            self.assertRaisesRegex(
                ValueError,
                "Nonexisting context entry in expression exPr: 'Not this time'.",
                datasources.in_scope,
                'exPr',
                {'some': 'context'},
            )

    # _has_test_result

    def test_has_test_result(self):
        item = {'kind': 'Notification', 'spec': {'testResults': 'foo'}}
        self.assertTrue(datasources._has_testresult(item))

    # _as_tag_list

    def test_as_tag_list(self):
        self.assertEqual(['string'], datasources._as_list('string'))

    # _uses_inception

    def test_uses_inception_ok_true(self):
        events = [
            {
                'kind': 'Workflow',
                'jobs': {
                    'job1': {'runs-on': 'inception'},
                    'job2': {'runs-on': 'not inception'},
                },
            }
        ]
        self.assertTrue(datasources._uses_inception(events))

    def test_uses_inception_ok_false(self):
        events = [
            {
                'kind': 'Workflow',
                'jobs': {
                    'job1': {'runs-on': 'not inception'},
                    'job2': {'runs-on': 'also not inception'},
                },
            }
        ]
        self.assertFalse(datasources._uses_inception(events))

    def test_uses_inception_ko_valueerror(self):
        events = [
            {
                'kind': 'WorkflowCompleted',
                'jobs': {
                    'job1': {'runs-on': 'not inception'},
                    'job2': {'runs-on': 'also not inception'},
                },
            }
        ]
        self.assertRaisesRegex(
            ValueError,
            'No Workflow event in workflow events...',
            datasources._uses_inception,
            events,
        )

    # get_jobs

    def test_get_jobs_ok(self):
        with (
            patch(
                'opentf.core.datasources._uses_inception', MagicMock(return_value=False)
            ),
            patch(
                'opentf.core.datasources.get_testcases',
                MagicMock(return_value=TESTCASES_DICT),
            ),
        ):
            jobs = datasources.get_jobs(JOB_EVENTS)
        self.assertEqual(1, len(jobs))
        self.assertEqual(
            {'cancelled': 0, 'success': 0, 'failure': 0, 'error': 0, 'skipped': 0},
            jobs['uuid']['status']['testCaseStatusSummary'],
        )
        self.assertEqual(0, jobs['uuid']['status']['testCaseCount'])
        self.assertEqual({'var1': 1, 'var2': 2}, jobs['uuid']['spec']['variables'])
        self.assertEqual(60000.0, jobs['uuid']['status']['duration'])

    def test_get_jobs_generator_ok(self):
        with (
            patch(
                'opentf.core.datasources._uses_inception', MagicMock(return_value=False)
            ),
            patch(
                'opentf.core.datasources.get_testcases',
                MagicMock(return_value=TESTCASES_DICT),
            ),
        ):
            jobs = datasources.get_jobs(GENERATOR_JOB_EVENTS)
        self.assertEqual(1, len(jobs))
        self.assertEqual(
            {'cancelled': 0, 'success': 0, 'failure': 0, 'error': 0, 'skipped': 0},
            jobs['uuid']['status']['testCaseStatusSummary'],
        )
        self.assertEqual(0, jobs['uuid']['status']['testCaseCount'])
        self.assertEqual({'var1': 1, 'var2': 2}, jobs['uuid']['spec']['variables'])
        self.assertEqual(60000.0, jobs['uuid']['status']['duration'])

    def test_get_jobs_ko_value_error(self):
        self.assertRaisesRegex(
            datasources.DataSourceDataError,
            'No job events found in workflow. Cannot extract data for jobs.',
            datasources.get_jobs,
            {},
        )

    # _get_job_times_and_id

    def test_get_job_times_and_id_no_start_end_time(self):
        events = []
        channel_request = {
            'name': 'job',
            'step_sequence_id': -1,
            'job_id': 'some_uuid',
            'creationTimestamp': 'today',
        }

        self.assertEqual(
            {'job_id': 'some_uuid', 'requestTime': 'today'},
            datasources._collect_job_times_and_id(events, channel_request),
        )

    # _make_job_datasource

    def test_make_job_datasource_empty_channelrequest(self):
        job = datasources._make_job_datasource(
            'job',
            {},
            {'runs-on': ['linux']},
            {
                'metadata': {
                    'namespace': 'ns',
                    'workflow_id': 'wf_id',
                    'creationTimestamp': 'today',
                }
            },
            [],
        )
        self.assertIsNone(job['metadata']['id'])
        self.assertIsNone(job['status']['requestTime'])

    # get_tags

    def test_get_tags_ok(self):
        with (
            patch(
                'opentf.core.datasources._uses_inception', MagicMock(return_value=False)
            ),
            patch(
                'opentf.core.datasources.get_testcases',
                MagicMock(return_value=TESTCASES_DICT),
            ),
        ):
            tags = datasources.get_tags(TAG_EVENTS)
        self.assertEqual(3, len(tags))
        self.assertEqual({'linux', 'cypress', 'foo'}, tags.keys())
        self.assertEqual(
            {'cancelled': 0, 'error': 0, 'failure': 0, 'skipped': 0, 'success': 0},
            tags['linux']['status']['testCaseStatusSummary'],
        )
        self.assertEqual(1, tags['cypress']['status']['jobCount'])
        self.assertEqual(
            {'name': 'foo', 'workflow_id': 'iD', 'namespace': 'ns'},
            tags['foo']['metadata'],
        )

    def test_get_tags_ok_runs_on_as_string(self):
        with (
            patch(
                'opentf.core.datasources._uses_inception', MagicMock(return_value=False)
            ),
            patch(
                'opentf.core.datasources.get_testcases',
                MagicMock(return_value=TESTCASE_ONE_TAG),
            ),
        ):
            tags = datasources.get_tags(TAG_EVENTS_RUNS_ON_AS_STRING)
        self.assertIsNotNone(tags.get('linux'))
        self.assertEqual(0, tags['linux']['status']['testCaseCount'])
        self.assertEqual(0, tags['linux']['status']['testCaseStatusSummary']['failure'])

    def test_get_tags_value_error(self):
        self.assertRaisesRegex(
            datasources.DataSourceDataError,
            'No job events found in workflow. Cannot extract data for tags.',
            datasources.get_tags,
            {},
        )
