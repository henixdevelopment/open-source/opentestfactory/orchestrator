# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Cypress Provider unit tests.

- cypress tests
"""

import logging
import os
import unittest
import sys

from unittest.mock import MagicMock, patch

from copy import deepcopy
from requests import Response

sys.argv = ['dummy', '--trusted-authorities', os.devnull]
from opentf.plugins.cypress import main, implementation

import opentf.toolkit

mockresponse = Response()
mockresponse.status_code = 200


########################################################################
# Templates

PROVIDERCOMMAND = {
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'kind': 'ProviderCommand',
    'metadata': {
        'name': 'NAME',
        'workflow_id': 'WORKFLOW_ID',
        'job_id': 'JOB_ID',
        'job_origin': [],
        'step_id': 'STEP_ID',
        'step_origin': [],
    },
    'contexts': {'runner': {'os': 'linux'}},
    'step': {'uses': 'foo'},
    'runs-on': ['linux'],
}

PROVIDERMANIFESTCACHE = {
    ('cypress', 'execute', None): ({'test': {'required': True}}, False),
    ('cypress', 'params', None): (
        {
            'data': {
                'description': 'The data to use for the automated test.',
                'required': True,
            },
            'format': {
                'description': 'The format to use for the automated test data.',
                'required': True,
            },
        },
        True,
    ),
}

########################################################################


def _dispatch(handler, body):
    opentf.toolkit._dispatch_providercommand(main.plugin, handler, body)


class TestCypress(unittest.TestCase):
    """
    The class containing the Cypress provider unit test methods.
    """

    @classmethod
    def setUpClass(cls):
        logging.disable(logging.CRITICAL)

    @classmethod
    def tearDownClass(cls):
        logging.disable(logging.NOTSET)

    def setUp(self):
        app = main.plugin
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONTEXT'][opentf.toolkit.INPUTS_KEY] = PROVIDERMANIFESTCACHE
        app.config['CONTEXT'][opentf.toolkit.OUTPUTS_KEY] = {}
        self.app = app.test_client()
        self.assertFalse(app.debug)

    ###########################

    # cypress/cypress

    def test_cypress_ok(self):
        """
        In a Cypress context, checks that providing no inputs is fine.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {'uses': 'cypress/cypress'}
            _dispatch(implementation.cypress_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 1)
        self.assertEqual(msg[0]['run'].strip(), 'cypress run')

    def test_cypress_all_args(self):
        """
        In a Cypress context, checks that providing all inputs is fine.
        """
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/cypress',
                'with': {
                    'headless': 'true',
                    'browser': 'brave',
                    'reporter': 'junit',
                    'reporter-options': 'silent',
                    'env': 'foo=bar',
                    'config-file': '/path/484',
                },
            }
            _dispatch(implementation.cypress_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(1, len(msg))
        self.assertIn('--headless', msg[0]['run'])
        self.assertIn('--browser brave', msg[0]['run'])
        self.assertIn('--env foo=bar', msg[0]['run'])

    def test_cypress_extraoptions(self):
        """
        In a Cypress context, checks that providing extra options is fine.
        """
        mock = MagicMock(return_value=mockresponse)
        extra_options = 'yAdQQQ ZZ 123'
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/cypress',
                'with': {'extra-options': extra_options},
            }
            _dispatch(implementation.cypress_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 1)
        self.assertIn(extra_options, msg[0]['run'])

    # cypress/execute

    def test_execute_ok(self):
        """
        In a Cypress context, checks the presence of 3 steps
        and the correct parsing of test reference when execute action is called:
        """
        test_reference = 'cypressProject#cypress/toto/abcD.spec.js'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn(
            'cypressProject/target/surefire-reports/abcD-report.xml',
            msg[0]['run'],
        )
        self.assertIn('--spec "./cypress/toto/abcD.spec.js"', msg[1]['run'])
        self.assertIn('target/surefire-reports/abcD-report.xml', msg[1]['run'])
        self.assertTrue(msg[2]['continue-on-error'])

    def test_execute_with_old_test_reference(self):
        """
        In a Cypress context, checks the presence of 3 steps
        and the correct parsing of test reference in old format when execute action is called:
        """
        test_reference = 'cypressProject/cypress/whaa/bcdE.spec.js'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn(
            'cypressProject/target/surefire-reports/bcdE-report.xml',
            msg[0]['run'],
        )
        self.assertIn('--spec "./cypress/whaa/bcdE.spec.js"', msg[1]['run'])
        self.assertIn('target/surefire-reports/bcdE-report.xml', msg[1]['run'])

    def test_execute_with_custom_dir(self):
        """
        In a Cypress context, checks that the test reference containing
        custom Cypress project directory is parsed correctly when execute action is called:
        """
        test_reference = 'cypressProject/custom/dir#cypress/test.spec.js'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn(
            'cypressProject/custom/dir/target/surefire-reports/test-report.xml',
            msg[0]['run'],
        )
        self.assertIn(
            '--project "./custom/dir" --spec "./custom/dir/cypress/test.spec.js"',
            msg[1]['run'],
        )
        self.assertIn('target/surefire-reports/test-report.xml', msg[0]['run'])

    def test_execute_screenshots_dir_check(self):
        """
        In a Cypress context, checks that for execute action, custom screenshots
        directory name is correctly passed to run command and the create-archive step
        that follows.
        """
        test_reference = 'cypressProject#cypress/toto/abcD.spec.js'
        screens_folder = 'uuId'
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch('opentf.commons.uuid4', MagicMock(return_value=screens_folder)),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn('--spec "./cypress/toto/abcD.spec.js"', msg[1]['run'])
        self.assertIn(f'--config screenshotsFolder="{screens_folder}"', msg[1]['run'])
        self.assertIn('../abcD_screenshots.tar', msg[3]['with']['path'])
        self.assertIn(f'cypressProject/./{screens_folder}', msg[3]['working-directory'])

    def test_execute_screenshots_dir_creation_lin(self):
        """
        In a Cypress, in Linux context, checks that for execute action, custom screenshot
        directory created before passing to archive creation steps
        """
        test_reference = 'cypressProject#cypress/toto/dEfG.spec.js'
        screens_folder = 'uuId'
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch('opentf.commons.uuid4', MagicMock(return_value=screens_folder)),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn(f'--config screenshotsFolder="{screens_folder}"', msg[1]['run'])
        self.assertIn(f'mkdir -p "cypressProject/{screens_folder}"', msg[0]['run'])

    def test_execute_screenshots_dir_creation_win(self):
        """
        In a Cypress, in Windows context, checks that for execute action, custom screenshot
        directory is created before passing to archive creation steps
        """
        test_reference = 'cypressProject#cypress/toto/wInD.spec.js'
        screens_folder = 'uuId'
        mock = MagicMock(return_value=mockresponse)
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch('opentf.commons.uuid4', MagicMock(return_value=screens_folder)),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            command['contexts']['runner']['os'] = 'windows'
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn(f'--config screenshotsFolder="{screens_folder}"', msg[1]['run'])
        self.assertIn(f'mkdir "cypressProject\\{screens_folder}"', msg[0]['run'])
        command['contexts']['runner']['os'] = 'linux'

    def test_execute_extra_options_win(self):
        """
        In a Cypress/Windows context, checks the presence of CYPRESS_EXTRA_OPTIONS
        environment variable in the cypress run command.
        """
        test_reference = 'cypressProject#cypress/toto/abcD.spec.js'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = deepcopy(PROVIDERCOMMAND)
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            command['contexts']['runner']['os'] = 'windows'
            command['runs-on'] = ['windows']
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn(
            '--reporter-options "mochaFile=target/surefire-reports/abcD-report.xml" %CYPRESS_EXTRA_OPTIONS%',
            msg[1]['run'],
        )

    def test_execute_extra_options_lin(self):
        """
        In a Cypress/Linux context, checks the presence of CYPRESS_EXTRA_OPTIONS
        environment variable in the cypress run command.
        """
        test_reference = 'cypressProject#cypress/toto/abcD.spec.js'
        mock = MagicMock(return_value=mockresponse)
        with patch('opentf.toolkit.core.publish_providerresult', mock):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/execute',
                'with': {'test': test_reference},
            }
            _dispatch(implementation.execute_action, command)
        mock.assert_called_once()
        msg = mock.call_args[0][0]
        self.assertEqual(len(msg), 6)
        self.assertIn(
            '--reporter-options "mochaFile=target/surefire-reports/abcD-report.xml" $CYPRESS_EXTRA_OPTIONS',
            msg[1]['run'],
        )

    # cypress/params

    def test_params_ok(self):
        mock = MagicMock(return_value=mockresponse)
        mock_validate = MagicMock()
        mock_export = MagicMock()
        with (
            patch('opentf.toolkit.core.publish_providerresult', mock),
            patch(
                'opentf.plugins.cypress.implementation.core.validate_params_inputs',
                mock_validate,
            ),
            patch(
                'opentf.plugins.cypress.implementation.core.export_variables',
                mock_export,
            ),
        ):
            command = PROVIDERCOMMAND.copy()
            command['step'] = {
                'uses': 'cypress/params',
                'with': {
                    'format': 'wroom',
                    'data': {
                        'global': {'GLVAR': 'global'},
                        'test': {'TESTVAR': 'test'},
                    },
                },
            }
            _dispatch(implementation.param_action, command)
        mock_validate.assert_called_once_with(
            {
                'format': 'wroom',
                'data': {'global': {'GLVAR': 'global'}, 'test': {'TESTVAR': 'test'}},
            }
        )
        mock_export.assert_called_once_with(
            {'CYPRESS_GLVAR': 'global'}, {'CYPRESS_TESTVAR': 'test'}, verbatim=True
        )
        mock.assert_called_once()


if __name__ == '__main__':
    unittest.main()
