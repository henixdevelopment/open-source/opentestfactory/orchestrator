import requests

publication = {
    'kind': 'ProviderCommand',
    'apiVersion': 'opentestfactory.org/v1alpha1',
    'metadata': {
        'name': 'a',
        'workflow_id': 'b',
        'job_id': 'c',
        'job_origin': [],
        'step_id': 'd',
        'step_origin': [],
        'labels': {
            'opentestfactory.org/categoryPrefix': 'robotframework',
            'opentestfactory.org/category': 'robot',
            'opentestfactory.org/categoryVersion': 'v1',
        },
    },
    'step': {
        'uses': 'robotframework/robot',
    },
    'runs-on': 'windows',
    'contexts': {},
}

for _ in range(1000):
    requests.post('http://127.0.0.1:8765/publications', json=publication)
