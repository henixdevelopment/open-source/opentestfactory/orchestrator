# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An agent-based channel plugin."""

from typing import Any, Callable, NoReturn

from collections import defaultdict
from datetime import datetime, timedelta, timezone
from enum import Enum
from importlib.metadata import version
from io import BytesIO
from queue import Queue, Empty
from sys import exit
from threading import Event, Timer, Thread

from flask import request, send_file, Response, make_response


from opentf.commons import (
    SERVICECONFIG,
    EXECUTIONRESULT,
    EXECUTIONERROR,
    AGENTREGISTRATION,
    NOTIFICATION,
    DEFAULT_NAMESPACE,
    REASON_STATUS,
    validate_schema,
    make_status_response,
    annotate_response,
    make_dispatchqueue,
    make_uuid,
    make_event,
    authorizer,
    can_use_namespace,
    get_context_parameter,
)

from opentf.commons.selectors import match_selectors, prepare_selectors
from opentf.toolkit import make_plugin, run_plugin, watch_and_notify

from opentf.toolkit.channels import (
    DEFAULT_CHANNEL_LEASE,
    CHANNEL_REQUEST,
    CHANNEL_RELEASE,
    CHANNEL_TERMINATE,
    JobState,
    make_script,
    process_opentf_variables,
    process_output,
    process_upload,
    RUNNER_OS,
    SCRIPTPATH_DEFAULT,
)

from opentf.core.telemetry import TelemetryManager


########################################################################
## Constants

CHUNK_SIZE = 4096
WAIT_LIMIT = 5  # in seconds, the publish_and_hold threshold

SERVICE_NAME = 'opentf.orchestrator.agentchannel'


########################################################################
## Logging Helpers


def info(msg: str, *args) -> None:
    """Log info message."""
    plugin.logger.info(msg, *args)


def error(msg: str, *args) -> None:
    """Log error message."""
    plugin.logger.error(msg, *args)


def fatal(msg: str, *args) -> NoReturn:
    """Log error message and exit with error code 2."""
    error(msg, *args)
    exit(2)


def warning(msg: str, *args) -> None:
    """Log warning message."""
    plugin.logger.warning(msg, *args)


def debug(msg: str, *args) -> None:
    """Log debug message."""
    plugin.logger.debug(msg, *args)


########################################################################
## Telemetry


class AgentChannelMetrics(Enum):
    REGISTRATIONS_COUNTER = (
        'opentf.orchestrator.agentchannel.registrations',
        '1',
        'Registered agents count',
        None,
    )
    DEREGISTRATIONS_COUNTER = (
        'opentf.orchestrator.agentchannel.deregistrations',
        '1',
        'De-registered agents count',
        None,
    )
    REGISTRATIONS_CURRENT_GAUGE = (
        'opentf.orchestrator.agentchannel.registrations.current',
        '1',
        'Currently registered agents count',
        None,
    )
    CHANNEL_REQUESTS_COUNTER = (
        'opentf.orchestrator.agentchannel.channels.requests',
        '1',
        'Received environment requests count',
        None,
    )
    CHANNEL_OFFERS_COUNTER = (
        'opentf.orchestrator.agentchannel.channels.offers',
        '1',
        'Produced environment offers count',
        None,
    )
    CHANNELS_BUSY_GAUGE = (
        'opentf.orchestrator.agentchannel.channels.busy',
        '1',
        'Busy agents count',
        None,
    )
    CHANNELS_IDLE_GAUGE = (
        'opentf.orchestrator.agentchannel.channels.idle',
        '1',
        'Idle agents count',
        None,
    )
    CHANNELS_PENDING_GAUGE = (
        'opentf.orchestrator.agentchannel.channels.pending',
        '1',
        'Pending agents count',
        None,
    )
    CHANNELS_UNREACHABLE_GAUGE = (
        'opentf.orchestrator.agentchannel.channels.unreachable',
        '1',
        'Unreachable agents count',
        None,
    )
    CHANNELS_TAGS_GAUGE = (
        'opentf.orchestrator.agentchannel.channels.tags',
        '1',
        'Registered agents per tag count',
        None,
    )

    def __init__(
        self,
        metric_name: str,
        unit: str,
        description: str,
        callbacks: list[Callable] | None,
    ):
        self.metric_name = metric_name
        self.unit = unit
        self.description = description
        self.callbacks = callbacks


PHASES_GAUGES = {
    'BUSY': AgentChannelMetrics.CHANNELS_BUSY_GAUGE,
    'IDLE': AgentChannelMetrics.CHANNELS_IDLE_GAUGE,
    'PENDING': AgentChannelMetrics.CHANNELS_PENDING_GAUGE,
    'UNREACHABLE': AgentChannelMetrics.CHANNELS_UNREACHABLE_GAUGE,
}

UNREACHABLE_SEEN: dict[str, datetime] = {}


########################################################################
## Decorators


def debounce(wait: float):
    """Decorator that will postpone a functions
    execution until after wait seconds
    have elapsed since the last time it was invoked."""

    def decorator(fn):
        def debounced(*args, **kwargs):
            def call_it():
                fn(*args, **kwargs)

            try:
                debounced.t.cancel()
            except AttributeError:
                pass
            debounced.t = Timer(wait, call_it)
            debounced.t.start()

        return debounced

    return decorator


########################################################################
## Job Helpers

type AgentRegistration = dict[str, Any]
type ExecutionCommand = dict[str, Any]
type ExecutionResult = dict[str, Any]

AGENTS: dict[str, AgentRegistration] = {}
AGENTS_LEASES: dict[str, datetime] = {}
AGENTS_EVENTS: dict[str, Event] = {}

STEPS_QUEUES: dict[str, Queue[dict[str, str] | str]] = defaultdict(Queue)
JOBS: dict[str, dict[str, Any]] = {}
STEPS: dict[str, ExecutionCommand] = {}
JOBS_AGENTS: dict[str, str] = {}
JOBS_MASKS: dict[str, JobState] = defaultdict(JobState)
CHANNELS_AGENTS: dict[str, str | list[str]] = {}

LOCALSTORE: dict[str, dict[str, Any]] = defaultdict(dict)
PENDINGSTORE: dict[str, dict[str, Any]] = defaultdict(dict)

REQUESTS_QUEUE: Queue[tuple[dict[str, Any], list[str]]] = Queue()

DEFERRED_RESULT: dict[str, ExecutionResult] = {}
DEFERRED_PROCESSING_SENTINEL = '__DEFERRED_PROCESSING_SENTINEL__'


# Steps helpers


def list_job_steps(job_id: str) -> dict[str, dict[str, Any]]:
    """List all steps for job."""
    if job_id not in JOBS:
        JOBS[job_id] = {}
    return JOBS[job_id]


def add_job_step(job_id: str, command: dict[str, Any]) -> None:
    """Adding new step to existing job."""
    JOBS[job_id][command['metadata']['step_sequence_id']] = command


def get_os(agent_id: str) -> str:
    """Get agent os."""
    return (set(AGENTS[agent_id]['spec']['tags']) & RUNNER_OS).pop()


# Job agent helpers


def get_job_agent(job_id: str, workflow_id: str, channel_id: str) -> str | None:
    """Get agent_id for job.

    # Required parameters

    - job_id: a string
    - workflow_id: a string
    - channel_id: a string

    # Returned value

    An _agent ID_ or None.  An agent_id is a string.
    """
    if agent_id := JOBS_AGENTS.get(job_id):
        return agent_id

    if channel_id not in CHANNELS_AGENTS:
        error(f'Unexpected unknown channel {channel_id}, ignoring.')
        return None
    candidates = CHANNELS_AGENTS[channel_id]
    if isinstance(candidates, str):
        debug(
            'Agent %s tied to channel %s but not to job %s, ignoring.',
            candidates,
            channel_id,
            job_id,
        )
        return None

    for candidate_id in candidates:
        lease = AGENTS_LEASES.get(candidate_id)
        if lease and lease >= datetime.now():
            try:
                candidate = AGENTS[candidate_id]
                candidate['status']['currentJobID'] = job_id
                phase = candidate['status']['phase']
                TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[phase], -1)
                candidate['status']['phase'] = 'BUSY'
                JOBS_AGENTS[job_id] = candidate_id
                TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNELS_BUSY_GAUGE, 1)
                notify_available_channels()
                debug(
                    'Agent "%s" (%s) handles job %s from workflow %s.',
                    candidate['metadata']['name'],
                    candidate_id,
                    job_id,
                    workflow_id,
                )
                return candidate_id
            except KeyError:
                debug(f'Candidate agent {candidate_id} no longer exists, ignoring.')

    return None


def get_matching_agents(tags: list[str], namespace: str) -> list[str]:
    """List all agents capable of handling tags.

    # Required parameters

    - tags: a collection of strings
    - namespace: a string

    # Returned value

    A possibly empty list of _agentids_.  An agent_id is a string.

    Agents in non-accessible namespaces are excluded.
    """
    namespace = namespace.lower()
    requiredtags = set(tags)
    matching: list[str] = []
    for agent_id, agent in AGENTS.items():
        if requiredtags.issubset(set(agent['spec']['tags'])) and (
            any(
                ns.lower() in (namespace, '*')
                for ns in agent['metadata']['namespaces'].split(',')
            )
        ):
            matching.append(agent_id)
    if not matching:
        info(
            f'Could not find environment matching request {tags} in namespace "{namespace}".'
        )
    return matching


_REASON_STATUS = {r: str(s) for r, s in REASON_STATUS.items()}


def _update_agent_status(agent_id: str, reason: str) -> None:
    if (agent := AGENTS.get(agent_id)) and (status := _REASON_STATUS.get(reason)):
        agent['status']['communicationStatusSummary'][status] += 1


def make_agent_response(
    agent_id: str, reason: str, message: str, details: dict[str, Any] | None = None
) -> Response:
    """Make status response for agent.

    Update agent status if appropriate.
    """
    _update_agent_status(agent_id, reason)
    if details is None:
        return make_status_response(reason, message)
    return make_status_response(reason, message, details=details)


def deregister_agent(agent_id: str) -> None:
    """Deregister agent."""
    try:
        debug('Deregistering agent %s.', agent_id)
        agent = AGENTS[agent_id]
        phase = agent['status']['phase']
        tags = agent['spec']['tags']
        del AGENTS[agent_id]
        TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.DEREGISTRATIONS_COUNTER, 1)
        TELEMETRY_MANAGER.set_metric(
            AgentChannelMetrics.REGISTRATIONS_CURRENT_GAUGE, -1
        )
        TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[phase], -1)
        for tag in tags:
            TELEMETRY_MANAGER.set_metric(
                AgentChannelMetrics.CHANNELS_TAGS_GAUGE, -1, {'tag': tag}
            )
        if agent_id in UNREACHABLE_SEEN:
            del UNREACHABLE_SEEN[agent_id]
            TELEMETRY_MANAGER.set_metric(
                AgentChannelMetrics.CHANNELS_UNREACHABLE_GAUGE, -1
            )
    except KeyError:
        pass
    try:
        del AGENTS_LEASES[agent_id]
    except KeyError:
        pass
    try:
        del AGENTS_EVENTS[agent_id]
    except KeyError:
        pass
    try:
        del DEFERRED_RESULT[agent_id]
    except KeyError:
        pass


def touch_agent(agent_id: str) -> None:
    """Refresh agent activity status."""
    if agent := AGENTS.get(agent_id):
        status = agent['status']
        status['lastCommunicationTimestamp'] = datetime.now(timezone.utc).isoformat()
        status['communicationCount'] += 1
        if agent_id in UNREACHABLE_SEEN:
            del UNREACHABLE_SEEN[agent_id]
            TELEMETRY_MANAGER.set_metric(
                AgentChannelMetrics.CHANNELS_UNREACHABLE_GAUGE, -1
            )
            status['phase'] = 'IDLE'


def is_alive(agent_id: str) -> bool:
    """Check if an agent has shown recent activity.

    A busy agent is considered alive, as is an agent that has reached
    the handler within the liveness limit.

    # Returned value

    A boolean.  True if agent has shown recent activity, False
    otherwise (including if the agent is not known).
    """
    if not (agent := AGENTS.get(agent_id)):
        debug('Agent %s not found in registered agents list.', agent_id)
        return False

    if agent_id in JOBS_AGENTS.values():
        return True

    limit = (
        datetime.now(timezone.utc)
        - timedelta(seconds=agent['spec'].get('liveness_probe', LIVENESS_LIMIT))
    ).isoformat()
    if agent['status']['lastCommunicationTimestamp'] >= limit:
        return True
    debug(
        'No news from agent %s (last contact on %s).',
        agent_id,
        agent['status']['lastCommunicationTimestamp'],
    )
    return False


def is_available(agent_id: str) -> bool:
    """Check if agent has a lease or is busy handling a job."""
    if agent_id in JOBS_AGENTS.values():
        return False
    if lease := AGENTS_LEASES.get(agent_id):
        return lease < datetime.now()
    return True


def _get_phase(agent_id: str) -> str | None:
    """Get agent phase.

    # Returned value

    One of `'BUSY'`, `'IDLE'`, `'PENDING'`, `'UNREACHABLE'`, or None.
    """
    if agent_id in JOBS_AGENTS.values():
        return 'BUSY'
    if is_alive(agent_id):
        return 'IDLE' if is_available(agent_id) else 'PENDING'
    if agent_id not in UNREACHABLE_SEEN:
        UNREACHABLE_SEEN[agent_id] = datetime.now()
        phase = AGENTS[agent_id]['status']['phase']
        TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[phase], -1)
        TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNELS_UNREACHABLE_GAUGE, 1)
        AGENTS[agent_id]['status']['phase'] = 'UNREACHABLE'
    elif UNREACHABLE_TIMEOUT_MINUTES and UNREACHABLE_SEEN[
        agent_id
    ] < datetime.now() - timedelta(minutes=UNREACHABLE_TIMEOUT_MINUTES):
        deregister_agent(agent_id)
        return None
    return 'UNREACHABLE'


def make_offers(metadata: dict[str, Any], candidates: list[str]) -> None:
    """Make at least one offer.

    Currently makes exactly one offer, assuming the tags are valid.
    This may change at any time.

    # Required parameters

    - metadata: a dictionary
    - candidates: a non-empty list of strings

    # Returned value

    None.
    """
    for candidate_id in candidates:
        if not is_available(candidate_id):
            continue
        if not is_alive(candidate_id):
            continue
        channel_id = metadata['channel_id'] = make_uuid()
        candidate = AGENTS[candidate_id]
        metadata['channelhandler_id'] = CHANNELHANDLER_ID
        metadata['channel_tags'] = candidate['spec']['tags']
        metadata['channel_os'] = get_os(candidate_id)
        metadata['channel_temp'] = candidate['spec'].get(
            'script_path', SCRIPTPATH_DEFAULT[metadata['channel_os']]
        )
        metadata['channel_lease'] = DEFAULT_CHANNEL_LEASE
        if hooks := plugin.config['CONFIG'].get('hooks'):
            metadata.setdefault('annotations', {})['hooks'] = hooks
        AGENTS_LEASES[candidate_id] = datetime.now() + timedelta(
            seconds=DEFAULT_CHANNEL_LEASE
        )
        phase = candidate['status']['phase']
        TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[phase], -1)
        candidate['status']['phase'] = 'PENDING'
        CHANNELS_AGENTS[channel_id] = [candidate_id]
        prepare = make_event(EXECUTIONRESULT, metadata=metadata, status=0)
        DISPATCH_QUEUE.put(prepare)
        notify_available_channels()
        TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNEL_OFFERS_COUNTER, 1)
        TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNELS_PENDING_GAUGE, 1)
        break
    else:
        info(
            f'Could not find valid candidate for job {metadata['job_id']}: compatible agents are either busy or unreachable.'
        )


@debounce(2)
def notify_available_channels():
    """Notify channels state"""
    agents = list(AGENTS.values())
    DISPATCH_QUEUE.put(
        make_event(
            NOTIFICATION,
            metadata={
                'name': f'channels available from channel handler {CHANNELHANDLER_ID}',
                'workflow_id': 'n/a',
            },
            spec={
                'channel.handler': {
                    'channelhandler_id': CHANNELHANDLER_ID,
                    'channels': [
                        {
                            'apiVersion': 'opentestfactory.org/v1alpha1',
                            'kind': 'Channel',
                            'metadata': {
                                'name': agent['metadata']['name'],
                                'namespaces': agent['metadata']['namespaces'],
                            },
                            'spec': {
                                'tags': agent['spec']['tags'],
                            },
                            'status': {
                                'phase': agent['status']['phase'],
                                'currentJobID': agent['status'].get('currentJobID'),
                            },
                        }
                        for agent in agents
                    ],
                }
            },
        )
    )


# os helpers


def publish_and_hold(result: Any, agent_id: str) -> bool:
    """Publish publication and hold for new command.

    Holds for `WAIT_LIMIT` seconds for new command to be received.

    # Returned value

    A boolean.  True if a new command was received, False otherwise.
    """
    if (evt := AGENTS_EVENTS.get(agent_id)) and evt.is_set():
        evt.clear()
    else:
        evt = AGENTS_EVENTS[agent_id] = Event()
    DISPATCH_QUEUE.put(result)
    return evt.wait(timeout=WAIT_LIMIT)


def publish_preparedresult(agent_id: str) -> None:
    """Publish prepared result if all deferred actions done.

    Holds for `WAIT_LIMIT` seconds for new command to be received.
    """
    if not DEFERRED_RESULT.get(agent_id):
        return

    if not LOCALSTORE.get(agent_id) and not PENDINGSTORE.get(agent_id):
        debug(
            'Pending attachments for agent %s received, publishing results.', agent_id
        )
        result = DEFERRED_RESULT[agent_id]

        if result['metadata'].get('upload') == 0:
            workflow_result = process_upload(result)
            DISPATCH_QUEUE.put(workflow_result)
        process_opentf_variables(result)
        try:
            del DEFERRED_RESULT[agent_id]
        except KeyError:
            warning(f'Could not remove prepared result for agent {agent_id}.')
        publish_and_hold(result, agent_id)


def prepare_executioncommand(command: ExecutionCommand) -> None:
    """Prepare command.

    Prepares command for execution via an agent.  Signals command
    availability to awaiting agent call.
    """
    metadata = command['metadata']
    job_id = metadata['job_id']
    channel_id = metadata['channel_id']

    agent_id = CHANNELS_AGENTS[channel_id]
    if isinstance(agent_id, list):
        # No agent set for channel yet
        if (
            agent_id := get_job_agent(job_id, metadata['workflow_id'], channel_id)
        ) is None:
            return
        CHANNELS_AGENTS[channel_id] = agent_id

    metadata['channel_os'] = get_os(agent_id)
    metadata['channel_temp'] = AGENTS[agent_id]['spec'].get(
        'script_path', SCRIPTPATH_DEFAULT[metadata['channel_os']]
    )
    script_path, script_content, script_command = make_script(
        command, AGENTS[agent_id]['spec'].get('script_path'), get_os(agent_id)
    )

    STEPS[agent_id] = command

    if (v_agent := AGENTS[agent_id]['metadata'].get('version')) and v_agent >= '1.9':
        evt = AGENTS_EVENTS.get(agent_id)
        STEPS_QUEUES[agent_id].put(
            {
                'kind': 'run',
                'filename': script_path,
                'content': script_content,
                'command': script_command,
            }
        )
    else:
        script_uuid = make_uuid()
        LOCALSTORE[agent_id][script_uuid] = {
            'content': script_content,
            'filename': script_path,
        }
        STEPS_QUEUES[agent_id].put(
            {'kind': 'put', 'path': script_path, 'file_id': script_uuid, 'root': ''}
        )
        evt = AGENTS_EVENTS.get(agent_id)
        STEPS_QUEUES[agent_id].put({'kind': 'exec', 'command': script_command})
    if evt:
        evt.set()


def release_resources(agent_id: str, job_id: str, channel_id: str) -> None:
    """Release resources after tear-down."""
    try:
        del STEPS_QUEUES[agent_id]
    except KeyError:
        pass
    try:
        del PENDINGSTORE[agent_id]
    except KeyError:
        pass
    try:
        del AGENTS_LEASES[agent_id]
    except KeyError:
        pass
    try:
        del JOBS_AGENTS[job_id]
    except KeyError:
        pass
    try:
        del JOBS[job_id]
    except KeyError:
        pass
    try:
        del JOBS_MASKS[job_id]
    except KeyError:
        pass
    try:
        del CHANNELS_AGENTS[channel_id]
    except KeyError:
        pass
    try:
        del AGENTS_EVENTS[agent_id]
    except KeyError:
        pass
    try:
        del AGENTS[agent_id]['status']['currentJobID']
    except KeyError:
        pass
    try:
        AGENTS[agent_id]['status']['phase'] = 'IDLE'
    except KeyError:
        pass
    debug('Agent %s released from handling job %s.', agent_id, job_id)
    notify_available_channels()
    TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNELS_IDLE_GAUGE, 1)
    TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNELS_BUSY_GAUGE, -1)


def prepare_result(body: dict[str, Any], agent_id: str) -> bool:
    """Prepare result.

    # Returned value

    A boolean.  True if the execution result was sent, False if it is
    deferred, awaiting attachments.
    """

    def _attach(remote_path: str, destination_url: str):
        file_uuid = make_uuid()
        PENDINGSTORE[agent_id][file_uuid] = destination_url
        STEPS_QUEUES[agent_id].put(
            {'kind': 'get', 'path': remote_path, 'file_id': file_uuid}
        )
        debug('Awaiting attachment %s from agent %s.', file_uuid, agent_id)

    def _put(remote_path: str, source_url: str):
        file_uuid = make_uuid()
        LOCALSTORE[agent_id][file_uuid] = {
            'filepath': source_url,
            'filename': source_url.split('_', 2)[-1],
        }
        STEPS_QUEUES[agent_id].put(
            {
                'kind': 'put',
                'path': remote_path,
                'file_id': file_uuid,
                'root': job_id,
            }
        )

    job_id, step_sequence_id = (
        STEPS[agent_id]['metadata']['job_id'],
        STEPS[agent_id]['metadata']['step_sequence_id'],
    )

    result = process_output(
        STEPS[agent_id],
        body['exit_status'],
        body['stdout'],
        body['stderr'],
        JOBS_MASKS[job_id],
        _attach,
        _put,
    )

    if result.get('attachments') or LOCALSTORE[agent_id]:
        STEPS_QUEUES[agent_id].put(DEFERRED_PROCESSING_SENTINEL)
        DEFERRED_RESULT[agent_id] = result
        return False

    if step_sequence_id == CHANNEL_RELEASE:
        DISPATCH_QUEUE.put(result)
        release_resources(agent_id, job_id, agent_id)
    else:
        publish_and_hold(result, agent_id)

    return True


def prepare_offers() -> None:
    """Prepare offers.

    Offers are serialized so that no race condition occurs and so that
    a 'fair' allocation strategy can be defined.
    """
    while True:
        try:
            metadata, candidates = REQUESTS_QUEUE.get()
            make_offers(metadata, candidates)
        except Exception as err:
            error(f'Internal error while making offer: {err}.')


def process_executioncommand(body: ExecutionCommand) -> None:
    """Process an incoming execution command.

    # Required parameters

    - body: a dictionary (a valid ExecutionCommand)
    """
    metadata = body['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']

    if step_sequence_id == CHANNEL_REQUEST:
        TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNEL_REQUESTS_COUNTER, 1)
        # A new job, can we make an offer?
        candidates = get_matching_agents(
            body['runs-on'], metadata.get('namespace', DEFAULT_NAMESPACE)
        )
        if candidates:
            REQUESTS_QUEUE.put((metadata, candidates))
        return

    channel_id = metadata['channel_id']
    if channel_id not in CHANNELS_AGENTS:
        return

    if step_sequence_id == CHANNEL_TERMINATE:
        STEPS_QUEUES[CHANNELS_AGENTS[channel_id]].put({'kind': 'cancel'})
        return

    if step_sequence_id in list_job_steps(job_id):
        warning(f'Step {step_sequence_id} already received for job {job_id}.')
        return

    prepare_executioncommand(body)


########################################################################
## Main

# Channel plugin entrypoint


plugin = make_plugin(
    name='agentchannel',
    description='Create and start an agent-based channel plugin.',
    channel=process_executioncommand,
    schema=SERVICECONFIG,
    args=[CHANNELS_AGENTS],
)

try:
    TELEMETRY_MANAGER = TelemetryManager(SERVICE_NAME, AgentChannelMetrics)
except Exception as err:
    fatal('Failed to initialize TelemetryManager for %s: %s.', SERVICE_NAME, str(err))


CHANNELHANDLER_ID = make_uuid()
DISPATCH_QUEUE: Queue[dict[str, Any]] = make_dispatchqueue(plugin)
UNREACHABLE_TIMEOUT_MINUTES: int = get_context_parameter(
    plugin, 'unreachable_deregistration_timeout_minutes'
)
LIVENESS_LIMIT: int = get_context_parameter(plugin, 'liveness_limit_seconds')

# Agents registration


@plugin.route('/agents', methods=['POST'])
@authorizer(resource='agents', verb='create')
def register_agent() -> Response:
    """Register a new agent.

    # Registration format

    ```yaml
    apiVersion: opentestfactory.org/v1alpha1
    kind: AgentRegistration
    metadata:
      name: {name}
      namespaces: {name}[,{name}...]
    spec:
      tags:
      - {tag1}
      - ...
      script_path: {path}  # not required
    ```

    # Returned value

    A _status_.  A status is a dictionary with the following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the registration is successful (`.status` is `'Success'`),
    `.details.uuid` is the agent ID.
    """
    if not request.is_json:
        return make_status_response('BadRequest', 'Not a JSON document.')

    try:
        body: dict[str, Any] = request.get_json() or {}
    except Exception as err:
        return make_status_response('BadRequest', f'Not a valid JSON document: {err}.')

    valid, extra = validate_schema(AGENTREGISTRATION, body)
    if not valid:
        return make_status_response(
            'Invalid',
            'Not a valid AgentRegistration manifest.',
            details={'error': str(extra)},
        )

    if len(set(body['spec']['tags']) & RUNNER_OS) != 1:
        return make_status_response(
            'Invalid',
            'AgentRegistration manifest tags must include one and only one of "%s"'
            % '\", \"'.join(RUNNER_OS),
        )

    body['metadata'].setdefault('namespaces', DEFAULT_NAMESPACE)
    for namespace in body['metadata']['namespaces'].split(','):
        if not can_use_namespace(namespace, resource='agents', verb='create'):
            return make_status_response(
                'Forbidden',
                f'Token cannot create agent in namespace {namespace}.',
            )
    body['metadata']['creationTimestamp'] = datetime.now(timezone.utc).isoformat()
    body['status'] = {
        'communicationCount': 0,
        'lastCommunicationTimestamp': body['metadata']['creationTimestamp'],
        'communicationStatusSummary': defaultdict(int),
        'phase': 'IDLE',
    }
    body['metadata']['agent_id'] = sid = make_uuid()
    AGENTS[sid] = body
    notify_available_channels()

    TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.REGISTRATIONS_COUNTER, 1)
    TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.REGISTRATIONS_CURRENT_GAUGE, 1)
    TELEMETRY_MANAGER.set_metric(AgentChannelMetrics.CHANNELS_IDLE_GAUGE, 1)
    for tag in body['spec']['tags']:
        TELEMETRY_MANAGER.set_metric(
            AgentChannelMetrics.CHANNELS_TAGS_GAUGE, 1, {'tag': tag}
        )

    msg = f'Agent "{body['metadata']['name']}" successfully registered (id={sid}, tags={':'.join(body['spec']['tags'])}).'
    info(msg)
    return make_status_response(
        'Created', msg, details={'uuid': sid, 'version': version('opentf-orchestrator')}
    )


@plugin.route('/agents', methods=['PATCH'])
@authorizer(resource='agents', verb='update')
def refresh_agents() -> Response:
    """Refresh agents status."""
    if not request.is_json:
        return make_status_response('BadRequest', 'Not a JSON document.')

    try:
        body: dict[str, Any] = request.get_json() or {}
    except Exception as err:
        return make_status_response('BadRequest', f'Not a valid JSON document: {err}.')

    for agent_id in body:
        touch_agent(agent_id)

    return make_status_response('OK', 'Agents status refreshed.')


@plugin.route('/agents', methods=['GET'])
@authorizer(resource='agents', verb='list')
def list_agents() -> Response:
    """Return list of agents.

    # Returned value

    A dictionary with the following entries:

    - kind: a string
    - apiVersion: a string (`'v1'`)
    - items: a list of dictionaries (agent registrations)
    """
    try:
        fieldselector, labelselector = prepare_selectors(request.args)
    except ValueError as err:
        return make_status_response('Invalid', str(err))

    items = [
        agent
        for agent in AGENTS.values()
        if any(
            can_use_namespace(ns, resource='agents', verb='list') or ns == '*'
            for ns in agent['metadata']['namespaces'].split(',')
        )
        and match_selectors(agent, fieldselector, labelselector)
    ]
    return annotate_response(
        make_response(
            {'apiVersion': 'v1', 'kind': 'AgentRegistrationsList', 'items': items}
        ),
        processed=['fieldSelector', 'labelSelector'],
    )


@plugin.route('/agents/<agent_id>', methods=['DELETE'])
@authorizer(resource='agents', verb='delete')
def cancel_agent(agent_id: str) -> Response:
    """Deregister agent.

    # Returned value

    A _status_.  A status is a dictionary with the following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)

    If the deregistration is successful, `.status` is `'Success'`.  If
    `agent_id` is not known, `.status` is `'Failure'` and `reason` is
    `'NotFound'`.
    """
    if not (body := AGENTS.get(agent_id)):
        return make_status_response('NotFound', f'Agent {agent_id} not known.')

    metadata, spec = body['metadata'], body['spec']
    if not all(
        can_use_namespace(ns, resource='agents', verb='delete') or ns == '*'
        for ns in metadata['namespaces'].split(',')
    ):
        return make_status_response(
            'Forbidden',
            'Token not allowed to deregister channel: not in accessible namespaces.',
        )

    msg = f'Agent "{metadata['name']}" de-registered (id={agent_id}, tags={':'.join(spec['tags'])}).'
    deregister_agent(agent_id)
    notify_available_channels()
    info(msg)
    return make_status_response('OK', f'Agent {agent_id} de-registered.')


# Agents endpoints


@plugin.route('/agents/<agent_id>', methods=['GET'])
@authorizer(resource='agents', verb='get')
def get_command(agent_id: str) -> Response:
    """Get command, if any.

    # Required parameter

    - agent_id: a string (a registered agent id)

    # Returned value

    A _status_.  `NotFound` if the agent id is not known.  `NoContent` if the
    agent id is known but there is no pending commands for it.  `OK` if at
    least one command is available.

    If `OK`, `details` contains the command to process.  The command is either
    an `execute` command, a `get` command, or a `put` command.
    """
    if agent_id not in AGENTS:
        return make_status_response('NotFound', f'Unknown agent {agent_id}.')

    touch_agent(agent_id)
    try:
        command = STEPS_QUEUES[agent_id].get(block=False)
    except Empty:
        return make_agent_response(
            agent_id, 'NoContent', 'Nothing to do, please try again later.'
        )

    if command == DEFERRED_PROCESSING_SENTINEL:
        publish_preparedresult(agent_id)
        return get_command(agent_id)

    return make_agent_response(agent_id, 'OK', 'Command available.', details=command)


@plugin.route('/agents/<agent_id>', methods=['POST'])
@authorizer(resource='agents', verb='update')
def set_execution_result(agent_id: str) -> Response:
    """Handling execution result.

    If there are attachments, defer publishing until they are all
    received.

    The request body is a JSON object.

    If no execution error occurred, it contains the following fields:

    - `stdout`: a possibly empty list of strings
    - `stderr`: a possibly empty list of strings
    - `exit_status`: an integer

    If an execution error occurred, it contains the following field:

    - `details`: a dictionary

    # Required parameters

    - agent_id: a string (a registered agent id)

    # Returned value

    A _status_.
    """
    if agent_id not in AGENTS:
        return make_status_response('NotFound', f'Unknown agent {agent_id}.')

    touch_agent(agent_id)
    try:
        body: dict[str, Any] = request.get_json()
    except Exception as err:
        return make_agent_response(
            agent_id, 'BadRequest', f'Could not parse body: {err}.'
        )

    if not isinstance(body, dict):
        return make_agent_response(agent_id, 'Invalid', 'Body is not an object.')

    if 'details' in body:
        # something went wrong while processing command or its side effects
        # (attachments & put-files deferred processing)
        if prepare := DEFERRED_RESULT.get(agent_id):
            prepare['status'] = 2
            prepare.setdefault('logs', [])
            prepare['logs'].append(f'ERROR,{body["details"].get("error")}')
            try:
                del DEFERRED_RESULT[agent_id]
            except KeyError:
                pass
            try:
                del PENDINGSTORE[agent_id]
            except KeyError:
                pass
            try:
                del LOCALSTORE[agent_id]
            except KeyError:
                pass
        else:
            prepare = make_event(
                EXECUTIONERROR,
                metadata=STEPS[agent_id]['metadata'],
                details=body['details'],
            )

        try:
            del STEPS_QUEUES[agent_id]
        except KeyError:
            pass
        DISPATCH_QUEUE.put(prepare)
        return make_agent_response(agent_id, 'OK', 'Propagating execution error.')

    try:
        if sent := prepare_result(body, agent_id):
            debug('Results from agent %s published.', agent_id)
        else:
            debug(
                'Awaiting attachments from agent %s before publishing results.',
                agent_id,
            )
        return make_agent_response(
            agent_id,
            'OK',
            'Awaiting attachments.' if not sent else 'Results published.',
        )
    except Exception as err:
        prepare = make_event(
            EXECUTIONERROR,
            metadata=STEPS[agent_id]['metadata'],
            details={'error': str(err)},
        )
        DISPATCH_QUEUE.put(prepare)
        return make_agent_response(
            agent_id, 'Invalid', f'An exception occurred while handling result: {err}.'
        )


@plugin.route('/agents/<agent_id>/files/<file_id>', methods=['GET'])
@authorizer(resource='agents', verb='update')
def get_file(agent_id: str, file_id: str) -> Response:
    """Return file content.

    Used by agents to copy orchestrator-side files to their local
    execution environment.

    Will publish ExecutionResult if all pending attachments have been
    received.

    # Required parameters

    - agent_id: a string
    - file_id: a string

    # Returned value

    A _Response_ object representing a _status_ or a file.  If `code` is
    not 200, a status.  A file otherwise.
    """
    touch_agent(agent_id)
    if agent_id not in LOCALSTORE:
        return make_agent_response(
            agent_id, 'NotFound', f'No file for agent_id {agent_id}.'
        )
    if file_id not in LOCALSTORE[agent_id]:
        return make_agent_response(
            agent_id, 'NotFound', f'File {file_id} not found for agent {agent_id}.'
        )

    if 'content' in LOCALSTORE[agent_id][file_id]:
        try:
            debug(
                'Sending command file "%s" to agent %s.',
                LOCALSTORE[agent_id][file_id]['filename'],
                agent_id,
            )
            what = send_file(
                BytesIO(LOCALSTORE[agent_id][file_id]['content'].encode()),
                download_name=LOCALSTORE[agent_id][file_id]['filename'],
                mimetype='application/octet-stream',
            )
            _update_agent_status(agent_id, 'OK')
        except OSError as err:
            msg = f'Could not deliver file {file_id}: {err}.'
            error(msg)
            what = make_agent_response(agent_id, 'InternalError', msg)
    elif 'filepath' in LOCALSTORE[agent_id][file_id]:
        try:
            debug(
                'Sending file %s to agent %s.',
                LOCALSTORE[agent_id][file_id]['filepath'],
                agent_id,
            )
            what = send_file(
                LOCALSTORE[agent_id][file_id]['filepath'],
                download_name=LOCALSTORE[agent_id][file_id]['filename'],
                mimetype='application/octet-stream',
            )
            _update_agent_status(agent_id, 'OK')
        except OSError as err:
            msg = f'Could not deliver file {file_id}: {err}.'
            error(msg)
            what = make_agent_response(agent_id, 'InternalError', msg)
    else:
        msg = (
            'Invalid data in LOCALSTORE: contains neither "content" nor "filepath" key.'
        )
        warning(msg)
        what = make_agent_response(agent_id, 'InternalError', msg)
    try:
        del LOCALSTORE[agent_id][file_id]
    except KeyError:
        warning(f'Could not remove entry {file_id} from local store.')

    return what


@plugin.route('/agents/<agent_id>/files/<file_id>', methods=['PUT', 'POST'])
@authorizer(resource='agents', verb='update')
def add_file(agent_id: str, file_id: str) -> Response:
    """Add file content.

    Used by agents to copy local execution environment files to the
    orchestrator side, so that plugins can process them.

    # Required parameters

    - agent_id: a string
    - file_id: a string

    # Returned value

    A _status_.  A status is a dictionary.

    - `OK` if the file was properly received ;
    - `NotFound` if `agent_id` is not expecting files or `file_id` is
      not expected ;
    - `BadRequest` if something went wrong while receiving file.
    """
    touch_agent(agent_id)
    if agent_id not in PENDINGSTORE:
        return make_agent_response(
            agent_id, 'NotFound', f'No attachment expected from agent_id {agent_id}.'
        )
    if file_id not in PENDINGSTORE[agent_id]:
        return make_agent_response(
            agent_id,
            'NotFound',
            f'Attachment {file_id} not found for agent {agent_id}.',
        )
    try:
        local_path = PENDINGSTORE[agent_id][file_id]
        file_size = 0
        with open(local_path, 'wb') as out:
            while True:
                chunk = request.stream.read(CHUNK_SIZE)
                if (chunk_size := len(chunk)) == 0:
                    break
                out.write(chunk)
                file_size += chunk_size
        debug(
            'Got attachment %s (%d bytes) from agent %s, locally stored as %s.',
            file_id,
            file_size,
            agent_id,
            local_path,
        )
        try:
            del PENDINGSTORE[agent_id][file_id]
        except KeyError:
            warning(f'Could not remove entry {file_id} from pending store.')

    except Exception as err:
        msg = f'Failed to get attachment {file_id}: {err}.'
        error(msg)
        return make_agent_response(agent_id, 'BadRequest', msg)

    return make_agent_response(agent_id, 'OK', f'Content received for {file_id}.')


# Main


def main():
    """Start the agent-based channel plugin."""
    watch_and_notify(plugin, _get_phase, AGENTS, notify_available_channels)

    try:
        debug('Starting offers handling thread.')
        Thread(target=prepare_offers, daemon=True).start()
    except Exception as err:
        fatal('Could not start offers handling thread: %s.', str(err))

    info('Channel handler ID: %s.', CHANNELHANDLER_ID)
    run_plugin(plugin)


if __name__ == '__main__':
    main()
