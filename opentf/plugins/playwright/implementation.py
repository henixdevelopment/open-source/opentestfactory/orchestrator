# Copyright (c) 2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Categories implementation for the playwright provider.

The following categories are provided:

- execute
- params
- npx
"""

import posixpath
from typing import Optional

from shlex import quote

from opentf.toolkit import core


########################################################################
## Constants

EXECUTE_CATEGORY = 'playwright/execute'
PARAM_CATEGORY = 'playwright/params'
NPX_CATEGORY = 'playwright/npx'

SQUASHTM_FORMAT = 'tm.squashtest.org/params@v1'

HTML_REPORTER = 'html'
HTML_REPORT_PATH = 'playwright-report'
HTML_REPORT_TYPE = 'application/vnd.opentestfactory.playwright-output+html'

XML_REPORTER = 'junit'
XML_REPORT_TYPE = 'application/vnd.opentestfactory.playwright-output+xml'
XML_REPORT_PATH = f'{HTML_REPORT_PATH}/pw_junit_report.xml'

ENV_SETUP = {
    'SERVE_HTML': (
        'PW_TEST_HTML_REPORT_OPEN',
        'never',
    ),  # do not serve HTML report in browser
    HTML_REPORTER: ('PLAYWRIGHT_HTML_REPORT', HTML_REPORT_PATH),
    XML_REPORTER: ('PLAYWRIGHT_JUNIT_OUTPUT_NAME', XML_REPORT_PATH),
}

REPORTERS = (HTML_REPORTER, XML_REPORTER)

########################################################################
## Steps

ATTACH_XML_REPORT = {
    'uses': 'actions/get-file@v1',
    'with': {'path': XML_REPORT_PATH, 'type': XML_REPORT_TYPE},
    'continue-on-error': True,
}

ATTACH_HTML_REPORT = {
    'uses': 'actions/get-file@v1',
    'with': {'path': f'{HTML_REPORT_PATH}/index.html', 'type': HTML_REPORT_TYPE},
    'continue-on-error': True,
}

REPORTERS_STEPS = {HTML_REPORTER: ATTACH_HTML_REPORT, XML_REPORTER: ATTACH_XML_REPORT}


########################################################################
## Helpers


def _quote(param: str) -> str:
    if not param:
        return ''
    if param[0] == param[-1] and param[0] in ('"', "'"):
        param = param[1:-1]
    if not core.runner_on_windows():
        return quote(param)
    return f'"{param}"'


def _normalize_path(path: str) -> str:
    if ' ' in path:
        path = f'"{path}"'
    return posixpath.normpath(path)


def _make_option(name: str, value: Optional[str]) -> str:
    return f' --{name}={value}' if value else ''


def _make_cmd(reporters, options, extra):
    env = ['='.join(ENV_SETUP[target]) for target in ['SERVE_HTML', *reporters]]
    cmd = (
        '\n'.join(
            f'{"set" if core.runner_on_windows() else "export"} {var}' for var in env
        )
        + '\n'
    )
    # The HTML reporter cleans the target directory, so it should be first
    reports = _make_option('reporter', ','.join(sorted(reporters)))
    return f'{cmd}npx playwright test {options}{reports} {extra}'


########################################################################
## 'execute' action


def execute_action(inputs):
    """Process 'execute' action.

    `execute` actions have a mandatory `test` input:

    ```yaml
    - uses: playwright/execute@v1
      with:
        test: foobar
    ```

    `test` is of the form `{project}[/{directory}]#{specfile}[#{grep}]`.
    """
    dir_path, _, tail = inputs['test'].partition('#')
    spec_file, _, grep = tail.partition('#')

    test_path = _normalize_path(f'{dir_path}/{spec_file}')
    grep_option = _make_option('grep', _quote(grep))

    extra_options = (
        '%PLAYWRIGHT_EXTRA_OPTIONS%'
        if core.runner_on_windows()
        else '$PLAYWRIGHT_EXTRA_OPTIONS'
    )

    return [
        {
            'run': f"{core.delete_directory(HTML_REPORT_PATH)}\n{_make_cmd(REPORTERS, f'{test_path}{grep_option}', extra_options)}",
            'continue-on-error': True,
        },
        ATTACH_HTML_REPORT,
        ATTACH_XML_REPORT,
    ]


########################################################################
## 'param' action


def param_action(inputs):
    """Process 'params' actions.

    `params` actions have mandatory `data` and `format` inputs:

    ```yaml
    - uses: playwright/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT.

    `data` can have two keys:

    * `global` for defining global parameters
    * `test` for defining test parameters
    """
    core.validate_params_inputs(inputs)
    data = inputs['data']
    return core.export_variables(data.get('test'), data.get('global'), verbatim=True)


########################################################################
## 'npx' action


def npx_action(inputs):
    """Process 'npx' action.

    ```yaml
    - uses: playwright/npx@v1
      with:
        test: path/to/spec.file.spec.ts
        grep: test to run
        browser: chrome
        reporters: [junit,html]
        extra-options: --debug
    ```
    """
    cleanup_steps = attach_steps = []
    filtered_reporters = {}
    if reporters := inputs.get('reporters', {}):
        reporters = {reporters} if isinstance(reporters, str) else set(reporters)
        if filtered_reporters := reporters.intersection(REPORTERS_STEPS):
            cleanup_steps = [{'run': core.delete_directory(HTML_REPORT_PATH)}]
            attach_steps = [REPORTERS_STEPS[r] for r in filtered_reporters]
        else:
            core.warning(
                'No valid reporters found. Accepted: `junit`, `html`, provided: `%s`.',
                ', '.join(reporters),
            )

    test = _normalize_path(inputs['test'])
    grep = _make_option('grep', _quote(inputs.get('grep', '')))
    browser = _make_option('browser', inputs.get('browser'))
    extra_options = inputs.get('extra-options', '')

    run_steps = [
        {
            'run': _make_cmd(
                filtered_reporters, f'{test}{grep}{browser}', extra_options
            ),
            'continue-on-error': True,
        }
    ]

    return cleanup_steps + run_steps + attach_steps


########################################################################
# known categories

KNOWN_CATEGORIES = {
    EXECUTE_CATEGORY: execute_action,
    PARAM_CATEGORY: param_action,
    NPX_CATEGORY: npx_action,
}
