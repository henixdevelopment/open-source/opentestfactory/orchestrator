# Copyright (c) Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A simple report generator for humans that can read html."""

from typing import Any, Callable, Dict, List, Optional, Set, Tuple


from datetime import datetime
from itertools import cycle

from collections import defaultdict

import logging
import re
import yaml

from zabel.palette.text import safe

from opentf.commons.expressions import evaluate_bool

from opentf.plugins.insightcollector.palette import (
    CHART_DATASET_COLORS,
    DOUGHNUT_COLORS,
    make_table,
    make_doughnut,
    make_barchart,
    make_scatterchart,
    make_block,
    make_element,
    make_dialog,
    make_action,
)

from opentf.plugins.insightcollector.report_utils import (
    FAILURE,
    ERROR,
    SKIPPED,
    SUCCESS,
    TOTAL,
    RESOURCE_JS,
    STATUSES_KO,
    STATUSES_ORDER,
    get_resources,
    _get_path,
    get_sorted_testcases,
    make_workflowresult_event,
    parse_testcase_name,
    summarize_test_results,
)

from opentf.plugins.insightcollector.exceptions import ScopeError


########################################################################
## Constants and helpers

HTML_REPORT_TYPE = 'application/vnd.opentestfactory.executionreport+html'

TEST_DATA_PATH = 'test.data'
TESTCASE_NAME = 'test.testCaseName'
TEST_OUTCOME = 'test.outcome'
ITEM_NAME = 'metadata.name'

PATHS_TO_REPLACE = (TESTCASE_NAME, TEST_OUTCOME, TEST_DATA_PATH)


def safestr(item):
    return safe(str(item))


########################################################################
## HTML report: generate HTML content

## Constants


HTML_REPORT_TITLE = 'Test Execution Summary Report'

HTML_REPORT_HEAD_TMPL = '''
    <!DOCTYPE html>
    <html>
      <head>
        <title>{title}</title>
                {scripts}
            <style>
                {style}
            </style>
      </head>
      <body class="second">
      <h1>{title}</h1>
      <div class="watermark">{watermark}</div>
'''

HTML_REPORT_TAIL = '</body></html>'

HTML_ERRORS_WARNINGS_LOG_TEMPLATE = '''
    <details class="{block_type}-log">
        <summary>{block_type}s</summary>
        <div class="{block_type}-log-details">
            <ul class="{block_type}-log-list">
                {errors}
            </ul>
        </div>
    </details>
'''

DATE_FORMAT = "%B %d, %Y at %I:%M%p"

STATUSES_ICONS = {
    FAILURE: '<span class="status-icon failed"></span>',
    SUCCESS: '<span class="status-icon passed"></span>',
    ERROR: '<span class="status-icon error"></span>',
    SKIPPED: '<span class="status-icon skipped"></span>',
}


TESTCASES_CLASSES = {
    SUCCESS: 'ok',
    FAILURE: 'nok',
    ERROR: 'err',
    SKIPPED: 'skip',
}

DEFAULT_COLUMNS_TAGS = {
    'Tag': ITEM_NAME,
    'OK': 'status.testCaseStatusSummary.success',
    'KO': 'status.testCaseStatusSummary.failure',
    'Total': 'status.testCaseCount',
}

DEFAULT_COLUMNS_TESTCASES = {
    'Test suite': 'test.suiteName',
    'Testcase': TESTCASE_NAME,
    'Dataset': TEST_DATA_PATH,
    'Status': TEST_OUTCOME,
    'Technology': 'test.technology',
    'Job': 'test.job',
}

DATASOURCE_DEFAULTS = {
    'tags': DEFAULT_COLUMNS_TAGS,
    'testcases': DEFAULT_COLUMNS_TESTCASES,
}

CHART_KINDS = ('ProgressBar', 'BarChart', 'ScatterChart')

FN_COUNT = 'count'
FN_SUM = 'sum'
FN_MIN = 'min'
FN_MAX = 'max'
FN_AVG = 'avg'

FN_METHOD = {
    FN_COUNT: sum,
    FN_SUM: sum,
    FN_MIN: min,
    FN_MAX: max,
    FN_AVG: lambda val: round(sum(val) / len(val), 2),
}

PROGRESS_CONTAINER_TMPL = '''
    <div id="progress-container" class="progress-container">{progress_bars}</div>
'''

PROGRESS_BAR_TMPL = '''
    <div class="progress-bar" style="width: {width}%; background-color: {color}" title="{title}">{label}</div>
'''

CSS_VAR_REGEX = re.compile(r'^\s*--([a-zA-Z0-9-_]+):\s*(.+);\s*$')
CSS_COLORS = {}


## Summary doughnuts and legends


def _generate_report_runtimes(wf_event: List[Dict[str, Any]]) -> str:
    runtimes_div = '<div class="runtimes">{content}</div>'
    if not wf_event:
        return runtimes_div.format(
            content=_span('Execution times could not be retieved.', class_='error')
        )

    start_time = wf_event[0]['metadata']['creationTimestamp']
    end_time = wf_event[0]['metadata']['completionTimestamp']
    start_time, end_time = datetime.fromisoformat(
        ''.join(start_time)
    ), datetime.fromisoformat(''.join(end_time))
    duration = end_time - start_time
    return f'''Workflow started on <strong>{start_time.strftime(DATE_FORMAT)}</strong>,
               ended on {end_time.strftime(DATE_FORMAT)}, for a total duration of
               <strong>{str(duration).split(".", maxsplit=1)[0]}</strong>.'''


def _make_inline_legend(rows: List[Tuple], cols: List[Tuple], width: int) -> str:
    return (
        '<div class="inline-legend">' + make_table(rows, cols, width=width) + '</div> '
    )


def _generate_execution_summary_legend(total_counts: Dict[str, int]) -> str:
    """Summarize test results."""
    total_with_icons = [
        (f'{STATUSES_ICONS[status]} {status.title()}', total_counts[status])
        for status in STATUSES_ORDER
    ]
    total_percentages = [
        (
            item[0],
            '{:.2f}%'.format(round(item[1] / total_counts[TOTAL] * 100, 2)),
            item[1],
        )
        for item in total_with_icons
    ]
    cols = [(0, 'Status', 5, str), (1, 'Ratio', -5, str), (2, 'Count', -5, str)]
    rows = [(item[0], item[1], item[2]) for item in total_percentages]
    return _make_inline_legend(rows, cols, width=20)


def _generate_tests_by_technology_legend(techno_count: Dict[str, int]) -> str:
    """Summarize technology results."""
    cols = [(0, 'Technology', 5, str), (1, 'Count', -5, str)]
    rows = [
        (
            _span(
                '', class_="technology-color", style=f"background: {DOUGHNUT_COLORS[x]}"
            )
            + f' {name}',
            value,
        )
        for x, (name, value) in enumerate(techno_count.items())
    ]
    return _make_inline_legend(rows, cols, width=20)


def _generate_report_doughnuts(sources: Dict[str, Any]) -> str:
    total = summarize_test_results(sources['testcases'])
    colors = list(CSS_COLORS.values())
    legends = (
        ('ok', 'Tests passed'),
        ('ko', 'Tests failed'),
        ('errors', 'Tests in error'),
        ('skipped', 'Tests skipped'),
    )
    doughnuts = make_doughnut(
        'Test execution summary',
        f"{total[TOTAL]} tests executed.",
        {legends[x][0]: total[status] for x, status in enumerate(STATUSES_ORDER)},
        legends,
        '',
        colors=colors,
    )
    doughnuts += _generate_execution_summary_legend(total)
    doughnuts += '\n<p>&nbsp</p>\n'
    jobs = sources['jobs']
    if len(jobs) > 1:
        for job_data in jobs.values():
            counts = job_data['status']['testCaseStatusSummary'].copy()
            counts['total count'] = job_data['status']['testCaseCount']
            if counts['total count']:
                doughnuts += make_doughnut(
                    f'Summary for job {job_data["metadata"]["name"]}',
                    f'{counts[TOTAL]} tests executed.',
                    {
                        legends[x][0]: counts[status]
                        for x, status in enumerate(STATUSES_ORDER)
                    },
                    legends,
                    '',
                    colors=colors,
                )
            else:
                doughnuts += f'''<div class="doughnut">
                <span class="empty-doughnut title">
                  Summary for job {job_data["metadata"]["name"]}
                  <span class="empty-doughnut content">No test cases executed for this job.</span>
                </span>
                </div>'''
    doughnuts += '\n<p>&nbsp</p>\n'
    technologies = get_sorted_testcases(sources['testcases'], ['test', 'technology'])
    techno_count = {technology: len(data) for technology, data in technologies.items()}
    techno_titles = [(technology, technology.title()) for technology in techno_count]
    doughnuts += make_doughnut(
        'Test cases by technology',
        f'{total[TOTAL]} tests executed.',
        techno_count,
        techno_titles,
        '',
    )
    doughnuts += _generate_tests_by_technology_legend(techno_count)
    return doughnuts


## Testcases and tags tables


def _check_and_format_row(content: Any, path: str) -> Any:
    if isinstance(content, list):
        return ' > '.join(content) if '.path' in path else ', '.join(content)
    if isinstance(content, dict) and path in ('test', 'test.collection'):
        return f'Path {path} not supported.'
    return content


def _generate_rows(
    columns: Dict[str, str], data: Dict[str, Any], datasource_kind: str
) -> List[Tuple]:
    rows = []
    for dataset in data.values():
        row = []
        for path in columns.values():
            path_list = path.split('.')
            if not path or not path_list:
                row.append('Invalid path: empty path.')
                continue
            try:
                if path in PATHS_TO_REPLACE and datasource_kind == 'testcases':
                    row.append(_get_path(dataset, []))
                    continue
                content = _get_path(dataset, path_list)
                row.append(_check_and_format_row(content, path))
            except KeyError as err:
                row.append(str(err))
        rows.append(tuple(row))
    return rows


### Test cases table columns generators

DETAILS_DIALOG_COUNTER = 1
DATASET_DIALOG_COUNTER = 1
FAILURES_DIALOG_COUNTER = 1


def _span(content: str, **kwargs) -> str:
    extra = ''
    for k, v in kwargs.items():
        extra += f' {k.strip("_")}="{v}"'
    return f'<span{extra}>{content}</span>'


def _pre(content: str) -> str:
    return '<pre>\n' + content + '\n</pre>'


def _dump(content: Dict[str, Any]) -> str:
    return safe(yaml.dump(content, indent=2, sort_keys=False))


def _dump_details(content: Dict[str, Any]) -> str:
    lines = []
    for k, v in content.items():
        lines.append(f'{safe(k)}:')
        if isinstance(v, str):
            for line in v.strip().splitlines():
                lines.append(f'  {safe(line)}')
        else:
            lines.append(_dump(v))
    return '\n'.join(lines)


def _maybe_get_errorslist(testcase):
    if errors_list := testcase['execution'].get('errorsList'):
        error_data = {
            error.get('timestamp'): error.get('message', 'Unable to get error message')
            for error in errors_list
        }
        return f'Some general errors occurred during execution: \n\n{_dump(error_data)}'
    return ''


def _make_details(testcase, details):
    """Make test case details action and dialog."""
    global DETAILS_DIALOG_COUNTER
    name = testcase['test']['testCaseName']
    testcase_info = {
        'name': name,
        'uses': testcase['test']['uses'],
        'runs-on': list(testcase['test']['runs-on']),
        'managed': testcase['test']['managed'],
    }
    if testcase['test']['managed']:
        testcase_info['data'] = {
            k: v
            for k, v in testcase['test'].get('data', {}).items()
            if k != 'DSNAME' and not k.startswith('DS_')
        }
        testcase_info['reference'] = testcase['test']['reference']
    details_dialog_id = f'execution_details_{DETAILS_DIALOG_COUNTER}'
    details_dialog = make_dialog(
        details_dialog_id,
        'Details for test case ' + str(name),
        _pre(_dump(testcase_info)),
    )
    details.append(details_dialog)
    DETAILS_DIALOG_COUNTER += 1

    return _span(
        make_action(_span(safestr(name), class_='testCaseName'), details_dialog_id),
        title='View test case details',
    )


def _make_execution_status(testcase: Dict[str, Any], failures: List[str]) -> str:
    """Make execution status action and dialog, adding failure details when relevant."""
    global FAILURES_DIALOG_COUNTER
    status = testcase['test']['outcome']
    action = ''
    if status in STATUSES_KO:
        if failure_details := (
            testcase['execution'].get('failureDetails')
            or testcase['execution'].get('errorDetails')
            or testcase['execution'].get('warningDetails')
        ):
            dialog_id = f'failures_details_{FAILURES_DIALOG_COUNTER}'
            dialog = make_dialog(
                dialog_id,
                'Failure details for test case '
                + parse_testcase_name(testcase['metadata']['name'])[1],
                _pre(
                    _dump_details(failure_details)
                    + '\n'
                    + _maybe_get_errorslist(testcase)
                ),
            )
            failures.append(dialog)
            FAILURES_DIALOG_COUNTER += 1
            action = make_action(
                _span('(view details...)', class_='failure-details'), dialog_id
            )
    tag = TESTCASES_CLASSES.get(status, 'noaccess')
    return _span(safestr(status.title()), class_=f'{tag} outcome') + action


def _make_dataset(testcase, dataset):
    """Make dataset action and dialog for test referential managed test cases."""
    global DATASET_DIALOG_COUNTER
    if testcase['test']['managed'] and testcase['test'].get('data', {}).get('DSNAME'):
        dataset_items = {
            k: v for k, v in testcase['test']['data'].items() if k.startswith('DS_')
        }
        data = _pre(_dump(dataset_items))
        dataset_dialog_id = f'dataset_details_{DATASET_DIALOG_COUNTER}'
        dataset_dialog = make_dialog(
            dataset_dialog_id, 'Dataset ' + testcase['test']['data']['DSNAME'], data
        )
        dataset.append(dataset_dialog)
        DATASET_DIALOG_COUNTER += 1
        hover_content = '\n'.join([f'{k}: {v}' for k, v in dataset_items.items()][:3])
        hover_tail = '...' if len(dataset_items) > 2 else ''
        return _span(
            make_action(
                _span(safe(testcase['test']['data']['DSNAME']), class_='data'),
                dataset_dialog_id,
            ),
            title=f'{hover_content}{hover_tail}',
        )
    return ''


### Testcases and tags tables generators


def _generate_testcases_table(
    testcases: Dict[str, Any], columns: Dict[str, str], datasource_kind: str
) -> Tuple[str, str]:

    def _make_details_wrapper(testcase):
        return _make_details(testcase, details)

    def _make_status_wrapper(testcase):
        return _make_execution_status(testcase, failures)

    def _make_dataset_wrapper(testcase):
        return _make_dataset(testcase, dataset)

    details = []
    dataset = []
    failures = []
    cols = []
    cols_functions = {
        TEST_DATA_PATH: _make_dataset_wrapper,
        TESTCASE_NAME: _make_details_wrapper,
        TEST_OUTCOME: _make_status_wrapper,
    }
    for n, (column, val) in enumerate(columns.items()):
        if cols_functions.get(val):
            fn = cols_functions[val]
        else:
            fn = safestr
        cols.append((n, column, 10, fn, val.split('.')[-1]))

    rows = _generate_rows(columns, testcases, datasource_kind)
    return (
        make_table(
            rows,
            cols,
            searchable=[col[-1] for col in cols],
            sortable=True,
        )
        .replace('<td class="testCaseName">', '<td>')
        .replace('<td class="data">', '<td>')
        .replace('<td class="outcome">', '<td>'),
        '\n'.join(details) + '\n'.join(dataset) + '\n'.join(failures),
    )


def _generate_stats_by_tags_table(
    tags: Dict[str, Any], columns: Dict[str, str], datasource_kind: str
) -> str:
    cols = []
    for n, (column, val) in enumerate(columns.items()):
        width = -10 if val != ITEM_NAME else 10
        cols.append((n, column, width, safestr, val.split('.')[-1]))
    rows = _generate_rows(columns, tags, datasource_kind)
    return make_table(
        rows,
        cols,
        searchable=[col[-1] for col in cols],
        sortable=True,
    )


## Jobs cloud


def _get_job_style(job_results: Dict[str, int], job_testcount: int):
    if not job_testcount:
        return 'empty'
    if job_results[FAILURE] == job_testcount:
        return 'nok'
    if job_results[SUCCESS] == job_testcount:
        return 'ok'
    return 'nokok'


def _generate_jobs_cloud(jobs: Dict[str, Any]) -> List[str]:
    jobs_cloud = []
    for job_data in jobs.values():
        counts = job_data['status']['testCaseStatusSummary']
        jobs_cloud.append(
            make_element(
                ', '.join(job_data['spec']['runs-on'])
                + '\n\n'
                + 'OK: '
                + str(counts[SUCCESS])
                + '\n'
                + 'KO: '
                + str(counts[FAILURE])
                + '\n'
                + 'ERROR: '
                + str(counts[ERROR])
                + '\n\n'
                + '\n'.join(
                    [
                        f'{name}: {value}'
                        for name, value in job_data['spec']['variables'].items()
                    ]
                ),
                _get_job_style(counts, job_data['status']['testCaseCount']),
                job_data['metadata']['name'],
            )
        )
    return jobs_cloud


## ProgressBar


def _get_progressbar_data(
    template: Dict[str, Any], aggregate_fn: str, datasource: Dict[str, Any]
) -> Dict[str, Any]:
    """Get data for progress bar.

    # Required arguments

    - template: a dictionary containing progress bar properties
    - aggregate_fn: a string, function applied to aggregated data, one of
      count, sum, max, min, avg
    - datasource: a dictionary containing datasource items

    # Returned value

    - progressbar_data: a dictionary containing data for progress bar generation,
    keys are `groupBy` paths, values are lists of data.
    """
    if not (data := template.get('data')) and aggregate_fn != FN_COUNT:
        raise ValueError(f'No defined data path for aggregate function {aggregate_fn}')

    if data and aggregate_fn == FN_COUNT:
        data = None
    groupby_path = template['groupBy'].split('.')
    progressbar_data = defaultdict(list)
    for item in datasource.values():
        group_by = _get_path(item, groupby_path)
        if data:
            to_aggregate = _get_path(item, data.split('.'))
            if not isinstance(to_aggregate, (float, int)):
                raise ValueError(
                    'Unsupported data type for aggregated data, int or float expected'
                )
        else:
            to_aggregate = 1
        progressbar_data[group_by].append(to_aggregate)
    return progressbar_data


def _maybe_adjust_progressbar_segments(
    counts: Dict[str, Any], total: float
) -> Dict[str, Any]:
    """Get progress bar segments widths. Adjust if total width > 100."""
    if not total:
        raise ValueError('Aggregated values sum is 0, cannot generate ratios')
    ratios = {key: round((value / total) * 100, 2) for key, value in counts.items()}
    total_ratio = sum(ratios.values())
    if total_ratio > 100:
        max_width = max(ratios.values())
        for key, value in ratios.copy().items():
            if value == max_width:
                ratios[key] = value - (total_ratio - 100)
    return ratios


def _generate_progressbar_legend(
    template: Dict[str, Any],
    aggregate_fn: str,
    counts: Dict[str, Any],
    ratios: Dict[str, Any],
) -> str:
    """Summarize progressbar results"""
    colors = cycle(DOUGHNUT_COLORS)
    data_path = template.get('data', '').split('.')[-1]
    if data_path and aggregate_fn == 'count':
        data_path = ''
    cols = [
        (0, f'{template["groupBy"].split(".")[-1].capitalize()}', 5, str),
        (1, f'{aggregate_fn.upper()}({data_path})', -5, str),
        (2, 'Ratio', -5, str),
    ]
    rows = [
        (
            _span('', class_='technology-color', style=f'background: {next(colors)}')
            + f' {group_by}',
            round(value, 2),
            f'{round(ratios[group_by], 2)}%',
        )
        for group_by, value in counts.items()
    ]
    return _make_inline_legend(rows, cols, width=25)


def _generate_progressbar(datasource: Dict[str, Any], template: Dict[str, Any]) -> str:
    aggregate_fn = template.get('aggregateFunction', 'sum')
    try:
        data = _get_progressbar_data(template, aggregate_fn, datasource)
        counts = {
            group_by: FN_METHOD[aggregate_fn](value) for group_by, value in data.items()
        }
        ratios = _maybe_adjust_progressbar_segments(counts, sum(counts.values()))
    except (KeyError, ValueError) as err:
        return _span(
            f'No progress bar data retrieved: {safestr(err)}.', class_="error-log"
        )

    colors = cycle(DOUGHNUT_COLORS)
    progress_bars = ''
    for group_by, aggregated in counts.items():
        progress_bars += PROGRESS_BAR_TMPL.format(
            width=ratios[group_by],
            color=next(colors),
            title=f'{group_by}: {aggregated} ({len(data[group_by])} {template["groupBy"].split(".")[-1]} items counted)',
            label=f'{group_by}',
        )

    progress_bar = PROGRESS_CONTAINER_TMPL.format(progress_bars=progress_bars)
    legends = _generate_progressbar_legend(template, aggregate_fn, counts, ratios)
    return f'{progress_bar}\n<p>&nbsp</p>\n{legends}'


#### BarChart


def _sort_barchart_datasource(
    order_by: Dict[str, Any], datasource: Dict[str, Any]
) -> List[Dict[str, Any]]:
    """Sort datasource for a barchart.

    # Required arguments:

    - order_by, a dictionary contaning sorting key and sorting order
    - datasource, a dictionary containing datasource items

    # Returned value:

    - list of a sorted datasource items
    """
    reverse = order_by.get('order', 'ascending') == 'descending'
    try:
        return sorted(
            datasource.values(),
            key=lambda item: _get_path(
                item, order_by.get('key', 'metadata.creationTimestamp').split('.')
            ),
            reverse=reverse,
        )
    except KeyError:
        return sorted(
            datasource.values(),
            key=lambda item: _get_path(item, order_by.get('key', ITEM_NAME).split('.')),
            reverse=reverse,
        )


def _generate_barchart(datasource: Dict[str, Any], template: Dict[str, Any]) -> str:
    label_path = template['primaryAxis']['data'].split('.')
    value_path = template['secondaryAxis']['data'].split('.')
    try:
        sorted_datasource = _sort_barchart_datasource(
            template.get('orderBy', {}), datasource
        )
        items = [
            (_get_path(item, label_path), _get_path(item, value_path))
            for item in sorted_datasource
        ]
    except KeyError as err:
        return _span(
            f'No bar chart data retrieved: {safestr(err)}.', class_='error-log'
        )

    max_items = template.get('maxItems')
    items = items[:max_items]
    if not items:
        return _span(
            f'No bar chart data retrieved (maxItems={max_items}).', class_='error-log'
        )

    if template.get('primaryAxis', {}).get('reversed'):
        items = reversed(items)

    labels, values = zip(*items)
    legend = safestr(template.get('legend', value_path[-1]))
    return make_barchart(
        list(labels),
        [(legend, list(values))],
        colors=[(c, None) for c in CHART_DATASET_COLORS],
        legend=True,
        stacked=False,
        template=template,
    )


#### ScatterChart


def _generate_scatterchart(datasource: Dict[str, Any], template: Dict[str, Any]) -> str:
    raw_data = defaultdict(list)
    try:
        for item in datasource.values():
            key = _get_path(item, template['groupBy'].split('.'))
            raw_data[key].append(
                _get_path(item, template['primaryAxis']['data'].split('.'))
            )
    except KeyError as err:
        return _span(
            f'No scatter chart data retrieved: {safestr(err)}.', class_="error-log"
        )

    datasets = []
    for n, (group, data) in enumerate(raw_data.items()):
        datasets.append((group, [{'x': item, 'y': n} for item in data]))

    return make_scatterchart(
        datasets,
        [group for group, _ in datasets],
        template,
        colors=[(c, None) for c in CHART_DATASET_COLORS],
        legend=True,
    )


## Blocks


def _make_block(
    title, subtitle, items, legend=None, details: str = ''
) -> Tuple[str, str]:
    return make_block(safestr(title), subtitle, items, legend=legend), details


def _make_errors_block(errors: Set[str], block_type: str = 'error') -> str:
    errors_list = ''.join([f'<li>{error}</li>' for error in errors])
    l = len(errors)
    return make_block(
        f'{block_type.capitalize()} log',
        f'{l} {block_type}{"s" if l != 1 else ""} encountered while generating reports.',
        HTML_ERRORS_WARNINGS_LOG_TEMPLATE.format(
            errors=errors_list, block_type=block_type
        ),
    )


def _make_summary_block(args: Dict[str, Any]) -> Tuple[str, str]:
    return _make_block(
        args['template']['name'],
        _generate_report_runtimes(args['workflow']),
        [_generate_report_doughnuts(args['datasource'])],
    )


def _make_jobs_block(args: Dict[str, Any]) -> Tuple[str, str]:
    jobs = args['datasource']
    return _make_block(
        args['template']['name'],
        f'{len(jobs)} jobs executed.',
        _generate_jobs_cloud(jobs),
        legend='''
            <p><span class="ok">All job test cases are in success.</span></p>
            <p><span class="nok">All job test cases are failed.</span></p>
            <p><span class="nokok">Job contains both failed and passed test cases.</span></p>
            <p><span class="empty">Job contains no test cases.</span></p>
        ''',
    )


def _make_tags_block(args: Dict[str, Any]) -> Tuple[str, str]:
    template = args['template']
    return _make_block(
        template['name'],
        '',
        _generate_stats_by_tags_table(
            args['datasource'],
            _get_columns_from_template(template),
            template['datasource'],
        ),
    )


def _make_chart_block(
    args: Dict[str, Any], _chart_generator: Callable
) -> Tuple[str, str]:
    return _make_block(
        args['template']['name'],
        'All execution times are in milliseconds.',
        _chart_generator(args['datasource'], args['template']),
    )


def _make_progressbar_block(args: Dict[str, Any]) -> Tuple[str, str]:
    return _make_chart_block(args, _generate_progressbar)


def _make_barchart_block(args: Dict[str, Any]) -> Tuple[str, str]:
    return _make_chart_block(args, _generate_barchart)


def _make_scatterchart_block(args: Dict[str, Any]) -> Tuple[str, str]:
    return _make_chart_block(args, _generate_scatterchart)


### Test cases block


def _get_columns_from_template(template: Dict[str, Any]) -> Dict[str, str]:
    columns = template.get('columns', DATASOURCE_DEFAULTS[template['datasource']])
    return {safestr(k): safestr(v) for k, v in columns.items()}


def _maybe_complete_with_errorslist(testcases: Dict[str, Any]):
    execution_errors = {
        data['metadata']['execution_id']: data['execution']['errorsList']
        for data in testcases.values()
        if data['execution'].get('errorsList')
    }
    for data in filter(
        lambda data: data['test']['outcome'] in STATUSES_KO
        and data['metadata']['execution_id'] in execution_errors,
        testcases.values(),
    ):
        data['execution']['errorsList'] = execution_errors[
            data['metadata']['execution_id']
        ]


def _make_testcases_block(args: Dict[str, Any]) -> Tuple[str, str]:
    testcases = args['datasource']
    _maybe_complete_with_errorslist(testcases)
    results = summarize_test_results(testcases)
    subtitle = f'''{results[TOTAL]} test cases executed,
                   {results[SUCCESS]} passed,
                   {results[FAILURE]} failed,
                   {results[ERROR]} in error,
                   {results[SKIPPED]} skipped.'''
    template = args['template']
    content, details = _generate_testcases_table(
        testcases,
        _get_columns_from_template(template),
        template['datasource'],
    )
    return _make_block(template['name'], subtitle, [content], details=details)


########################################################################
## HTML report: create HTML file

BLOCKS_GENERATORS = {
    'Cloud': {'jobs': _make_jobs_block},
    'Table': {'tags': _make_tags_block, 'testcases': _make_testcases_block},
    'SummaryBlock': {None: _make_summary_block},
    'ProgressBar': {'any': _make_progressbar_block},
    'BarChart': {'any': _make_barchart_block},
    'ScatterChart': {'any': _make_scatterchart_block},
}

BLOCKS_DATASOURCES = {
    'Cloud': lambda item: (item['datasource'],),
    'Table': lambda item: (item['datasource'],),
    'SummaryBlock': lambda _: ('jobs', 'testcases'),
    'ProgressBar': lambda item: (item['datasource'],),
    'BarChart': lambda item: (item['datasource'],),
    'ScatterChart': lambda item: (item['datasource'],),
}

## Handle errors


def _create_error_report(errors: Set[str], comment: str, css: str) -> str:
    comment_block, _ = _make_block('Cannot generate report', '', comment)
    return (
        HTML_REPORT_HEAD_TMPL.format(title='ERROR', style=css, watermark='', scripts='')
        + comment_block
        + (_make_errors_block(errors) if errors else '')
        + HTML_REPORT_TAIL
    )


def _log_and_report_no_templates(
    spec: Dict[str, Any], title: str, workflow_id: str, css: str
) -> str:
    msg = f'No relevant template items found for the report "{title}" (workflow {workflow_id}). Most probably, no item matched its own if condition.'
    logging.debug(msg)
    cols = [(0, 'Name', 10, str), (1, 'Kind', 10, str), (2, 'Condition', 10, str)]
    rows = [
        (item['name'], item['kind'], item.get('if', 'None'))
        for item in spec['template']
    ]
    comment = f"<p>{msg}</p> {make_table(rows, cols, width=30)}"
    return _create_error_report(set(), comment, css)


## Generate report


def _has_testcases(datasource_kind: Optional[str], datasource: Dict[str, Any]) -> bool:
    """Check if test cases count > 0 for jobs and tags datasources."""
    if datasource_kind in ('tags', 'jobs'):
        return (
            sum(
                v.get('status', {}).get('testCaseCount', 0) for v in datasource.values()
            )
            > 0
        )
    return True


def _get_datasources(
    item: Dict[str, Any],
    scope: str,
    observer_proxy,
    errors: Set[str],
    warnings: Set[str],
) -> Optional[Dict[str, Any]]:
    """Query datasource(s)."""
    inputs = BLOCKS_DATASOURCES[item['kind']](item)
    block_scope = f'({scope}) && ({item.get("scope", "true")})'
    try:
        datasources = {}
        for name in inputs:
            data = observer_proxy.get_datasource(name, block_scope)
            if data.get('EMPTY_ITEMS'):
                datasources[name] = {}
            else:
                datasources[name] = data
        if item['kind'] == 'SummaryBlock' and not all(
            datasources[k] for k in ['jobs', 'testcases']
        ):
            datasources = {'jobs': {}, 'testcases': {}}
        if not any(datasources.values()):
            if not item.get('skipIfNotFound'):
                msg = f'{item["kind"]} {item["name"]} not published: no data retrieved.'
                logging.debug(msg)
                errors.add(msg)
            return None

        if len(inputs) == 1:
            source = datasources[inputs[0]]
            if not _has_testcases(item.get('datasource'), source):
                warnings.add(
                    f'No test cases matching scope `{block_scope}` for {item["kind"]} "{item["name"]}", generated item may contain no data.'
                )
            return source
        return datasources
    except ScopeError as err:
        msg = str(err)
        if 'No test cases matching scope' in msg:
            msg = f'Cannot generate {item["kind"]} "{item["name"]}". ' + msg
        logging.debug(msg)
        errors.add(msg)
    return None


def _create_report_items(
    templates: List[Dict[str, Any]],
    observer_proxy,
    scope: str,
) -> Tuple[Set[str], Set[str], str]:
    html_body = html_details = ''
    errors, warnings = set(), set()
    wf_event = observer_proxy.get_workflow_event()
    for template in templates:
        if not (
            datasources := _get_datasources(
                template, scope, observer_proxy, errors, warnings
            )
        ):
            continue
        args = {
            'workflow': wf_event,
            'datasource': datasources,
            'template': template,
        }
        kind = template['kind']
        if kind in CHART_KINDS:
            datasource = 'any'
        else:
            datasource = template.get('datasource')
        content, details = BLOCKS_GENERATORS[kind][datasource](args)
        html_body += content
        html_details += details

    return errors, warnings, html_body + html_details


def _get_css(css_list: List[str], spec: Dict[str, Any]) -> str:
    css = '\n\n'.join(css_list) + '\n\n' + spec.get('style', '')
    for line in css.splitlines():
        if (var := CSS_VAR_REGEX.match(line)) and (var.group(1) in STATUSES_ORDER):
            CSS_COLORS[var.group(1)] = var.group(2)
    return css


def _create_report_html(
    observer_proxy,
    spec: Dict[str, Any],
    title: str,
    contexts: Dict[str, Any],
    css_list: List[str],
    default_scope: str,
) -> str:
    css = _get_css(css_list, spec)
    relevant_templates = [
        item
        for item in spec['template']
        if evaluate_bool(item.get('if', 'true'), contexts)
    ]
    if not relevant_templates:
        return _log_and_report_no_templates(
            spec, title, contexts['workflow']['workflow_id'], css
        )
    try:
        watermark = datetime.fromisoformat(
            contexts['workflow']['creationTimestamp']
        ).strftime(DATE_FORMAT)
    except Exception as err:
        logging.warning(
            'Cannot retrieve workflow creation timestamp for HTML report: %s.', str(err)
        )
        watermark = 'Unavailable'

    html_head = HTML_REPORT_HEAD_TMPL.format(
        title=title,
        style=css,
        watermark=watermark,
        scripts=get_resources(RESOURCE_JS) + spec.get('scripts', ''),
    )
    errors, warnings, html_body = _create_report_items(
        relevant_templates, observer_proxy, spec.get('scope', default_scope)
    )
    errors_block = _make_errors_block(errors) if errors else ''
    warnings_block = (
        _make_errors_block(warnings, block_type='warning') if warnings else ''
    )
    return html_head + errors_block + warnings_block + html_body + HTML_REPORT_TAIL


def _generate_html_report(
    report_html: str, name: str, workflow_id: str, request_id: Optional[str]
) -> Dict[str, Any]:
    return make_workflowresult_event(
        lambda x: x.write(report_html),
        name,
        HTML_REPORT_TYPE,
        'html',
        workflow_id,
        request_id,
    )


def prepare_html_report_event(
    observer_proxy,
    model: Dict[str, Any],
    workflow_id: str,
    request_id: Optional[str],
    contexts: Dict[str, Any],
    css_list: List[str],
    default_scope: str,
) -> Dict[str, Any]:
    """Prepare HTML execution report event to be published."""
    name = model['name']
    logging.debug('Preparing HTML report %s for workflow %s.', name, workflow_id)
    report_html = _create_report_html(
        observer_proxy,
        model['spec'],
        model.get('title', HTML_REPORT_TITLE),
        contexts,
        css_list,
        default_scope,
    )
    return _generate_html_report(report_html, name, workflow_id, request_id)
