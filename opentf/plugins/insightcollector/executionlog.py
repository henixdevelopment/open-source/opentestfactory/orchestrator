# Copyright (c) 2024 Henix, Henix.fr

"""A simple execution log generator."""

from typing import Any, Dict, List, Optional, TextIO

import logging

from opentf.tools.ctlworkflows import (
    emit_event,
    OutputContext,
    OUTPUTCONTEXT_PARAMETERS,
)
from opentf.plugins.insightcollector.exceptions import ExecutionLogError
from opentf.plugins.insightcollector.report_utils import make_workflowresult_event


########################################################################
## Constants and helpers

EXECUTIONLOG_TYPE = 'application/vnd.opentestfactory.executionlog+text'


########################################################################
## Execution log


def _make_outputcontext(file: TextIO, spec: Dict[str, Any]) -> OutputContext:
    """Get the execution log depth parameters from configuration."""

    context: Dict[str, Any] = {'file': file}
    for k, v in OUTPUTCONTEXT_PARAMETERS.items():
        context[k] = spec.get(k, v)
        if isinstance(v, int):
            try:
                context[k] = int(context[k])
            except ValueError:
                logging.warning(
                    f'Invalid context parameter {k}: {context[k]}, using the default value {v}.'
                )
                context[k] = v
    return OutputContext(**{k.replace('-', '_'): v for k, v in context.items()})


def _dump_events(events: List[Dict[str, Any]], output_context: OutputContext) -> None:
    """Dumps events to file."""
    for event in events:
        emit_event(
            event['kind'],
            event,
            output_context,
            first=True,
            namespace=event.get('metadata', {}).get('namespace'),
        )


def prepare_executionlog_event(
    datasources,
    model: Dict[str, Any],
    workflow_id: str,
    request_id: Optional[str],
    *_,
):
    if not (events := datasources.get_events()):
        raise ExecutionLogError(
            f'No events retrieved for the workflow {workflow_id}, not publishing the execution log.'
        )

    return make_workflowresult_event(
        lambda file: _dump_events(
            events, _make_outputcontext(file, model.get('spec', {}))
        ),
        model['name'],
        EXECUTIONLOG_TYPE,
        'txt',
        workflow_id,
        request_id,
    )
