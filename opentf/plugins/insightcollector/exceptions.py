# Copyright (c) 2024 Henix, Henix.fr

"""A small class for Insight Collector exceptions."""


class InsightCollectorError(Exception):

    def __init__(self, msg, details=None):
        self.msg = msg
        self.details = details


class HtmlReportError(InsightCollectorError):
    """HtmlReport error"""


class XmlReportError(InsightCollectorError):
    """XmlReport error"""


class ExecutionLogError(InsightCollectorError):
    """ExecutionLog error"""


class ScopeError(InsightCollectorError):
    """Datasource scope error"""
