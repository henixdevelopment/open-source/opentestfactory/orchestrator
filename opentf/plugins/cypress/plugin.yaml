# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# plugin.yaml
---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: cypress
  title: Cypress
  license: apache2
  hooks: default
  description: |
    This plugin provides functions that handle Cypress tests.  It has been
    validated with Cypress v8.5.0 and should work with any recent version of Cypress.

    It can be used directly in a workflow, or indirectly via generators (such as those
    providing access to test case managers).

    A working Cypress installation must be available in the targeted execution environments.

    The functions have a `cypress` category prefix.
  notes: |
    ## Using with inception

    Please refer to "[Inception](https://opentestfactory.org/guides/inception.html)" for
    more information on what inception is.

    Preload the inception environment with at least the tests execution report data.

    ### Example

    Assuming the following workflow and an existing execution report `report.xml`:

    ```yaml title="my_workflow.yaml"
    metadata:
      name: cypress Inception
    resources:
      files:
      - surefire-report
    jobs:
      my_cypress_job:
        runs-on: inception
        steps:
        - uses: actions/prepare-inception@v1
          with:
            openweather-report.xml: ${{ resources.files.surefire-report }}
        - uses: cypress/execute@v1
          with:
            test: OpenWeather/src/openweather.spec.js
    ```

    You can use the following command to run it:

    === "Bash"

        ```bash
        opentf-ctl \
            run workflow my_workflow.yaml -f surefire-report=report.xml
        ```

    === "CMD"

        ```bat
        opentf-ctl ^
            run workflow my_workflow.yaml -f surefire-report=report.xml
        ```

    === "PowerShell"

        ```powershell
        opentf-ctl `
            run workflow my_workflow.yaml -f surefire-report=report.xml
        ```
events:
- categoryPrefix: cypress

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: 'cypress'
  action: cypress/cypress
  description: |
    Run all 'cypress' tests.

    If the function is used more than once in a job, it is up to the caller to ensure no
    previous test execution results remain before executing a new test.

    !!! warning
        It is also up to the caller to attach the relevant reports so that publishers can do
        their job, by using the `actions/get-files@v1` function or some other means.  No
        reports are attached by default.

    If the function is called after a repository checkout (`actions/checkout@v2`), the
    `working-directory` keyword must be used to indicate to Cypress the root directory of
    the project.  Otherwise, Cypress will fail to find the spec files. See the example
    below.

  notes: |
    ## Examples

    This is an example of `cypress` action with all inputs specified:

    ```yaml
    - uses: cypress/cypress@v1
      with:
        browser: chrome
        reporter: junit
        reporter-options: "mochaFile=mocha_results/test-output-[hash].xml,toConsole=true"
        headless: true
        env: profile=postgres
        config-file: cypress/config/...
        extra-options: --port 8023
    ```

    `extra-options` is an optional input that allows you to pass additional parameters to a
    Cypress action.  You can find the list of possible command line options in the
    <a href="https://docs.cypress.io/guides/guides/command-line#Options" target="_blank">Cypress run options</a>
    chapter of the Cypress documentation.

    This is an example of calling the `cypress` action after the `checkout` action.  As
    described above, the `working-directory` keyword must be provided:

    ```yaml
    - name: gitcheckout
      uses: actions/checkout@v2
      with:
        repository: https://github.com/mygit/my_cypress_project
    - name: cypresstests
    - uses: cypress/cypress@v1
      with:
        browser: chrome
      working-directory: my_cypress_project
    ```

branding:
  icon: play
cmd: 'python3 -m opentf.plugins.cypress.main'
events:
- categoryPrefix: cypress
  category: cypress
  categoryVersion: v1
inputs:
  browser:
    description: The browser to use.
    required: false
  reporter:
    description: The report format to use.
    required: false
  reporter-options:
    description: Additional flags for the report generation.
    required: false
  headless:
    description: A boolean, false by default.
    required: false
  env:
    description: Additional properties.
    required: false
  config-file:
    description: Configuration file path.
    required: false
  extra-options:
    description: Additional parameters to pass to Cypress.
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: 'cypress'
  action: cypress/execute
  description: |
    An 'execute' function for use by generators.

    ## Test Reference format

    The test reference format used by `cypress/execute@v1` is as follows:

    If the Cypress project is situated at the root of the source code repository:

    - `{project}#{spec_file}`

    If the Cypress project is not situated at the root of the source code repository:

    - `{project}[/{cypress_project_directory}]#{spec_file}`

    With:

    - `{project}` (required): name of the project on the source code repository.
    - `{cypress_project_directory}` (optional): path to the directory of the Cypress project
      (i.e. the directory containing the `cypress.json`/`cypress.Config.js` file and the
      `cypress` directory)
    - `{spec_file}` (required): path (relative to the previous directory) and name of the
      Cypress test file, from the root of the project, including the `.spec.js` extension.

    !!! information
        In this version, it is not possible to select a specific test in a spec file
        containing several ones: all tests in the spec file are therefore executed together.
        The result of each executed test case is calculated by taking into account the
        individual results of each test included in the bound file:

        If at least one test has an 'Error' status (in case of a technical issue), the
        status of the execution will be 'Blocked'.

        If at least one test fails functionally and none of the others has an 'Error'
        status, the status of the execution will be 'Failed'.

        If all tests succeed, the status of the execution will be 'Success'.

    !!! information
        Old versions of the orchestrator only supported the following test reference syntax:

        - `{project}/[{spec_file}]`

        That old syntax is still accepted, but it has been deprecated because it is not
        applicable if the Cypress project is not at the root of the source directory.
  notes: |
    ## Customization

    The `CYPRESS_EXTRA_OPTIONS` environment variable can be used to pass additional
    parameters to the `cypress run` command.

    If defined it will be appended to the end of the command line.

    The following parameters are used in the provider-generated `cypress run` command:

    ```bash
    cypress run \
      --project "{project_path}" --spec "{spec_path}" \
      --config screenshotsFolder="{screenshots_folder_uuid}" \
      --reporter junit --reporter-options "mochaFile={report_file_path}" $CYPRESS_EXTRA_OPTIONS
    ```

    You must avoid passing, _via_ the `CYPRESS_EXTRA_OPTIONS` variable, the command line
    parameters that conflict with the parameters already used, or the parameters
    that impact the generation or alter the path of the reports expected by the orchestrator
    (view ["Reports" section](#reports)).

    ## Examples

    In this first example, the Cypress project is at the root of the source code repository:

    ```yaml
    - uses: cypress/execute@v1
      with:
        test: cypressProject#cypress/test.spec.js
    ```

    In this second example, the Cypress project is not at the root of the source code
    repository:

    ```yaml
    - uses: cypress/execute@v1
      with:
        test: cypressProject/custom/cypress/dir#cypress/test.spec.js
    ```
branding:
  icon: play-circle
  color: red
cmd: 'python3 -m opentf.plugins.cypress.main'
events:
- categoryPrefix: cypress
  category: execute
  categoryVersion: v1
inputs:
  test:
    description: The test reference.
    required: true
reports:
  '{spec_test}-report.xml':
    description: |
      A Surefire report (XML). `{spec_test}` is the first component of the dot-separated
      spec file name (e.g. `openweather.spec.js` will generate a report named
      `openweather-report.xml`, `calculator.sub.ko.spec.js` a report named
      `calculator-report.xml`, and so on).
      <!--
      For the time being, the Cypress parser does not support having no spec file, so we comment out the next sentence.
      If no spec file is specified, it is an empty string (the report name will hence be `-report.xml`).
      -->

      The Surefire report has the `application/vnd.opentestfactory.cypress-surefire+xml`
      content type.
  '{spec_test}_screenshots.tar':
    description: |
      A `.tar` archive containing test execution screenshots, if there are any.  Please note
      that the `cypress/execute` action overrides the Cypress screenshot directory (the one
      possibly defined by `screenshotsFolder`, otherwise the default
      `repo_root/cypress/screenshots`).

      This new, UUID-named, directory is created at the root of the source code repository
      and is available in an `after` hook via the `OPENTF_CYPRESS_SCREENSHOT_FOLDER`
      environment variable.

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: 'cypress'
  action: cypress/params
  description: |
    A 'params' function for use by generators.
  notes: |
    ## Example

    ```yaml
    - uses: cypress/params@v1
      with:
        data:
          global:
            key1: value1
            key2: value2
          test:
            key1: value3
            key3: value4
        format: format
    ```

    `format` must so far be SQUASHTM_FORMAT (`tm.squashtest.org/params@v1`).

    `data` can have two keys:

    - `global` for defining global parameters.
    - `test` for defining test parameters.
branding:
  icon: plus-circle
  color: red
cmd: 'python3 -m opentf.plugins.cypress.main'
events:
- categoryPrefix: cypress
  category: params
  categoryVersion: v1
inputs:
  data:
    description: The data to use for the automated test.
    required: true
  format:
    description: The format to use for the automated test data.
    required: true
additionalInputs: true
