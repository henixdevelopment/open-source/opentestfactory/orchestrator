# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A simple publisher plugin.

This publisher plugin copies published documents to a S3 bucket.

# Usage

```
python3 -m opentf.plugins.s3publisher.main [--context context] [--config configfile]
```


# Endpoints

This module exposes one endpoint:

- /inbox (POST)

Whenever calling this endpoint, a signed token must be specified
via the `Authorization` header.

This header will be of form:

    Authorization: Bearer xxxxxxxx

It must be signed with one of the trusted authorities specified in the
current context.
"""

from typing import Any, List

import json
import os
import sys
import threading

from queue import Queue

from flask import request

import boto3

from opentf.commons import (
    EXECUTIONRESULT,
    WORKFLOWRESULT,
    validate_schema,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
    get_context_parameter,
)


########################################################################
# Helpers

ATTACHMENTS = Queue()


def push_attachments_to_s3(attachments: List[str], workflow_id: str):
    """Push attachments to a s3 bucket.

    # Required parameters

    - attachments: a list of strings
    - workflow_id: a string
    """
    if check_credential_file():
        if client := create_s3_client():
            app.logger.debug('Files to upload: %s', attachments)
            for output in attachments:
                try:
                    client.upload_file(
                        output,
                        bucket,
                        f'{workflow_id}/{output.split("/")[-1]}',
                    )
                    app.logger.debug('Uploaded file: %s', output)
                except Exception as err:
                    app.logger.error('Could not upload file: %s', str(err))


def check_credential_file() -> bool:
    """Ensure credential file exists."""
    cred_file_exist = os.path.exists(s3credentials) and os.access(
        s3credentials, os.R_OK
    )
    if not cred_file_exist:
        app.logger.debug('Missing S3 credentials, ignoring attachments.')
    return cred_file_exist


def create_s3_client() -> Any:
    """Create s3 client using credential file."""
    try:
        with open(s3credentials, 'r', encoding='utf-8') as f:
            settings = json.load(f)
        client = boto3.client(
            's3',
            region_name=settings['region_name'],
            endpoint_url=settings['endpoint_url'],
            aws_access_key_id=settings['aws_access_key_id'],
            aws_secret_access_key=settings['aws_secret_access_key'],
        )
    except Exception as err:
        app.logger.error('Could not initialize S3: %s', str(err))
        client = None

    return client


def handle_publications():
    """Thread handling S3 publication."""
    while True:
        attachments, workflow_id = ATTACHMENTS.get()
        push_attachments_to_s3(attachments, workflow_id)


########################################################################
## Main

SUBSCRIPTIONS = (EXECUTIONRESULT, WORKFLOWRESULT)

app = make_app(
    name='s3publisher',
    description='Create and start a S3 publisher plugin.',
    descriptor='plugin.yaml',
)

context = app.config['CONTEXT']
s3credentials = get_context_parameter(app, 's3credentials')
bucket = get_context_parameter(app, 'bucket')


@app.route('/inbox', methods=['POST'])
def process_result():
    """Process a new publication."""
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict):
        return make_status_response('BadRequest', 'Body must be a JSON object.')

    if 'apiVersion' not in body or 'kind' not in body:
        return make_status_response('BadRequest', 'Missing apiVersion or kind.')

    if 'attachments' not in body:
        return make_status_response('OK', '')

    schema = f'{body["apiVersion"]}/{body["kind"]}'
    if schema in (EXECUTIONRESULT, WORKFLOWRESULT):
        valid, extra = validate_schema(schema, body)
        if not valid:
            return make_status_response(
                'BadRequest', f'Not a valid {body["kind"]} request: {extra}'
            )
    else:
        return make_status_response(
            'BadRequest', f'No kind or invalid kind: {body.get("kind")}.'
        )

    ATTACHMENTS.put((body['attachments'], body['metadata']['workflow_id']))

    return make_status_response('OK', '')


def main(svc):
    """Start the S3 Publisher service."""
    try:
        svc.logger.debug('Starting workflow events handling thread.')
        threading.Thread(target=handle_publications, daemon=True).start()
    except Exception as err:
        svc.logger.error('Could not start publication handling thread: %s.', str(err))
        sys.exit(2)

    try:
        svc.logger.debug(f'Subscribing to {", ".join(SUBSCRIPTIONS)}.')
        subs = [
            subscribe(
                kind=kind,
                fieldexpressions=[{'key': 'attachments', 'operator': 'Exists'}],
                target='inbox',
                app=svc,
            )
            for kind in SUBSCRIPTIONS
        ]
    except Exception as err:
        svc.logger.error('Could not subscribe to eventbus: %s', str(err))
        sys.exit(2)

    try:
        if not check_credential_file():
            svc.logger.warning(f'Missing S3 credentials {s3credentials}.')
        svc.logger.debug('Starting service.')
        run_app(svc)
    finally:
        for sub in subs:
            unsubscribe(sub, app=svc)


if __name__ == '__main__':
    main(app)
