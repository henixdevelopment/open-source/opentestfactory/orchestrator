# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A SSH channel plugin."""

from typing import Any, Callable, NoReturn

import logging
import os
import re
import sys
import threading

from collections import defaultdict
from datetime import datetime, timedelta, timezone
from io import BytesIO
from queue import Queue

from enum import Enum

import yaml

from paramiko.client import SSHClient, AutoAddPolicy
from scp import SCPClient

from opentf.commons import (
    SSHSERVICECONFIG,
    EXECUTIONERROR,
    EXECUTIONRESULT,
    NOTIFICATION,
    DEFAULT_NAMESPACE,
    validate_schema,
    publish,
    make_uuid,
    make_event,
)

from opentf.toolkit import make_plugin, run_plugin, watch_file, watch_and_notify

from opentf.toolkit.channels import (
    DEFAULT_CHANNEL_LEASE,
    JobState,
    make_script,
    process_opentf_variables,
    process_output,
    process_upload,
    RUNNER_OS,
    SCRIPTPATH_DEFAULT,
    CHANNEL_REQUEST,
    CHANNEL_RELEASE,
)

from opentf.core.telemetry import TelemetryManager


########################################################################
## Constants

CONFIG_FILE = 'conf/sshee.yaml'

TAG_PATTERN = r'^[a-zA-Z][a-zA-Z0-9-]*$'

MAKEOFFER_REQUEST = 2
REFRESHHOSTS_REQUEST = 1
RELEASERESOURCES_REQUEST = 3


SERVICE_NAME = 'opentf.orchestrator.sshchannel'

IDLE = 'IDLE'
PENDING = 'PENDING'
BUSY = 'BUSY'
UNREACHABLE = 'UNREACHABLE'

########################################################################
## Environment variables

ENVIRONMENT_VARIABLES = {
    'SSHCHANNEL_DEBUG_LEVEL': None,
    'SSH_CHANNEL_HOST': None,
    'SSH_CHANNEL_PORT': '22',
    'SSH_CHANNEL_PASSWORD': None,
    'SSH_CHANNEL_POOLS': None,
    'SSH_CHANNEL_TAGS': None,
    'SSH_CHANNEL_USER': None,
    'SSHCHANNEL_HOST': None,
    'SSHCHANNEL_PORT': '22',
    'SSHCHANNEL_PASSWORD': None,
    'SSHCHANNEL_POOLS': None,
    'SSHCHANNEL_TAGS': None,
    'SSHCHANNEL_USER': None,
}

ENVIRONMENT_SECRETS = {'SSHCHANNEL_PASSWORD', 'SSH_CHANNEL_PASSWORD'}


########################################################################
## Logging Helpers


def info(msg: str, *args) -> None:
    """Log info message."""
    plugin.logger.info(msg, *args)


def error(msg: str, *args) -> None:
    """Log error message."""
    plugin.logger.error(msg, *args)


def fatal(msg: str, *args) -> NoReturn:
    """Log error message and exit with error code 2."""
    error(msg, *args)
    sys.exit(2)


def warning(msg: str, *args) -> None:
    """Log warning message."""
    plugin.logger.warning(msg, *args)


def debug(msg: str, *args) -> None:
    """Log debug message."""
    plugin.logger.debug(msg, *args)


########################################################################
## Telemetry


class SSHChannelMetrics(Enum):
    # HOSTS_ADDED_COUNTER = (
    #     'opentf.orchestrator.sshchannel.hosts.added',
    #     '1',
    #     'Added hosts count',
    #     None,
    # )
    # HOSTS_REMOVED_COUNTER = (
    #     'opentf.orchestrator.sshchannel.hosts.removed',
    #     '1',
    #     'Removed hosts count',
    #     None,
    # )
    HOSTS_CURRENT_GAUGE = (
        'opentf.orchestrator.sshchannel.hosts.current',
        '1',
        'Current hosts count',
        None,
    )
    HOSTS_TAGS_GAUGE = (
        'opentf.orchestrator.sshchannel.hosts.tags',
        '1',
        'Added hosts per tag count',
        None,
    )
    CHANNEL_REQUESTS_COUNTER = (
        'opentf.orchestrator.sshchannel.channels.requests',
        '1',
        'Received environment requests count',
        None,
    )
    CHANNEL_OFFERS_COUNTER = (
        'opentf.orchestrator.sshchannel.channels.offers',
        '1',
        'Produced environment offers count',
        None,
    )
    CHANNELS_BUSY_GAUGE = (
        'opentf.orchestrator.sshchannel.channels.busy',
        '1',
        'Busy channels count',
        None,
    )
    CHANNELS_IDLE_GAUGE = (
        'opentf.orchestrator.sshchannel.channels.idle',
        '1',
        'Idle channels count',
        None,
    )
    CHANNELS_PENDING_GAUGE = (
        'opentf.orchestrator.sshchannel.channels.pending',
        '1',
        'Pending channels count',
        None,
    )
    CHANNELS_UNREACHABLE_GAUGE = (
        'opentf.orchestrator.sshchannel.channels.unreachable',
        '1',
        'Unreachable channels count',
        None,
    )

    def __init__(
        self,
        metric_name: str,
        unit: str,
        description: str,
        callbacks: list[Callable] | None,
    ):
        self.metric_name = metric_name
        self.unit = unit
        self.description = description
        self.callbacks = callbacks


PHASES_GAUGES = {
    IDLE: SSHChannelMetrics.CHANNELS_IDLE_GAUGE,
    PENDING: SSHChannelMetrics.CHANNELS_PENDING_GAUGE,
    BUSY: SSHChannelMetrics.CHANNELS_BUSY_GAUGE,
    UNREACHABLE: SSHChannelMetrics.CHANNELS_UNREACHABLE_GAUGE,
}

HOSTS_PHASES = {}


def _fill_host_data(
    host_id: str, host_description: str | None = None
) -> dict[str, Any]:
    if not host_description:
        host_description = _get_host_description(host_id)
    return {
        host_description: {'host_id': host_id, 'tags': HOSTS[host_id].get('tags', [])}
    }


def _update_tags_gauges(tags: list[str], value: int) -> None:
    for tag in tags:
        TELEMETRY_MANAGER.set_metric(
            SSHChannelMetrics.HOSTS_TAGS_GAUGE, value, {'tag': tag}
        )


def _update_phase_gauges(host_id: str, current_phase: str) -> None:
    """Update phase gauges.

    # Required parameters:
    - host_id: a string, host UUID
    - current_phase: a string, host current phase
    """
    if (
        host_id in HOSTS
        and host_id in HOSTS_PHASES
        and HOSTS_PHASES[host_id] != current_phase
    ):
        TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[HOSTS_PHASES[host_id]], -1)
        TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[current_phase], 1)
        HOSTS_PHASES[host_id] = current_phase


def _update_hosts_phases(current: dict[str, Any], new: dict[str, Any]) -> None:
    """Update HOSTS_PHASES dictionary with new host UUIDs when POOLS
    definition is refreshed."""
    for data in current.values():
        TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[HOSTS_PHASES[data['host_id']]], -1)
        _update_tags_gauges(data['tags'], -1)
        del HOSTS_PHASES[data['host_id']]
    for data in new.values():
        HOSTS_PHASES[data['host_id']] = IDLE
        TELEMETRY_MANAGER.set_metric(PHASES_GAUGES[IDLE], 1)
        _update_tags_gauges(data['tags'], 1)


def _update_hosts_counters_gauges(current: dict[str, Any], new: dict[str, Any]) -> None:
    """Update hosts counters and gauges when POOLS definition is refreshed.

    # Required parameters:
    - current: a dictionary of strings, current hosts
    - new: a dictionary of strings, new hosts
    """
    if added := set(new) - set(current):
        for _ in added:
            TELEMETRY_MANAGER.set_metric(SSHChannelMetrics.HOSTS_CURRENT_GAUGE, 1)
    if removed := set(current) - set(new):
        for _ in removed:
            TELEMETRY_MANAGER.set_metric(SSHChannelMetrics.HOSTS_CURRENT_GAUGE, -1)

    _update_hosts_phases(current, new)


########################################################################
## Configuration Helpers


def _get_environ(suffix: str, default: str = '') -> str:
    return os.environ.get(
        f'SSHCHANNEL_{suffix}', os.environ.get(f'SSH_CHANNEL_{suffix}', default)
    )


def _maybe_add_environ_host(pools: dict[str, Any]) -> bool:
    """Add environment-defined host if specified.

    The environment-defined host is added in a `agents` pool.  The
    `agents` pool is added to `pools` if it does not exist.

    The host is added as the first host in the pool.

    # Required parameters

    - pools: a dictionary

    # Returned value

    A boolean.  True if the environment-defined host has been added to
    `pools`.  False otherwise.
    """
    if (
        _get_environ('HOST')
        and _get_environ('USER')
        and _get_environ('TAGS')
        and _get_environ('PASSWORD')
    ):
        debug('Adding execution environment defined by SSHCHANNEL_*...')
        agent = {
            'host': _get_environ('HOST'),
            'port': int(_get_environ('PORT', '22')),
            'username': _get_environ('USER'),
            'password': _get_environ('PASSWORD'),
            'tags': _get_environ('TAGS').split(','),
            'missing_host_key_policy': 'auto-add',
            'script_path': '/tmp',
        }
        pools['agents'] = [agent] + pools.get('agents', [])
        return True
    return False


def _read_external_pools_definition(pools_definition: str) -> dict[str, Any]:
    """Use environment-defined external pools if specified.

    The environment-defined external pools replaces those in `pools`.

    If no environment-defined external pools are specified, `pools` is
    unchanged.

    # Required parameters

    - pools_definition: a string

    # Returned value

    A dictionary of pools.  It may be empty.
    """

    info('Reading external pools definition "%s".', pools_definition)
    try:
        with open(pools_definition, 'r', encoding='utf-8') as f:
            manifest = yaml.safe_load(f)
        if 'pools' in manifest:
            return manifest['pools']
        error(
            'External pools definition "%s" needs a "pools" entry.  Ignoring.',
            pools_definition,
        )
    except Exception as err:
        error(
            'Could not read external pools definition "%s": %s.  Ignoring.',
            pools_definition,
            err,
        )
    return {}


def read_pools() -> None:
    """Configure pools.

    By default, use configuration-file pools and targets.

    If an environment-defined host is specified, it replaces the
    configuration-file pools and targets.

    If an external pools definition is provided, it replace the
    configuration-file pools and targets.  If this external pools
    definition is invalid, the pools and targets will be empty.

    If an external pools definition is provided as well as an
    environment-defined host, this environment-defined host is added
    to the pools and targets.  If the external pools definition is
    invalid, the only available pool and target will be the environment-
    defined host.
    """
    config = plugin.config['CONFIG']
    context = plugin.config['CONTEXT']
    if pools_definition := _get_environ('POOLS'):
        config['pools'] = _read_external_pools_definition(pools_definition)
        context['targets'] = list(config['pools'])
        new_config = config.copy()
        if 'hooks' in new_config:
            del new_config['hooks']
        valid, extra = validate_schema(SSHSERVICECONFIG, new_config)
        if not valid:
            error('Invalid pools definition: %s.  Ignoring.', extra)
            config['pools'] = {}
            context['targets'] = []
        if _maybe_add_environ_host(config['pools']):
            context['targets'] = list(config['pools'])
    elif _maybe_add_environ_host(config['pools']):
        context['targets'] = ['agents']
        config['pools']['agents'] = [config['pools']['agents'][0]]

    if context['targets']:
        info('Using pools: %s.', ', '.join(context['targets']))
    else:
        warning(
            'No targets defined, will not serve any execution environment requests.'
        )


def get_hosts_from_conf(*_) -> None:
    """Exit if plugin configuration is invalid.

    Fills the HOST list with valid entries.
    """
    debug('Reading pool configuration.')
    read_pools()

    new_hosts = {}
    for target in plugin.config['CONTEXT']['targets']:
        if target not in plugin.config['CONFIG']['pools']:
            error(f'Could not find target "{target}" in pool.')
            continue

        pool = plugin.config['CONFIG']['pools'][target]
        for entry in pool:
            if 'key_filename' in entry and 'password' in entry:
                error(
                    f'Bad host specification for {entry['host']} in pool "{target}",'
                    + ' cannot specify "password" and "key_filename" at the same time.'
                )
                entry['disabled'] = True
            if 'key_filename' not in entry and 'password' not in entry:
                error(
                    f'Bad host specification for {entry['host']} in pool "{target}",'
                    + ' must specify either "password" or "key_filename".'
                )
                entry['disabled'] = True
            if len(set(entry['tags']) & RUNNER_OS) != 1:
                error(
                    f'Bad host specification for {entry['host']} in pool "{target}",'
                    + f' must specify exactly one of {RUNNER_OS}.'
                )
                entry['disabled'] = True
            if 'disabled' not in entry:
                what = entry.copy()
                if 'host' in entry:
                    what['id'] = make_uuid()
                    new_hosts[what['id']] = what
                else:
                    for host in entry['hosts']:
                        what = entry.copy()
                        del what['hosts']
                        what['id'] = make_uuid()
                        what['host'] = host
                        new_hosts[what['id']] = what

    REQUESTS_QUEUE.put((new_hosts,))


def refresh_hosts(hosts: dict[str, Any]) -> None:
    """Refresh POOLS definition.

    # Required parameters

    - hosts: a dictionary, the new hosts
    """
    in_use = {
        _get_host_description(host_id): host_id
        for host_id in HOSTS
        if not is_available(host_id)
    }
    current, new = {}, {}
    for host_id in list(HOSTS):
        host_description = _get_host_description(host_id)
        if host_description in in_use:
            HOSTS[host_id]['transient'] = True
        else:
            try:
                current.update(_fill_host_data(host_id, host_description))
                del HOSTS[host_id]
            except KeyError:
                pass
    for host_id, host in hosts.items():
        host_description = f'{host["username"]}@{host["host"]}:{host.get("port", 22)}'
        if host_description in in_use:
            try:
                del HOSTS[in_use[host_description]]['transient']
            except KeyError:
                pass
            HOSTS[in_use[host_description]]['become'] = host
        else:
            HOSTS[host_id] = host
            new.update(_fill_host_data(host_id, host_description))

    _update_hosts_counters_gauges(current, new)
    notify_available_channels()
    if tags := get_tags():
        info(f'Providing environments for the following tags: {', '.join(tags)}')
    else:
        warning('No valid host specification found.  Providing no environments.')


def _initialize_pools(plugin) -> None:
    logging.info('Environment variables:')
    for var, val in ENVIRONMENT_VARIABLES.items():
        if newval := os.environ.get(var):
            logging.info(
                '  %s: %s',
                var,
                repr(newval) if var not in ENVIRONMENT_SECRETS else '*' * len(newval),
            )
        else:
            if val is not None:
                logging.info(
                    '  %s not set. Using default value: %s.',
                    var,
                    repr(val),
                )
            else:
                logging.info('  %s not set.  No default value.', var)
    logging.info(
        '  (If defined, SSHCHANNEL_* environment variables take precedence over SSH_CHANNEL_*)'
    )

    ref = ('HOST', 'USER', 'TAGS', 'PASSWORD')
    env = list(map(_get_environ, ref))
    if any(env) and not all(env):
        error('Incomplete execution environment specification, aborting:')
        for var in ref:
            if not _get_environ(var):
                error(f'  SSHCHANNEL_{var} not set')
        sys.exit(2)
    if all(env):
        tags = _get_environ('TAGS').split(',')
        for tag in tags:
            if not re.match(TAG_PATTERN, tag):
                error('Invalid tag specification in SSHCHANNEL_TAGS, aborting:')
                error('  the "%s" tag does not match "%s"', tag, TAG_PATTERN)
                sys.exit(2)
        if len(set(tags) & RUNNER_OS) != 1:
            error('Invalid tag specification in SSHCHANNEL_TAGS, aborting:')
            error(f'  must include exactly one of {RUNNER_OS}')
            sys.exit(2)

    if pools_definition := _get_environ('POOLS'):
        watch_file(plugin, pools_definition, get_hosts_from_conf)
    else:
        get_hosts_from_conf()


########################################################################
## Job Helpers

HOSTS: dict[str, Any] = {}
HOSTS_LEASES = {}

STEPS_QUEUE = Queue()
JOBS: dict[str, dict[str, Any]] = {}  # keys = job_id, values = job def
JOBS_HOSTS: dict[str, str] = {}  # keys = job_id, values = host_id
JOBS_MASKS = defaultdict(JobState)
CHANNELS = {}

REQUESTS_QUEUE = Queue()


# Steps helpers


def list_job_steps(job_id: str) -> dict[str, Any]:
    """List all steps for job.

    Returns a dictionary.  Keys are step sequence IDs and values are
    commands.
    """
    if job_id not in JOBS:
        JOBS[job_id] = {}
    return JOBS[job_id]


def add_job_step(job_id: str, command: dict[str, Any]) -> None:
    """Adding new step to existing job."""
    JOBS[job_id][command['metadata']['step_sequence_id']] = command


def get_job_host(job_id: str, channel_id: str) -> str | None:
    """Get host_id for job.

    # Required parameters

    - job_id: a string
    - channel_id: a string

    # Returned value

    A _host ID_ or None.  A host_id a string.
    """
    if job_id in JOBS_HOSTS:
        return JOBS_HOSTS[job_id]

    for host_id in CHANNELS[channel_id]:
        if host_id in HOSTS_LEASES and HOSTS_LEASES[host_id] >= datetime.now(
            timezone.utc
        ):
            JOBS_HOSTS[job_id] = host_id
            notify_available_channels()
            return host_id

    return None


def connect(host_id: str, client) -> None:
    """Connect to host.

    Handling various connection modes:

    - username, password
    - username, key_filename
    - username, key_filename, passphrase
    """
    host = HOSTS[host_id]
    if 'ssh_host_keys' in host:
        client.load_host_keys(host['ssh_host_keys'])
    if host.get('missing_host_key_policy', 'reject') == 'auto-add':
        client.set_missing_host_key_policy(AutoAddPolicy)
    if 'key_filename' in host and 'passphrase' in host:
        client.connect(
            host['host'],
            port=host.get('port', 22),
            username=host['username'],
            key_filename=host['key_filename'],
            passphrase=host['passphrase'],
        )
    elif 'key_filename' in host:
        client.connect(
            host['host'],
            port=host.get('port', 22),
            username=host['username'],
            key_filename=host['key_filename'],
        )
    else:
        client.connect(
            host['host'],
            port=host.get('port', 22),
            username=host['username'],
            password=host['password'],
        )


def get_tags() -> set[str]:
    """Get tags provided by plugin configuration.

    # Returned value

    A possibly empty set of strings.
    """
    tags = set()
    for host in HOSTS.values():
        for tag in host['tags']:
            tags.add(tag)
    return tags


def notify_available_channels() -> None:
    """Publish known channels and state."""
    publish(
        make_event(
            NOTIFICATION,
            metadata={
                'name': f'channels available from channel handler {CHANNELHANDLER_ID}',
                'workflow_id': 'n/a',
            },
            spec={
                'channel.handler': {
                    'channelhandler_id': CHANNELHANDLER_ID,
                    'channels': [
                        {
                            'apiVersion': 'opentestfactory.org/v1alpha1',
                            'kind': 'Channel',
                            'metadata': {
                                'name': host['host'],
                                'namespaces': ','.join(
                                    ns.strip()
                                    for ns in host.get(
                                        'namespaces', DEFAULT_NAMESPACE
                                    ).split(',')
                                ),
                            },
                            'spec': {
                                'tags': host['tags'],
                            },
                            'status': {
                                'phase': _get_phase(host_id),
                                'currentJobID': _get_job(host_id),
                            },
                        }
                        for host_id, host in HOSTS.items()
                        if 'disabled' not in host
                    ],
                }
            },
        ),
        context=plugin.config['CONTEXT'],
    )


def _get_phase(host_id: str) -> str:
    if not is_alive(host_id):
        _update_phase_gauges(host_id, UNREACHABLE)
        return UNREACHABLE
    if host_id in JOBS_HOSTS.values():
        _update_phase_gauges(host_id, BUSY)
        return BUSY
    if lease := HOSTS_LEASES.get(host_id):
        if lease > datetime.now(timezone.utc):
            _update_phase_gauges(host_id, PENDING)
            return PENDING
    _update_phase_gauges(host_id, IDLE)
    return IDLE


def _get_job(host_id: str) -> str | None:
    what = [job for job, host in JOBS_HOSTS.items() if host == host_id]
    if what:
        return what[0]
    return None


def _get_host_description(host_id: str) -> str:
    host = HOSTS[host_id]
    return f'{host["username"]}@{host["host"]}:{host.get("port", 22)}'


def get_matching_hosts(tags: list[str], namespace: str) -> list[str]:
    """List all nondisabled hosts capable of handling tags.

    # Required parameters

    - tags: a collection of strings
    - namespace: a string

    # Returned value

    A possibly empty list of _hosts IDs_.  A host ID is a string.
    """
    namespace = namespace.lower()
    requiredtags = set(tags)
    matching = []
    for host in HOSTS.values():
        if requiredtags.issubset(set(host['tags'])) and (
            any(
                ns.strip().lower() in (namespace, '*')
                for ns in host.get('namespaces', DEFAULT_NAMESPACE).split(',')
            )
        ):
            matching.append(host['id'])
    if not matching:
        info(
            f'Could not find environment matching request {tags} in namespace "{namespace}".'
        )
    return matching


def get_os(host_id: str) -> str:
    """Get host os."""
    return (set(HOSTS[host_id]['tags']) & RUNNER_OS).pop()


def is_available(host_id: str) -> bool:
    """Check if host has a lease or is busy handling a job."""
    if host_id in JOBS_HOSTS.values() or HOSTS.get(host_id, {}).get('transient'):
        return False
    if host_id in HOSTS_LEASES:
        return HOSTS_LEASES[host_id] < datetime.now(timezone.utc)
    return True


def is_alive(host_id: str) -> bool:
    """Checks if host is available."""
    try:
        with SSHClient() as client:
            connect(host_id, client)
            return True
    except Exception as err:
        debug(f'Could not reach host {_get_host_description(host_id)}: {err}.')
    return False


def make_offers(metadata: dict[str, Any], candidates: list[str]) -> None:
    """Make at least one offer.

    Currently makes exactly one offer, assuming the tags are valid.
    This may change at any time.

    # Required parameters

    - metadata: a dictionary
    - candidates: a non-empty list of strings

    # Returned value

    None.
    """
    for candidate_id in candidates:
        if not is_available(candidate_id):
            continue
        if not is_alive(candidate_id):
            continue
        channel_id = metadata['channel_id'] = make_uuid()
        metadata['channelhandler_id'] = CHANNELHANDLER_ID
        metadata['channel_tags'] = HOSTS[candidate_id]['tags']
        metadata['channel_os'] = get_os(candidate_id)
        metadata['channel_temp'] = HOSTS[candidate_id].get(
            'script_path', SCRIPTPATH_DEFAULT[metadata['channel_os']]
        )
        metadata['channel_lease'] = DEFAULT_CHANNEL_LEASE
        if hooks := plugin.config['CONFIG'].get('hooks'):
            metadata.setdefault('annotations', {})['hooks'] = hooks
        HOSTS_LEASES[candidate_id] = datetime.now(timezone.utc) + timedelta(
            seconds=DEFAULT_CHANNEL_LEASE
        )
        CHANNELS[channel_id] = [candidate_id]
        prepare = make_event(EXECUTIONRESULT, metadata=metadata, status=0)
        publish(prepare, context=plugin.config['CONTEXT'])
        TELEMETRY_MANAGER.set_metric(SSHChannelMetrics.CHANNEL_OFFERS_COUNTER, 1)
        notify_available_channels()
        break
    else:
        info(f'Could not find valid candidate for job {metadata['job_id']}.')


def release_resources(host_id: str | None, job_id: str, channel_id: str) -> None:
    """Release resources after teardown."""
    try:
        del JOBS_HOSTS[job_id]
    except KeyError:
        pass
    try:
        del JOBS[job_id]
    except KeyError:
        pass
    try:
        del JOBS_MASKS[job_id]
    except KeyError:
        pass
    try:
        del HOSTS_LEASES[host_id]
    except KeyError:
        pass
    try:
        del CHANNELS[channel_id]
    except KeyError:
        pass
    current, new = {}, {}
    try:
        if host_id and HOSTS[host_id].get('transient'):
            current = _fill_host_data(host_id)
            del HOSTS[host_id]
    except KeyError:
        pass
    try:
        if host_id and (new_host := HOSTS[host_id].get('become')):
            current.update(_fill_host_data(host_id))
            del HOSTS[host_id]
            HOSTS[new_host['id']] = new_host
            new = _fill_host_data(new_host['id'])
    except KeyError:
        pass
    _update_hosts_counters_gauges(current, new)
    notify_available_channels()


########################################################################


def run_command(command: dict[str, Any]) -> None:
    """Run a command.

    Runs in its own thread, so that it does not prevent other steps from
    running in parallel.

    Will publish an ExecutionError event if a technical error occurs
    while processing the command.

    # Required parameters

    - command: an _ExecutionCommand_ event
    """

    def _attach(remote_path, destination_url):
        debug(
            'Awaiting attachment %s from host %s.',
            remote_path,
            _get_host_description(host_id),
        )
        try:
            scp.get(remote_path=remote_path, local_path=destination_url)
        finally:
            # scp.py does not close the channel properly, so we do it ourselves
            scp.close()
        debug(
            'Got attachment %s from host %s, locally stored at %s.',
            remote_path,
            _get_host_description(host_id),
            destination_url,
        )

    def _put(remote_path, source_url):
        debug('Sending file %s to host %s.', source_url, _get_host_description(host_id))
        try:
            scp.put(remote_path=remote_path.encode(), files=source_url)
        finally:
            # scp.py does not close the channel properly, so we do it ourselves
            scp.close()
        debug(
            'Sent file %s to host %s, remotely stored as %s.',
            source_url,
            _get_host_description(host_id),
            remote_path,
        )

    metadata = command['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']
    channel_id = metadata['channel_id']

    if isinstance(CHANNELS[channel_id], list):
        if (host_id := get_job_host(job_id, channel_id)) is None:
            msg = f'Channel lease {channel_id} expired for job {job_id}'
            error(msg)
            publish(
                make_event(EXECUTIONERROR, metadata=metadata, details={'error': msg}),
                context=plugin.config['CONTEXT'],
            )
            REQUESTS_QUEUE.put((None, job_id, channel_id))
            return
        CHANNELS[channel_id] = host_id
        debug('SSH host %s handles job %s.', _get_host_description(host_id), job_id)
    else:
        host_id = CHANNELS[channel_id]

    metadata['channel_os'] = get_os(host_id)
    metadata['channel_temp'] = HOSTS[host_id].get(
        'script_path', SCRIPTPATH_DEFAULT[metadata['channel_os']]
    )
    script_path, script_content, script_command = make_script(
        command, HOSTS[host_id].get('script_path'), get_os(host_id)
    )

    try:
        with SSHClient() as client:
            connect(host_id, client)
            scp = SCPClient(client.get_transport())
            scp.putfo(BytesIO(bytes(script_content, 'utf-8')), script_path)
            _, stdout, stderr = client.exec_command(script_command)
            resp = stdout.channel.recv_exit_status()
            result = process_output(
                command,
                resp,
                stdout,
                stderr,
                JOBS_MASKS[job_id],
                _attach,
                _put,
            )
        if result['metadata'].get('upload') == 0:
            workflow_result = process_upload(result)
            publish(workflow_result, context=plugin.config['CONTEXT'])
        process_opentf_variables(result)
        publish(result, context=plugin.config['CONTEXT'])
    except Exception as err:
        error(f'An exception occurred while communicating to SSH environment:\n{err}')
        publish(
            make_event(EXECUTIONERROR, metadata=metadata, details={'error': str(err)}),
            context=plugin.config['CONTEXT'],
        )
    finally:
        if step_sequence_id == CHANNEL_RELEASE:
            REQUESTS_QUEUE.put((host_id, job_id, metadata['channel_id']))
            debug(
                'SSH host %s released from handling job %s.',
                _get_host_description(host_id),
                job_id,
            )


def prepare_commands():
    """Prepare command handler and start it."""
    while True:
        try:
            command = STEPS_QUEUE.get()
            job_id = command['metadata']['job_id']
            if command['metadata']['step_sequence_id'] in list_job_steps(job_id):
                continue
            add_job_step(job_id, command)

            threading.Thread(target=run_command, args=(command,), daemon=True).start()
        except Exception as err:
            error(f'Internal error while preparing command thread: {err}')


def prepare_channels():
    """Prepare hosts-related commands.

    Offers are serialized so that no race condition occurs and so that
    a 'fair' allocation stragegy can be defined.
    """
    while True:
        try:
            command = REQUESTS_QUEUE.get()
            if len(command) == MAKEOFFER_REQUEST:
                make_offers(*command)
            elif len(command) == REFRESHHOSTS_REQUEST:
                refresh_hosts(*command)
            elif len(command) == RELEASERESOURCES_REQUEST:
                release_resources(*command)
        except Exception as err:
            error(f'Internal error while making offer: {err}.')


def process_executioncommand(body: dict[str, Any]) -> None:
    """Process an incoming execution command.

    # Required parameters

    - body: a dictionary (a valid ExecutionCommand)
    """
    metadata = body['metadata']
    job_id = metadata['job_id']
    step_sequence_id = metadata['step_sequence_id']

    if step_sequence_id == CHANNEL_REQUEST:
        TELEMETRY_MANAGER.set_metric(SSHChannelMetrics.CHANNEL_REQUESTS_COUNTER, 1)
        # A new job, can we make an offer?
        candidates = get_matching_hosts(
            body['runs-on'], metadata.get('namespace', DEFAULT_NAMESPACE)
        )
        if candidates:
            REQUESTS_QUEUE.put((metadata, candidates))
        return

    if metadata.get('channel_id') not in CHANNELS:
        return

    if step_sequence_id in list_job_steps(job_id):
        warning(f'Step {step_sequence_id} already received for job {job_id}.')
        return

    STEPS_QUEUE.put(body)


########################################################################
## Main

CHANNELHANDLER_ID = make_uuid()

plugin = make_plugin(
    name='sshchannel',
    description='Create and start a SSH channel plugin.',
    channel=process_executioncommand,
    configfile=CONFIG_FILE,
    schema=SSHSERVICECONFIG,
    args=[CHANNELS],
)
if os.environ.get(f'{plugin.name.upper()}_DEBUG_LEVEL') in ('DEBUG', 'TRACE', 'NOTSET'):
    logging.getLogger('paramiko').setLevel(logging.DEBUG)
else:
    logging.getLogger('paramiko').setLevel(logging.WARNING)


try:
    TELEMETRY_MANAGER = TelemetryManager(SERVICE_NAME, SSHChannelMetrics)
except Exception as err:
    fatal('Failed to initialize TelemetryManager for %s: %s.', SERVICE_NAME, str(err))


def main(plugin):
    """Start the SSH channel plugin."""
    _initialize_pools(plugin)

    watch_and_notify(plugin, _get_phase, HOSTS, notify_available_channels)
    try:
        debug('Starting steps handling thread.')
        threading.Thread(target=prepare_commands, daemon=True).start()
    except Exception as err:
        fatal('Could not start steps handling thread: %s.', str(err))

    try:
        debug('Starting channel handling thread.')
        threading.Thread(target=prepare_channels, daemon=True).start()
    except Exception as err:
        fatal('Could not start offers handling thread: %s.', str(err))

    info('Channel handler ID: %s.', CHANNELHANDLER_ID)
    run_plugin(plugin)


if __name__ == '__main__':
    main(plugin)
