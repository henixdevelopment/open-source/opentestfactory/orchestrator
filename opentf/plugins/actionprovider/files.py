# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Category implementation for the 'action' provider.

The following categories are provided:

- create-archive

- create-file
- delete-file
- get-file
- get-files
- put-file
- touch-file

- prepare-inception

- upload-artifact
- download-artifact
"""

import json

import yaml

from opentf.toolkit import core


########################################################################
## Constants

TOUCHFILE_CATEGORY = 'touch-file'
CREATEARCHIVE_CATEGORY = 'create-archive'
CREATEFILE_CATEGORY = 'create-file'
DELETEFILE_CATEGORY = 'delete-file'
GETFILES_CATEGORY = 'get-files'
GETFILE_CATEGORY = 'get-file'
PREPAREINCEPTION_CATEGORY = 'prepare-inception'
PUTFILE_CATEGORY = 'put-file'
UPLOADARTIFACT_CATEGORY = 'upload-artifact'
DOWNLOADARTIFACT_CATEGORY = 'download-artifact'

UPLOADARTIFACT_TYPE = 'application/vnd.opentestfactory.artifact+binary'

########################################################################
## Formatters


def produce_ini_content(data):
    """Format data as ini file content."""
    content = ''

    for primary in data:
        content += f'[{primary}]\n'
        for key, value in data[primary].items():
            content += f'{key}={value}\n'

    return content


def produce_json_content(data):
    """Format data as json string."""
    return json.dumps(data)


def produce_txt_content(data):
    """Format data as txt string."""
    return data


def produce_yaml_content(data):
    """Format data as yaml string."""
    return yaml.safe_dump(data)


########################################################################
## 'prepare-inception' action


def prepareinception_action(inputs):
    """Process 'prepare-inception' action.

    `prepare-inception` actions have a mandatory input per file it
    prepares.

    ```yaml
    - uses: actions/prepare-inception@v1
      with:
        export.xml: ${{ resources.files.export }}
        report.html: ${{ resources.files.report }}
    ```
    """
    workflow_id = core._getbody()['metadata']['workflow_id']
    if not all(inputs.values()):
        core.fail(
            f'Unknown resources.files: {", ".join(k for k, v in inputs.items() if not v)}.'
        )
    return [
        {'run': f'::inception::{workflow_id}::{name}::{value["url"]}'}
        for name, value in inputs.items()
    ]


########################################################################
## 'touch-file' action


def touchfile_action(inputs):
    """Process 'touch-file' action.

    Ensure file exists on the execution environment.

    `touch-file` actions have mandatory `path` input:

    ```yaml
    - uses: actions/touch-file@v1
      with:
        path: foobar.ini
    ```

    This would create an empty `'foobar.ini'` file if it did not already
    exist.
    """
    return [{'run': core.touch_file(inputs['path'])}]


########################################################################
## 'create-file' action


SUPPORTED_CONTENT_TYPES = {
    'ini': produce_ini_content,
    'json': produce_json_content,
    'txt': produce_txt_content,
    'yaml': produce_yaml_content,
}


def createfile_action(inputs):
    """Process 'create-file' action.

    `create-file` actions have mandatory `data`, `format` and `path`
    inputs:

    ```yaml
    - uses: actions/create-file@v1
      with:
        data:
          foo:
            key1: value1
            key2: value2
          bar:
            key1: value1
            key2: value2
        format: ini
        path: foobar.ini
    ```

    This would create a file `'foobar.ini'` with the following content:

    ```ini
    [foo]
    key1=value1
    key2=value2
    [bar]
    key1=value1
    key2=value2
    ```

    The following values for `format` are handled so far:

    - ini
    - json
    - txt
    - yaml
    """
    format_ = inputs['format']
    path = core.normalize_path(inputs['path'])
    data = inputs['data']

    if format_ not in SUPPORTED_CONTENT_TYPES:
        core.fail(
            f'Unknown format {format_}, expecting one of {set(SUPPORTED_CONTENT_TYPES)}.'
        )

    content = SUPPORTED_CONTENT_TYPES[format_](data)

    return [{'run': core.create_file(path, content)}]


def deletefile_action(inputs):
    """Process 'delete-file' action.

    `delete-file` actions have a mandatory `path` input:

    ```yaml
    - uses: actions/delete-file@v1
      with:
        path: foobar.ini
    ```
    """
    return [{'run': core.delete_file(inputs['path'])}]


def getfile_action(inputs):
    """Process 'get-file' action.

    `get-file` actions attach the specified file so that publisher
    plugins can process them.  They have mandatory `path` input:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'foo.xml'
    ```

    The `get_file` action has an optional `type` input, which is
    expected to be a media type:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'foobar.xml'
        type: 'application/xml'
    ```

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'bar.json'
      working-directory: /data/foo
    ```
    """
    if 'type' in inputs:
        attach = core.attach_file(inputs['path'], type=inputs['type'])
    else:
        attach = core.attach_file(inputs['path'])
    return [{'run': attach}]


########################################################################
## 'get-files' action

NO_FILES_ERROR_MESSAGE = 'The specified pattern does not match any files.'
NO_FILES_WARNING_MESSAGE = (
    'The specified pattern does not match any files. No files were attached.'
)


def getfiles_action(inputs):
    """Process 'get-files' action.

    `get-files` actions attach the matching files so that publisher
    plugins can process them.  They have mandatory `pattern` input:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.xml'
    ```

    An optional `type` input may be specified.  It is expected to be
    a media type.

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.json'
      working-directory: /data/foo
    ```

    If you want to get files recursively in a folder tree, use the
    `**/*.ext` pattern format:

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '**/*.html'
      working-directory: /data/foo
    ```

    An optional `warn-if-not-found` input may be specified.  A warning
    will be issued if no file matching pattern is found.

    An optional `skip-if-not-found` input may be specified.  No message
    will be issued if no file matching pattern is found.

    `warn-if-not-found` and `skip-if-not-found` can not be specified
    simultaneously.
    """
    pattern = inputs['pattern']
    skip = str(inputs.get('skip-if-not-found')).lower() == 'true'
    warn = inputs.get('warn-if-not-found')

    quote = '' if core.runner_on_windows() else '"'
    warn = NO_FILES_WARNING_MESSAGE if warn is True else warn

    files_not_found = ''

    if skip and warn:
        core.fail(
            'Both warn-if-not-found and skip-if-not-found specified.  You can only specify one.'
        )
    if skip and not warn:
        files_not_found = 'exit 0'
    else:
        files_not_found = (
            f'echo {quote}::warning::{warn}{quote} & exit 0'
            if warn
            else f'(echo {quote}::error::{NO_FILES_ERROR_MESSAGE}{quote} & exit 1)'
        )

    type_ = inputs.get('type')
    if type_:
        type_ = ' type=' + type_.strip()
    else:
        type_ = ''

    if pattern.startswith('**/') or pattern.startswith('**\\'):
        file_pattern = pattern[3:]
        if core.runner_on_windows():
            attach_files = f'@for /f %%i in (\'dir /s /b /a:-d "{file_pattern}"\') do @echo ::attach{type_}::%%i'
            what = f'({attach_files}) || {files_not_found}'
        else:
            attach_pattern = (
                f'\\{file_pattern}'
                if file_pattern in ('*', '**')
                else f"'{file_pattern}'"
            )
            attach_files = f'for f in $(find -name {attach_pattern}); do if test -f $f; then echo "::attach{type_}::$(pwd)/$f"; fi; done'
            what = f'if test -z "$(find . -name \'{file_pattern}\' -print -quit)"; then {files_not_found}; else {attach_files} fi'
    else:
        if core.runner_on_windows():
            attach_files = (
                f'(@for %%i in ({pattern}) do @echo ::attach{type_}::%CD%\\%%i)'
            )
            what = f'{attach_files} & if not exist {pattern} {files_not_found}'
        else:
            attach_files = f'for f in {pattern} ; do test -f "$(pwd)/$f" && echo "::attach{type_}::$(pwd)/$f" ; done'
            what = f'if test -z "$(find . -maxdepth 1 -name \'{pattern}\' -print -quit)"; then {files_not_found}; else {attach_files}; fi'

    return [{'run': what}]


def putfile_action(inputs):
    """Process 'put-file' action.

    `put-file` actions have mandatory `data` and `path` inputs:

    ```yaml
    - uses: actions/put-file@v1
      with:
        file: file-to-put
        path: destination-path
    ```
    """
    if core.runner_on_windows():
        what = f'echo ::put file={inputs["file"]}::{inputs["path"]}'
    else:
        what = f'echo "::put file={inputs["file"]}::{inputs["path"]}"'

    return [{'run': what}]


########################################################################
## 'upload-artifact' action


def uploadartifact_action(inputs):
    """Process 'upload-artifact' action.

    `upload-artifact` actions have mandatory `path` input:

    ```yaml
    - uses: actions/upload-artifact@v1
      with:
        name: my-artifact
        path: path/to/artifact/world.txt
        if-no-file-found: warn | error | ignore (defaults to warn)
    ```
    """
    not_found = inputs.get('if-no-file-found', 'warn')
    if not_found not in ('error', 'warn', 'ignore'):
        not_found = 'warn'

    filetype = f'type={UPLOADARTIFACT_TYPE}'
    if name := inputs.get('name'):
        upload = f'::upload {filetype},name={name}::'
    else:
        upload = f'::upload {filetype}::'

    quote = '' if core.runner_on_windows() else '"'

    path = inputs['path']
    msg = f'File {path} not found.'
    if not_found == 'error':
        no_files_action = f'echo {quote}::error::{msg}{quote} && exit 1'
    elif not_found == 'warn':
        no_files_action = f'echo {quote}::warning::{msg}{quote} && exit 0'
    else:
        no_files_action = 'exit 0'

    if core.runner_on_windows():
        path = core.normalize_path(path)
        upload = (
            f'@if exist {path} (@echo {upload}%CD%\\{path}) else ({no_files_action})'
        )
    else:
        upload = f'[ -f {path} ] && echo "{upload}`pwd`/{path}" || ({no_files_action})'
    return [{'run': upload}]


########################################################################
## 'download-artifact' action


def downloadartifact_action(inputs):
    """Process 'download-artifact' action.

    ```yaml
    - uses: actions/download-artifact@v1
      with:
        name: my-artifact         # at most one of 'name', 'pattern'
        pattern: a pattern
        path: in-this-directory   # optional, workspace if unspecified
    ```
    """
    name = inputs.get('name')
    pattern = inputs.get('pattern')
    path = inputs.get('path', '')

    download = ''

    if name and pattern:
        core.fail('Only one of `name`, `pattern` should be specified.')
    if not name and not pattern:
        pattern = '*'
    if name:
        download = f'::download file={name}::{path}'
    if pattern:
        download = f'::download pattern={pattern}::{path}'

    if core.runner_on_windows():
        download = f'@echo {download}'
    else:
        download = f'echo "{download}"'
    return [{'run': download}]


########################################################################
## 'create-archive' action


DEFAULT_ARCHIVE_FORMAT = 'tar.gz'
SUPPORTED_ARCHIVE_FORMATS = {
    DEFAULT_ARCHIVE_FORMAT: 'tar -czf',
    'tar': 'tar -cf',
}
LIST_ARCHIVE_FILES = 'list_tar_files'
TMP_ARCHIVE_FILES = 'tmp_tar_files'
EMPTY_ARCHIVE_FILE = 'intentionally_empty_archive'
DEPTH_LEVEL1 = ' -maxdepth 1'
ARCHIVE_ERROR_MESSAGE = 'The specified patterns do not match any files.'
ARCHIVE_WARNING_MESSAGE = (
    'The specified patterns do not match any files. The generated archive is empty.'
)


def pattern_is_directory(pattern: str) -> bool:
    """Test if pattern is directory or not?"""
    return pattern.endswith(('/', '\\'))


def dirname_exists_in_new_list(patt_list, sep: str, pattern: str) -> bool:
    """Test if dirname exists in new pattern list or not?"""
    dirname = pattern.rpartition(sep)[0].rpartition(sep)[0] + sep
    while len(dirname) > 1:
        if dirname in patt_list:
            return True
        dirname = dirname.rpartition(sep)[0].rpartition(sep)[0] + sep
    return False


def filter_pattern_list(patterns):
    """Filter the pattern list."""
    new_list = []
    for pattern in patterns:
        # Processing a directory.
        if pattern_is_directory(pattern):
            if pattern not in new_list:
                if pattern.endswith('/'):
                    if not dirname_exists_in_new_list(new_list, '/', pattern):
                        new_list.append(pattern)
                else:
                    if not dirname_exists_in_new_list(new_list, '\\', pattern):
                        new_list.append(pattern)
        else:
            # Processing a file.
            if pattern not in new_list:
                if pattern.rpartition('/')[1]:
                    if not dirname_exists_in_new_list(new_list, '/', pattern + '/'):
                        new_list.append(pattern)
                elif pattern.rpartition('\\')[1]:
                    if not dirname_exists_in_new_list(new_list, '\\', pattern + '\\'):
                        new_list.append(pattern)
                else:
                    new_list.append(pattern)
    return new_list


def _add_pattern_handling_steps(
    steps, file_pattern: str, depth_level: str, recursive_mode: bool
):
    """Add to steps the commands required to handle file_pattern.

    `depth_level` allows to authorize or not the recursive searches
    in the subfolders of the current directory.
    If `depth_level` = DEPTH_LEVEL1, then the search is only performed
    in the current directory.

    `recursive_mode` is True when the pattern format starts with '**/',
    or with '**\\' otherwise it defaults to False.
    """
    if core.runner_on_windows():
        if depth_level == DEPTH_LEVEL1 or pattern_is_directory(file_pattern):
            if pattern_is_directory(file_pattern):
                # To select a directory pattern.
                file_pattern = core.normalize_path(file_pattern)
                search_pattern = f'''@echo off
                        for /f %%G in ("{file_pattern}") do if exist %%G\\nul (echo %%G>> {LIST_ARCHIVE_FILES})'''
            else:
                file_pattern = core.normalize_path(file_pattern)
                if file_pattern != '*':
                    # To select a file pattern.
                    search_pattern = f'''@echo off
                            for /f %%G in ("{file_pattern}") do if exist %%G (echo %%G>> {LIST_ARCHIVE_FILES})'''
                else:
                    # To list only files in the current directory.
                    search_pattern = f'''@echo off
                        if exist {TMP_ARCHIVE_FILES} del /f/q {TMP_ARCHIVE_FILES}
                        for /f "tokens=*" %%G in ('dir /b /a:-D "{file_pattern}"') do (
                        echo %%G>> {TMP_ARCHIVE_FILES}
                        )
                        if exist {TMP_ARCHIVE_FILES} (
                        findstr /v {LIST_ARCHIVE_FILES} {TMP_ARCHIVE_FILES}>> {LIST_ARCHIVE_FILES}
                        )'''
            steps.append(
                {
                    'run': search_pattern,
                    'continue-on-error': True,
                }
            )
        else:
            if recursive_mode and file_pattern == '*':
                # To list only directories, if recursive mode is true.
                search_pattern = f'''@echo off
                    set "firstDirectory=%CD%"
                    setlocal EnableDelayedExpansion
                    for /f "tokens=*" %%G in ('dir /b /s /a:D "{file_pattern}"') do (
                    set "item=%%G"
                    set "item=!item:%firstDirectory%\\=!"
                    echo !item!>> {LIST_ARCHIVE_FILES}
                    )'''
                steps.append(
                    {
                        'run': search_pattern,
                        'continue-on-error': True,
                    }
                )
            if recursive_mode:
                # Options to list only files.
                dir_options = '/b /s /a:-D'
            else:
                # Options to list all objects.
                dir_options = '/b /s'
            search_pattern = f'''@echo off
                set "firstDirectory=%CD%"
                setlocal EnableDelayedExpansion
                if exist {TMP_ARCHIVE_FILES} del /f/q {TMP_ARCHIVE_FILES}
                for /f "tokens=*" %%G in ('dir {dir_options} "{file_pattern}"') do (
                set "item=%%G"
                set "item=!item:%firstDirectory%\\=!"
                echo !item!>> {TMP_ARCHIVE_FILES}
                )
                if exist {TMP_ARCHIVE_FILES} (
                findstr /v {LIST_ARCHIVE_FILES} {TMP_ARCHIVE_FILES}>> {LIST_ARCHIVE_FILES}
                )'''
            steps.append(
                {
                    'run': search_pattern,
                    'continue-on-error': True,
                }
            )
    else:
        if recursive_mode is False and file_pattern.count("*") == 0:
            if pattern_is_directory(file_pattern):
                # To select a directory pattern.
                file_pattern = core.normalize_path(file_pattern)
                steps.append(
                    {
                        'run': f'if [ -d "{file_pattern}" ]; then echo "{file_pattern}">> {LIST_ARCHIVE_FILES};fi',
                        'continue-on-error': True,
                    }
                )
            else:
                # To select a file pattern.
                file_pattern = core.normalize_path(file_pattern)
                steps.append(
                    {
                        'run': f'if [ -f "{file_pattern}" ]; then echo "{file_pattern}">> {LIST_ARCHIVE_FILES};fi',
                        'continue-on-error': True,
                    }
                )
        else:
            pattern_directory = file_pattern.rpartition('/')[0]
            if not pattern_directory:
                pattern_directory = '.'
            else:
                pattern_directory = core.normalize_path(pattern_directory)
            pattern_name = file_pattern.rpartition('/')[2]
            if not pattern_name:
                pattern_name = '*'
            else:
                pattern_name = core.normalize_path(pattern_name)
            # To list only files.
            search_pattern = f'''find {pattern_directory}{depth_level} -name "{pattern_name}" -type f -print | grep -v {LIST_ARCHIVE_FILES}>> {LIST_ARCHIVE_FILES}'''
            steps.append(
                {
                    'run': search_pattern,
                    'continue-on-error': True,
                }
            )
            if recursive_mode and pattern_name == '*':
                # To add only empty directories.
                search_pattern = f'''for i in $(find {pattern_directory}{depth_level} -name "{pattern_name}" -type d -print | sort -r | grep -v "^.$");
                        do if [ "$(grep "^$i" {LIST_ARCHIVE_FILES})" == "" ]; then echo "$i">> {LIST_ARCHIVE_FILES}; fi;
                        done'''
                steps.append(
                    {
                        'run': search_pattern,
                        'continue-on-error': True,
                    }
                )


def createarchive_action(inputs):
    """Process 'create-archive' action.

    `create-archive` actions create an archive from selected files or directories.
    They have mandatory `path` and `patterns` inputs.

    If no files or directories match the specified patterns, an error message is issued,
    no archive is created, and the workflow is stopped. This is the default behavior.

    An optional `warn-if-not-found` input can be specified. Instead of the default behavior,
    if the specified patterns do not match any files, a warning message is issued,
    an empty archive is created, and the workflow continues.

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - bar/
        - foo.html
        - '*.png'
    ```

    An optional `format` input may be specified.
    Possible archive formats are `tar` or `tar.gz`.
    Default format is `tar.gz`.

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar
        format: tar
        patterns:
        - 'data/foo/screenshots/*.svg'
        - data/foo/bar/
        - data/foo/foo.html
        - 'data/foo/*.png'
    ```

    If you need to archive files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        format: tar.gz
        patterns:
        - 'screenshots/*.svg'
        - bar/
        - foo.html
        - '*.png'
      working-directory: /data/foo
    ```

    If you want to archive files recursively in a folder tree, use the
    `**/*.ext` pattern format:
    The `*` character in a subdirectory is not supported in Windows environment.

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - '**/*.png'
        - 'screenshots/*.svg'
        - bar/
        - foo.html
    ```

    If you want to display a warning message when no files matching one of the patterns are found,
    use the `warn-if-not-found` input. This allows to create an empty archive:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - bar/
        - foo.html
        - '*.png'
        warn-if-not-found: true
    ```

    If you want to archive all present files and subfolders recursively in the working-directory
    `/data/foo`, use the `**/*` pattern format:

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - '**/*'
      working-directory: /data/foo
    ```
    """
    path = inputs['path']
    patterns = inputs['patterns']

    warn = inputs.get('warn-if-not-found')
    warn = ARCHIVE_WARNING_MESSAGE if warn is True else warn
    format_ = inputs.get('format')
    if format_ is None:
        format_ = DEFAULT_ARCHIVE_FORMAT
    if format_ not in SUPPORTED_ARCHIVE_FORMATS:
        core.fail(
            f'Unknown {format_} archive format, expecting one of {set(SUPPORTED_ARCHIVE_FORMATS)}.'
        )
    archive_cmd = SUPPORTED_ARCHIVE_FORMATS[format_]

    cleanup = '\n'.join(
        core.delete_file(f)
        for f in (path, TMP_ARCHIVE_FILES, EMPTY_ARCHIVE_FILE, LIST_ARCHIVE_FILES)
    )
    steps = [
        {'run': cleanup + '\n' + core.touch_file(LIST_ARCHIVE_FILES)},
    ]

    patterns.sort()
    new_patterns = filter_pattern_list(patterns)
    for pattern in new_patterns:
        depth_level = ' '
        recursive_mode = False
        if pattern.startswith(('**/', '**\\')):
            file_pattern = pattern[3:]
            recursive_mode = True
            if not file_pattern:
                file_pattern = '*'
        elif pattern_is_directory(pattern):
            file_pattern = pattern
        else:
            depth_level = DEPTH_LEVEL1
            if pattern.count('*') > 1 and pattern.count('*') == len(pattern):
                file_pattern = '*'
            else:
                file_pattern = pattern
        _add_pattern_handling_steps(steps, file_pattern, depth_level, recursive_mode)
        if recursive_mode and file_pattern == '*':
            break

    if core.runner_on_windows() and recursive_mode:
        what = f'{archive_cmd} "{path}" --no-recursion -T {LIST_ARCHIVE_FILES}'
    else:
        what = f'{archive_cmd} "{path}" -T {LIST_ARCHIVE_FILES}'
    if core.runner_on_windows():
        if warn:
            steps.append(
                {
                    'run': f'for /f %%i in ("{LIST_ARCHIVE_FILES}") do if %%~zi gtr 0 ({what}) else (@type nul >>{EMPTY_ARCHIVE_FILE} & {archive_cmd} "{path}" {EMPTY_ARCHIVE_FILE} & echo ::warning::{warn})'
                }
            )
        else:
            steps.append(
                {
                    'run': f'for /f %%i in ("{LIST_ARCHIVE_FILES}") do if %%~zi gtr 0 ({what}) else (echo ::error::{ARCHIVE_ERROR_MESSAGE} & exit 1)'
                }
            )
    else:
        steps.append(
            {
                'run': f'if [ -s "{LIST_ARCHIVE_FILES}" ]; then mv {LIST_ARCHIVE_FILES} {TMP_ARCHIVE_FILES}; cat {TMP_ARCHIVE_FILES} | sed "s/\\(^.\\/\\)\\(.*\\)/\\2/" > {LIST_ARCHIVE_FILES};fi'
            }
        )
        if warn:
            steps.append(
                {
                    'run': f'if [ -s "{LIST_ARCHIVE_FILES}" ]; then {what}; else touch {EMPTY_ARCHIVE_FILE}; {archive_cmd} "{path}" {EMPTY_ARCHIVE_FILE}; echo "::warning::{warn}"; fi'
                }
            )
        else:
            steps.append(
                {
                    'run': f'if [ -s "{LIST_ARCHIVE_FILES}" ]; then {what}; else echo "::error::{ARCHIVE_ERROR_MESSAGE}"; exit 1;fi'
                }
            )

    return steps
