# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# plugin.yaml
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  title: Actions
  license: apache2
  description: |
    This plugin provides common functions that can be used on any execution environment.

    They cover the following areas:

    - `git` commands
    - files commands
    - inception commands

    The functions have an `actions` category prefix.
events:
- categoryPrefix: actions

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: checkout
  description: |
    Checkout a repository at a particular version.

    `checkout` functions have a mandatory `repository` input:

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
    ```

    They also allow for two optional inputs:

    - `ref`, which is the git reference to checkout: a branch name, a
      tag name, or a commit SHA (defaults to the default branch if
      unspecified). You must provide a complete commit SHA (40 characters for SHA-1
      or 64 characters for SHA-256).
    - `path`, which is where to clone/checkout the repository, relative
      to the current workspace.

    If the `repository` input contains a password or an authorization token, it
    will be masked in the console logs output.

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
        ref: dev
        path: foo/bar
    ```
branding:
  icon: arrow-down
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: checkout
  categoryVersion: v2
- categoryPrefix: action  # for TM generator
  category: checkout
  categoryVersion: v2
inputs:
  repository:
    description: the repository to clone
    required: true
  ref:
    description: the branch, tag, or SHA to checkout
    required: false
  path:
    description: where to clone the repository
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: checkout
  description: |
    Checkout a repository at a particular version. Compared to `actions/checkout@v2`,
    `actions/checkout@v3` allows additionally to indicate the number of commits to fetch.

    `checkout` functions have a mandatory `repository` input:

    ```yaml
    - uses: actions/checkout@v3
      with:
        repository: https://github.com/robotframework/RobotDemo.git
    ```

    They also allow for three optional inputs:

    - `ref`, which is the git reference to checkout: a branch name, a
      tag name, or a commit SHA (defaults to the default branch if
      unspecified). You must provide a complete commit SHA (40 characters for SHA-1
      or 64 characters for SHA-256).
    - `path`, which is where to clone/checkout the repository, relative
      to the current workspace.
    - `fetch-depth`, which indicates the number of commits to fetch. The default
      value is `1`. To fetch all history, set the value to `0`.

    If the git reference to check out is a commit SHA, you must ensure that
    a correct `fetch-depth` value is used to fetch the commit, or set it to `0`.

    When the `ref` parameter is unspecified, history is fetched for all the
    branches at a given depth (1 by default).

    If the `repository` input contains a password or an authorization token, it
    will be masked in the console logs output.
  notes: |
    ## Examples

    In this example, only the two last commits on `dev` branch are fetched
    into `foo/bar`.

    ```yaml
    - uses: actions/checkout@v3
      with:
        repository: https://github.com/robotframework/RobotDemo.git
        ref: dev
        path: foo/bar
        fetch-depth: 2
    ```

    In this example, only the last commit on `v22.1` branch is fetched.

    ```yaml
    - uses: actions/checkout@v3
      with:
        repository: https://github.com/robotframework/RobotDemo.git
        ref: v22.1
    ```
branding:
  icon: arrow-down
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: checkout
  categoryVersion: v3
- categoryPrefix: action  # for TM generator
  category: checkout
  categoryVersion: v3
inputs:
  repository:
    description: the repository to clone
    required: true
  ref:
    description: the branch, tag, or SHA to checkout
    required: false
  path:
    description: where to clone the repository
    required: false
  fetch-depth:
    description: number of commits to fetch
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: create-file
  description: |
    Create a file on the execution environment.
  notes: |
    ## Example

    ```yaml
    - uses: actions/create-file@v1
      with:
        data:
          foo:
            key1: value1
            key2: value2
          bar:
            key1: value1
            key2: value2
        format: ini
        path: foobar.ini
    ```

    This will create a `foobar.ini` file with the following content:

    ```ini
    [foo]
    key1=value1
    key2=value2
    [bar]
    key1=value1
    key2=value2
    ```
branding:
  icon: file-plus
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: create-file
inputs:
  data:
    description: the file content
    required: true
  format:
    description: the file format, one of `ini`, `json`, `txt`, or `yaml`
    required: true
  path:
    description: the file location, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: put-file
  description: |
    Put an existing file on the execution environment.
  notes: |
    ## Examples

    Put a file in the current directory in an execution environment:

    ```yaml
    - uses: actions/put-file@v1
      with:
        file: file-to-put
        path: destination-path
    ```

    If you need to put a file somewhere other than the current directory,
    use the `working-directory` statement:

    ```yaml hl_lines="5"
    - uses: actions/put-file@v1
      with:
        file: file-to-put
        path: destination-path
      working-directory: /foo/bar
    ```

    If you want to put a file in the execution environment from a workflow, the file
    must be declared as a workflow resource:

    ```yaml hl_lines="3-5"
    metadata:
      name: put-file example
    resources:
      files:
      - foo.json
    jobs:
      keyword-driven:
        runs-on: ssh
        steps:
        - uses: actions/put-file@v1
          with:
            file: foo.json
            path: bar/baz.json
          working-directory: /qux/quux/corge
    ```

    This example can be run with the following `cURL` command:

    ```bash
    curl -X POST \
         -F workflow=@{my-workflow.yaml} \
         -F foo.json=@foo.json \
         -H "Authorization: Bearer {my-token}" \
         http://example.com/workflows
    ```

    If the referred file is not part of the `resources` section of your workflow file,
    the step will fail with an error code 2.
branding:
  icon: file-text
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
  - categoryPrefix: actions
    category: put-file
inputs:
  file:
    description: |
      the file to upload, which must be an entry in the `resources.files` part of
      the workflow
    required: true
  path:
    description: the file destination, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: touch-file
  description: |
    Ensure a file exists on the execution environment.

    If the file does not exist, it will be created (with an empty content).
  notes: |
    ## Example

    ```yaml
    - uses: actions/touch-file@v1
      with:
        path: foobar.ini
    ```
branding:
  icon: file
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: touch-file
inputs:
  path:
    description: the file location, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: delete-file
  description: |
    Delete a file on the execution environment.
  notes: |
    ## Example

    ```yaml
    - uses: actions/delete-file@v1
      with:
        path: foobar.ini
    ```
branding:
  icon: file-minus
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: delete-file
inputs:
  path:
    description: the file location, relative to the current workspace
    required: true

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: create-archive
  description: |
    Create an archive from a set of selected files or directories on the execution environment.

    `create-archive` functions create an archive from selected files or directories.
    They have mandatory `path` and `patterns` inputs.

    If no files or directories match the specified patterns, an error message is issued,
    no archive is created, and the workflow is stopped. This is the default behavior.

    An optional `warn-if-not-found` input can be specified.
    Instead of the default behavior, if the specified patterns do not match any files,
    a warning message is issued, an archive containing an empty file `intentionnally_empty_archive`
    is created, and the workflow continues.

    An optional `format` input may be specified.
    Possible archive formats are `tar` or `tar.gz`. The default format is `tar.gz`.
  notes: |
    ## Examples

    ```yaml
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - bar/
        - foo.html
        - '*.png'
    ```

    If you want to specify a `tar` format to create the archive instead of the `tar.gz`
    default format, use the `format` input:

    ```yaml hl_lines="4"
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar
        format: tar
        patterns:
        - 'data/foo/screenshots/*.svg'
        - data/foo/bar/
        - data/foo/foo.html
        - 'data/foo/*.png'
    ```

    If you need to archive files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml hl_lines="10"
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        format: tar.gz
        patterns:
        - 'screenshots/*.svg'
        - bar/
        - foo.html
        - '*.png'
      working-directory: /data/foo
    ```

    If you want to archive files recursively in a directory tree, use the
    `**/*.ext` pattern format:

    ```yaml hl_lines="5"
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - '**/*.png'
        - 'screenshots/*.svg'
        - bar/
        - foo.html
    ```

    If you want to display a warning message when no files matching one of the patterns are found,
    use the `warn-if-not-found` input. This allows to create an empty archive:

    ```yaml hl_lines="9"
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - bar/
        - foo.html
        - '*.png'
        warn-if-not-found: "No files to archive found, an empty archive will be created."
    ```

    If you want to create an empty archive and display a default warning message
    when no files matching one of the patterns are found, set the `warn-if-not-found`
    input to `true`.

    ```yaml hl_lines="9"
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - 'screenshots/*.svg'
        - bar/
        - foo.html
        - '*.png'
        warn-if-not-found: true
    ```

    If you want to archive all present files and sub-directories recursively in the working directory
    `/data/foo`, use the `**/*` pattern format:

    ```yaml hl_lines="5"
    - uses: actions/create-archive@v1
      with:
        path: myarchive.tar.gz
        patterns:
        - '**/*'
      working-directory: /data/foo
    ```
branding:
  icon: archive
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
  - categoryPrefix: actions
    category: create-archive
inputs:
  path:
    description: The archive name, relative to the current working directory.
    required: true
  patterns:
    description: |
        A list of directory or file name patterns, possibly with paths.
        They may contain placeholders (`*` or `**`).
        The `*` character in a sub-directory is not supported in the Windows environment.
    required: true
  format:
    description: The archive format, either `tar` or `tar.gz` if specified, defaulting to `tar.gz` if not specified.
    required: false
  warn-if-not-found:
    description: |
      A message to display when no files matching one of the patterns are found.
      If specified, enables the generation of an empty archive.
      May also be set to `true`, in this case, the default warning message is logged.
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: get-file
  description: |
    Attach a file from the execution environment.

    `get-file` functions attach the specified file so that publisher
    plugins can process them.  They have a mandatory `path` input and
    an optional `type` input.
  notes: |
    ## Examples

    ```yaml
    - uses: actions/get-file@v1
      with:
        path: 'foo.xml'
    ```

    It is a good practice to specify the attachment type, if known:

    ```yaml hl_lines="4"
    - uses: actions/get-file@v1
      with:
        path: 'foobar.xml'
        type: 'application/xml'
    ```

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml hl_lines="4"
    - uses: actions/get-file@v1
      with:
        path: 'bar.json'
      working-directory: /data/foo
    ```
branding:
  icon: arrow-up
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: get-file
inputs:
  path:
    description: the file to attach
    required: true
  type:
    description: the file type (a media type such as `application/xml`)
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: get-files
  description: |
    Attach a set of files from the execution environment.

    `get-files` functions attach the matching files so that publisher
    plugins can process them.  They have mandatory `pattern` input.

    An optional `type` input may be specified.  It is expected to be
    a media type.

    If no files match the specified patterns, an error message is issued, and
    the workflow is stopped. This is the default behavior.

    An optional `warn-if-not-found` input may be specified.  Instead of the
    default behaviour, a warning is issued if no file-matching pattern is
    found, and the workflow continues.

    An optional `skip-if-not-found` input may be specified. In this case, no
    message is issued if no file-matching pattern is found, and the workflow
    continues.

    `warn-if-not-found` and `skip-if-not-found` can not be specified
    simultaneously.
  notes: |
    ## Examples

    ```yaml
    - uses: actions/get-files@v1
      with:
        pattern: '*.xml'
    ```

    It is a good practice to specify the attachment type if known.  Please
    note that all matching files will be decorated with the specified type:

    ```yaml hl_lines="4"
    - uses: actions/get-files@v1
      with:
        pattern: '*.xml'
        type: 'application/xml'
    ```

    If you want to get files recursively in a directory tree, use the
    `**/*.ext` pattern format:

    ```yaml hl_lines="3"
    - uses: actions/get-files@v1
      with:
        pattern: '**/*.html'
      working-directory: /data/foo
    ```

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml hl_lines="4"
    - uses: actions/get-files@v1
      with:
        pattern: '*.json'
      working-directory: /data/foo
    ```

    If you want to display a warning message when no file matching pattern
    is found, use the `warn-if-not-found` statement:

    ```yaml hl_lines="4"
    - uses: actions/get-files@v1
      with:
        pattern: '*.xml'
        warn-if-not-found: 'No *.xml files found.'
    ```

    For silent operations in case where no file matching pattern is found, use
    the `skip-if-not-found` statement:

    ```yaml hl_lines="4"
    - uses: actions/get-files@v1
      with:
        pattern: '*.txt'
        skip-if-not-found: true
    ```
branding:
  icon: upload
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: get-files
inputs:
  pattern:
    description: the pattern that identifies the files to attach
    required: true
  type:
    description: the file type (a media type such as `application/xml`)
    required: false
  warn-if-not-found:
    description: the warning to display if no matching file is found
    required: false
  skip-if-not-found:
    description: when set to `true`, remains silent if no matching file is found
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: upload-artifact
  description: |
    Upload an artifact from the execution environment.

    `upload-artifact` functions attach the specified file so that publisher
    plugins can process them. The file is attached at the workflow level, not on the step
    level. These functions have a mandatory `path` input and optional `name` and `if-no-files-found`
    inputs.

    `path` is a file path, relative to the current workspace, that specifies the artifact to attach.

    `name` can be used to specify a custom attachment name.

    `if-no-file-found` defines the behavior in case the artifact is not found. This property
    can be set to `warn`, `error` or `ignore`. If not specified, it defaults to `warn`.
    When this property is set to `warn` and the artifact is not found, a warning message is issued.
    When it is set to `error`, an error message is displayed and the step fails. When `ignore`
    is used, no message is displayed.

    Files uploaded with `upload-artifact` functions can be downloaded with `download-artifact`
    functions. It is recommended that uploaded artifacts have an unique name to avoid their
    overwriting during retrieval.
  notes: |
    ## Examples

    ```yaml
    - uses: actions/upload-artifact@v1
      with:
        path: 'foo.xml'
    ```

    If you need to customize your attachment name, use the `name`
    property:

    ```yaml
    - uses: actions/upload-artifact@v1
      with:
        name: 'bar.txt'
        path: 'foo.txt'
    ```

    If you need to get files that are not in the current directory,
    use the `working-directory` statement:

    ```yaml hl_lines="4"
    - uses: actions/upload-artifact@v1
      with:
        path: 'bar.json'
      working-directory: /data/foo
    ```
branding:
  icon: arrow-up
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: upload-artifact
inputs:
  path:
    description: the file to attach, relative to the current workspace
    required: true
  name:
    description: the custom file name
    required: false
  if-no-file-found:
    description: the behavior in case the file is not found (warn, error or ignore, defaults to warn)
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: download-artifact
  description: |
    Download an artifact to the execution environment.

    `download-artifact` functions download workflow artifacts previously uploaded with
    `upload-artifact` to the execution environment.

    These functions have optional `name`, `pattern` and `path` inputs.

    If no input is specified, all workflow artifacts are downloaded to the current workspace.

    To download single artifact, `name` input can be used. To download multiple artifacts,
    you can specify `pattern`.

    `path` allows to specify the directory the artifacts should be downloaded to. It
    defaults to the current workspace. When `name` and `path` are specified, `path` should
    be a file path, when `pattern` and `path` are specified, `path` should be a directory.

    If the artifacts uploaded with `upload-artifact` function have identical names, only the
    last uploaded artifact will be retrieved.
  notes: |
    ## Examples

    Downloading an artifact providing its name.

    ```yaml
    - uses: actions/download-artifact@v1
      with:
        name: foo.json
        path: /path/to/foo.json
    ```

    Downloading multiple artifacts providing a pattern.

    ```yaml
    - uses: actions/download-artifact@v1
      with:
        pattern: *.html
        path: /some/dir/path
    ```

    Downloading multiple artifacts to the current workspace.

    ```yaml
    - uses: actions/download-artifact@v1
      with:
        pattern: *tar*
    ```

    Downloading all workflow artifacts to the current workspace.

    ```yaml
    - uses: actions/download-artifact@v1
    ```
branding:
  icon: arrow-up
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: download-artifact
inputs:
  name:
    description: the name of artifact to download
    required: false
  pattern:
    description: the pattern of artifacts to download
    required: false
  path:
    description: the path to download artifacts to, file path or directory
    required: false

---
apiVersion: 'opentestfactory.org/v1alpha1'
kind: 'ProviderPlugin'
metadata:
  name: actionprovider
  action: prepare-inception
  description: |
    Preload the inception environment with data.

    A common use case is to prepare test execution results. Provider plugins
    typically document the artifacts they generate. Please refer to their
    documentation for more information.
  notes: |
    ## Example

    ```yaml
    - uses: actions/prepare-inception@v1
      with:
        export.xml: ${{ resources.files.export }}
        report.html: ${{ resources.files.report }}
    ```
branding:
  icon: film
cmd: 'python3 -m opentf.plugins.actionprovider.main'
events:
- categoryPrefix: actions
  category: prepare-inception
inputs:
  "{pattern}":
    description: the pattern that identifies the files to attach
    required: false
additionalInputs: true
