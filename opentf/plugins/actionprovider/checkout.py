# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""The checkout action"""

from typing import Any, Dict, List, Tuple

import re

from urllib.parse import urlsplit, urlunsplit

from opentf.toolkit import core


########################################################################
# Constants

CHECKOUTV2_CATEGORY = 'checkout@v2'
CHECKOUTV3_CATEGORY = 'checkout'

SHA_1_PATTERN = re.compile(r'^[0-9a-fA-F]{40}$')
SHA_256_PATTERN = re.compile(r'^[0-9a-fA-F]{64}$')


def maybe_override(repository: str, contexts: Dict[str, Any]) -> str:
    """Override credentials if respository defined in resources."""
    if contexts is None or contexts.get('resources') is None:
        return repository
    if 'repositories' not in contexts['resources']:
        return repository
    predefined = contexts['resources']['repositories']
    org = urlsplit(repository)
    for item in predefined.values():
        if f'/{item["repository"]}' == org.path:
            dst = urlsplit(item['endpoint'])
            if (
                org.scheme == dst.scheme
                and org.hostname == dst.hostname
                and org.port == dst.port
            ):
                if org.netloc == dst.netloc:
                    return repository
                return urlunsplit(
                    (org.scheme, dst.netloc, org.path, org.query, org.fragment)
                )
    return repository


def maybe_mask_password(repository: str, steps: List[Dict[str, str]]) -> None:
    """Mask password if there is any."""

    def _mask(password: str) -> bool:
        return all(c.isalnum() or c in ('-_') for c in password)

    creds, _, _ = urlsplit(repository).netloc.rpartition('@')
    if creds and ':' in creds:
        _, _, password = creds.rpartition(':')
        if password and _mask(password):
            steps.append({'run': core.set_secret(password)})


def parse_ref(repository: str, ref: str, path: str) -> Tuple[str, str]:
    """Parse the ref input."""
    sha = ''
    if SHA_256_PATTERN.match(ref) or SHA_1_PATTERN.match(ref):
        if not path:
            # humanish name, aka, what's before /.git[/]
            target = repository.strip('/').split('/')[-1]
            if target.endswith('.git'):
                target = target[:-4]
        else:
            target = path
        sha = f'&& cd {target} && git checkout {ref}'
        ref = '-n '
    else:
        ref = f'-b {ref} '
    return sha, ref


def checkoutv2_action(inputs: Dict[str, Any]) -> List[Dict[str, Any]]:
    """Process the checkout message.

    `checkout` actions have a mandatory `repository` input:

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
    ```

    They also allow for two optional inputs:

    - `ref`, which is the git reference to checkout: a branch name, a
      tag name or a commit sha (defaults to the default branch if
      unspecified)
    - `path`, which is where to clone/checkout the repository, relative
      to the current workspace

    ```yaml
    - uses: actions/checkout@v2
      with:
        repository: https://github.com/robotframework/RobotDemo.git
        ref: dev
        path: foo/bar
    ```
    """
    repository = maybe_override(inputs['repository'], core._getcontexts())
    steps = []
    maybe_mask_password(repository, steps)
    sha = ''
    ref = inputs.get('ref', '')
    path = inputs.get('path', '')
    if ref:
        sha, ref = parse_ref(repository, ref, path)
    steps += [{'run': f'git clone {ref}{repository} {path}{sha}'}]
    return steps


def checkoutv3_action(inputs: Dict[str, Any]) -> List[Dict[str, Any]]:
    """Process the checkout message.

    `checkout` actions have a mandatory `repository` input:

    ```yaml
    - uses: actions/checkout@v3
      with:
        repository: https://github.com/robotframework/RobotDemo.git
    ```

    They also allow for three optional inputs:

    - `ref`, which is the git reference to checkout: a branch name, a
      tag name or a commit sha (defaults to the default branch if
      unspecified)
    - `path`, which is where to clone/checkout the repository, relative
      to the current workspace
    - `fetch-depth`, which allows to specify the number of commits to fetch.
    Default `fetch-depth` value is 1. If `fetch-depth` equals 0, the entire
    history is fetched.

    ```yaml
    - uses: actions/checkout@v3
      with:
        repository: https://github.com/robotframework/RobotDemo.git
        ref: dev
        path: foo/bar
        fetch-depth: 3
    ```
    """
    repository = maybe_override(inputs['repository'], core._getcontexts())
    steps = []
    maybe_mask_password(repository, steps)
    sha = ''
    ref = inputs.get('ref', '')
    path = inputs.get('path', '')
    depth = inputs.get('fetch-depth', '1')
    if ref:
        sha, ref = parse_ref(repository, ref, path)
    if depth == 0:
        depth = ''
    elif depth != 0 and ref:
        depth = f'--depth {depth} '
    else:
        depth = f'--depth {depth} --no-single-branch '
    steps += [{'run': f'git clone {depth}{ref}{repository} {path}{sha}'}]
    return steps
