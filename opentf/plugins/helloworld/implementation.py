# Copyright (c) 2021 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# implementation.py


def handler(inputs):
    return [
        {
            'if': "runner.os == 'windows'",
            'run': 'echo Hello ' + inputs['who-to-greets'] + '.',
        },
        {
            'if': "runner.os != 'windows'",
            'run': 'echo "Hello ' + inputs['who-to-greets'] + '."',
        },
        {
            'if': "runner.os != 'windows'",
            'id': 'random-number-generator',
            'run': 'echo "::set-output name=random-id::$(echo $RANDOM)"',
        },
        {
            'if': "runner.os == 'windows'",
            'id': 'random-number-generator',
            'run': 'echo ::set-output name=random-id::%RANDOM%',
        },
    ]
