"""Telemetry"""

from typing import Any, Dict, Optional, TYPE_CHECKING

from enum import Enum

import logging
import os

from opentf.commons.exceptions import OpentfError

########################################################################
### Init & callbacks

OTLP_EXPORTER_ENDPOINT = 'http://127.0.0.1:4318/v1/metrics'

if TYPE_CHECKING or (
    TELEMETRY_ENABLED := os.environ.get('OPENTF_TELEMETRY', '').lower()
    in (
        'yes',
        'true',
        '1',
        'on',
    )
):
    from opentelemetry import metrics
    from opentelemetry.sdk.metrics.export import PeriodicExportingMetricReader
    from opentelemetry.exporter.otlp.proto.http.metric_exporter import (
        OTLPMetricExporter,
    )
    from opentelemetry.sdk.metrics import MeterProvider
    from opentelemetry.sdk.resources import SERVICE_NAME, Resource


def _initialize_telemetry(service: str):
    """Initialize OpenTelemetry for a service.

    # Required parameters

    - service: a string, service name
    - host: a string, metrics server host
    - port: an integer, metrics server port
    """
    if not TELEMETRY_ENABLED:
        return None

    try:
        resource = Resource(attributes={SERVICE_NAME: service})
        reader = PeriodicExportingMetricReader(
            OTLPMetricExporter(
                endpoint=os.environ.get('OPENTF_EXPORTER', OTLP_EXPORTER_ENDPOINT)
            )
        )
        provider = MeterProvider(resource=resource, metric_readers=[reader])

        metrics.set_meter_provider(provider)

        return metrics.get_meter(__name__)
    except Exception as err:
        logging.error(
            'Failed to initialize OpenTelemetry for %s: %s.', service, str(err)
        )
        raise


def _callback_wrapper(callback):
    def wrapper(value):
        yield metrics.Observation(callback(value))

    return wrapper


########################################################################
### TelemetryManager


class TelemetryManager:
    """TelemetryManager class."""

    def __init__(self, service: str, svc_metrics) -> None:
        self.enabled = TELEMETRY_ENABLED
        self.meter = _initialize_telemetry(service)
        self.svc_metrics = self._create_svc_metrics(svc_metrics)

    def set_metric(
        self, svc_metric, value: int, attributes: Optional[Dict[str, Any]] = None
    ) -> None:
        """Set metric value if telemetry is enabled."""
        if not self.enabled:
            return
        if not isinstance(svc_metric, Enum):
            raise TypeError(
                f'Metric must be an Enum instance, got {type(svc_metric).__name__}.'
            )
        try:
            self.svc_metrics[svc_metric.name].add(value, attributes or {})
        except KeyError:
            logging.error('Metric %s not found.', svc_metric.name)
        except Exception as err:
            logging.error('Failed to set metric %s: %s.', svc_metric.name, str(err))

    def _create_svc_metrics(self, svc_metrics) -> Dict[str, Any]:
        """Create metrics for a service."""
        if not self.enabled:
            return {}

        if not self.meter:
            raise OpentfError('Telemetry must be initialized before creating metrics.')

        metrics_dict = {}

        for metric in svc_metrics:
            if 'COUNTER' in metric.name:
                metrics_dict[metric.name] = self.meter.create_counter(
                    metric.metric_name, unit=metric.unit, description=metric.description
                )
            elif 'OBSERVABLE_GAUGE' in metric.name:
                metrics_dict[metric.name] = self.meter.create_observable_gauge(
                    metric.metric_name,
                    unit=metric.unit,
                    description=metric.description,
                    callbacks=[_callback_wrapper(metric.callbacks)],
                )
            elif 'GAUGE' in metric.name:
                metrics_dict[metric.name] = self.meter.create_up_down_counter(
                    metric.metric_name, unit=metric.unit, description=metric.description
                )
        return metrics_dict
