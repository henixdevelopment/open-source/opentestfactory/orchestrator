# Copyright (c) 2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A channel allocator service, for use by the arranger."""

from typing import Any, Callable, NamedTuple

from collections import defaultdict
from datetime import datetime
from queue import Queue
from time import sleep, time

import logging
import threading

from opentf.commons import make_uuid, make_event, publish
from opentf.commons.schemas import EXECUTIONCOMMAND

from opentf.toolkit.channels import CHANNEL_REQUEST

# from opentf.core.arranger import fatal, error, debug


def error(*msg):
    logging.error(*msg)


def fatal(*msg):
    logging.error(*msg)
    exit()


def debug(*msg):
    logging.debug(*msg)


########################################################################

PRIORITY_THRESHOLD = 42


type RequiredJobs = list[tuple[str, list[str], str, list[str], str]]
type RequiredCallback = Callable[[str, str, dict[str, Any]], None]

type ChannelOffer = tuple[str, dict[str, Any]]
type ChannelsRequest = tuple[RequiredJobs, str, RequiredCallback, int]


class PendingJobs(NamedTuple):
    """A pending job (aka. a job awaiting an execution environment."""

    workflow_id: str
    callback: RequiredCallback
    jobs: RequiredJobs
    max_parallel: int


EVENTS_QUEUE: Queue[ChannelOffer | ChannelsRequest] = Queue()
PENDING_JOBS: dict[str, PendingJobs] = {}
JOBS_TIMESTAMPS: dict[str, Any] = {}
CHANNELS: dict[str, list[Any]] = defaultdict(list)  # Channel handlers `Channels` list


def update_channels(channelhandler_id: str, channels: list[dict[str, Any]]):
    CHANNELS[channelhandler_id] = channels


def handle_require_channels(
    new_jobs: RequiredJobs,
    workflow_id: str,
    callback: RequiredCallback,
    max_parallel: int,
    context: dict[str, Any],
) -> None:
    """Handle updated channel requirements of a workflow.

    - new_jobs: a list of (uuid, [tags], name, [uuids], namespace)
    - workflow_id: a string
    - callback: a function of three parameters
    - max_parallel: an integer
    - context: a dictionary
    """
    if previous := PENDING_JOBS.get(workflow_id):
        new_jobs_ids = {job[0] for job in new_jobs}
        previous_jobs_ids = {job[0] for job in previous.jobs}
        for job_id in previous_jobs_ids - new_jobs_ids:
            try:
                del JOBS_TIMESTAMPS[job_id]
            except KeyError:
                pass

    if not new_jobs:
        if workflow_id in PENDING_JOBS:
            del PENDING_JOBS[workflow_id]
        return

    PENDING_JOBS[workflow_id] = PendingJobs(
        workflow_id, callback, new_jobs, max_parallel
    )
    for job in PENDING_JOBS[workflow_id].jobs:
        JOBS_TIMESTAMPS.setdefault(job[0], {'timestamp': datetime.now(), 'priority': 0})
    publish_channel_requests(context)


def sorted_job_list() -> list[str]:
    """Order pending jobs by timestamps and priority.

    Jobs with a priority greater than PRIORITY_THRESHOLD have priority
    over jobs with a lesser priority, regardless of their timestamps.

    # Returned value

    A list of job_ids.
    """
    possible_jobs = {
        job[0]
        for pending in PENDING_JOBS.values()
        for job in pending.jobs
        if pending.max_parallel > 0
    }
    by_timestamps = list(
        sorted(JOBS_TIMESTAMPS, key=lambda x: JOBS_TIMESTAMPS[x]['timestamp'])
    )
    what = [
        job
        for job in by_timestamps
        if JOBS_TIMESTAMPS[job]['priority'] >= PRIORITY_THRESHOLD
        and job in possible_jobs
    ] + [
        job
        for job in by_timestamps
        if JOBS_TIMESTAMPS[job]['priority'] < PRIORITY_THRESHOLD
        and job in possible_jobs
    ]
    return what


def _get_available_environments() -> list[dict[str, Any]]:
    return [
        channel['spec']['tags']
        for channels in CHANNELS.values()
        for channel in channels
        if channel['status']['phase'] == 'IDLE'
    ]


def publish_channel_requests(context: dict[str, Any]) -> None:
    """Publish channel requests for pending jobs."""
    sent_request_tags = {}

    for job_id in sorted_job_list():
        job_tags = [
            tag
            for pendings in PENDING_JOBS.values()
            for job in pendings.jobs
            for tag in job[1]
            if job[0] == job_id
        ]

        available_envs = [
            tags
            for tags in _get_available_environments()
            if set(job_tags).issubset(set(tags))
        ]

        workflow_id = [
            workflow_id
            for workflow_id, pendings in PENDING_JOBS.items()
            for job in pendings.jobs
            if job[0] == job_id
        ][0]

        if (
            not any(
                set(job_tags).issubset(set(tags)) for tags in sent_request_tags.values()
            )
            or available_envs
        ):
            try:
                JOBS_TIMESTAMPS[job_id]['priority'] = 0
                name, job_origin, namespace = [
                    (job[2], job[3], job[4])
                    for job in PENDING_JOBS[workflow_id].jobs
                    if job[0] == job_id
                ][0]
                _publish_channel_request(
                    workflow_id, job_id, name, namespace, job_tags, job_origin, context
                )
                sent_request_tags[job_id] = job_tags
            except KeyError:
                debug('Could not find key %s in JOBS_TIMESTAMPS.', job_id)
        else:
            try:
                JOBS_TIMESTAMPS[job_id]['priority'] += 1
            except KeyError:
                debug('Could not find key %s in JOBS_TIMESTAMPS.', job_id)


def _publish_channel_request(
    workflow_id: str,
    job_id: str,
    name: str,
    namespace: str,
    job_tags: list[str],
    job_origin: list[str],
    context: dict[str, Any],
) -> None:
    """Publish channel request.

    # Required parameter

    - workflow_id: a string
    - job_id: a string
    - name: a string
    - namespace: a string
    - job_tags: a list of strings
    - job_origin: a possibly empty list of strings
    - context: a dictionary
    """
    try:
        metadata = {
            'name': name,
            'namespace': namespace,
            'workflow_id': workflow_id,
            'job_id': job_id,
            'job_origin': job_origin,
            'step_origin': [],
            'step_sequence_id': CHANNEL_REQUEST,
            'step_id': make_uuid(),
        }

        prepare = make_event(EXECUTIONCOMMAND, metadata=metadata, scripts=[])
        prepare['runs-on'] = job_tags
        publish(prepare, context=context)
    except Exception as err:
        error('Error while managing requests: %s.', str(err))


def handle_candidate(workflow_id: str, candidate: dict[str, Any]) -> None:
    """Handle channel candidate.

    Accept candidate if the max_parallel limit for the workflow is at
    least 1.
    """
    if not (pending_jobs := PENDING_JOBS.get(workflow_id)):
        return
    if pending_jobs.max_parallel == 0:
        return
    job_id = candidate['metadata']['job_id']
    if job_id in [job[0] for job in pending_jobs.jobs]:
        try:
            del JOBS_TIMESTAMPS[job_id]
        except KeyError:
            return
        pending_jobs.callback(workflow_id, job_id, candidate['metadata'])
        remaining_jobs = [job for job in pending_jobs.jobs if job[0] != job_id]
        if remaining_jobs:
            PENDING_JOBS[workflow_id] = PendingJobs(
                workflow_id,
                pending_jobs.callback,
                remaining_jobs,
                pending_jobs.max_parallel - 1,
            )
        else:
            del PENDING_JOBS[workflow_id]


def _handle_event(context: dict[str, Any]) -> None:
    """Handle incoming channel requests."""
    event = EVENTS_QUEUE.get()
    if len(event) == 2:
        handle_candidate(*event)  # event is a ExecutionResult event
    elif len(event) == 4:
        handle_require_channels(*event, context=context)


def handle_events_queue(context: dict[str, Any]) -> None:
    """Handle channel handlers offers.

    This process ExecutionResults with a step_id of CHANNEL_REQUEST
    coming from channel handlers.
    """
    while True:
        try:
            _handle_event(context)
        except Exception as err:
            error(f'Internal error while handling event: {err}.')
            raise


def handle_refresh_requests(context: dict[str, Any]):
    """Periodically check if there are channels available for pending jobs."""
    while True:
        if PENDING_JOBS:
            publish_channel_requests(context)
        sleep(5)


########################################################################
# exposed helpers


def process_candidate(workflow_id: str, candidate: dict[str, Any]) -> None:
    """Process a channel response.

    A CHANNEL_REQUEST event has been answered.

    # Required parameters

    - workflow_id: a UUID (a string)
    - candidate: an EXECUTIONRESULT event (JSON)
    """
    debug('Processing channel candidate for workflow %s.', workflow_id)
    EVENTS_QUEUE.put((workflow_id, candidate))


def require_channels(
    workflow_id: str, jobs: RequiredJobs, callback: RequiredCallback, max_parallel: int
) -> None:
    """Handling a channels request from a workflow.

    # Required parameters

    - workflow_id: a string (a UUID)
    - jobs: a list of tuples (job_id, [tags], name, [job_origin], namespace)
    - callback: a function of 3 arguments
    - max_parallel: an integer
    """
    debug('Queuing channels request for workflow %s.', workflow_id)
    EVENTS_QUEUE.put((jobs, workflow_id, callback, max_parallel))


def init(context: dict[str, Any]) -> None:
    """Initialize channel allocator."""
    try:
        debug('Starting channel requests and offers managing thread.')
        threading.Thread(
            target=handle_events_queue, daemon=True, args=(context,)
        ).start()
    except Exception as err:
        fatal(f'Could not start channel requests and offers managing thread: {err}.')
    try:
        debug('Starting channel requests handler thread.')
        threading.Thread(
            target=handle_refresh_requests, daemon=True, args=(context,)
        ).start()
    except Exception as err:
        fatal(f'Could not start channel requests handler thread: {err}.')
