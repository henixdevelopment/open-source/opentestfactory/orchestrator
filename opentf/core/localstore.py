# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""A Localstore service."""

from typing import Dict

import os
import sys
import threading

from collections import defaultdict
from datetime import datetime, timedelta
from queue import Queue
from time import sleep

from flask import make_response, request, send_file

from opentf.commons import (
    WORKFLOW,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    EXECUTIONRESULT,
    WORKFLOWRESULT,
    can_use_namespace,
    make_status_response,
    subscribe,
    unsubscribe,
    make_app,
    run_app,
)


########################################################################
## Constants

SLEEP_DELAY = 60  # in seconds
PENDING_DURATION = 3600  # in seconds

UUID_LENGTH = 36
PREFIX_LENGTH = len('/tmp/')
ALLURE_PREFIX_LENGTH = len('allureReporting/')


########################################################################
## Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def fatal(*args):
    """Log error message and exit with error code 2."""
    error(*args)
    sys.exit(2)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################


def _matches(file: str, attachment_id: str, workflow_id: str) -> bool:
    file = file[PREFIX_LENGTH:]
    if attachment_id == workflow_id:
        return file.endswith(f'{attachment_id}/allure-report.tar')
    return file[UUID_LENGTH] == '-' and file[UUID_LENGTH + 1 :].startswith(
        attachment_id
    )


def _get_downloadname(src: str, attachment_id: str, workflow_id: str) -> str:
    if attachment_id == workflow_id:
        return f'{attachment_id}_allure-report.tar'
    return f'{attachment_id}_{src.split("_", maxsplit=2)[2]}'


def _remove_attachments(workflow_id):
    for attachment in WORKFLOWS_ATTACHMENTS[workflow_id]:
        try:
            os.unlink(attachment)
        except IOError as err:
            warning('Could not remove attachment %s: %s.', attachment, err)
    try:
        del WORKFLOWS_ATTACHMENTS[workflow_id]
    except KeyError:
        pass


def clean_attachments():
    """Cleanup attachments."""
    while True:
        try:
            stamp, workflow_id = CLEANUP_PENDING.get()
            if (datetime.now() - stamp) < timedelta(seconds=PENDING_DURATION):
                CLEANUP_PENDING.put((stamp, workflow_id))
                sleep(SLEEP_DELAY)
                continue

            if workflow_id not in WORKFLOWS_ATTACHMENTS:
                continue

            _remove_attachments(workflow_id)

            try:
                del WORKFLOWS_NAMESPACES[workflow_id]
            except KeyError:
                pass
        except Exception as err:
            error('Internal error while cleaning attachments: %s.', err)


########################################################################
## Main

SUBSCRIPTIONS = (
    WORKFLOW,
    WORKFLOWCOMPLETED,
    WORKFLOWCANCELED,
    EXECUTIONRESULT,
    WORKFLOWRESULT,
)

CLEANUP_PENDING = Queue()
WORKFLOWS_ATTACHMENTS = defaultdict(set)
WORKFLOWS_NAMESPACES: Dict[str, str] = {}
DEFAULT_NAMESPACE = 'default'

########################################################################

app = make_app(
    name='localstore',
    description='Create and start a local file store service.',
)


@app.route('/workflows/<workflow_id>/files/<attachment_id>', methods=['GET', 'HEAD'])
def get_file(workflow_id, attachment_id):
    """Get an attachment.

    # Required parameters

    - workflow_id: an UUID
    - attachment_id: an UUID

    # Returned value

    A requested file content.
    """
    if workflow_id not in WORKFLOWS_ATTACHMENTS:
        return make_status_response(
            'NotFound',
            f'Workflow {workflow_id} not found in localstore. Its attachments may have been previously cleaned up.',
        )
    namespace = WORKFLOWS_NAMESPACES.get(workflow_id)
    if not namespace:
        return make_status_response(
            'NotFound',
            f'Workflow {workflow_id} not found in localstore. It may have been already cleaned up.',
        )
    if not can_use_namespace(namespace, resource='workflows', verb='get'):
        return make_status_response(
            'Forbidden',
            f'Token not allowed to access workflows in namespace {namespace}.',
        )

    candidates = [
        file
        for file in WORKFLOWS_ATTACHMENTS[workflow_id]
        if _matches(file, attachment_id, workflow_id)
    ]

    if not candidates:
        if request.method == 'GET':
            return make_status_response(
                'NotFound',
                f'Attachment {attachment_id} not found for workflow {workflow_id}.',
            )
        # to prevent log generation with 'HEAD' request
        return make_response(
            {
                'kind': 'Status',
                'apiVersion': 'v1',
                'metadata': {},
                'status': 'Failure',
                'message': f'Attachment {attachment_id} not found for workflow {workflow_id}.',
                'reason': 'NotFound',
                'details': None,
                'code': 404,
            },
            404,
        )

    src = candidates.pop()
    try:
        if request.method == 'GET':
            return send_file(
                src,
                download_name=_get_downloadname(src, attachment_id, workflow_id),
                mimetype='application/octet-stream',
            )
        elif request.method == 'HEAD' and not os.path.exists(src):
            return make_status_response(
                'NotFound',
                f'Expected attachment {attachment_id} not found for workflow {workflow_id}.',
            )
        return make_status_response('OK', '')
    except Exception as err:
        return make_status_response(
            'InternalError' if os.path.exists(src) else 'NotFound',
            f'Could not send file `{src}` ({attachment_id}): {str(err)}',
        )


@app.route('/inbox', methods=['POST'])
def process_inbox():
    """Process a new publication.

    It may be of any kind.

    # Returned value

    A _status manifest_.  A status manifest is a dictionary with the
    following entries:

    - kind: a string (`'Status'`)
    - apiVersion: a string (`'v1'`)
    - metadata: an empty dictionary
    - status: a string (either `'Success'` or `'Failure'`)
    - message: a string (`message`)
    - reason: a string (`reason`)
    - details: a dictionary or None (`details`)
    - code: an integer (derived from `reason`)
    """
    try:
        body = request.get_json()
    except Exception as err:
        return make_status_response('BadRequest', f'Could not parse body: {err}.')

    if not isinstance(body, dict) or 'kind' not in body or 'apiVersion' not in body:
        return make_status_response('Invalid', 'Not an object or no kind specified.')

    if not body.get('metadata', {}).get('workflow_id'):
        return make_status_response('Invalid', 'No workflow_id specified.')

    workflow_id = body['metadata']['workflow_id']
    schema = f'{body["apiVersion"]}/{body["kind"]}'
    if schema == WORKFLOW:
        WORKFLOWS_NAMESPACES[workflow_id] = body['metadata'].get(
            'namespace', DEFAULT_NAMESPACE
        )
    elif schema in (EXECUTIONRESULT, WORKFLOWRESULT):
        for item in body.get('attachments', ()):
            if os.path.exists(item):
                WORKFLOWS_ATTACHMENTS[workflow_id].add(item)
    elif schema in (WORKFLOWCOMPLETED, WORKFLOWCANCELED):
        debug(f'Scheduling cleanup for workflow {workflow_id} attachments.')
        CLEANUP_PENDING.put((datetime.now(), workflow_id))

    return make_status_response('OK', '')


def main(svc):
    """Start the local file store service."""
    try:
        debug('Starting attachment cleanup thread.')
        threading.Thread(target=clean_attachments, daemon=True).start()
    except Exception as err:
        error('Could not start attachment cleanup thread: %s.', str(err))
        sys.exit(2)

    try:
        debug(f'Subscribing to {", ".join(SUBSCRIPTIONS)}.')
        subs = [
            subscribe(kind=kind, target='inbox', app=svc)
            for kind in (WORKFLOW, WORKFLOWCOMPLETED, WORKFLOWCANCELED)
        ]
        subs += [
            subscribe(
                kind=kind,
                fieldexpressions=[{'key': 'attachments', 'operator': 'Exists'}],
                target='inbox',
                app=svc,
            )
            for kind in (EXECUTIONRESULT, WORKFLOWRESULT)
        ]
    except Exception as err:
        fatal('Could not subscribe to eventbus: %s.', str(err))

    try:
        debug('Starting service.')
        run_app(svc)
    finally:
        for sub in subs:
            unsubscribe(sub, app=svc)


if __name__ == '__main__':
    main(app)
