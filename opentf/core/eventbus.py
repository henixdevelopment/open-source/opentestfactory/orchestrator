# Copyright (c) 2021-2023 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""An EventBus facade.

Can work as a standalone service, but may be replaced by a facade to an
existing eventbus/mq manager.
"""

from typing import Any, Callable, Dict, List, Optional, Tuple

from collections import defaultdict
from datetime import datetime, timezone
from enum import Enum
from time import time

from flask import request, make_response

import requests
import sys

from opentf.commons import (
    make_status_response,
    annotate_response,
    validate_schema,
    EVENTBUSCONFIG,
    SUBSCRIPTION,
    make_app,
    run_app,
    make_uuid,
    get_context_parameter,
    authorizer,
    make_dispatchqueue,
)
from opentf.commons.selectors import (
    match_compiledfieldselector,
    match_compiledlabelselector,
    compile_selector,
    match_selectors,
    prepare_selectors,
)

from opentf.core.telemetry import TelemetryManager

########################################################################
## Constants

FIELD_SELECTOR = 'opentestfactory.org/fieldselector'
LABEL_SELECTOR = 'opentestfactory.org/labelselector'

_STR_STATUSES = {i: str(i) for i in range(100, 600)}

########################################################################
## Logging Helpers


def info(*args):
    """Log info message."""
    app.logger.info(*args)


def warning(*args):
    """Log warning message."""
    app.logger.warning(*args)


def error(*args):
    """Log error message."""
    app.logger.error(*args)


def debug(*args):
    """Log debug message."""
    app.logger.debug(*args)


########################################################################
## Telemetry


class EventBusMetrics(Enum):
    PUBLICATIONS_SENT_COUNTER = (
        'opentf.orchestrator.eventbus.publications.sent',
        '1',
        'Sent publications count',
        None,
    )
    PUBLICATIONS_RECEIVED_COUNTER = (
        'opentf.orchestrator.eventbus.publications.received',
        '1',
        'Received publications count',
        None,
    )
    SUBSCRIPTIONS_PUBLICATIONS_COUNTER = (
        'opentf.orchestrator.eventbus.subscriptions_publications',
        '1',
        'Publications count per subscription',
        None,
    )
    PUBLICATIONS_RESPONSES_STATUSCODE_COUNTER = (
        'opentf.orchestrator.eventbus.publications.http.responses.status_code',
        '1',
        'Publications HTTP response codes global count',
        None,
    )
    SUBSCRIPTIONS_RESPONSES_STATUSCODE_COUNTER = (
        'opentf.orchestrator.eventbus.subscriptions.http.responses.status_code',
        '1',
        'Publications HTTP response codes count per subscription',
        None,
    )

    def __init__(
        self,
        metric_name: str,
        unit: str,
        description: str,
        callbacks: Optional[List[Callable]],
    ):
        self.metric_name = metric_name
        self.unit = unit
        self.description = description
        self.callbacks = callbacks


SERVICE_NAME = 'opentf.orchestrator.eventbus'

########################################################################
## Helpers

SUBSCRIPTIONS: Dict[str, Dict[str, Any]] = {}
TOKENS: Dict[str, str] = {}
SELECTORS: Dict[str, Tuple[List[Any], List[Any]]] = {}

DISPATCHERS = {}


def prepare_subscription(body: Dict[str, Any]):
    """Prepare subscription selectors.

    ```yaml
    selector:
      matchKind: kind
      matchLabels:
        component: redis
      matchExpressions:
        - { key: tier, operator: In, values: [cache] }
        - { key: environment, operator: NotIn, values: [dev] }
      matchFields:
        field: value
      matchFieldExpressions:
        - { key: runs-on, operator: ContainsAll, values: [inception]}
    ```

    Possible values for `operator` are: `In`, `NotIn`, `Exists`, and
    `DoesNotExist`.
    """

    def _expr(what: Dict[str, Any]) -> str:
        if what['operator'] == 'Exists':
            return what['key']
        if what['operator'] == 'DoesNotExist':
            return f'!{what["key"]}'
        if what['operator'] == 'ContainsAll':
            return f'({", ".join(what["values"])}) in {what["key"]}'
        if what['operator'] == 'DoesNotContainAll':
            return f'({", ".join(what["values"])}) notin {what["key"]}'
        return f'{what["key"]} {what["operator"].lower()} ({", ".join(what["values"])})'

    debug("Processing subscription request for '%s':", body['metadata']['name'])
    if selector := body['spec'].get('selector'):
        labelselector = [
            f'{label}=={value}'
            for label, value in selector.get('matchLabels', {}).items()
        ] + [_expr(expr) for expr in selector.get('matchExpressions', [])]
        fieldselector = [
            f'{field}=={value}'
            for field, value in selector.get('matchFields', {}).items()
        ] + [_expr(expr) for expr in selector.get('matchFieldExpressions', [])]
        if 'matchKind' in selector:
            fieldselector = ['kind==' + selector['matchKind']] + fieldselector
    else:
        fieldselector = labelselector = []
    debug(f'  FieldSelector is {fieldselector}.')
    debug(f'  LabelSelector is {labelselector}.')
    body['metadata']['annotations'] = {
        FIELD_SELECTOR: ','.join(fieldselector),
        LABEL_SELECTOR: ','.join(labelselector),
    }
    return compile_selector(','.join(fieldselector)), compile_selector(
        ','.join(labelselector), resolve_path=False
    )


def is_interested(subscription_id: str, publication: Dict[str, Any]) -> bool:
    """Check whether a subscriber is interested in a given publication."""
    try:
        fieldselector, labelselector = SELECTORS[subscription_id]
        if labelselector:
            return match_compiledlabelselector(
                publication, labelselector
            ) and match_compiledfieldselector(publication, fieldselector)
        return match_compiledfieldselector(publication, fieldselector)
    except Exception as err:
        error(f'While looking for {subscription_id} interest: {err}.')
        return False


def update_status(
    subscription: Dict[str, Any], resp: requests.Response, now: str, delta
) -> None:
    """Update subscription status after dispatch.

    Raises an _Exception_ if the response status code is 5xx or 429.
    """
    TELEMETRY_MANAGER.set_metric(EventBusMetrics.PUBLICATIONS_SENT_COUNTER, 1)
    TELEMETRY_MANAGER.set_metric(
        EventBusMetrics.SUBSCRIPTIONS_PUBLICATIONS_COUNTER,
        1,
        {'subscription_name': subscription['metadata']['name']},
    )
    TELEMETRY_MANAGER.set_metric(
        EventBusMetrics.PUBLICATIONS_RESPONSES_STATUSCODE_COUNTER,
        1,
        {'http.response.status_code': resp.status_code},
    )
    TELEMETRY_MANAGER.set_metric(
        EventBusMetrics.SUBSCRIPTIONS_RESPONSES_STATUSCODE_COUNTER,
        1,
        {
            'subscription_name': subscription['metadata']['name'],
            'http.response.status_code': resp.status_code,
        },
    )

    gauge = GAUGES[subscription['metadata']['subscription_id']]
    gauge['count'] += 1
    gauge['sum'] += delta
    gauge['max'] = max(gauge['max'], delta)
    gauge['min'] = min(gauge['min'], delta)
    gauge['timestamp'] = now

    status = subscription['status']
    status['publicationCount'] += 1
    status['lastPublicationTimestamp'] = now
    status['publicationStatusSummary'][_STR_STATUSES.get(resp.status_code)] += 1
    if resp.status_code // 100 == 5 or resp.status_code == 429:
        status['quarantine'] += 1
        warning(
            'Publication status code from %s is %d.  Failure details: %s.',
            subscription['metadata']['subscription_id'],
            resp.status_code,
            resp.text,
        )
        raise Exception('retry')

    debug(
        'Publication status code from %s is %d.',
        subscription['metadata']['subscription_id'],
        resp.status_code,
    )


def dispatch(pub, _):
    """Dispatch event to subscriber."""
    start = time()
    data, subscription_id, now, pub_id, delta0 = pub
    if not (subscription := SUBSCRIPTIONS.get(subscription_id)):
        info('Subscription %s no longer exists.  Ignoring.', subscription_id)
        return
    headers = {
        'X-Subscription-ID': subscription_id,
        'X-Publication-ID': pub_id,
        'Content-Type': 'application/json',
    }
    if subscription_id in TOKENS:
        debug('Adding authorization header from subscriber tokens.')
        headers['Authorization'] = TOKENS[subscription_id]
    subscriber = subscription['spec']['subscriber']
    response = DISPATCHERS[subscription['metadata']['dispatcher_id']]['session'].post(
        subscriber['endpoint'],
        data=data,
        headers=headers,
        verify=not subscriber.get('insecure-skip-tls-verify', False),
        timeout=POST_TIMEOUT_SECONDS,
    )
    delta = time() - start
    update_status(subscription, response, now, delta0 + delta)


########################################################################
## Main


app = make_app(
    name='eventbus',
    description='Create and start an eventbus service.',
    defaultcontext={'host': '127.0.0.1', 'port': 38368, 'ssl_context': 'adhoc'},
    schema=EVENTBUSCONFIG,
)
POST_TIMEOUT_SECONDS = get_context_parameter(app, 'post_timeout_seconds')

try:
    TELEMETRY_MANAGER = TelemetryManager(SERVICE_NAME, EventBusMetrics)
except Exception as err:
    error('Failed to initialize TelemetryManager for %s: %s.', SERVICE_NAME, str(err))
    sys.exit(2)


@app.route('/metrics', methods=['GET'])
def get_metrics():
    """Get metrics."""
    return {
        'gauges': GAUGES,
        'queues': {k: v['queue'].qsize() for k, v in DISPATCHERS.items()},
        'dispatchers': {
            k: {sk: sv for sk, sv in v.items() if sk not in ('queue', 'session')}
            for k, v in DISPATCHERS.items()
        },
    }


@app.route('/subscriptions', methods=['POST'])
@authorizer(resource='subscriptions', verb='create')
def register_subscription():
    """Register a new subscription.

    # Subscription format

    ```yaml
    apiVersion: opentestfactory.org/v1alpha1
    kind: Subscription
    metadata:
      name: {name}
    spec:
      selector:
        matchKind: {kind}
        matchLabels:
          {label}: {value}
        matchExpressions:
          - {key: {key}, operator: {operator}[, values=[...]]}
        matchFields:
          {field}: {value}
        matchFieldExpressions:
          - {key: {key}, operator: {operator}[, values=[...]]}
      subscriber:
        endpoint: {endpoint}
    ```

    The `.spec.selector` part is optional (if not specified, will match
    all publications), as are its subparts.

    # Returned value

    A _status_.  A status is a dictionary.

    If the subscription is successful (`.status` is `'Success'`),
    `.details.uuid` is the subscription ID.
    """
    if not request.is_json:
        return make_status_response('BadRequest', 'Not a JSON document.')

    try:
        body = request.get_json()
        if not isinstance(body, dict):
            return make_status_response('BadRequest', 'Not a JSON object.')
    except:
        return make_status_response('BadRequest', 'Not a valid JSON document.')

    valid, extra = validate_schema(SUBSCRIPTION, body)
    if not valid:
        return make_status_response(
            'Invalid',
            'Not a valid Subscription manifest.',
            details={'error': str(extra)},
        )

    try:
        fieldselector, labelselector = prepare_subscription(body)
    except Exception as err:
        error('  Failed to parse selector, subscription rejected: %s.', str(err))
        return make_status_response(
            'Invalid', 'Failed to parse selector.', details={'error': str(err)}
        )

    body['metadata']['creationTimestamp'] = datetime.now(timezone.utc).isoformat()
    body['metadata']['subscription_id'] = sid = make_uuid()
    body['metadata']['dispatcher_id'] = '//'.join(
        body['spec']['subscriber']['endpoint'].split('/', maxsplit=3)[:3]
    )
    body['status'] = {
        'publicationCount': 0,
        'lastPublicationTimestamp': None,
        'publicationStatusSummary': defaultdict(int),
        'quarantine': 0,
    }

    SELECTORS[sid] = (fieldselector, labelselector)

    authorization = request.headers.get('Authorization')
    if authorization and authorization.lower().strip() not in (
        'bearer reuse',
        'bearer',
    ):
        TOKENS[sid] = authorization
        debug('Registering subscriber token.')

    SUBSCRIPTIONS[sid] = body

    dispatcher = DISPATCHERS.setdefault(
        body['metadata']['dispatcher_id'], {'throttle': 0}
    )
    if 'queue' not in dispatcher:
        dispatcher['queue'] = make_dispatchqueue(app, fn=dispatch)
    if 'session' not in dispatcher:
        dispatcher['session'] = requests.Session()

    msg = f'Subscription \'{body["metadata"]["name"]}\' successfully registered (id={sid}).'
    debug(msg)
    return make_status_response('Created', msg, details={'uuid': sid})


@app.route('/subscriptions', methods=['GET'])
@authorizer(resource='subscriptions', verb='list')
def list_subscriptions():
    """Return list of subscriptions.

    # Returned value

    A _list_.  A list is a dictionary with the following entries:

    - kind: a string
    - apiVersion: a string (`'v1'`)
    - items: a list of dictionaries (`what`)
    """
    try:
        fieldselector, labelselector = prepare_selectors(request.args)
    except ValueError as err:
        return make_status_response('Invalid', str(err))

    items = {
        sid: sub
        for sid, sub in SUBSCRIPTIONS.items()
        if match_selectors(sub, fieldselector, labelselector)
    }
    return annotate_response(
        response=make_response(
            {'apiVersion': 'v1', 'kind': 'SubscriptionsList', 'items': items}
        ),
        processed=['fieldSelector', 'labelSelector'],
    )


@app.route('/subscriptions/<subscription_id>', methods=['DELETE'])
@authorizer(resource='subscriptions', verb='delete')
def cancel_subscription(subscription_id):
    """Cancel subscription.

    # Returned value

    A _status_.  A status is a dictionary.

    If the cancellation is successful, `.status` is `'Success'`.  If
    `subscription_id` is not known, `.status` is `'Failure'` and
    `.reason` is `'NotFound'`.
    """

    if not (subscription := SUBSCRIPTIONS.get(subscription_id)):
        return make_status_response(
            'NotFound', f'Subscription {subscription_id} not known.'
        )

    for collection in (SUBSCRIPTIONS, TOKENS, SELECTORS):
        try:
            del collection[subscription_id]
        except KeyError:
            pass

    dispatcher = subscription['metadata']['dispatcher_id']
    dispatchers = {s['metadata'].get('dispatcher_id') for s in SUBSCRIPTIONS.values()}
    if dispatcher not in dispatchers:
        debug('Releasing dispatcher for endpoint %s.', dispatcher)
        del DISPATCHERS[dispatcher]

    return make_status_response('OK', f'Subscription {subscription_id} canceled.')


GAUGES = defaultdict(lambda: {'sum': 0, 'count': 0, 'min': 999999, 'max': -1})


@app.route('/publications', methods=['POST'])
def add_publication():
    """Receive a new publication.

    Publication must be a valid JSON document.

    # Returned value

    A _status_.  A status is a dictionary.

    If the publication is successful (`.status` is `'Success'`),
    `.message` tells whether there are interested listeners or not.
    """
    start = time()
    try:
        body = request.get_json()
        if not isinstance(body, dict):
            return make_status_response('BadRequest', 'Not a JSON object.')
    except:
        return make_status_response('BadRequest', 'Not a valid JSON object.')

    if 'kind' in body and 'apiVersion' in body:
        desc = f'{body["apiVersion"]}/{body["kind"]}'
    else:
        desc = 'generic'

    debug('Processing a %s publication.', desc)
    TELEMETRY_MANAGER.set_metric(EventBusMetrics.PUBLICATIONS_RECEIVED_COUNTER, 1)

    recipients = [sid for sid in list(SUBSCRIPTIONS) if is_interested(sid, body)]
    if not recipients:
        debug('No matching subscription found for %s publication.', desc)
        return make_status_response(
            'OK', 'Publication received, but no matching subscription.'
        )

    debug(
        'Found the following matching subscriptions, dispatching: %s.', str(recipients)
    )
    pub_id = make_uuid()
    delta = time() - start
    for sid in recipients:
        if subscription := SUBSCRIPTIONS.get(sid):
            DISPATCHERS[subscription['metadata']['dispatcher_id']]['queue'].put(
                (
                    request.data,
                    sid,
                    datetime.now(timezone.utc).isoformat(),
                    pub_id,
                    delta,
                )
            )
    return make_status_response('OK', f'Publication {pub_id} received.')


def main(svc):
    """Start eventbus."""
    debug('Starting service.')
    run_app(svc)


if __name__ == '__main__':
    main(app)
